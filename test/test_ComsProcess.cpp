#include "gtest/gtest.h"

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

#include "Communication/comsprocess.h"

/* C++ detection */
#ifdef __cplusplus
}
#endif

/* External funtion prototypes */
extern int8_t coms_encodePacket(packet_t* packet, uint8_t address, uint16_t code, uint8_t length, uint8_t* data, uint8_t priority);

/* Definitions */
#define ENCODED_PACKET_LENGTH 12
#define OPTIONS_LENGTH 1
#define COBS_DELIMITER_LENGTH 2

// circularQueue outPackets;
// circularQueueConstruct(&outPackets, 100);

/* ***** 
 * Test standard protocol V1 funtionality 
 *
 *  Assumptions:
 *          - When an option flag is recived, then the corresponding encode packet hasOptions
 *          - If hasOptions is not set, then PACKET_OPT_ALL_ENABLED
 * *****/
struct TestComsProcess: public ::testing::Test {
    void SetUp() {
        // code here will execute just before the test ensues 
    }
    void TearDown() {
        // code here will be called just after the test completes
    }
};

/// @brief Test if standard V1 packets can be encoded (no options set)
TEST_F(TestComsProcess, EncodePacket_NoOPt) {
    packet_t packet;
    packet.useOption = 1;
    
    uint8_t address = 1; 
    uint8_t code = 2;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_NOT_DEMAND;
    
    coms_encodePacket(&packet, address, code, length, data, option);

    EXPECT_EQ(packet.address, address);
    EXPECT_EQ(packet.code, code);
    EXPECT_EQ(packet.length, 6 + COBS_DELIMITER_LENGTH + OPTIONS_LENGTH);
    EXPECT_EQ(packet.data[0], data[0]);
    EXPECT_EQ(packet.useOption, 1);
    EXPECT_EQ(packet.option, option);
}

/// @brief Test if standard V1 packets can be encoded with options set 
TEST_F(TestComsProcess, EncodePacket_OPt) {
    packet_t packet;
    packet.useOption = 1;
    
    uint8_t address = 1; 
    uint8_t code = 2;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_FWD_ALLOW | PACKET_OPT_IS_DEMAND;
    
    coms_encodePacket(&packet, address, code, length, data, option);

    EXPECT_EQ(packet.address, address);
    EXPECT_EQ(packet.code, code);
    EXPECT_EQ(packet.length, 6 + COBS_DELIMITER_LENGTH + OPTIONS_LENGTH);
    EXPECT_EQ(packet.data[0], data[0]);
    EXPECT_EQ(packet.useOption, 1);
    EXPECT_EQ(packet.option, option);
}

/// @brief Test if standard V1 packets can be encoded with options disabled
TEST_F(TestComsProcess, EncodePacket_OPtDisabled) {
    packet_t packet;
    packet.useOption = 0;
    
    uint8_t address = 1; 
    uint8_t code = 2;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_FWD_ALLOW | PACKET_OPT_IS_DEMAND;
    
    coms_encodePacket(&packet, address, code, length, data, option);

    EXPECT_EQ(packet.address, address);
    EXPECT_EQ(packet.code, code);
    EXPECT_EQ(packet.length, 6 + COBS_DELIMITER_LENGTH);
    EXPECT_EQ(packet.data[0], data[0]);
    EXPECT_EQ(packet.useOption, 0);
    EXPECT_EQ(packet.option, option);
}

/// @brief Test is standard V1 packets can be decoded with options enabled
TEST_F(TestComsProcess, DecodePacket_OptEnabled) {
    uint8_t enc_packet[9] = {8, 3, 4, 3, 2, 1, 135, 24, 0};
    packet_t p;
   
    EXPECT_EQ(coms_decodePacket(&p, enc_packet, 9), 1);

    EXPECT_EQ(p.length + 1, enc_packet[9-PKT_HEADER_LEN] ^ 0x80);
    EXPECT_EQ(p.address, enc_packet[9-PKT_HEADER_LEN-1]);
    EXPECT_EQ(p.code, enc_packet[9-PKT_HEADER_LEN-2]);
    EXPECT_EQ(p.useOption, (enc_packet[9-PKT_HEADER_LEN] & 0x80) >> 7);
    EXPECT_EQ(p.option, enc_packet[9-PKT_HEADER_LEN-3]);
}

/// @brief Test is standard V1 packets can be decoded with options disabled 
TEST_F(TestComsProcess, DecodePacket_OptDisabled) {
    uint8_t enc_packet[ENCODED_PACKET_LENGTH] = {1, 1, 1, 1, 1, 1, 5, 2, 1, 10, 101, 0};
    packet_t p;
    
    EXPECT_EQ(coms_decodePacket(&p, enc_packet, ENCODED_PACKET_LENGTH), 1);

    EXPECT_EQ(p.length, enc_packet[ENCODED_PACKET_LENGTH-PKT_HEADER_LEN]);
    EXPECT_EQ(p.address, enc_packet[ENCODED_PACKET_LENGTH-PKT_HEADER_LEN-1]);
    EXPECT_EQ(p.code, enc_packet[ENCODED_PACKET_LENGTH-PKT_HEADER_LEN-2]);
    EXPECT_EQ(p.useOption, (enc_packet[ENCODED_PACKET_LENGTH-PKT_HEADER_LEN] & 0x80) >> 7);
    EXPECT_EQ(p.option, PACKET_OPT_ALL_ENABLED);
}

/// @brief Test is standard V1 encode/decode together with options enabled
TEST_F(TestComsProcess, EncodeDecodePacket_OptEnabled) {
    packet_t encodePacket;
    packet_t decodePacket;
    
    encodePacket.useOption = 1;
    
    uint8_t address = 1; 
    uint8_t code = 2;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_FWD_ALLOW | PACKET_OPT_IS_DEMAND;
    
    coms_encodePacket(&encodePacket, address, code, length, data, option);

    // printf("Length: %d\n", encodePacket.length);
    // printf("Address: %d\n", encodePacket.address);
    // printf("Code: %d\n", encodePacket.code);
    // printf("Options: %d\n", encodePacket.option);
    
    EXPECT_EQ(encodePacket.address, address);
    EXPECT_EQ(encodePacket.code, code);
    EXPECT_EQ(encodePacket.length, 6 + COBS_DELIMITER_LENGTH + OPTIONS_LENGTH);
    EXPECT_EQ(encodePacket.data[0], data[0]);
    EXPECT_EQ(encodePacket.useOption, 1);
    EXPECT_EQ(encodePacket.option, option);

    EXPECT_EQ(coms_decodePacket(&decodePacket, encodePacket.transmitData, encodePacket.length), 1);

    EXPECT_EQ(decodePacket.length, encodePacket.length - COBS_DELIMITER_LENGTH - OPTIONS_LENGTH);
    EXPECT_EQ(decodePacket.address, encodePacket.address);
    EXPECT_EQ(decodePacket.code, encodePacket.code);
    EXPECT_EQ(decodePacket.useOption, 1);
    EXPECT_EQ(decodePacket.option, option);
}

/// @brief Test is standard V1 encode/decode together with options disabled
TEST_F(TestComsProcess, EncodeDecodePacket_OptDisabled) {
    packet_t encodePacket;
    packet_t decodePacket;
    
    encodePacket.useOption = 0;
    
    uint8_t address = 1; 
    uint8_t code = 2;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_FWD_ALLOW | PACKET_OPT_IS_DEMAND;
    
    coms_encodePacket(&encodePacket, address, code, length, data, option);
    
    EXPECT_EQ(encodePacket.address, address);
    EXPECT_EQ(encodePacket.code, code);
    EXPECT_EQ(encodePacket.length, 6 + COBS_DELIMITER_LENGTH);
    EXPECT_EQ(encodePacket.data[0], data[0]);
    EXPECT_EQ(encodePacket.useOption, 0);
    EXPECT_EQ(encodePacket.option, option);

    EXPECT_EQ(coms_decodePacket(&decodePacket, encodePacket.transmitData, encodePacket.length), 1);
    
    EXPECT_EQ(decodePacket.length, encodePacket.length - COBS_DELIMITER_LENGTH);
    EXPECT_EQ(decodePacket.address, encodePacket.address);
    EXPECT_EQ(decodePacket.code, encodePacket.code);
    EXPECT_EQ(decodePacket.useOption, 0);
    EXPECT_EQ(decodePacket.option, option);
}

/// @brief Test if cobs can be unstuffed 
TEST_F(TestComsProcess, CobsDecode) {
    uint8_t buff_in[ENCODED_PACKET_LENGTH] = {1, 1, 1, 1, 1, 1, 5, 2, 1, 10, 101, 0};
    uint8_t buff_out[ENCODED_PACKET_LENGTH];
   
    uint8_t write_index = cobs_decode(buff_in, ENCODED_PACKET_LENGTH, buff_out);
    EXPECT_EQ(write_index, ENCODED_PACKET_LENGTH - 1);
}

/* ***** 
 * Test protocol V2 switching funtionality 
 *
 *  Assumptions:
 *          - Options byte is zero'd by default
 *          - Default to V1 on startup. 
 *          - If in protocol V1 and packet ID > 255, do not try to encode packet. Skip it.
 *          - REQUEST packet in V2 are 16bit numbers (2 bytes).
 * *****/
struct TestComsProcess_ProtocolV2: public ::testing::Test {
    void SetUp() {
        // code here will execute just before the test ensues 
    }
    void TearDown() {
        // code here will be called just after the test completes
    }
};

/// @brief Test if 16 bit packet ID is blocked when options disabled
TEST_F(TestComsProcess_ProtocolV2, EncodePacket_16bitCode_OptDisabled) {
    packet_t packet;
    packet.useOption = 0;
    
    uint8_t address = 1; 
    uint16_t code = 257;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_FWD_ALLOW | PACKET_OPT_IS_DEMAND;
    
    EXPECT_NE(coms_encodePacket(&packet, address, code, length, data, option), 1);
}

/// @brief Test if 16 bit packet ID can be encoded when options enabled
TEST_F(TestComsProcess_ProtocolV2, EncodePacket_16bitCode_Opt) {
    packet_t packet;
    packet.useOption = 1;
    
    uint8_t address = 1; 
    uint16_t code = 257;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_FWD_ALLOW | PACKET_OPT_IS_DEMAND | PACKET_OPT_IS_V2;
    
    EXPECT_EQ(coms_encodePacket(&packet, address, code, length, data, option), 1);
    
    EXPECT_EQ(packet.address, address);
    EXPECT_EQ(packet.code, code);
    EXPECT_EQ(packet.length, 6 + COBS_DELIMITER_LENGTH + OPTIONS_LENGTH);
    EXPECT_EQ(packet.data[0], data[0]);

    // Parse transmit data and check for values
    EXPECT_EQ(packet.transmitData[packet.length - 3] & 0b01111111, packet.length - COBS_DELIMITER_LENGTH);
    EXPECT_EQ(((packet.transmitData[packet.length - 6] & 0b11000) << 5) | packet.transmitData[packet.length - 5] , packet.code);
}

/// @brief Test if 16 bit packet ID is blocked when above 11 bits 
TEST_F(TestComsProcess_ProtocolV2, EncodePacket_16bitCode_OverSizeCode) {
    packet_t packet;
    packet.useOption = 1;
    
    uint8_t address = 1; 
    uint16_t code = 0x800;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_FWD_ALLOW | PACKET_OPT_IS_DEMAND;

    EXPECT_NE(coms_encodePacket(&packet, address, code, length, data, option), 1);
}

/// @brief Test if 16 bit packet ID can be encoded when code is at maximumvalue
TEST_F(TestComsProcess_ProtocolV2, EncodePacket_16bitCode_AtBoundary) {
    packet_t packet;
    packet.useOption = 1;
    
    uint8_t address = 1; 
    uint16_t code = 0x3FF;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_FWD_ALLOW | PACKET_OPT_IS_DEMAND | PACKET_OPT_IS_V2;
    
    EXPECT_EQ(coms_encodePacket(&packet, address, code, length, data, option), 1);
    
    EXPECT_EQ(packet.address, address);
    EXPECT_EQ(packet.code, code);
    EXPECT_EQ(packet.length, 6 + COBS_DELIMITER_LENGTH + OPTIONS_LENGTH);
    EXPECT_EQ(packet.data[0], data[0]);

    // Parse transmit data and check for values
    EXPECT_EQ(packet.transmitData[packet.length - 3] & 0b01111111, packet.length - COBS_DELIMITER_LENGTH); // Check length is correct 
    EXPECT_EQ(((packet.transmitData[packet.length - 6] & 0b11000) << 5) | packet.transmitData[packet.length - 5] , packet.code); // Check code is correct
    EXPECT_EQ( (packet.transmitData[packet.length - 6] & 0b11000) >> 3, 0x3); // Check both upper bits are high 
}

/// @brief Test is standard V2 encode/decode together with options enabled and >8 bit code
TEST_F(TestComsProcess_ProtocolV2, EncodeDecodePacket_16bitCode) {
    packet_t encodePacket;
    packet_t decodePacket;
    
    encodePacket.useOption = 1;
    
    uint8_t address = 1; 
    uint16_t code = 257;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_FWD_ALLOW | PACKET_OPT_IS_DEMAND | PACKET_OPT_IS_V2;
    
    coms_encodePacket(&encodePacket, address, code, length, data, option);
    
    EXPECT_EQ(encodePacket.address, address);
    EXPECT_EQ(encodePacket.code, code);
    EXPECT_EQ(encodePacket.length, 6 + COBS_DELIMITER_LENGTH + OPTIONS_LENGTH);
    EXPECT_EQ(encodePacket.data[0], data[0]);
    EXPECT_EQ(encodePacket.useOption, 1);
    EXPECT_EQ(encodePacket.option, option);

    EXPECT_EQ(coms_decodePacket(&decodePacket, encodePacket.transmitData, encodePacket.length), 1);
    
    EXPECT_EQ(decodePacket.length, encodePacket.length - COBS_DELIMITER_LENGTH - OPTIONS_LENGTH);
    EXPECT_EQ(decodePacket.address, encodePacket.address);
    EXPECT_EQ(decodePacket.code, encodePacket.code);
    EXPECT_EQ(decodePacket.useOption, 1);
    EXPECT_EQ(decodePacket.option, option);
}

/// @brief Test is standard V2 encode/decode together with options enabled and >8 bit code at boundary 
TEST_F(TestComsProcess_ProtocolV2, EncodeDecodePacket_16bitCode_AtBoundary) {
    packet_t encodePacket;
    packet_t decodePacket;
    
    encodePacket.useOption = 1;
    
    uint8_t address = 1; 
    uint16_t code = 0x7FF;
    uint8_t length = 6; 
    uint8_t data[64]; 
    data[0] = 3;
    data[1] = 4;
    uint8_t option = PACKET_OPT_FWD_ALLOW | PACKET_OPT_IS_DEMAND | PACKET_OPT_IS_V2;
    
    coms_encodePacket(&encodePacket, address, code, length, data, option);
    
    EXPECT_EQ(encodePacket.address, address);
    EXPECT_EQ(encodePacket.code, code);
    EXPECT_EQ(encodePacket.length, 6 + COBS_DELIMITER_LENGTH + OPTIONS_LENGTH);
    EXPECT_EQ(encodePacket.data[0], data[0]);
    EXPECT_EQ(encodePacket.useOption, 1);
    EXPECT_EQ(encodePacket.option, option);

    EXPECT_EQ(coms_decodePacket(&decodePacket, encodePacket.transmitData, encodePacket.length), 1);
    
    EXPECT_EQ(decodePacket.length, encodePacket.length - COBS_DELIMITER_LENGTH - OPTIONS_LENGTH);
    EXPECT_EQ(decodePacket.address, encodePacket.address);
    EXPECT_EQ(decodePacket.code, encodePacket.code);
    EXPECT_EQ(decodePacket.useOption, 1);
    EXPECT_EQ(decodePacket.option, option);
}

/* ***** 
 * Test packet queue implimetnation
 *
 *  Assumptions:
 *          - 
 * *****/
struct TestComsProcess_PacketQueue: public ::testing::Test {
    void SetUp() {
        // code here will execute just before the test ensues 
    }
    void TearDown() {
        // code here will be called just after the test completes
    }
};

/// @brief Test read packet queue funtionality 
TEST_F(TestComsProcess_PacketQueue, ReadPacketQueue) {
    packet_t packet;
	circularQueue Q;
	circularQueueConstruct(&Q, 64);

    uint8_t encode_packet[] = {0x06, 0x01, 0x01, 0x01, 0x05, 0xb0, 0x00};
    Q.pushArray(&Q, encode_packet, sizeof(encode_packet)/sizeof(encode_packet[0]));
    
    EXPECT_EQ(coms_readPacketFromQ(&packet, &Q), 1);

    EXPECT_EQ(packet.address, 1);
	EXPECT_EQ(packet.code, 1);
	EXPECT_EQ(packet.length, 5);
	EXPECT_EQ(packet.data[0], 1);
	EXPECT_EQ(Q.numel, 0);
}

// /// @brief Test extra bytes in front of packet will still parse correctly
// TEST_F(TestComsProcess_PacketQueue, ReadPacketQueue_ExtraBytes) {
//     packet_t packet;
// 	circularQueue Q;
// 	circularQueueConstruct(&Q, 64);

//     uint8_t encode_packet[] = {0x07, 0x06, 0x01, 0x01, 0x01, 0x05, 0xb0, 0x00};
//     Q.pushArray(&Q, encode_packet, sizeof(encode_packet)/sizeof(encode_packet[0]));

//     EXPECT_EQ(coms_readPacketFromQ(&packet, &Q), 1);

//     EXPECT_EQ(packet.address, 1);
// 	EXPECT_EQ(packet.code, 1);
// 	EXPECT_EQ(packet.length, 5);
// 	EXPECT_EQ(packet.data[0], 1);
// 	EXPECT_EQ(Q.numel, 0);
// }

/// @brief Test incorrect length byte will result in a zeroed packet
TEST_F(TestComsProcess_PacketQueue, ReadPacketQueue_IncorrectLength) {
    packet_t packet;
	circularQueue Q;
	circularQueueConstruct(&Q, 64);

    uint8_t encode_packet[] = {0x07, 0x06, 0x01, 0x01, 0x01, 0x04, 0xb0, 0x00};
    Q.pushArray(&Q, encode_packet, sizeof(encode_packet)/sizeof(encode_packet[0]));
    
    EXPECT_NE(coms_readPacketFromQ(&packet, &Q), 1);

    EXPECT_EQ(packet.address, 0);
	EXPECT_EQ(packet.code, 0);
	EXPECT_EQ(packet.length, 0);
	EXPECT_EQ(packet.data[0], 0);
	EXPECT_EQ(Q.numel, 0);
}

/// @brief Test incorrect length (that is larger than buffer) byte will result in a zeroed packet
TEST_F(TestComsProcess_PacketQueue, ReadPacketQueue_IncorrectLength_LargerThanBuff) {
    packet_t packet;
	circularQueue Q;
	circularQueueConstruct(&Q, 64);

    uint8_t encode_packet[] = {0x07, 0x06, 0x01, 0x01, 0x01, 0x09, 0xb0, 0x00};
    Q.pushArray(&Q, encode_packet, sizeof(encode_packet)/sizeof(encode_packet[0]));
    
    EXPECT_NE(coms_readPacketFromQ(&packet, &Q), 1);

    EXPECT_EQ(packet.address, 0);
	EXPECT_EQ(packet.code, 0);
	EXPECT_EQ(packet.length, 0);
	EXPECT_EQ(packet.data[0], 0);
	EXPECT_EQ(Q.numel, 0);
}

/// @brief Test An incomplete packet will not be popped from the queue; 
/// Once the rest of the packet arrives in the queue it will be parsed
TEST_F(TestComsProcess_ProtocolV2, ReadPacketQueue_IncorrectLength) {
    packet_t packet;
	circularQueue Q;
	circularQueueConstruct(&Q, 64);

    uint8_t encode_packet_1[] = {0x06, 0x01, 0x01};
    uint8_t encode_packet_2[] = {0x01, 0x05, 0xb0, 0x00};

    Q.pushArray(&Q, encode_packet_1, sizeof(encode_packet_1)/sizeof(encode_packet_1[0]));

    EXPECT_NE(coms_readPacketFromQ(&packet, &Q), 1);

    EXPECT_EQ(packet.address, 0);
	EXPECT_EQ(packet.code, 0);
	EXPECT_EQ(packet.length, 0);
	EXPECT_EQ(packet.data[0], 0);
	EXPECT_EQ(Q.numel, sizeof(encode_packet_1)/sizeof(encode_packet_1[0]));

    Q.pushArray(&Q, encode_packet_2, sizeof(encode_packet_2)/sizeof(encode_packet_2[0]));

    EXPECT_EQ(coms_readPacketFromQ(&packet, &Q), 1);

    EXPECT_EQ(packet.address, 1);
	EXPECT_EQ(packet.code, 1);
	EXPECT_EQ(packet.length, 5);
	EXPECT_EQ(packet.data[0], 1);
	EXPECT_EQ(Q.numel, 0);
}