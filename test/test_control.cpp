#include <stdio.h>

#include "gtest/gtest.h"

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

#include "DataStructures/control.h"

#ifndef M_TWOPI
#define M_TWOPI 6.28318530718
#endif

/* C++ detection */
#ifdef __cplusplus
}
#endif


struct TestControl: public ::testing::Test {
    void SetUp() {
        // code here will execute just before the test ensues 
    }

    void TearDown() {
        // code here will be called just after the test completes
    }
};


TEST_F (TestControl, test_cap_circular_setpoint_standard_case) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = 0.7;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 1.0;
    float lowerLimit = 0.5;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    EXPECT_EQ(positionSetpoint, desired_pos_setpoint);
}


TEST_F (TestControl, test_cap_circular_setpoint_standard_above_limit_case) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = 1.1;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 1.0;
    float lowerLimit = 0.5;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_EQ(positionSetpoint, upperLimit-tolerance);
}

TEST_F (TestControl, test_cap_circular_setpoint_standard_below_limit_case) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = 0.4;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 1.0;
    float lowerLimit = 0.5;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_EQ(positionSetpoint, lowerLimit+tolerance);
}

TEST_F (TestControl, test_cap_circular_setpoint_above_limit_closer_lower_limit) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = 6.0;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 1.0;
    float lowerLimit = 0.2;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_EQ(positionSetpoint, lowerLimit+tolerance);
}

TEST_F (TestControl, test_cap_circular_setpoint_below_limit_closer_upper_limit) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = 1.0;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 6.0;
    float lowerLimit = 5.0;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_EQ(positionSetpoint, upperLimit-tolerance);
}

TEST_F (TestControl, test_cap_circular_setpoint_below_zero) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = -0.3;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 1.0;
    float lowerLimit = 0.5;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_EQ(positionSetpoint, lowerLimit+tolerance);
}


TEST_F (TestControl, test_cap_circular_setpoint_above_2PI) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = 6.3;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 6.0;
    float lowerLimit = 2.0;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_EQ(positionSetpoint, upperLimit-tolerance);
}

TEST_F (TestControl, test_cap_circular_setpoint_below_zero_close_within_limits) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = -3.14;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 3.5;
    float lowerLimit = 2.5;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_FLOAT_EQ(positionSetpoint, M_TWOPI + desired_pos_setpoint);
}

TEST_F (TestControl, test_cap_circular_setpoint_above_2PI_within_limits) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = 9.0;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 3.5;
    float lowerLimit = 2.5;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_FLOAT_EQ(positionSetpoint, desired_pos_setpoint - M_TWOPI);
}

TEST_F (TestControl, test_cap_circular_setpoint_below_zero_close_below_limits) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = -4.0;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 3.5;
    float lowerLimit = 2.5;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_FLOAT_EQ(positionSetpoint, lowerLimit + tolerance);
}


TEST_F (TestControl, test_cap_circular_setpoint_below_zero_close_above_upper) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = -1;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 3.5;
    float lowerLimit = 2.5;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_FLOAT_EQ(positionSetpoint, upperLimit - tolerance);
}

TEST_F (TestControl, test_cap_circular_setpoint_above_2PI_close_below_lower) {  
    float desired_pos_setpoint = 8.0;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 3.5;
    float lowerLimit = 2.5;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_FLOAT_EQ(positionSetpoint, lowerLimit + tolerance);
}

TEST_F (TestControl, test_cap_circular_setpoint_above_2PI_close_above_upper) {  
    float desired_pos_setpoint = 10.0;
    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 3.5;
    float lowerLimit = 2.5;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_FLOAT_EQ(positionSetpoint, upperLimit - tolerance);
}



TEST_F (TestControl, test_cap_circular_setpoint_negative_limits_standard_case) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = -0.7;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = -0.5;
    float lowerLimit = -1;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    

    EXPECT_NEAR(positionSetpoint, desired_pos_setpoint, 0.0001);
}


TEST_F (TestControl, test_cap_circular_setpoint_negative_limits_above_limits) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = 2.5;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 2.0;
    float lowerLimit = -2.0;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_NEAR(positionSetpoint, upperLimit-tolerance, 0.0001);
}



TEST_F (TestControl, test_cap_circular_setpoint_negative_limits_below_limits) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = -2.5;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 2.0;
    float lowerLimit = -2.0;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_NEAR(positionSetpoint, lowerLimit+tolerance, 0.0001);
}

TEST_F (TestControl, test_cap_circular_setpoint_negative_limits_below_limits_close_to_upper) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = -4.0;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 2.0;
    float lowerLimit = -2.0;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_NEAR(positionSetpoint, upperLimit-tolerance, 0.0001);
}


TEST_F (TestControl, test_cap_circular_setpoint_negative_limits_above_limits_close_to_lower) {  

    // Test that capping in the normal situation is expected value  
    float desired_pos_setpoint = 4.0;

    float positionSetpoint = desired_pos_setpoint;
    float upperLimit = 2.0;
    float lowerLimit = -2.0;
    float tolerance = 0.01;
    capCircularSetpoint(&positionSetpoint, upperLimit, lowerLimit, tolerance);
    
    ASSERT_NE(positionSetpoint, desired_pos_setpoint);

    EXPECT_NEAR(positionSetpoint, lowerLimit+tolerance, 0.0001);
}


TEST_F (TestControl, test_cap_circular_error_standard_use) {  

    float currentPosition = 0.5;
    float setpoint = 0.6;

    float upperLimit = 1.0;

    float lowerLimit = 0.3;

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    EXPECT_FLOAT_EQ(error, setpoint-currentPosition);
}


TEST_F (TestControl, test_cap_circular_error_setpoint_above_limit) {  

    float currentPosition = 0.5;
    float setpoint = 1.1;

    float upperLimit = 1.0;

    float lowerLimit = 0.3;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, upperLimit-0.01-currentPosition);
}


TEST_F (TestControl, test_cap_circular_error_setpoint_below_limit) {  

    float currentPosition = 0.5;
    float setpoint = 0.2;

    float upperLimit = 1.0;

    float lowerLimit = 0.3;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, lowerLimit+0.01-currentPosition);
}


TEST_F (TestControl, test_cap_circular_error_position_above_limit) {  

    float currentPosition = 1.1;
    float setpoint = 0.5;

    float upperLimit = 1.0;

    float lowerLimit = 0.3;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, setpoint - currentPosition);
}


TEST_F (TestControl, test_cap_circular_error_position_below_limit) {  

    float currentPosition = 0.2;
    float setpoint = 0.5;

    float upperLimit = 1.0;

    float lowerLimit = 0.3;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, setpoint - currentPosition);
}

TEST_F (TestControl, test_cap_circular_error_position_below_limit_close_to_upper) {  

    float currentPosition = 0.1;
    float setpoint = 0.5;

    float upperLimit = 6.2;

    float lowerLimit = 0.3;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, -2*M_PI + (setpoint - currentPosition));
}



TEST_F (TestControl, test_cap_circular_error_position_above_limit_close_to_lower) {  

    float currentPosition = 6.20;
    float setpoint = 0.5;

    float upperLimit = 3.0;

    float lowerLimit = 0.3;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, 2*M_PI - currentPosition  + setpoint);
}



TEST_F (TestControl, test_cap_circular_error_position_below_zero) {  

    float currentPosition = -0.5;
    float setpoint = 0.5;

    float upperLimit = 3.0;

    float lowerLimit = 0.3;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, setpoint - currentPosition);
}


TEST_F (TestControl, test_cap_circular_error_position_above_2_pi) {  

    float currentPosition = 6.4;
    float setpoint = 5.9;

    float upperLimit = 6.2;

    float lowerLimit = 0.6;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, setpoint - currentPosition);
}


TEST_F (TestControl, test_cap_circular_error_negative_limits_standard_case) {  

    float currentPosition = -0.5;
    float setpoint = -0.6;

    float upperLimit = -0.3;

    float lowerLimit = -1.0;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, setpoint - currentPosition);
}

TEST_F (TestControl, test_cap_circular_error_negative_limits_standard_case2) {  

    float currentPosition = -0.5;
    float setpoint = 1.0;

    float upperLimit = 2.0;

    float lowerLimit = -2.0;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, setpoint - currentPosition);
}



TEST_F (TestControl, test_cap_circular_error_negative_limits_position_below_lower) {  

    float currentPosition = -2.1;
    float setpoint = 1.0;

    float upperLimit = 2.0;

    float lowerLimit = -2.0;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, setpoint - currentPosition);
}


TEST_F (TestControl, test_cap_circular_error_negative_limits_position_above_upper) {  

    float currentPosition = 2.1;
    float setpoint = 1.0;

    float upperLimit = 2.0;

    float lowerLimit = -2.0;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    EXPECT_FLOAT_EQ(error, setpoint - currentPosition);
}


TEST_F (TestControl, test_cap_circular_error_negative_limits_position_below_lower_close_to_upper) {  

    float currentPosition = -3.5;
    float setpoint = 1.0;

    float upperLimit = 2.0;

    float lowerLimit = -2.0;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    ASSERT_NE(error, setpoint - currentPosition);

    EXPECT_FLOAT_EQ(error, -((2*M_PI) + currentPosition  - setpoint));
}


TEST_F (TestControl, test_cap_circular_error_negative_limits_position_above_upper_close_to_lower) {  

    float currentPosition = 4.1;
    float setpoint = 1.0;

    float upperLimit = 2.0;

    float lowerLimit = -2.0;

    capCircularSetpoint(&setpoint, upperLimit, lowerLimit, 0.01);

    float error = capCircularError(currentPosition, setpoint, upperLimit, lowerLimit);

    // ASSERT_NE(error, setpoint-currentPosition);

    ASSERT_NE(error, setpoint - currentPosition);


    EXPECT_FLOAT_EQ(error, -currentPosition + 2*M_PI  + setpoint);

}
