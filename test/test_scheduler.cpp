#include <stdio.h>

#include "gtest/gtest.h"

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

#include "DataStructures/scheduler.h"

bool task_executed = false;
uint32_t num_executions = 0;
void task(void); 

/* C++ detection */
#ifdef __cplusplus
} 
#endif


// Test fixture for the scheduler
class SchedulerTest : public ::testing::Test {
protected:
    scheduler_t scheduler;

    void SetUp() override {
        task_executed = false;
        num_executions = 0;
        memset(&scheduler, 0, sizeof(scheduler));
        scheduler_construct(&scheduler);
    }
};

void task(void) {
    task_executed = true;
    num_executions++;
}

void nop(void) { }

// Test case for registering a task
TEST_F(SchedulerTest, RegisterTaskFrequency) {
    uint32_t frequency = 10;
    uint32_t period = 0;

    // Register a task with a frequency
    scheduler.RegisterTask(&scheduler, task, &frequency, period);

    // Run the scheduler for 1 second
    uint32_t ticks = 1000;
    for (uint32_t i = 0; i <= ticks; i++) { scheduler.Run(&scheduler, i); }

    // Check that the task was executed
    EXPECT_TRUE(task_executed);

    // Check the frequency is correct 
    EXPECT_EQ(num_executions, (uint32_t)(1.0f/(float)frequency * 100.0f));

    // Check frequency is correct
    EXPECT_EQ(scheduler.tasks[0].measuredFrequency, 10);
}

// Test case for registering a task
TEST_F(SchedulerTest, RegisterTaskPeriod) {
    uint32_t period = 100;

    // Register a task with a frequency and period
    scheduler.RegisterTask(&scheduler, task, NULL, period);

    // Run the scheduler for 1 second
    uint32_t ticks = 1000;
    for (uint32_t i = 0; i <= ticks; i++) { scheduler.Run(&scheduler, i); }

    // Check that the task was executed
    EXPECT_TRUE(task_executed);

    // Check the period is correct 
    EXPECT_EQ(num_executions, (uint32_t)((float)ticks / (float)period));

    // Check frequency is correct
    EXPECT_EQ(scheduler.tasks[0].measuredFrequency, 10);
}

// Test case for registering a single-shot task
TEST_F(SchedulerTest, RegisterSingleShot) {
    uint32_t period = 100;

    // Register a single-shot task
    scheduler.RegisterSingleshot(&scheduler, task, period);

    // Run the scheduler for 1 second
    uint32_t ticks = 1000;
    for (uint32_t i = 0; i <= ticks; i++) { scheduler.Run(&scheduler, i); }

    // Check that the task was executed
    EXPECT_TRUE(task_executed);

    // Check that task only executes once
    EXPECT_EQ(num_executions, 1);

    // Check frequency is correct
    EXPECT_EQ(scheduler.tasks[0].measuredFrequency, 10);
}

// Test case for unregistering a task
TEST_F(SchedulerTest, UnregisterTask) {
    uint32_t period = 100;

    // Register a task with a frequency and period
    scheduler.RegisterTask(&scheduler, task, NULL, period);

    // Unregister the task
    scheduler.UnregisterTask(&scheduler, task);

    // Run the scheduler for 1 second
    uint32_t ticks = 1000;
    for (uint32_t i = 0; i <= ticks; i++) { scheduler.Run(&scheduler, i); }

    // Check that the task was not executed
    EXPECT_FALSE(task_executed);

    // Check that task only executes once
    EXPECT_EQ(num_executions, 0);
}

// Test case for multiple tasks
TEST_F(SchedulerTest, MultipleTasks) {
    uint32_t period = 100;

    // Register a task with a frequency and period 
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, task, NULL, period);

    // Run the scheduler for 1 second
    uint32_t ticks = 10000;
    for (uint32_t i = 0; i <= ticks; i++) { scheduler.Run(&scheduler, i); }

    // Check that the task was executed
    EXPECT_TRUE(task_executed);

    // Check the period is correct 
    EXPECT_EQ(num_executions, (uint32_t)((float)ticks / (float)period));

    // Check frequency is correct
    EXPECT_EQ(scheduler.tasks[0].measuredFrequency, 10);
}

// Test case for multiple tasks
TEST_F(SchedulerTest, MultipleTasksWithSingleshot) {
    uint32_t period = 100;

    // Register a task with a frequency and period 
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, nop, NULL, period);
    scheduler.RegisterTask(&scheduler, nop, NULL, period);

    for (uint32_t i = 0; i <= 4000; i++) { scheduler.Run(&scheduler, i); }

    scheduler.RegisterSingleshot(&scheduler, task, period);

    for (uint32_t i = 0; i <= 99; i++) { scheduler.Run(&scheduler, 4000 + i); }

    // Check that the task was executed
    EXPECT_FALSE(task_executed);

    for (uint32_t i = 0; i <= 1; i++) { scheduler.Run(&scheduler, 4099 + i); }

    // Check that the task was executed
    EXPECT_TRUE(task_executed);

    // Check the period is correct 
    EXPECT_EQ(num_executions, 1);
}