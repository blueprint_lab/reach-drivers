import ctypes as c

DATA_BYTES_PER_PACKET = 64

class Packet_t(c.Structure):
    """
        /* Packet object - Used for all communication types*/
        struct packet_t{
            uint8_t length;
            uint8_t address;
            uint16_t code;
            uint16_t crc;
            uint8_t data[DATA_BYTES_PER_PACKET];
            uint8_t transmitData[DATA_BYTES_PER_PACKET];
            uint8_t protocol;
            uint8_t option;
            uint8_t useOption;
            uint16_t receiveRegister; // used in receive queue to track which frames have been received
            uint8_t totalFrames; // total number of CAN frames that complete this packet
        };
    """
    _fields_ = [
        ("length",          c.c_uint8),
        ("address",         c.c_uint8),
        ("code",            c.c_uint16),
        ("crc",             c.c_uint16),
        ("data",            c.c_uint8 * DATA_BYTES_PER_PACKET),
        ("transmitData",    c.c_uint8 * DATA_BYTES_PER_PACKET),
        ("protocol",        c.c_uint8),
        ("option",          c.c_uint8),
        ("useOption",       c.c_uint8),
        ("receiveRegister", c.c_uint16),
        ("totalFrames",     c.c_uint8),
    ]