import logging
import unittest
import ctypes as c
import platform
import random
import numpy as np
import os

from multiprocessing import Process, Pool

from RC_Core.protocols.bpl_protocol import parse_packet, encode_packet
from packetstruct import Packet_t

# multiprocessing can't run functions that are a TestCase subclass, this has to live outside the class
def randomised_encode_decode():
    print('process id:', os.getpid())
    for i in range(1000000):
        device_id = 1
        packet_id = random.randint(0, 2047)
        data = list(np.random.randint(255, size=8))
        enc_packet = encode_packet(device_id, packet_id, bytes(data))


        # -1 to remove the terminating 0 - normally handled by the packet splitter
        dec_packet = parse_packet(bytes(enc_packet[:-1]))

    print('done')
    #return device_id, packet_id, data, dec_packet

def randomised_rubbish_decode():
    logging.disable('WARNING')
    print('process id:', os.getpid())

    for i in range(1000000):
        rubbish = bytearray(os.urandom(63))
        rubbish.append(0)

        # -1 to remove the terminating 0 - normally handled by the packet splitter
        dec_packet = parse_packet(bytes(rubbish[:-1]))

    print('done')


class PacketStructTest(unittest.TestCase):
    print(f"System: {platform.system()}, Processor: {platform.processor()}")

    if platform.system() == "Windows" and platform.processor() == "x86_64":
        lib_path = "lib/librs1_drivers_windows_x86_64.dll"
        print(f"Running windows x86 64 tests on: {lib_path}")
    elif platform.system() == "Linux" and platform.processor() == "x86_64":
        lib_path = "lib/librs1_drivers_linux_x86_64.so"
        print(f"Running linux x86 64 tests on: {lib_path}")
    elif platform.system() == "Linux" and platform.processor() == "aarch64":
        lib_path = "lib/librs1_drivers_linux_aarch64.so"
        print(f"Running linux aarch64 tests on: {lib_path}")


    def test_can_load_dll(self):
        drivers = c.cdll.LoadLibrary(self.lib_path)
        self.assertEqual(1, 1)

    def test_can_encode_packet(self):
        drivers = c.cdll.LoadLibrary(self.lib_path)
        p = Packet_t()
        p_pointer = c.byref(p)
        address = c.c_uint8(1)
        code = c.c_uint8(2)
        length = c.c_uint8(10)
        data_t = c.c_uint8 * 64
        data = data_t()
        option = c.c_uint8(0)

        drivers.coms_encodePacket(
            p_pointer,
            address,
            code,
            length,
            c.byref(data),
            option)

        # print([i for i in p.transmitData[0:p.length]])
        self.assertEqual(address.value, p.address)
        self.assertEqual(code.value, p.code)

    def test_can_decode_packet(self):
        drivers = c.cdll.LoadLibrary(self.lib_path)

        enc_packet = [1, 1, 1, 1, 1, 1, 5, 2, 1, 10, 101, 0]
        p = Packet_t()

        data_t = c.c_uint8 * 64
        buffer = data_t()

        def set_data(d, i, x):
            d[i] = x

        [set_data(buffer, i, x) for i, x in enumerate(enc_packet)]

        self.assertEqual(len(enc_packet), 12)
        # print([buffer[i] for i in range(len(enc_packet))])
        ret = drivers.coms_decodePacket(c.byref(p), c.byref(buffer), len(enc_packet))
        # print([buffer[i] for i in range(len(enc_packet))])
        # print([p.transmitData[i] for i in range(len(enc_packet))])
        self.assertEqual(ret, 1)

        # self.assertEqual()
        # print([p.data[i] for i in range(len(enc_packet))])
        self.assertEqual(p.length, enc_packet[9])
        self.assertEqual(p.address, enc_packet[8])
        self.assertEqual(p.code, enc_packet[7])
    
    def test_cobs(self):
        drivers = c.cdll.LoadLibrary(self.lib_path)
        length = 12
        buffer_t = c.c_uint8 * length
        buff_in = buffer_t()
        buff_out = buffer_t()

        buff_in[0] = 1
        buff_in[1] = 1
        buff_in[2] = 1
        buff_in[3] = 1
        buff_in[4] = 1
        buff_in[5] = 1
        buff_in[6] = 5
        buff_in[7] = 2
        buff_in[8] = 1
        buff_in[9] = 10
        buff_in[10] = 101
        buff_in[11] = 0

        drivers.cobs_decode(buff_in, length, buff_out)
        # print([buff_in[i] for i in range(length)])
        # print([buff_out[i] for i in range(length)])

    def test_bpl_protocol_parse_packet_runs_id1_pkt2(self):

        enc_packet = [1, 1, 1, 1, 1, 1, 5, 2, 1, 10, 101, 0]

        result = parse_packet(enc_packet)
        print(result)

        self.assertEqual(1, result[0])  # device id
        self.assertEqual(2, result[1])  # packet id

    def test_encode_then_bpl_protocol_parse_packet_runs(self):
        drivers = c.cdll.LoadLibrary(
            "C:/Users/Yige/Documents/bpl/reachcontrol/RC_Core/rs1_drivers/lib/librs1_drivers_windows_x86_64.dll")

        data_array = [8, 8, 8, 8, 8, 8, 8, 8]

        p = Packet_t()
        p_pointer = c.byref(p)
        address = c.c_uint8(1)
        code = c.c_uint8(2)
        length = c.c_uint8(12)
        data_t = c.c_uint8 * 64
        data = data_t(*data_array)
        option = c.c_uint8(0)

        #print(f'before encode: {data[:]}')
        drivers.coms_encodePacket(p_pointer, address, code, length, c.byref(data), option)
        #print(f'transmit data: {[i for i in p.transmitData[0:p.length-1]]}')

        # assuming packet length has been handled by packet splitter. -1 to remove the terminating zero
        result = parse_packet(bytes(p.transmitData[0:p.length-1]))
        #print(f'decode result: {result}')

        self.assertEqual(1, result[0])  # device id correct
        self.assertEqual(2, result[1])  # packet id correct
        self.assertEqual(bytes(data_array), result[2])  # data correct

    def test_bpl_protocol_encode_packet_then_bpl_protocol_decode_packet(self):
        device_id = 1
        packet_id = 2
        data = [1, 2, 3, 4, 5, 6, 7, 8]

        enc_packet = encode_packet(device_id, packet_id, bytes(data))

        # -1 to remove the terminating 0 - normally handled by the packet splitter
        dec_packet = parse_packet(bytes(enc_packet[:-1]))

        self.assertEqual(device_id, dec_packet[0])  # device id
        self.assertEqual(packet_id, dec_packet[1])  # packet id
        self.assertEqual(bytes(data), dec_packet[2])  # packet id

    def test_bpl_protocol_encode_packet_randomised(self):

        for i in range(100):
            device_id = 1
            packet_id = random.randint(0,2047)
            data = list(np.random.randint(16, size=8))

            enc_packet = encode_packet(device_id, packet_id, bytes(data))

            #print(enc_packet)

    def test_bpl_protocol_encode_packet_parse_packet_randomised(self):

        for i in range(1000000):
            device_id = 1
            packet_id = random.randint(0, 2047)
            data = list(np.random.randint(255, size=8))

            enc_packet = encode_packet(device_id, packet_id, bytes(data))

            # -1 to remove the terminating 0 - normally handled by the packet splitter
            dec_packet = parse_packet(bytes(enc_packet[:-1]))

            self.assertEqual(device_id, dec_packet[0])  # device id
            self.assertEqual(packet_id, dec_packet[1])  # packet id
            self.assertEqual(bytes(data), dec_packet[2])  # packet id

    def test_bpl_protocol_encode_packet_parse_packet_randomised_multiprocessing_process(self):
        #p1 = Process(target = self.randomised_encode_decode, args=())
        p1 = Process(target=randomised_encode_decode, args=())
        p2 = Process(target=randomised_encode_decode, args=())
        p1.start()
        p2.start()

    def test_randomised_rubbish_decode(self):
        randomised_rubbish_decode()

    def test_randomised_rubbish_decode_multiprocessing_process(self):
        p1 = Process(target=randomised_rubbish_decode, args=())
        p2 = Process(target=randomised_rubbish_decode, args=())
        p1.start()
        p2.start()


if __name__ == '__main__':
    unittest.main(verbosity=2)
