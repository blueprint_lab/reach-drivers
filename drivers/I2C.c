/*
 * I2C.c
 *
 *  Created on: Jun 19, 2020
 *      Author: Shaun Barlow
 */


#include "Communication/I2C.h"


HAL_StatusTypeDef I2C_readDMA(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t* data, uint16_t len) {
	uint16_t DevAddress = (uint16_t) dev_id << 1;
	HAL_StatusTypeDef status;
	status = HAL_I2C_Master_Receive_DMA(phi2c, DevAddress, data, len);
	return status;
}

HAL_StatusTypeDef I2C_writeDMA(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t* data, uint16_t len) {
	uint16_t DevAddress = (uint16_t) dev_id << 1;
	HAL_StatusTypeDef status;
	status = HAL_I2C_Master_Transmit_DMA(phi2c, DevAddress, data, len);
	return status;
}
