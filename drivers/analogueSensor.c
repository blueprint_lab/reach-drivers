/*
 * analogueSensor.c
 *
 *  Created on: 20Feb.,2017
 *      Author: The Blueprint Labs
 */


/*
 * AnalogueJoystick.c
 *
 *  Created on: Sep 1, 2016
 *      Author: p.phillips
 */
#define ADC_MAX_VALUE 4096.0
#define ADC_BUFF_SIZE 9

#define ADC_NUM_SAMPLES 8
//#define NUM_ADC 6
#include <stdlib.h>
#include "../Inc/stm32_device_select.h"
#include "Hardware/analogueSensor.h"
#include "DataStructures/auxMath.h"



uint16_t ADC_BUFFER1[ADC_BUFF_SIZE];
uint16_t ADC_BUFFER2[ADC_BUFF_SIZE];
//uint16_t ADC_BUFFER1[ADC_BUFF_SIZE];
//uint16_t ADC_ACCUMALATOR_BUFFER[ADC_BUFF_SIZE];
//uint32_t numChannels[NUM_ADC] ={2,5};
//uint8_t numChannels1 = 0;

ADC_HandleTypeDef AdcHandlePrev;

void AnalogueSensor_NOP_void(AnalogueSensor_t * self);
float AnalogueSensor_NOP_float(AnalogueSensor_t * self);



void Sensor_Error_Handler(){

}

/*
 * Initialize data in the AnalogueJoystick struct.
 * Arguments:
 * 		AnalogueJoystick_t* self: Pointer to self
 * 		ADC_HandleTypeDef AdcHandle:
 * 		uint8_t rank: the rank of the joystick is set up in the device configuration.
 *
 * 		 float (*mapping)(AnalogueJoystick_t* self): A points to a function that maps from a pot to a
 * 		 			a joystick in some way. If this is left as NULL then it behaves just like a pot.
 *
 * 		 float input: I don't know what this does. Probably to be deleted.
 */

void AnalogueSensor_Construct(AnalogueSensor_t* self, ADC_HandleTypeDef AdcHandle,
		uint8_t rank, float (*mapping)(AnalogueSensor_t* self, float input)){

	self->Enable = AnalogueSensor_Initialise;
	self->Read = AnalogueSensor_Read;
	self->Get = AnalogueSensor_Get;
	self->Deconstruct = AnalogueSensor_Deconstruct;
	self->callibrate = AnalogueSensor_Calibrate;

	self->AdcHandle = AdcHandle;
	self->ADC_Accumalator_Buf = 0;
	self->numSamples = ADC_NUM_SAMPLES;
	self->sampleCounter=self->numSamples;
	self->rank = rank;
	self->mapping = mapping;
	assert_param( HAL_ADC_Start(&(self->AdcHandle)) == HAL_OK);

		//self->ADC_Buff = ADC_BUFFER1;
		//++numChannels1;

	uint8_t dmaStartStatus;

	if(AdcHandle.Instance==ADC1)
	{
		self->ADC_Buff = ADC_BUFFER1;
		//++numChannels[0];
		dmaStartStatus = HAL_ADC_Start_DMA(&self->AdcHandle, (uint32_t*)(self->ADC_Buff), (uint32_t)(AdcHandle.Init.NbrOfConversion)); //+self->rank-1

	}
#ifdef STM32F405xx
	if(AdcHandle.Instance==ADC2)
	{
		self->ADC_Buff = ADC_BUFFER2;
		//++numChannels[1];
		dmaStartStatus = HAL_ADC_Start_DMA(&self->AdcHandle, (uint32_t*)(self->ADC_Buff), (uint32_t)(AdcHandle.Init.NbrOfConversion));

	}
#endif
	if ( dmaStartStatus != HAL_OK)
	  {
		  Sensor_Error_Handler();
	  }

		//ADCHandlePrev=AdcHandle;
	self->deadBand = 0.1;
	self->center  = 0.5;
	self->lowerLim = 0.0;
	self->upperLim = 1.0;
}



void AnalogueSensor_Deconstruct(AnalogueSensor_t* self){
	self->numSamples = 0;
	self->mapping = NULL;
	//--numChannels;
	self->Enable = AnalogueSensor_NOP_void;
	self->Read = AnalogueSensor_NOP_float;
	self->Get = AnalogueSensor_NOP_float;
}

void AnalogueSensor_NOP_void(AnalogueSensor_t * self) {

}

float AnalogueSensor_NOP_float(AnalogueSensor_t * self) {
	return 0.0;
}

/*
 * Enable the analogue joystick's adc.
 * BUG: calling this twice raises an error
 */
void AnalogueSensor_Initialise(AnalogueSensor_t * self)
{
	assert_param( HAL_ADC_Start(&(self->AdcHandle)) == HAL_OK);

	//set up pot
	  uint8_t dmaStartStatus = HAL_ADC_Start_DMA(&self->AdcHandle, (uint32_t*)(self->ADC_Buff+self->rank-1), (uint32_t)(self->AdcHandle.Init.NbrOfConversion)); //was number of channels//*sizeof(uint16_t)); //removed sizeof(uintt16_t)
	//uint8_t dmaStartStatus=HAL_ADCEx_MultiModeStart_DMA(&self->AdcHandle, (uint32_t*)(self->ADC_Buff), (uint32_t)(2));
	if ( dmaStartStatus != HAL_OK)
	  {
		  Sensor_Error_Handler();
	  }

}



/*
 * Return the most recent read from the ADC.
 */



float AnalogueSensor_Read(AnalogueSensor_t * self)
{
//	uint32_t ADCvalue=0;
	uint8_t rank_id = self->rank-1;

//	self->ADC_Accumalator_Buf+=self->ADC_Buff[rank_id];
//
//	self->sampleCounter--;
//
//	if(self->sampleCounter==0)
//	{
//		self->sampleCounter=self->numSamples;
//		self->ADCvalue=self->ADC_Accumalator_Buf>>3;
//		self->ADC_Accumalator_Buf=0;
//	}
//
//	return self->ADC_Buff[rank_id];
	return self->ADC_Buff[rank_id];

}

float AnalogueSensor_Get(AnalogueSensor_t* self)
{
	if(self->mapping == NULL){
		return (float)((float)self->ADCvalue/ADC_MAX_VALUE); //returns between 0 and 1
	}else{
		return self->mapping(self, self->ADCvalue/ADC_MAX_VALUE);
	}
}

/*
 * Set the center, deadband and upper and lower limits of the analogue joystick.
 */

void AnalogueSensor_Calibrate(AnalogueSensor_t* self, float center, float deadband, float upperLim, float lowerLim){
	self->lowerLim = lowerLim;
	self->center = center;
	self->upperLim = upperLim;
	self->deadBand = deadband;
}




/*
 * Mappings
 *
 */

float sign_mapping(AnalogueSensor_t* self, float input){
	if(input < 0.5){
		return -1.0;
	}else{
		return 1.0;
	}
}

float polynomial_mapping(AnalogueSensor_t* self, float input)
{
	return 0;
}

float linear_joystick_mapping(AnalogueSensor_t* self, float input){
	float deadband = 0.1;
	float center = 0.5;
	float lowerLim = 0;
	float upperLim = 1.0;
	float output = 0;

	deadband = self->deadBand;
	center = self->center;
	lowerLim = self->lowerLim;
	upperLim = self->upperLim;

	if(input < center+deadband && input > center-deadband){
		return 0;
	}
	if(input < center-deadband){
		float x2 = center-deadband;
		float x1 = lowerLim;
		float y2 = 0;
		float y1 = -1;
		output = (input-x1)*(y2-y1)/(x2-x1) + y1;
	}
	if(input > center+deadband){
		float x2 = upperLim;
		float x1 = center+deadband;
		float y2 = 1;
		float y1 = 0;
		output =  (input-x1)*(y2-y1)/(x2-x1) + y1;
	}
	if(output>1){
		output = 1;
	}else if(output < -1.0){
		output = -1.0;
	}

	return output;
}
