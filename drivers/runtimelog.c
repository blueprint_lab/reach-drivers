/*
 * runtimelog.c
 *
 *  Created on: 29Apr.,2020
 *      Author: Shaun Barlow
 */

#include "DataStructures/runtimelog.h"

uint32_t lastReadAddress;


uint8_t runtimelog_VerifySector() {
	uint8_t status;
	uint16_t runtimeValue = 0;
	lastReadAddress = RUNTIMELOG_FLASH_ADDRESS;
	for (uint32_t address = RUNTIMELOG_FLASH_ADDRESS;
			address < RUNTIMELOG_FLASH_ADDRESS + RUNTIMELOG_PAGE_SIZE;
			address += sizeof(uint16_t)) {

		status = runtimelog_VerifyAddress(address);

		switch (status) {
		case RUNTIME_LOG_FULL_SECTOR:
		case RUNTIME_LOG_ERROR_ADDRESS:
			// get previous value
			runtimeValue = *(uint16_t*)(address - sizeof(uint16_t));
//			// save previous value in eeprom
//			hardwareParameter_saveByteArray(I_RUNTIME_LOG_MSB, sizeof(uint16_t), (uint8_t*)&runtimeValue, A);
			// format sector
			runtimelog_EraseSector();
			// write value to first address of runtime sector
			runtimelog_Save(runtimeValue);
			return RUNTIME_LOG_ERROR_ADDRESS;
			break;
		case RUNTIME_LOG_EMPTY_SECTOR:
			// sector is empty
//			// check if there is a runtime entry in eeprom. If not, default return value is zero anyway.
//			hardwareParameter_getByteArray(I_RUNTIME_LOG_MSB, sizeof(uint16_t), (uint8_t*)&runtimeValue, A);
			// save it in the first address
			runtimelog_Save(runtimeValue);
			return RUNTIME_LOG_EMPTY_SECTOR;
			break;
		case RUNTIME_LOG_VALID_RECORD:
			// valid record, continue reading through sector
			runtimeValue = *(uint16_t*)(address - sizeof(uint16_t));
			break;
		case RUNTIME_LOG_EMPTY_ADDRESS:
			// address is the next blank address.
			// Write here next time.
			lastReadAddress = address - sizeof(uint16_t);
			return RUNTIME_LOG_OK;
			break;
		case RUNTIME_LOG_END_OF_SEQUENCE:
			// recorded the maximum number of entries
			// time to wrap back to zero

			// format sector
			runtimelog_EraseSector();
			// write value to first address of runtime sector
			runtimelog_Save(0);
			return RUNTIME_LOG_OK;
		}
	}
	// We've checked through the whole sector and not found an empty address.
	// Nor managed to
	return RUNTIME_LOG_OK;
}

/**
 * @brief
 */
uint8_t runtimelog_VerifyAddress(uint32_t address) {
	uint16_t data = *(uint16_t*)address;

	if (address == RUNTIMELOG_FLASH_ADDRESS) {
		if (data == RUNTIMELOG_EMPTY_VALUE) {
			return RUNTIME_LOG_EMPTY_SECTOR;
		} else if (data == RUNTIMELOG_MAX_VALUE) {
			return RUNTIME_LOG_END_OF_SEQUENCE;
		} else {
			return RUNTIME_LOG_VALID_RECORD;
		}
	}

	if (address >= RUNTIMELOG_LAST_ADDRESS) {
		return RUNTIME_LOG_FULL_SECTOR;
	}

	uint16_t dataPrevious = *(uint16_t*)(address - sizeof(uint16_t));
	if (data == dataPrevious + 1) {
		return RUNTIME_LOG_VALID_RECORD;
	} else if (data == RUNTIMELOG_EMPTY_VALUE) {
		return RUNTIME_LOG_EMPTY_ADDRESS;
	} else if (data == RUNTIMELOG_MAX_VALUE) {
		return RUNTIME_LOG_END_OF_SEQUENCE;
	} else {
		return RUNTIME_LOG_ERROR_ADDRESS;
	}

}

uint8_t runtimelog_EraseSector() {
	HAL_FLASH_Unlock();
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );
	FLASH_Erase_Sector(FLASH_SECTOR_9, VOLTAGE_RANGE_3);
	return 0;
}

void runtimelog_Increment() {
	uint16_t prevValue = runtimelog_Read();
	runtimelog_Save(prevValue + 1);
}

uint8_t runtimelog_Save(uint16_t time) {
	uint64_t flash_data = time;
	uint32_t writeAddress;
	writeAddress = runtimelog_GetWriteAddress();
	// check if write address is past the last address in the sector
	if (writeAddress >= RUNTIMELOG_LAST_ADDRESS) {
		// format the sector so we can start writing to the start again
		runtimelog_EraseSector();
		writeAddress = runtimelog_GetWriteAddress();
	}


	if (lastReadAddress >= RUNTIMELOG_FLASH_ADDRESS
			&& lastReadAddress < RUNTIMELOG_FLASH_ADDRESS + RUNTIMELOG_PAGE_SIZE) {

		HAL_FLASH_Unlock();
		__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR
				| FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );
		uint8_t status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, writeAddress, flash_data);
		//	HAL_FLASH_Lock();
		return status;
	}
	return 0;
}

uint16_t runtimelog_Read() {
	uint32_t address = runtimelog_GetReadAddress();
	uint16_t runtimeValue = *(uint16_t*) address;
	return runtimeValue;
}

float runtimelog_GetHours() {
	uint16_t xTenMinutes = runtimelog_Read();
	float hours = (float)(xTenMinutes) / 6;
	return hours;
}

uint32_t runtimelog_GetReadAddress() {
	lastReadAddress = RUNTIMELOG_FLASH_ADDRESS;
	for (uint32_t address = RUNTIMELOG_FLASH_ADDRESS;
			address < RUNTIMELOG_FLASH_ADDRESS + RUNTIMELOG_PAGE_SIZE;
			address += sizeof(uint16_t)) {
		uint16_t data = *(uint16_t*)address;
		if (data == RUNTIMELOG_EMPTY_VALUE) {
			return lastReadAddress;
		}
		lastReadAddress = address;
	}
	// No empty fields found. Assume sector is full.
	return RUNTIMELOG_LAST_ADDRESS; // return last address in sector
}

uint32_t runtimelog_GetWriteAddress() {
	lastReadAddress = RUNTIMELOG_FLASH_ADDRESS;
	for (uint32_t address = RUNTIMELOG_FLASH_ADDRESS;
			address < RUNTIMELOG_FLASH_ADDRESS + RUNTIMELOG_PAGE_SIZE;
			address += sizeof(uint16_t)) {
		uint16_t data = *(uint16_t*)address;
		if (data == RUNTIMELOG_EMPTY_VALUE) {
			return address;
		}
		lastReadAddress = address;
	}
	// no empty values found
	return RUNTIMELOG_LAST_ADDRESS; // return last address in sector
}
