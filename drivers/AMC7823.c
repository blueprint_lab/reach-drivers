/*
 * AMC7823.c
 *
 *  Created on: Sep 24, 2019
 *      Author: 61416
 */
#include "Hardware/AMC7823.h"


void AMC7823_Construct(AMC7823_t* self, SPI_HandleTypeDef* serialObj, uint16_t chipSelect, GPIO_TypeDef* chipSelectPort)
{

	self->ReadGPIO = AMC7823_ReadGPIO;
	self->ReadADC = AMC7823_ReadContinuousADC;
	self->WriteDAC = AMC7823_WriteDAC;
	self->Destroy = AMC7823_Destroy;

	self->spiObj = serialObj;
	self->chipSelect = chipSelect;
	self->chipSelectPort = chipSelectPort ;
	self->receiveBuff = (volatile uint16_t*)calloc(AMC7823_RX_BUFF_SIZE, sizeof(uint16_t));
	self->sendBuff = (uint16_t*)calloc(AMC7823_TX_BUFF_SIZE, sizeof(uint16_t));

	self->spiObj->Init.DataSize=SPI_DATASIZE_16BIT;
	self->spiObj->Init.CLKPolarity = SPI_POLARITY_LOW;
	self->spiObj->Init.CLKPhase = SPI_PHASE_2EDGE;

	HAL_SPI_Init(self->spiObj);
	HAL_SPI_MspInit(self->spiObj);

	self->enabledADCStartChannel = 0;
	self->enabledADCEndChannel = 0;
	//hspi->Init.DataSize

	GPIO_InitTypeDef GPIO_InitStruct = {0};

	  GPIO_InitStruct.Pin = self->chipSelect;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLUP;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	  HAL_GPIO_Init(self->chipSelectPort, &GPIO_InitStruct);
	  HAL_GPIO_WritePin( self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
}

void AMC7823_ReadGPIO(AMC7823_t* self)
{


    self->sendBuff[0]=0;
    self->sendBuff[0]|=READ_OPERATION<<RW;
    self->sendBuff[0]|=0<<PG0;
    self->sendBuff[0]|=0<<PG1;
    self->sendBuff[0]|=GPIO_ADDRESS<<SADR;

    self->sendBuff[1]=0;

    self->receiveBuff[0]=0;
    self->receiveBuff[1]=0;

    //convert this to transmit function

    HAL_GPIO_WritePin( self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);

    HAL_SPI_TransmitReceive(self->spiObj, self->sendBuff, self->receiveBuff , 2, 1);


    HAL_GPIO_WritePin( self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);

 // convert bits to bytes in GPIO state array
    for(int i=0;i<6;i++)
    {
    	self->GPIOState[i]=0x0001&(self->receiveBuff[1]>>i);
    }


}

/**
 *
 */
void AMC7823_WriteRegister(AMC7823_t* self, uint8_t page,
						uint8_t startRegister, uint8_t endRegister, uint16_t* dataBuffer) {

	self->sendBuff[0] = 0;
	self->sendBuff[0] |= (WRITE_OPERATION << RW);  // set write bit
	if (page > 0) { 	// valid page values are PG0 is 0 (first page) or 1 (second page)
		self->sendBuff[0] |= (1 << PG0);
	} else {
		self->sendBuff[0] |= (0 << PG0);
	}
	self->sendBuff[0] |= (0 << PG1);
	// register addresses are 5bits integers
	self->sendBuff[0] |= ((startRegister & 0x1F) << SADR);
	self->sendBuff[0] |= ((endRegister & 0x1F) << EADR);

	// calculate num of registers to write to
	uint8_t numRegistersToWrite = 1;
	if (endRegister > startRegister) {
		numRegistersToWrite += endRegister - startRegister;
	}

	// copy all bytes into the sendBuff
	for (uint8_t i = 0; i < numRegistersToWrite; i++) {
		self->sendBuff[i + 1] = 0;
		self->sendBuff[i + 1] = *dataBuffer;
		dataBuffer++;  // increment pointer
	}

	uint8_t sizeTransmitData = numRegistersToWrite + 1;  // +1 to include the command byte in this size

	// reset select pin to enable comms with AMC7823
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);
	// transmit via SPI
	HAL_SPI_TransmitReceive(self->spiObj, (uint8_t*)self->sendBuff, (uint8_t*)self->receiveBuff,
			sizeTransmitData, 1);
	// set select pin to disable comms with AMC7823
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
}


void AMC7823_ReadRegisters(AMC7823_t* self, uint8_t page,
						uint8_t startRegister, uint8_t endRegister, uint16_t* dataBuffer) {

	self->sendBuff[0] = 0;
	self->sendBuff[0] |= (READ_OPERATION << RW);  // set write bit
	if (page > 0) { 	// valid page values are PG0 is 0 (first page) or 1 (second page)
		self->sendBuff[0] |= (1 << PG0);
	} else {
		self->sendBuff[0] |= (0 << PG0);
	}
	self->sendBuff[0] |= (0 << PG1);
	// register addresses are 5bits integers
	self->sendBuff[0] |= ((startRegister & 0x1F) << SADR);
	self->sendBuff[0] |= ((endRegister & 0x1F) << EADR);

	// calculate num of registers to receive
	uint8_t numRegistersToReceive = 1;
	if (endRegister > startRegister) {
		numRegistersToReceive += endRegister - startRegister;
	}

	uint8_t sizeTransmitData = numRegistersToReceive + 1;  // +1 to include the command byte in this size
	// reset select pin to enable comms with AMC7823
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);
	// transmit via SPI
	HAL_SPI_TransmitReceive(self->spiObj, (uint8_t*)self->sendBuff, (uint8_t*)dataBuffer, sizeTransmitData, 1);
	// set select pin to disable comms with AMC7823
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);

}

/**
 * Enable ADC single conversion mode.
 * Uses less power but takes longer than continuous
 */
void AMC7823_EnableADC(AMC7823_t* self) {

	uint16_t sendData=0;

	// write to AMC control register
	// set GREF bit to 1 for DAC output range of 0-5v
	// reset ECNVT to 0
	sendData = 0;
	sendData |= 1 << GREF;
	AMC7823_WriteRegister(self, PAGE_1, AMC_STATUS, 0, &sendData);

	//read Power Down Register
	self->receiveBuff[0]=0;
	self->receiveBuff[1]=0;
	AMC7823_ReadRegisters(self, PAGE_1, POWER_DOWN, 0, self->receiveBuff);

	// POWER DOWN reg
	sendData = 0;
	sendData = self->receiveBuff[1]; // use receive buffer data to avoid turning off other components
	sendData |= 1 << PADC;		// turn on ADC
	AMC7823_WriteRegister(self, PAGE_1, POWER_DOWN, 0, &sendData);
}

/**
 * Enable ADC in continuous conversion mode
 * Faster than single conversion mode but uses more power.
 */
void AMC7823_EnableContinuousADC(AMC7823_t* self, uint8_t startChannel, uint8_t endChannel) {

	uint16_t sendData=0;

	// write to AMC control register
	// set GREF bit to 1 for DAC output range of 0-5v
	// reset ECNVT to 0
	sendData = 0;
	sendData |= 1 << GREF;
	AMC7823_WriteRegister(self, PAGE_1, AMC_STATUS, 0, &sendData);


	// Write to ADC Control Register to begin auto conversions
	sendData = 0;
	sendData |= ADC_AUTO_MODE << ADC_CTRL_CMODE;
	sendData |= startChannel << ADC_CTRL_SADR;
	sendData |= endChannel << ADC_CTRL_EADR;

	AMC7823_WriteRegister(self, PAGE_1, ADC_CONTROL, 0, &sendData);

	//read Power Down Register
	self->receiveBuff[0]=0;
	self->receiveBuff[1]=0;
	AMC7823_ReadRegisters(self, PAGE_1, POWER_DOWN, 0, self->receiveBuff);

	// write to POWER DOWN register
	sendData = 0;
	sendData = self->receiveBuff[1]; // use receive buffer data to avoid turning off other components
	sendData |= 1 << PADC;		// turn on ADC
	AMC7823_WriteRegister(self, PAGE_1, POWER_DOWN, 0, &sendData);

	self->enabledADCStartChannel = startChannel;
	self->enabledADCEndChannel = endChannel;

}

/**
 * Read ADC after it has been initialised in continuous conversion mode
 * Faster than single conversion mode but uses more power.
 */
void AMC7823_ReadContinuousADC(AMC7823_t* self)
{

	uint16_t readData=0;
	uint16_t sendData=0;

	// Read ADC
    for (int i=0; i<AMC7823_RX_BUFF_SIZE; i++) {
    	self->receiveBuff[i]=0;
    }

    AMC7823_ReadRegisters(self, PAGE_0,
    		ADC_ADDRESS + self->enabledADCStartChannel,
    		ADC_ADDRESS + self->enabledADCEndChannel,
			self->receiveBuff);

    uint8_t numChannels = 1 + self->enabledADCEndChannel - self->enabledADCStartChannel;
    for (int i=0; i<numChannels; i++) {
    	self->ADCReadings[i + self->enabledADCStartChannel] = self->receiveBuff[i+1] & 0xFFF;
    }
}

/**
 * Read ADC in single conversion mode.
 * Uses less power but takes longer than continuous mode
 */
void AMC7823_ReadADC(AMC7823_t* self)
{

	uint16_t readData=0;
	uint16_t sendData=0;

    // Write to ADC Control Register to begin conversion
	sendData = 0;
	sendData |= ADC_DIRECT_MODE << ADC_CTRL_CMODE;
	sendData |= 0 << ADC_CTRL_SADR;
	sendData |= 1 << ADC_CTRL_EADR;

	AMC7823_WriteRegister(self, PAGE_1, ADC_CONTROL, 0, &sendData);

	uint16_t dataAvailable = 0;
	while (dataAvailable == 0) {
		// Read AMC Control register
		self->receiveBuff[0]=0;
		self->receiveBuff[1]=0;
		AMC7823_ReadRegisters(self, PAGE_1, AMC_STATUS, 0, self->receiveBuff);

		// Check for DAVF bit high
		dataAvailable = self->receiveBuff[1] & (1<<DAVF);
	}

	// Read ADC
    for (int i=0; i<AMC7823_RX_BUFF_SIZE; i++) {
    	self->receiveBuff[i]=0;
    }
    AMC7823_ReadRegisters(self, PAGE_0, ADC_ADDRESS, ADC_ADDRESS + 1, self->receiveBuff);

    for (int i=0; i<AMC7823_NUM_ADC_CHANNELS; i++) {
    	self->ADCReadings[i] = self->receiveBuff[i+1] & 0xFFF;
    }
}

void AMC7823_WriteDAC(AMC7823_t* self, uint8_t channel, uint16_t value) {
	uint16_t sendData = 0;
	sendData = 0xFFF & value;
	AMC7823_WriteRegister(self, PAGE_1, DAC_DATA_0 + channel, 0, &sendData);
}

void AMC7823_EnableDAC(AMC7823_t* self, uint8_t channel) {
	uint16_t sendData=0;

	// write to AMC control register
	// set GREF bit to 1 for DAC output range of 0-5v
	sendData = 0;
	sendData |= 1 << GREF;
	AMC7823_WriteRegister(self, PAGE_1, AMC_STATUS, 0, &sendData);

	// read DAC config register
	self->receiveBuff[0]=0;
	self->receiveBuff[1]=0;
	AMC7823_ReadRegisters(self, 1, DAC_CONFIG, 0, self->receiveBuff);

	// DAC config register
	//		 SLDAn to 0 (async load)
	//		 GDACn to 1
	sendData = self->receiveBuff[1];
	sendData |= 1 << (channel + GDAC0);
	sendData &= ~(1 << (channel + SLDA0));
	AMC7823_WriteRegister(self, PAGE_1, DAC_CONFIG, 0, &sendData);

	//read Power Down Register
	self->receiveBuff[0]=0;
	self->receiveBuff[1]=0;
	AMC7823_ReadRegisters(self, PAGE_1, POWER_DOWN, 0, self->receiveBuff);

	// POWER DOWN reg
	sendData = 0;
	sendData = self->receiveBuff[1];  // use receive buffer data to avoid turning off other components
	sendData |= 1 << (PDAC0 + channel);  // turn on the DAC channel
	sendData |= 1 << PREFB;		// turn on the Reference Buffer Amplifier
	AMC7823_WriteRegister(self, PAGE_1, POWER_DOWN, 0, &sendData);

}

void AMC7823_Destroy(AMC7823_t* self) {
	self->ReadGPIO = AMC7823_NOP;
	self->ReadADC = AMC7823_NOP;
	self->WriteDAC = AMC7823_NOP_DAC;
}

void AMC7823_NOP(AMC7823_t* self) {
	return;
}

void AMC7823_NOP_DAC(AMC7823_t* self, uint8_t pin, uint16_t value) {
	return;
}
