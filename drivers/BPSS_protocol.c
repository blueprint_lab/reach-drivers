/*
 * uartBPSS.c
 *
 *  Created on: 16 Jan 2018
 *      Author: p.phillips
 *
 *      Implementation of the Blueprint Subsea protocol used on the Fusion
 *
 *      Refactored by Kyle McLean 17/03/2022:
 *      	- Updated funtionality to work with current BPL system
 *      	- Minor bug fixes
 *      	- Reformat file
 *      	- Efficiency improvoments
 *      	- Added commenting
 *      	- Implimented check sum for packets decode functionality
 */

#include <string.h>
#include <stdlib.h>
#include "Communication/BPSS_protocol.h"

/* Private variables ---------------------------------------------------------------------------------------------------------- */
const char HEX_SYMBOLS[] = "0123456789ABCDEF";

/* Private funtion protypes --------------------------------------------------------------------------------------------------- */
void reset_packet(packet_t packet);
void asctohex(char a, char *s);
void abin2hex(uint8_t value, uint8_t * pBuf);
uint8_t ahex2bin (uint8_t MSB,uint8_t LSB);
uint16_t calculateChecksum(uint8_t* buf, uint16_t len);

/* BPSS encode and decode methods --------------------------------------------------------------------------------------------- */

/*
 * @breif Decodes a packet using BPSS potocol
 */
uint8_t BPSS_protocol_decodePacket(coms_link* parent) {
	uint8_t exit_status;

	uint16_t endByte = 0;;
	uint16_t checksum;

	size_t startIdx = 0;
	size_t length = 0;
	size_t startByte = 0;

	/* Fetch the UART struct pointer */
	uart_comms_link* self = parent->uartLink;

	/* Set the write pointer to the buffer length - number of data register */
	self->writePointer = self->rxBuffLen - self->huart->hdmarx->Instance->NDTR;

	/* Get a copy of the current writePointer */
	uint16_t writePointCopy = self->writePointer;

	/* No data since last read; then exit */
	if (writePointCopy == self->readPointer) return 0;

	/* Compute the index of the packet end */
	while(endByte != 0x0A0D) {
		self->readPointer++;

		if(self->readPointer >= self->rxBuffLen) self->readPointer = 0;
		if(self->packetEnd >= self->rxBuffLen) self->packetEnd=0;

		endByte = (self->rxBuff[self->packetEnd + 1] << 8) | self->rxBuff[self->packetEnd];

		self->packetEnd++;
	}

	/* If no packet found in data - Exit */
	if(self->readPointer == writePointCopy) {
		reset_packet(parent->currPacket);

		return 0;
	}

	/* Look for the start byte working backwards from the end of the buffer */
	while(startByte != '#') {
		startByte = fetchByte(self->rxBuff, self->rxBuffLen, self->packetEnd - startIdx);

		/* Check for wrap around */
		if (self->packetEnd - startIdx == 0) {
			startIdx = self->rxBuffLen; /* Go to the back of the buffer */
		} else {
			startIdx++; /* Move to the next byte in the buffer */
		}

		/* Check if entire buffer has been checked */
		if(self->packetEnd == startIdx) return 0;

		/* Increment the length of the packet */
		length++;
	}

	/* Remove the CR from the packet end */
	self->rxBuff[self->packetEnd] = 0xFF;

	/* Validate packet length */
	if(length < 10 || length >= self->rxBuffLen || ((length - 1) % 2) != 0) {
		reset_packet(parent->currPacket);

		return 0;
	}

	/* Copy out the data into an unwrapped buffer so that it can be decoded. */
    uint8_t *unwrappedBuffer = malloc(length - 1);
	for (int i = 0; i < length - 1; ++i) {
		// TODO: handle wrap condition
		uint8_t byte = fetchByte(self->rxBuff, self->rxBuffLen, self->packetEnd - length + 1 + i);
		writeByte(unwrappedBuffer, length + 1, i, byte);
	}

	/* Reduce length as we don't have the two end bytes and first byte */
	length = length - 2;

	/* Compute the address and coed fome the buffer */
	parent->currPacket.address = ahex2bin(unwrappedBuffer[1],unwrappedBuffer[2]);
	parent->currPacket.code = ahex2bin(unwrappedBuffer[3],unwrappedBuffer[4]);

	/* Copy the data out of the string */
	uint8_t j = 0;
	uint8_t dataStart = 5;
	uint8_t dataLength = (length - 8) / 2;

	if (dataLength-1 > 64 || dataStart + j + 1 > length - 1) {
		while (1){}
	}

	for (uint8_t i=0; i < dataLength; i++) {
		parent->currPacket.data[dataLength-i-1] = ahex2bin(unwrappedBuffer[dataStart + j], unwrappedBuffer[dataStart + j + 1]);
		j = j + 2; // 2 characters for every byte
	}

	/* Extract the crc value */
	uint16_t crc16 = (ahex2bin((unwrappedBuffer[7 + j]), (unwrappedBuffer[8 + j])) << 8) | ahex2bin((unwrappedBuffer[5 + j]), (unwrappedBuffer[6 + j]));

	/* Validate check sum */
	uint8_t *dataBytes = calloc(dataLength + 2, sizeof(uint8_t));

	memcpy(dataBytes + 2, parent->currPacket.data, dataLength);

	dataBytes[0] = parent->currPacket.address;
	dataBytes[1] = parent->currPacket.code;

	checksum = calculateChecksum(dataBytes, (uint16_t)dataLength + 2);

	if(checksum != crc16)  {
		reset_packet(parent->currPacket);
		exit_status = 0;

		goto EXIT_STUB;
	}

	/* Add packet headder to length */
	parent->currPacket.length = dataLength + UART_PACKET_HEADER_SIZE;

	/* Succesful packet read */
	exit_status = 1;

	/* Clean up and exit */
	EXIT_STUB:

	/* Free dynamically allocated memory */
	free(dataBytes);
	free(unwrappedBuffer);

	/* Return exit status */
	return exit_status;
}

void reset_packet(packet_t packet) {
	packet.length = 0;
	packet.code = 0;
	packet.address = 0;
}

/*
 * @breif Encodes a packet using BPSS potocol
 */
uint8_t BPSS_protocol_encodePacket(packet_t* packet, uint8_t address ,uint8_t cmd_id, uint8_t length, uint8_t* data) {
	uint8_t dataLength = length - UART_PACKET_HEADER_SIZE;
	uint8_t *dataBytes = malloc(length);
	uint8_t j = 0;
	uint8_t crc[2];
	uint16_t checksum;

	/* Fill in packet attributes */
	packet->address = address;
	packet->code = cmd_id;
	packet->length = length;

	/* Compute CRC */
	dataBytes[0] = packet->address;
	dataBytes[1] = packet->code;

	memcpy(dataBytes + 2, data, dataLength);

	checksum = calculateChecksum(dataBytes,length - 2);

	crc[0] = ((checksum >> 8) & 0xff);
	crc[1] = checksum & 0xff;

	/* Construct transmit packet */
	*packet->transmitData='$';

	abin2hex(packet->address, packet->transmitData + 1);
	abin2hex(packet->code, packet->transmitData + 3);

	for(uint8_t i = 0; i < dataLength; i++) {
		abin2hex(data[dataLength-i-1], packet->transmitData + 5 + j); //little endian
		j = j + 2;
	}

	/* Pack CRC into transmit data buffer */
	abin2hex(crc[1], packet->transmitData + 5 + j);
	abin2hex(crc[0], packet->transmitData + 7 + j);

	/* Pack termination char into transmit data buffer */
	packet->transmitData[9 + j] = 0x0D;
	packet->transmitData[10 + j] = 0x0A;

	/* Update the packet length */
	packet->length = 11 + j;

	/* Free dynamically allocated memory */
	free(dataBytes);

	return 1;
}

/* Helper funtions --------------------------------------------------------------------------------------------- */

/*
 * @breif Function that computes the CRC16 value for an array of sequential bytes stored in memory.
 * NB: Types uint8 and uint16 represent unsigned 8 and 16 bit integers respectively.
 *
 * @param buf Pointer to the start of the buffer to compute the CRC16 for.
 * @param len The number of bytes in the buffer to compute the CRC16 for.
 * @result The new CRC16 value.
 */
uint16_t calculateChecksum(uint8_t* buf, uint16_t len) {
	uint16_t poly = 0xA001;
	uint16_t crc = 0;

	for(uint16_t b = 0; b < len; b++) {
		uint8_t v = *buf;
		for(uint8_t i = 0; i < 8; i++) {
			if((v & 0x01) ^ (crc & 0x01)) {
				crc >>= 1;
				crc ^= poly;
			}
			else{
				crc >>= 1;
			}
			v >>= 1;
		}
		buf++;
	}

	return crc;
}

/*
 * @breif Converts ASC to HEX
 */
void asctohex(char a, char *s) {
	char c;
	c = (a >> 4) & 0x0f;
	if (c <= 9) c+= '0'; else c += 'a' - 10;
	*s++ = c;
	c = a & 0x0f;
	if (c <= 9) c+= '0'; else c += 'a' - 10;
	*s++ = c;
	*s = 0;
}

/*
 * @breif Converts BIN to HEX
 */
void abin2hex(uint8_t value, uint8_t * pBuf) {
	*pBuf++ = HEX_SYMBOLS[(value >> 4)];
	*pBuf++ = HEX_SYMBOLS[(value & 0x0F)];
}

/*
 * @breif Converts HEX to BIN
 */
uint8_t ahex2bin (uint8_t MSB,uint8_t LSB) {
	if (MSB > '9') MSB -= 7;   // Convert MSB value to a contiguous range (0x30..0x3F)
	if (LSB > '9') LSB -= 7;   // Convert LSB value to a contiguous range (0x30..0x3F)
	return (MSB <<4) | (LSB & 0x0F);   // Make a result byte  using only low nibbles of MSB and LSB thus neglecting the input register case
}

/*
 * @breif Returns number of packets
 */
size_t BPSS_protocol_numPackets(coms_link* parent) {
	uint8_t i = 0;
	size_t count = 0;

	for(i = 0; i < parent->uartLink->rxBuffLen; ++i){
		if(parent->uartLink->rxBuff[i] == 0x0D  ){
			++count;
		}
	}

	return count;
}
