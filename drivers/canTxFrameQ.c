/*
 * canTxFrameQ.c
 *
 *  Created on: 12 Mar 2021
 *      Author: Shaun Barlow
 */

#ifdef TEST_TX_FRAME_CIRC_Q
/***************** Test only ********************/
#include "../inc/Communication/canTxFrameQ.h"

#else
/***************** Build only *******************/
#include "Communication/canTxFrameQ.h"

#endif

/***************** Common ***********************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


void txFrameQ_Deconstruct(txFrameQ_t* self);
void txFrameQ_Reset(txFrameQ_t* self);
uint8_t txFrameQ_Push(txFrameQ_t* self, txFrame_t* elem);
uint8_t txFrameQ_Erase(txFrameQ_t* self);
uint8_t txFrameQ_PopPreview(txFrameQ_t* self, txFrame_t* elem);
uint8_t txFrameQ_Pop(txFrameQ_t* self, txFrame_t* elem);

void txFrameQ_Construct(txFrameQ_t* self, size_t size) {
	self->Deconstruct 	= txFrameQ_Deconstruct;
	self->Reset 		= txFrameQ_Reset;
	self->Push 			= txFrameQ_Push;
	self->Erase 		= txFrameQ_Erase;
	self->PopPreview	= txFrameQ_PopPreview;
	self->Pop			= txFrameQ_Pop;


	self->origin = (txFrame_t*)calloc(size, sizeof(txFrame_t));
	self->front = self->origin;
	self->back = self->origin;
	self->maxel = size;
	self->numel = 0;
	self->locked = 0;
}

void txFrameQ_Deconstruct(txFrameQ_t* self) {
	free(self->origin);
	self->origin = NULL;
	self->back= NULL;
	self->front= NULL;

	self->numel = 0;
	self->maxel = 0;
}


void txFrameQ_Reset(txFrameQ_t* self) {
	self->front = self->origin;
	self->back = self->origin;
	self->numel = 0;
}

uint8_t txFrameQ_Push(txFrameQ_t* self, txFrame_t* elem) {
	// Lock the frame queue to prevent memory access violation via CAN TX callback.
	// Note: the push functionality must be locked since it can be preempted by the other functions that access its members
	self->locked = 1;

	if (self->numel >= self->maxel) {
		printf("txFrameQ_Push(). There has been an overflow!\n");
		self->locked = 0;
		return 0; // Overflow
	}
	else if (self->back >= (&self->origin[self->maxel - 1])) {  // reached the end and must rap around
		memcpy(self->back, elem, sizeof(txFrame_t));
		self->back = self->origin;
		self->numel++;
	}
	else {
		memcpy(self->back, elem, sizeof(txFrame_t));
		self->back++;
		self->numel++;
	}

	// Unlock the queue
	self->locked = 0;

	return 1;
}

uint8_t txFrameQ_Erase(txFrameQ_t* self) {
	// Lock the frame queue to prevent memory access violation via CAN TX callback.
	// Note: the push functionality must be locked since it can be preempted by the other functions that access its members
	self->locked = 1;

	if (self->numel <= 0) {
		printf("txFrameQ_Erase(). There has been an underflow!\n");
		self->locked = 0;
		return 0; // underflow
	}

	// Remove and clear element at front
	memset(self->front, 0, sizeof(txFrame_t));
	self->numel--;

	if (self->front == &self->origin[self->maxel - 1]) { // wrap around
		self->front = self->origin;
	}
	else {
		self->front++;
	}

	// Unlock the queue
	self->locked = 0;

	return 1;
}

uint8_t txFrameQ_PopPreview(txFrameQ_t* self, txFrame_t* elem) {
	// Lock the frame queue to prevent memory access violation via CAN TX callback.
	// Note: the push functionality must be locked since it can be preempted by the other functions that access its members
	self->locked = 1;

	if (self->numel <= 0 ) {
		printf("txFrameQ_PopPreview(). There has been an underflow!\n");
		self->locked = 0;
		return 0; //underflow
	}

	// Get the contents of the first element on the queue
	memcpy(elem, self->front, sizeof(txFrame_t));

	// Unlock the queue
	self->locked = 0;

	return 1;
}

uint8_t txFrameQ_Pop(txFrameQ_t* self, txFrame_t* elem) {
	// Lock the frame queue to prevent memory access violation via CAN-ISR.
	// Note: the pop functionality must be locked since it can be preempted by the other functions that access its members
	self->locked = 1;

	if (self->numel <= 0 )
	{
		printf("txFrameQ_Pop(). There has been an underflow!\n");
		self->locked = 0;
		return 0; //underflow
	}

	// remove and clear element at front
	memcpy(elem, self->front, sizeof(txFrame_t));
	memset(self->front, 0, sizeof(txFrame_t));

	// Decrement the number of elements on the queue
	self->numel--;

	if (self->front == &self->origin[self->maxel - 1]) { // wrap around
		self->front = self->origin;
	}
	else {
		self->front++;
	}

	// Unlock the queue
	self->locked = 0;

	return 1;
}


#ifdef TEST_TX_FRAME_CIRC_Q
/*
 * To run this test -
 * Open terminal, run:
 * > cd /to/this/directory
 * > gcc canTxFrameQ.c -o test -D TEST_TX_FRAME_CIRC_Q
 * > ./test.exe
 *
 * If gcc is not found, install on Windows with MinGW.
 */
#define TEST(num, eq) eq ? printf("TEST %d: PASS\n", num) : printf("TEST %d: FAIL\n", num)

uint8_t main() {
	printf("TEST_TX_FRAME_CIRC_Q\n\n");

	txFrameQ_t q;
	txFrameQ_Construct(&q, 4);

	TEST(100, q.maxel == 4);
	TEST(101, q.numel == 0);

	// pop from queue should return 0 and not change elem
	txFrame_t dest;
	dest.header.ExtId = 0x1234567;
	uint8_t result = q.PopPreview(&q, &dest);
	uint8_t result = q.Erase(&q);
	TEST(201, result == 0);
	TEST(202, dest.header.ExtId == 0x1234567);

	// push to queue and check that numel is updated
	txFrame_t source;
	source.header.ExtId = 0x2345678;
	result = q.Push(&q, &source);
	TEST(301, result == 1);
	TEST(302, q.numel == 1);

	// Push again
	source.header.ExtId = 0x2334455;
	result = q.Push(&q, &source);
	TEST(303, result == 1);
	TEST(304, q.numel == 2);

	// Pop and check the first elem
	uint8_t result = q.PopPreview(&q, &dest);
	uint8_t result = q.Erase(&q);
	TEST(401, dest.header.ExtId == 0x2345678);
	TEST(402, result == 1);

	// Pop again
	uint8_t result = q.PopPreview(&q, &dest);
	uint8_t result = q.Erase(&q);
	TEST(403, dest.header.ExtId == 0x2334455);
	TEST(404, result == 1);
	TEST(405, q.numel == 0);

	// Push three more to test the wrapping
	source.header.ExtId = 1;
	result = q.Push(&q, &source);
	TEST(501, result == 1);
	TEST(502, q.numel == 1);

	source.header.ExtId = 2;
	result = q.Push(&q, &source);
	TEST(503, result == 1);
	TEST(504, q.numel == 2);

	source.header.ExtId = 3;
	result = q.Push(&q, &source);
	TEST(505, result == 1);
	TEST(506, q.numel == 3);

	// Pop all three from the buffer and check the ExtIds
	uint8_t result = q.PopPreview(&q, &dest);
	uint8_t result = q.Erase(&q);
	TEST(603, dest.header.ExtId == 1);
	TEST(604, result == 1);
	TEST(605, q.numel == 2);

	uint8_t result = q.PopPreview(&q, &dest);
	uint8_t result = q.Erase(&q);
	TEST(703, dest.header.ExtId == 2);
	TEST(704, result == 1);
	TEST(705, q.numel == 1);

	uint8_t result = q.PopPreview(&q, &dest);
	uint8_t result = q.Erase(&q);
	TEST(803, dest.header.ExtId == 3);
	TEST(804, result == 1);
	TEST(805, q.numel == 0);

	// Pop again, ensure that Q is empty and does fill in dest
	dest.header.ExtId = 0x1234567;
	uint8_t result = q.PopPreview(&q, &dest);
	uint8_t result = q.Erase(&q);
	TEST(901, result == 0);
	TEST(902, dest.header.ExtId == 0x1234567);
	return 0;
}

#endif /* TEST_TX_FRAME_CIRC_Q */
