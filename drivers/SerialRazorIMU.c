/*
 * SerialMPU6000.c
 *
 *  Created on: 9Feb.,2017
 *      Author: TheBlueprintLabs
 */

#include <string.h>
#include <stdlib.h>
#include <Math.h>
//#include "stm32_device_select.h"
#include "Hardware/SerialRazorIMU.h"

/*
 * Size should be 6 for the moment
 */
void razorIMU_Construct(razorIMU_t* self, UART_HandleTypeDef* huart, size_t size){
	self->Read = razorIMU_readAngle;
	self->Transmit = razorIMU_transmit;
	self->reset = razorIMU_reset;
	self->enable = razorImu_enable;

	self->huart =  huart;
	self->rxBuffLen = size;
	self->txBuffLen = size;

	self->txBuff = (uint8_t*)calloc(size, sizeof(uint8_t));
	self->rxBuff = (uint8_t*)calloc(size, sizeof(uint8_t));

	//uint8_t* txBuff;
	//uint16_t rxBuffLen;
	//uint16_t txBuffLen;
}

uint8_t razorImu_enable(razorIMU_t* self){

	if(self->rxBuff == NULL ){
		// Error
		return 0;
	}
	if (HAL_UART_Receive_DMA(self->huart, (uint8_t*)self->rxBuff, self->rxBuffLen) == HAL_ERROR){
		return 0;
	}
	return 1;
}

float razorIMU_readAngle(razorIMU_t* self){

	uint32_t timeout = 1000;
	while(HAL_DMA_GetState(self->huart->hdmarx) != HAL_DMA_STATE_READY && timeout > 0){
		timeout--;
	}
	if( !timeout ){
//		uint8_t index = 0;
//		for(int i = 0; i < self->rxBuffLen; ++i){ // find the end of the string
//			index = i;
//			if(self->rxBuff[i] == '\r' )
//				break;
//		}
//		uint8_t miniBuff[RX_MSG_LEN];
//		for(int i = 0; i < RX_MSG_LEN; ++i){ // copy from the end of the data to the start
//			miniBuff[RX_MSG_LEN - 1 - i] = self->rxBuff[(index - 1 -i)%self->rxBuffLen];
//		}
		float output = (float)atof( (const char *)self->rxBuff);
		output = fmod((output+180), 360) - 180;
		return (output)*M_PI / 180.0;
	}
	else{
		return 100.0;  // this is never ordinarily returned

	}
}

void razorIMU_transmit(razorIMU_t* self, uint8_t* transmitQueue){

}
void razorIMU_reset(razorIMU_t* self){

}
