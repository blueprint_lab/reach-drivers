/*
 * comsprocess.c
 *
 *  Created on: 6 Jan 2021
 *      Author: shaun
 */
#include <string.h>
#include <stdio.h>

#ifdef TEST_COMSPROCESS
#include "../inc/Communication/comsprocess.h"
#include "../inc/Communication/comslink.h"
#include "../inc/DataStructures/circularQueue.h"
#else
#include "Communication/comsprocess.h"
#include "Communication/comslink.h"
#include "DataStructures/circularQueue.h"
#endif

static unsigned char const crc8_table[] = {
    0xea, 0xd4, 0x96, 0xa8, 0x12, 0x2c, 0x6e, 0x50, 0x7f, 0x41, 0x03, 0x3d,
    0x87, 0xb9, 0xfb, 0xc5, 0xa5, 0x9b, 0xd9, 0xe7, 0x5d, 0x63, 0x21, 0x1f,
    0x30, 0x0e, 0x4c, 0x72, 0xc8, 0xf6, 0xb4, 0x8a, 0x74, 0x4a, 0x08, 0x36,
    0x8c, 0xb2, 0xf0, 0xce, 0xe1, 0xdf, 0x9d, 0xa3, 0x19, 0x27, 0x65, 0x5b,
    0x3b, 0x05, 0x47, 0x79, 0xc3, 0xfd, 0xbf, 0x81, 0xae, 0x90, 0xd2, 0xec,
    0x56, 0x68, 0x2a, 0x14, 0xb3, 0x8d, 0xcf, 0xf1, 0x4b, 0x75, 0x37, 0x09,
    0x26, 0x18, 0x5a, 0x64, 0xde, 0xe0, 0xa2, 0x9c, 0xfc, 0xc2, 0x80, 0xbe,
    0x04, 0x3a, 0x78, 0x46, 0x69, 0x57, 0x15, 0x2b, 0x91, 0xaf, 0xed, 0xd3,
    0x2d, 0x13, 0x51, 0x6f, 0xd5, 0xeb, 0xa9, 0x97, 0xb8, 0x86, 0xc4, 0xfa,
    0x40, 0x7e, 0x3c, 0x02, 0x62, 0x5c, 0x1e, 0x20, 0x9a, 0xa4, 0xe6, 0xd8,
    0xf7, 0xc9, 0x8b, 0xb5, 0x0f, 0x31, 0x73, 0x4d, 0x58, 0x66, 0x24, 0x1a,
    0xa0, 0x9e, 0xdc, 0xe2, 0xcd, 0xf3, 0xb1, 0x8f, 0x35, 0x0b, 0x49, 0x77,
    0x17, 0x29, 0x6b, 0x55, 0xef, 0xd1, 0x93, 0xad, 0x82, 0xbc, 0xfe, 0xc0,
    0x7a, 0x44, 0x06, 0x38, 0xc6, 0xf8, 0xba, 0x84, 0x3e, 0x00, 0x42, 0x7c,
    0x53, 0x6d, 0x2f, 0x11, 0xab, 0x95, 0xd7, 0xe9, 0x89, 0xb7, 0xf5, 0xcb,
    0x71, 0x4f, 0x0d, 0x33, 0x1c, 0x22, 0x60, 0x5e, 0xe4, 0xda, 0x98, 0xa6,
    0x01, 0x3f, 0x7d, 0x43, 0xf9, 0xc7, 0x85, 0xbb, 0x94, 0xaa, 0xe8, 0xd6,
    0x6c, 0x52, 0x10, 0x2e, 0x4e, 0x70, 0x32, 0x0c, 0xb6, 0x88, 0xca, 0xf4,
    0xdb, 0xe5, 0xa7, 0x99, 0x23, 0x1d, 0x5f, 0x61, 0x9f, 0xa1, 0xe3, 0xdd,
    0x67, 0x59, 0x1b, 0x25, 0x0a, 0x34, 0x76, 0x48, 0xf2, 0xcc, 0x8e, 0xb0,
    0xd0, 0xee, 0xac, 0x92, 0x28, 0x16, 0x54, 0x6a, 0x45, 0x7b, 0x39, 0x07,
    0xbd, 0x83, 0xc1, 0xff};

// Return the CRC-8 of data[0..len-1] applied to the seed crc. This permits the
// calculation of a CRC a chunk at a time, using the previously returned value
// for the next seed. If data is NULL, then return the initial seed. See the
// test code for an example of the proper usage.
unsigned crc8(unsigned crc, unsigned char const *data, size_t len)
{
    if (data == NULL)
        return 0;
    crc &= 0xff;
    unsigned char const *end = data + len;
    while (data < end)
        crc = crc8_table[crc ^ *data++];
    return crc;
}

/* @brief Read a complete packet from the packet queue */
uint8_t coms_readPacketFromQ(packet_t* packet, circularQueue* Q) {
	// reset packet
	packet->length = 0;
	packet->code = 0;
	packet->address = 0;
	packet->option = 0;
	
	// check for bytes in the Q
	if (Q->numel == 0) return 0;
		
	// Check for a zero (terminating byte)
	int32_t zeroIndex = Q->Find(Q, PKT_TERMINATING_BYTE);
	if (zeroIndex < 0) return 0; 
		
	// Pop bytes from Q.
	uint8_t buffer[zeroIndex + 1];
	Q->popArray(Q, buffer, zeroIndex + 1);
	
	return coms_decodePacket(packet, buffer, zeroIndex + 1);
}

/* @brief Decode packet from a buffer of known length. This funtion supports
 * 		  V1 and V2 BPL packets.
 *
 * 		  Note 1: This funtion assumes the packet structure has been zero'd by it's caller.
 */
int8_t coms_decodePacket(packet_t* packet, uint8_t* buffer, uint8_t buffLength) {
	// Compute zero index from buffer length
	uint8_t zeroIndex = buffLength - 1;
	
	// Validate CRC is non zero 
	if (buffer[zeroIndex] != PKT_TERMINATING_BYTE) return -6;
	
	// Extract packet length (Bit 6:0) and options flag (Bit 7)
	uint8_t length = buffer[zeroIndex-2] & 0x7F;
	packet->useOption = (buffer[zeroIndex-2] >> 7) & 0x1;

	// Validate length against boundary values 
	if (length < PKT_HEADER_LEN) return -1;
	if (length > MAX_PACKET_DATA_SIZE) return -2;
	if (length != buffLength - 2) return -7;

	// Move start of buffer if packet length requires it
	uint8_t* pBuffer;
	pBuffer = &(buffer[zeroIndex]) - length - 1;
	
	// Copy raw packet into transmit data in case we're forwarding this packet
	memcpy(packet->transmitData, pBuffer, length + 1);
	packet->transmitData[length + 1] = PKT_TERMINATING_BYTE; // Terminate just in case length is corrupt.

	// Copy the buffer and then decode to the original buffer
	uint8_t bufferCpy[length + 1];
	memcpy(bufferCpy, pBuffer, length + 1);
	
	// Decode cobs and check crc byte for artifacts
	if (cobs_decode(bufferCpy, length + 1, pBuffer) == 0) return -3;
	if (crc8(0xFF, pBuffer, length-1) != pBuffer[length - 1]) return -4;
	
	// Packet is valid. Parse packet contents
	if (packet->useOption) {
		uint8_t option = pBuffer[length - 5] & 0x07;						// Extract bit 0:2
		uint8_t useProcotolV2 = (option >> 2) & 0x01;					 	// Extract bit 2

		uint8_t code_msb = 0;
		if (useProcotolV2) code_msb = (pBuffer[length - 5] >> 3) & 0x07; 	// Extract bit 3:5
		
		packet->option = option;
		packet->length = length - 1;
		packet->code = (code_msb << 8) | pBuffer[length - 4];
		packet->address = pBuffer[length - 3];
		
		memcpy(packet->data, pBuffer, length - 5);
	} else {
		packet->option = PACKET_OPT_ALL_ENABLED;
		packet->length = length;
		packet->address = pBuffer[length - 3];
		packet->code = pBuffer[length - 4];

		memcpy(packet->data, pBuffer, length - 4);
	}

	return 1; // 1 packet read
}

/* @brief Encode packet using BPL V1 or V2 protocol. This funtion supports
 * 		  V1 and V2 BPL packets.
 *
 * 		  Note 1: The useProcotolV2 option flag must be set in the options byte to send V2 packets.
 * 		  Note 2: The useOptions flag must be set in packet to encode options.
 */
int8_t coms_encodePacket(packet_t* packet, uint8_t address, uint16_t code, uint8_t length, uint8_t* data, uint8_t option) {
	uint8_t useProcotolV2 = (option >> 2) & 0x01;

	if (useProcotolV2) packet->useOption = 1;												// Packet must encode options to use V2

	if (length < HEADER_SIZE) return -1; 													// must include HEADER_SIZE in length
	if (length > DATA_BYTES_PER_PACKET + HEADER_SIZE - 2) return -2; 						// - 2 is COBS and delimiter
	if ((code > 0xFF && !useProcotolV2) || code > 0x7ff) return -3; 						// Check that code is supported in this verison
	if (packet->useOption) length++; 														// Increament packet length for extra options byte

	uint8_t unstuffedPacket[length];
	memcpy(unstuffedPacket, data, length-HEADER_SIZE);
	
	uint8_t code_lsb = (code >> 0) & 0xFF;
	uint8_t code_msb = (code >> 8) & 0x07;
	
	unstuffedPacket[length-4] = code_lsb;
	unstuffedPacket[length-3] = address;
	unstuffedPacket[length-2] = length;
	
	// Apply options data if required
	if (packet->useOption) {
		unstuffedPacket[length-5] = option | ((code_msb & 0x07) << 3);
		unstuffedPacket[length-2] |= OPTION_FLAG;
	} 

	// Compute crc check and apply cobs packet 
	unstuffedPacket[length-1] = crc8(0xff, unstuffedPacket, length-1);
	cobs_encode(unstuffedPacket, length, packet->transmitData);
	
	packet->transmitData[length+1] = 0; // add the terminating zero
	packet->length = length + 2; 		// +2 for cobs and delimiter
	
	// Fill in the other packet fields in case we need to identify the packet later.
	packet->address = address;
	packet->code = code;
	packet->option = option;
	
	// Copy data into packet
	memcpy(packet->data, data, length-HEADER_SIZE);
	
	return 1;
}

/* Unstuffs "length" bytes of data at the location pointed to by
 * "input", writing the output * to the location pointed to by
 * "output". Returns the number of bytes written to "output" if
 * "input" was successfully unstuffed, and 0 if there was an
 * error unstuffing "input".
 *
 * Remove the "restrict" qualifiers if compiling with a
 * pre-C99 C dialect.
 */
uint8_t cobs_decode(const uint8_t* input, uint8_t length, uint8_t* output) {
	uint8_t read_index = 0;
	uint8_t write_index = 0;
    uint8_t code;
    uint8_t i;

    while(read_index < length) {
        code = input[read_index];
        if(read_index + code > length && code != 1) return 0;
        read_index++;
        for(i = 1; i < code; i++) output[write_index++] = input[read_index++];
        if(code != 0xFF && read_index != length)
        	output[write_index++] = '\0';
    }
    return write_index;
}

/* Stuffs "length" bytes of data at the location pointed to by
 * "input", writing the output to the location pointed to by
 * "output". Returns the number of bytes written to "output".
 *
 * Remove the "restrict" qualifiers if compiling with a
 * pre-C99 C dialect.
 */
uint8_t cobs_encode( uint8_t* input, uint8_t length, uint8_t * output)
{
	uint8_t read_index = 0;
	uint8_t write_index = 1;
	uint8_t code_index = 0;
    uint8_t code = 1;

    while(read_index < length) {
        if(input[read_index] == 0) {
            output[code_index] = code;
            code = 1;
            code_index = write_index++;
            read_index++;
        } else {
            output[write_index++] = input[read_index++];
            code++;
            if(code == 0xFF) {
                output[code_index] = code;
                code = 1;
                code_index = write_index++;
            }
        }
    }
    output[code_index] = code;
    return write_index;
}
#ifdef TEST_COMSPROCESS
/*
 * To run this test, uncomment #define TEST_COMSPROCESS here and in .h file.
 * Open terminal, run:
 * > cd /to/this/directory
 * > gcc .\comsprocess.c .\circularQueue.c -o testcomsproc -DTEST_COMSPROCESS
 * > ./testcomsproc.exe
 *
 * If gcc is not found, install on Windows with MinGW.
 */
#define TEST(num, eq) eq ? printf("TEST %d: PASS\n", num) : printf("TEST %d: FAIL\n", num)

int main() {
	uint8_t testNum;
	/* ######################################################
	 * ################ coms_readPacketFromQ ################
	 * ######################################################
	 * test_packets = [[1, 1, bytearray([1])],
                [2, 3, 0.1],
                [4, 1, bytearray([1, 2, 3, 4])]]
encoded_packets = ['0601010105b000', '09cdcccc3d030208e300', '09010203040104087b00']
	 *
	 * */
	/** A single packet with all bytes intact **/
	packet_t packet;
	circularQueue Q;
	circularQueueConstruct(&Q, 64);
#define CASE_1_ENCODED {0x06, 0x01, 0x01, 0x01, 0x05, 0xb0, 0x00}
#define CASE_1_EXP_ADDRESS 1
#define CASE_1_EXP_CODE 1
#define CASE_1_EXP_DATA_0 1
#define CASE_1_EXP_LENGTH 5
	uint8_t temp1[] = CASE_1_ENCODED;
	Q.pushArray(&Q, temp1, sizeof(temp1)/sizeof(temp1[0]));
	coms_readPacketFromQ(&packet, &Q);
	TEST(1, packet.address == CASE_1_EXP_ADDRESS);
	TEST(2, packet.code == CASE_1_EXP_CODE);
	TEST(3, packet.length == CASE_1_EXP_LENGTH);
	TEST(4, packet.data[0] == CASE_1_EXP_DATA_0);
	TEST(5, Q.numel == 0);

	/** Extra bytes in front of packet will still parse correctly **/
#define CASE_2_ENCODED {0x07, 0x06, 0x01, 0x01, 0x01, 0x05, 0xb0, 0x00}
#define CASE_2_EXP_ADDRESS 1
#define CASE_2_EXP_CODE 1
#define CASE_2_EXP_DATA_0 1
#define CASE_2_EXP_LENGTH 5
	uint8_t temp2[] = CASE_1_ENCODED;
	Q.pushArray(&Q, temp2, sizeof(temp2)/sizeof(temp2[0]));
	coms_readPacketFromQ(&packet, &Q);
	TEST(6, packet.address == CASE_2_EXP_ADDRESS);
	TEST(7, packet.code == CASE_2_EXP_CODE);
	TEST(8, packet.length == CASE_2_EXP_LENGTH);
	TEST(9, packet.data[0] == CASE_2_EXP_DATA_0);
	TEST(10, Q.numel == 0);

	/** Incorrect length byte will result in a zeroed packet **/
#define CASE_3_ENCODED {0x07, 0x06, 0x01, 0x01, 0x01, 0x04, 0xb0, 0x00}
#define CASE_3_EXP_ADDRESS 0
#define CASE_3_EXP_CODE 0
#define CASE_3_EXP_DATA_0 0
#define CASE_3_EXP_LENGTH 0
	uint8_t temp3[] = CASE_3_ENCODED;
	Q.pushArray(&Q, temp3, sizeof(temp3)/sizeof(temp3[0]));
	uint8_t res = coms_readPacketFromQ(&packet, &Q);
	TEST(11, res == 0);
	TEST(12, packet.address == CASE_3_EXP_ADDRESS);
	TEST(13, packet.code == CASE_3_EXP_CODE);
	TEST(14, packet.length == CASE_3_EXP_LENGTH);
	TEST(15, Q.numel == 0);

	/** An incomplete packet will not be popped from the queue.
	 * 	Once the rest of the packet arrives in the queue it will be parsed
	 * 	**/
#define CASE_4_ENCODED_1 {0x06, 0x01, 0x01}
#define CASE_4_ENCODED_2 {0x01, 0x05, 0xb0, 0x00}
#define CASE_4_EXP_ADDRESS 1
#define CASE_4_EXP_CODE 1
#define CASE_4_EXP_DATA_0 1
#define CASE_4_EXP_LENGTH 5
	uint8_t temp4[] = CASE_4_ENCODED_1;
	Q.pushArray(&Q, temp4, sizeof(temp4)/sizeof(temp4[0]));
	uint8_t res4 = coms_readPacketFromQ(&packet, &Q);
	TEST(16, res4 == 0);
	uint8_t temp4a[] = CASE_4_ENCODED_2;
	Q.pushArray(&Q, temp4a, sizeof(temp4a)/sizeof(temp4a[0]));
	uint8_t res4a = coms_readPacketFromQ(&packet, &Q);
	TEST(17, res4a == 1);
	TEST(18, packet.address == CASE_4_EXP_ADDRESS);
	TEST(19, packet.code == CASE_4_EXP_CODE);
	TEST(20, packet.length == CASE_4_EXP_LENGTH);
	TEST(21, Q.numel == 0);

	/**
	 * ########################################################
	 * ################## coms_encodePacket() #################
	 * ########################################################
	 */
#define CASE_5_ADDRESS 1
#define CASE_5_CODE 1
#define CASE_5_DATA_0 1
#define CASE_5_LENGTH 1 + HEADER_SIZE
#define CASE_5_OPTION 0
#define CASE_5_EXP_TRANSMIT_DATA {0x06, 0x01, 0x01, 0x01, 0x05, 0xb0, 0x00}
	packet.address = 0;
	packet.code = 0;
	packet.data[0] = 0;
	packet.length = 0;
	memset(packet.transmitData, 0, 10);
	uint8_t temp5[] = {CASE_5_DATA_0};
	TEST(22, coms_encodePacket(&packet, CASE_5_ADDRESS, CASE_5_CODE, CASE_5_LENGTH, temp5, CASE_5_OPTION));
	uint8_t expectedTransmitData[] = CASE_5_EXP_TRANSMIT_DATA;
	TEST(23, (packet.transmitData[0] == expectedTransmitData[0])
			&& (packet.transmitData[1] == expectedTransmitData[1])
			&& (packet.transmitData[2] == expectedTransmitData[2])
			&& (packet.transmitData[3] == expectedTransmitData[3])
			&& (packet.transmitData[4] == expectedTransmitData[4])
			&& (packet.transmitData[5] == expectedTransmitData[5])
			&& (packet.transmitData[6] == expectedTransmitData[6]));
	return 0;
}
#endif
