/*
 * buttons.c
 *
 *  Created on: 17May,2019
 *      Author: p.phillips
 */
#include "Hardware/button.h"

uint8_t button_run(button_t *self);
void button_pressCallback(button_t *self);
void button_NOP(button_t *self);
uint8_t button_runNOP(button_t *self);
void button_setIndicatorModeNOP(button_t* self,indicatorMode_t mode);

uint8_t buttonCount = 0;


void button_setReleaseCallback( button_t *self,void (*Callback)( button_t *self))
{
	self->releaseCallback=Callback;
}

void button_setPressCallback( button_t *self,void (*Callback)( button_t *self))
{
	self->pressCallback=Callback;
}

//Set the callback for long hold press on button
void button_setLongPressCallback( button_t *self,void (*Callback)( button_t *self))
{
	self->longPressCallback=Callback;
}

//Set the callback for release after long press on button
void button_setLongReleaseCallback( button_t *self,void (*Callback)( button_t *self))
{
	self->longReleaseCallback=Callback;
}

//Set the callback for single tap press on button
void button_setSinglePressCallback( button_t *self,void (*Callback)( button_t *self))
{
	self->singlePressCallback=Callback;
}


//Set the callback for double tap press on button
void button_setDoublePressCallback( button_t *self,void (*Callback)( button_t *self))
{
	self->doublePressCallback=Callback;
}

//Set the callback for single release on button
void button_setSingleReleaseCallback( button_t *self,void (*Callback)( button_t *self))
{
	self->singleReleaseCallback=Callback;
}


//Set the callback for double tap release on button
void button_setDoubleReleaseCallback( button_t *self,void (*Callback)( button_t *self))
{
	self->doubleReleaseCallback=Callback;
}

void button_constructIndicator(button_t *self, uint16_t outputPin, GPIO_TypeDef* outputPort)
{
	self->isIndicator=1;

	  GPIO_InitTypeDef GPIO_InitStruct = {0};

	  self->indicatorTimer=0;

	  self->pIndicatorTimer = &self->indicatorTimer;

	  self->outputPort=outputPort;
	  self->outputPin=outputPin;

	  GPIO_InitStruct.Pin = self->outputPin;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	  GPIO_InitStruct.Alternate= 0;
	  HAL_GPIO_Init(self->outputPort, &GPIO_InitStruct);
}

void button_invertIndicator(button_t *self, inverted_t inverted) {
	self->invertIndicator = inverted;
}

void button_setIndicatorMode(button_t *self, indicatorMode_t mode)
{
	self->indicatorMode=mode;
}

uint8_t button_getStateGPIO(button_t *self) {
	return HAL_GPIO_ReadPin(self->inputPort,  self->inputPin);
}

uint8_t button_getStatePointer(button_t *self) {
	return *(self->statePointer);
}


/********button_run**********/
/*
 * Called at 1Khz
 *
 */
uint8_t button_run(button_t *self)
{
	uint8_t tempState=self->state;

	self->indicatorTimer++;

	self->state = self->getState(self);
	if (self->invertInput) {
		self->state = 1 - self->state;
	}

	if(self->state==0 && tempState==1 ) //Button Released
	{
		self->releaseTimer=0; //reset timer
		self->pressTimer=0;
		self->releaseCallback(self);
		self->eventQ.push(&self->eventQ, RELEASE);

		if (self->wasDoublePressed || self->wasLongPressed) {
			if (self->wasDoublePressed) {
				self->wasDoublePressed = 0;
				self->doubleReleaseCallback(self);
				self->eventQ.push(&self->eventQ, DOUBLE_RELEASE);
			}
			if (self->wasLongPressed) {
				self->longReleaseCallback(self);
				self->eventQ.push(&self->eventQ, LONG_RELEASE);
				self->wasLongPressed = 0;
			}
		} else {
			self->singleReleaseCallback(self);
			self->eventQ.push(&self->eventQ, SINGLE_RELEASE);
		}
	}

	if(self->releaseTimer == self->doublePressTime && self->pressTimer < self->longPressTime  )
	{
		self->singlePressCallback(self);
		self->eventQ.push(&self->eventQ, SINGLE_PRESS);
	}

	if(self->state==1 && tempState==0) //Button Pressed
	{
		 //reset timer

		self->pressCallback(self);
		self->eventQ.push(&self->eventQ, PRESS);
		if(self->releaseTimer<self->doublePressTime) //double press
		{
			self->doublePressCallback(self);
			self->eventQ.push(&self->eventQ, DOUBLE_PRESS);
			self->wasDoublePressed = 1;
		}
		else
		{
			self->pressTimer=0;
		}
	}

	if(self->state==1)
	{
		self->pressTimer++;
		//self->releaseTimer=0;
	}
	else
	{
		//self->pressTimer=0;
		self->releaseTimer++;
	}

	if(self->pressTimer>=self->longPressTime)
	{
		self->pressTimer=0;
		self->longPressCallback(self);
		self->eventQ.push(&self->eventQ, LONG_PRESS);
		self->wasLongPressed = 1;
	}

	// if button has indicator check which mode

    if(self->isIndicator==1)
		{
			switch(self->indicatorMode)
			{
			case OFF:
				if (self->invertIndicator == INVERTED)
					HAL_GPIO_WritePin(self->outputPort, self->outputPin, SET);
				else
					HAL_GPIO_WritePin(self->outputPort, self->outputPin, RESET);
				break;

			case ON:
				if (self->invertIndicator == INVERTED)
					HAL_GPIO_WritePin(self->outputPort, self->outputPin, RESET);
				else
					HAL_GPIO_WritePin(self->outputPort, self->outputPin, SET);
				break;
			case FLASH_FAST:
                if((*self->pIndicatorTimer)%100 == 0)				{
					HAL_GPIO_TogglePin(self->outputPort,  self->outputPin);
				}
				break;
			case FLASH_SLOW:
                if((*self->pIndicatorTimer)%500 == 0)				{
					HAL_GPIO_TogglePin(self->outputPort,  self->outputPin);
				}
				break;
			case LATCH:
				if(self->state==0 && tempState==1 )
				{
					HAL_GPIO_TogglePin(self->outputPort,  self->outputPin);
				}

				break;
			case MOMENTARY:
				if (self->invertIndicator == INVERTED)
					HAL_GPIO_WritePin(self->outputPort,  self->outputPin, !self->state);
				else
					HAL_GPIO_WritePin(self->outputPort,  self->outputPin, self->state);
				break;
			case MOMENTARY_FLASH_ON:
				if(self->state==1)
				{
                    if((*self->pIndicatorTimer)%200 == 0)					{
						HAL_GPIO_TogglePin(self->outputPort,  self->outputPin);
					}
				}
				else
				{
					if (self->invertIndicator == INVERTED)
						HAL_GPIO_WritePin(self->outputPort, self->outputPin, RESET);
					else
						HAL_GPIO_WritePin(self->outputPort, self->outputPin, SET);
				}
				break;

			case MOMENTARY_FLASH_OFF:
				if(self->state==1)
				{
                    if((*self->pIndicatorTimer)%200 == 0)					{
						HAL_GPIO_TogglePin(self->outputPort,  self->outputPin);
					}
				}
				else
				{
					if (self->invertIndicator == INVERTED)
						HAL_GPIO_WritePin(self->outputPort, self->outputPin, SET);
					else
						HAL_GPIO_WritePin(self->outputPort, self->outputPin, RESET);
				}
				break;
		}
	}

	return self->state;
}

//************************Default callbacks**************************/

void button_releaseCallback(button_t *self)
{
	return;
}

void button_pressCallback(button_t *self)
{
	return;
}

void button_longPressCallback(button_t *self)
{
	return;
}

void button_longReleaseCallback(button_t *self)
{
	return;
}

void button_singlePressCallback(button_t *self)
{
	return;
}

void button_doublePressCallback(button_t *self)
{
	return;
}

void button_singleReleaseCallback(button_t *self)
{
	return;
}

void button_doubleReleaseCallback(button_t *self)
{
	return;
}

void button_construct( button_t *self, uint16_t inputPin, GPIO_TypeDef* inputPort) {
	button_constructGPIO(self, inputPin, inputPort);
}

void button_constructGPIO( button_t *self, uint16_t inputPin, GPIO_TypeDef* inputPort)
{

	  self->Run=button_run;
	  self->pressCallback=button_pressCallback;
	  self->releaseCallback=button_releaseCallback;
	  self->longPressCallback=button_longPressCallback;
	  self->longReleaseCallback=button_longReleaseCallback;
	  self->singlePressCallback=button_singlePressCallback;
	  self->doublePressCallback=button_doublePressCallback;
	  self->singleReleaseCallback=button_singleReleaseCallback;
	  self->doubleReleaseCallback=button_doubleReleaseCallback;
	  self->setIndicatorMode=button_setIndicatorMode;
	  self->receiveEvent=button_receiveEvent;

	  self->pressTimer=0;
	  self->longPressTime=3000; //default to 3 seconds
	  self->releaseTimer=0;
	  self->doublePressTime=500;

	  self->wasDoublePressed = 0;
	  self->wasLongPressed = 0;

	  GPIO_InitTypeDef GPIO_InitStruct = {0};
	  self->inputPort=inputPort;
	  self->inputPin=inputPin;

	  GPIO_InitStruct.Pin = inputPin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	  GPIO_InitStruct.Alternate= 0;
	  HAL_GPIO_Init(inputPort, &GPIO_InitStruct);

	  self->getState = button_getStateGPIO;

	  self->isIndicator=0;
	  self->invertInput = 0;

	  circularQueueConstruct(&self->eventQ, EVENT_Q_SIZE);
	  self->id = buttonCount;
	  Buttons[buttonCount++] = self;
}

void button_constructPointer(button_t *self, uint8_t* statePointer, uint8_t invertInput) {
	self->Run=button_run;
	self->pressCallback=button_pressCallback;
	self->releaseCallback=button_releaseCallback;
	self->longPressCallback=button_longPressCallback;
	self->longReleaseCallback=button_longReleaseCallback;
	self->singlePressCallback=button_singlePressCallback;
	self->doublePressCallback=button_doublePressCallback;
	self->singleReleaseCallback=button_singleReleaseCallback;
	self->doubleReleaseCallback=button_doubleReleaseCallback;
	self->setIndicatorMode=button_setIndicatorMode;
	self->receiveEvent=button_receiveEvent;

	self->pressTimer=0;
	self->longPressTime=3000; //default to 3 seconds
	self->releaseTimer=0;
	self->doublePressTime=500;

	self->wasDoublePressed = 0;
	self->wasLongPressed = 0;

	self->statePointer = statePointer;
	self->getState = button_getStatePointer;

	self->isIndicator=0;
	self->invertInput = invertInput;

	circularQueueConstruct(&self->eventQ, EVENT_Q_SIZE);
	self->id = buttonCount;
	Buttons[buttonCount++] = self;
}

/**
 * @brief	Construct button for receiving BUTTON_DEMAND packet from other PCB
 */
void button_constructReceiver(button_t *self)
{
	self->Run=button_runNOP;
	self->pressCallback=button_pressCallback;
	self->releaseCallback=button_releaseCallback;
	self->longPressCallback=button_longPressCallback;
	self->longReleaseCallback=button_longReleaseCallback;
	self->singlePressCallback=button_singlePressCallback;
	self->doublePressCallback=button_doublePressCallback;
	self->singleReleaseCallback=button_singleReleaseCallback;
	self->doubleReleaseCallback=button_doubleReleaseCallback;
	self->receiveEvent=button_receiveEvent;
	self->id = buttonCount;
	Buttons[buttonCount++] = self;
}

void button_NOP(button_t *self)
{
	return;
}

uint8_t button_runNOP(button_t *self)
{
	return 0;
}

void button_setIndicatorModeNOP(button_t* self,indicatorMode_t mode)
{
	return;
}

void button_destroy(button_t *self)
{
	  self->Run=button_runNOP;
	  self->pressCallback=button_NOP;
	  self->releaseCallback=button_NOP;
	  self->longPressCallback=button_NOP;
	  self->longReleaseCallback=button_NOP;
	  self->singlePressCallback=button_NOP;
	  self->doublePressCallback=button_NOP;
	  self->singleReleaseCallback=button_NOP;
	  self->doubleReleaseCallback=button_NOP;
	  self->setIndicatorMode=button_setIndicatorModeNOP;
}

void button_receiveEvent(button_t* self, buttonEvent_t event) {
	switch(event){
		case RELEASE:
			self->releaseCallback(self);
			break;
		case PRESS:
			self->pressCallback(self);
			break;
		case DOUBLE_PRESS:
			self->doublePressCallback(self);
			break;
		case SINGLE_PRESS:
			self->singlePressCallback(self);
			break;
		case LONG_PRESS:
			self->longPressCallback(self);
			break;
		case LONG_RELEASE:
			self->longReleaseCallback(self);
			break;
		case DOUBLE_RELEASE:
			self->doubleReleaseCallback(self);
			break;
		case SINGLE_RELEASE:
			self->singleReleaseCallback(self);
			break;
	}
}


