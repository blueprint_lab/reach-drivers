/*
 * circularQueue.c.h
 *
 *  Created on: Unknown
 *      Author: SB
 */


#ifdef TEST_CIRC_Q
/***************** Test only *******************/
#include "../inc/DataStructures/circularQueue.h"
#else

#ifndef TEST_COMSPROCESS
/***************** build only ******************/
#include "DataStructures/circularQueue.h"

#else

/********* COMSPROCESS test only **************/
#include "../inc/DataStructures/circularQueue.h"

#endif
#endif

/***************** Common *********************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef TEST_CIRC_Q
#include <time.h>
/*
 * To run this test -
 * Open terminal, run:
 * > cd /to/this/directory
 * > gcc circularQueue.c -o testq -D TEST_CIRC_Q -std=c99
 * > ./testq.exe
 *
 * If gcc is not found, install on Windows with MinGW.
 */
#define TEST(num, eq) eq ? printf("TEST %d: PASS\n", num) : printf("TEST %d: FAIL\n", num)

uint8_t main() {
	circularQueue q;
	circularQueueConstruct(&q, 8);

	uint8_t a[8] = {1, 2, 3, 4, 5, 6, 7, 8};
	q.pushArray(&q, a, 8);
	TEST(1, q.numel == 8);

	uint8_t b = q.pop(&q);
	TEST(2, q.numel == 7);

	uint8_t buffer[8];
	q.popArray(&q, buffer, 7);
	TEST(3, (buffer[0] == 2)
			&& (buffer[1] == 3)
			&& (buffer[2] == 4)
			&& (buffer[3] == 5)
			&& (buffer[4] == 6)
			&& (buffer[5] == 7));
	TEST(4, q.numel == 0);

	uint8_t c[8] = {11, 12, 13, 14, 15, 16, 17, 18};
	q.pushArray(&q, c, 8);
	printf("buffer %d, %d, %d, %d, %d, %d, %d, %d\n",
				buffer[0], buffer[1], buffer[2], buffer[3],
				buffer[4], buffer[5], buffer[6], buffer[7]);
	TEST(5, q.numel == 8);

	q.popArray(&q, buffer, 8);
	printf("buffer %d, %d, %d, %d, %d, %d, %d, %d\n",
			buffer[0], buffer[1], buffer[2], buffer[3],
			buffer[4], buffer[5], buffer[6], buffer[7]);
	TEST(6, q.numel == 0);
	TEST(7, (buffer[0] == c[0]));
	TEST(8, (buffer[1] == c[1]));
	TEST(9, (buffer[2] == c[2]));
	TEST(10, (buffer[3] == c[3]));
	TEST(11, (buffer[4] == c[4]));
	TEST(12, (buffer[5] == c[5]));
	TEST(13, (buffer[6] == c[6]));
	TEST(14, (buffer[7] == c[7]));

	// Find()
	q.deconstruct(&q);
	circularQueueConstruct(&q, 8);
	q.push(&q, 1);
	q.push(&q, 2);
	q.push(&q, 3);
	q.push(&q, 4);
	// If no matching bytes return -1
	TEST(15, q.Find(&q, 0) == -1);
	// If matching byte, return index of first match
	TEST(16, q.Find(&q, 1) == 0);
	TEST(17, q.Find(&q, 2) == 1);
	q.push(&q, 5);
	q.push(&q, 6);
	q.push(&q, 7);
	q.push(&q, 8);
	TEST(21, q.Find(&q, 8) == 7);
	q.push(&q, 9);
	TEST(22, q.Find(&q, 9) == -1);
	q.pop(&q);
	TEST(23, q.Find(&q, 9) == -1);
	q.push(&q, 9);
	// Test matching byte when wrapping around buffer.
	TEST(18, q.Find(&q, 9) == 7);
	TEST(19, q.Find(&q, 8) == 6);
	q.pop(&q);
	q.pop(&q);
	q.pop(&q);
	q.push(&q, 10);
	q.push(&q, 11);
	TEST(20, q.Find(&q, 11) == 6);

	// Test that filling buffer to end, popping to end, pushing, popping from start gives correct results.
	q.Reset(&q);
	uint8_t d[8] = {101, 102, 103, 104, 105, 106, 107, 108};
	q.pushArray(&q, d, 8);
	TEST(30, q.back == q.front + q.maxSize);
	q.popArray(&q, buffer, 4);
	TEST(31, q.front == q.origin + 4);
	TEST(32, q.back == q.origin + q.maxSize);
	q.popArray(&q, buffer, 4);
	d[0] = 201;
	d[1] = 202;
	d[2] = 203;
	q.pushArray(&q, d, 3);
	TEST(33, q.Find(&q, 203) == 2);
	TEST(34, *(q.origin + 2) == 203);
	TEST(35, *(q.front + 2) == 203);
	TEST(36, q.numel == 3);

	// Check popArray does not duplicate bytes when wrapping around buffer
	q.Reset(&q);
	q.push(&q, 1);
	q.push(&q, 2);
	q.push(&q, 3);
	q.push(&q, 4);
	TEST(39, *q.origin == 1);
	q.pop(&q);
	q.pop(&q);
	q.pop(&q);
	q.pop(&q);
	TEST(40, q.back == q.origin + 4);
	TEST(41, q.front == q.origin + 4);
	uint8_t e[8] = {200, 201, 202, 203, 204, 205, 206, 207};
	q.pushArray(&q, e, 8);
	printf("q origin is %d %d %d %d %d %d %d %d\n", *q.origin, *(q.origin+1), *(q.origin+2), *(q.origin+3), *(q.origin+4), *(q.origin+5), *(q.origin+6), *(q.origin+7));
	TEST(42, *q.origin == 204);

	// Compare performance of functions
	clock_t start, end;
	double cpu_time_used;
	q.deconstruct(&q);
	uint16_t longBuffLen = 10000;
	circularQueueConstruct(&q, longBuffLen);
	uint8_t longBuff[longBuffLen];

	// Push Array against Push Array2
	start = clock();
	for (uint16_t i=0; i<60000; i++) {
		circularQueuePushArray2(&q, longBuff, longBuffLen);
		q.Reset(&q);
	}
	end = clock();
	printf("pushArray2 time for %d bytes: %fs\n", longBuffLen, ((double) (end - start)) / CLOCKS_PER_SEC);


	start = clock();
	for (uint16_t i=0; i<60000; i++) {
		circularQueuePushArray(&q, longBuff, longBuffLen);
		q.Reset(&q);
	}
	end = clock();
	printf("pushArray time for %d bytes: %fs\n", longBuffLen, ((double) (end - start)) / CLOCKS_PER_SEC);

	// Pop against PopArray
	uint16_t numRuns = 60000;
	uint8_t k;
	start = clock();
	for (uint16_t i=0; i<numRuns; i++) {
		circularQueuePushArray2(&q, longBuff, longBuffLen);
		for (uint16_t j=0; j<longBuffLen; j++) {
			k = q.pop(&q);
		}
	}
	end = clock();
	printf("pop time for %d bytes: %fs\n", longBuffLen, ((double) (end - start)) / CLOCKS_PER_SEC);

	uint8_t destBuff[longBuffLen];
	start = clock();
	for (uint16_t i=0; i<numRuns; i++) {
		circularQueuePushArray2(&q, longBuff, longBuffLen);
		q.popArray(&q, destBuff, longBuffLen);
	}
	end = clock();
	printf("popArray time for %d bytes: %fs\n", longBuffLen, ((double) (end - start)) / CLOCKS_PER_SEC);

	return 0;
}
#endif

void circularQueueConstruct(circularQueue * self, size_t size)
{
	self->maxSize = size;
	self->numel = 0;
	self->blockState = Q_UNBLOCKED;

	self->origin = (queueEl*)calloc(size, sizeof(queueEl));
	self->front = self->origin;
	self->back = self->origin;

	self->pop = circularQueuePop;
	self->push = circularQueuePush;
	self->pushArray = circularQueuePushArray2;
	self->popArray = circularQueuePopArray;
	self->deconstruct = circularQueueDeconstruct;
	self->Reset = circularQueueReset;
	self->Find = circularQueueFind;
}


void circularQueueDeconstruct(circularQueue * self) {
	free(self->origin);
	self->origin = NULL;
	self->back= NULL;
	self->front= NULL;

	self->numel = 0;
	self->maxSize = 0;
}

void circularQueueReset(circularQueue * self) {
	self->front = self->origin;
	self->back = self->origin;
	self->numel = 0;
}

queueEl* circularQueueGetNext(circularQueue * self, queueEl* pElem) {
	if ((pElem < (self->origin + self->maxSize))
			&& !(pElem < self->origin)) {
		return ++pElem;
	} else {
		return self->origin;
	}
}

int32_t circularQueueFind(circularQueue* self, queueEl target) {
	queueEl* source = self->front;
	for (uint16_t i=0; i<self->numel; i++) {
		if (*source++ == target) return i; // match found

		if (source >= self->origin + self->maxSize) {
			source = self->origin; // reached the back, search from origin
		}
	}
	return -1; // not found
}

uint8_t circularQueuePush(circularQueue * self, queueEl elem)
{
	if (self->numel >= self->maxSize) 
	{
		printf("circularQueuePush(). There has been an overflow!\n");
		return 1; // Overflow
	}
	else if (self->back >= self->origin + self->maxSize - 1) 
	{  // reached the end and must rap around
		*self->back = elem;
		self->back = self->origin;
		self->numel++;
	}
	else 
	{
		*self->back = elem;
		self->back = self->back + 1;
		self->numel++;
	}
	return 0;
}

queueEl circularQueuePop(circularQueue * self)
{
	if (self->numel <= 0 )
	{
		printf("circularQueuePop(). There has been an underflow!\n");
		return 0; //underflow
	}

	// remove and clear element at front
	queueEl element = *self->front;
	*self->front = 0; 
	self->numel--;


	if (self->front == self->origin + self->maxSize - 1)
	{ // wrap around
		self->front = self->origin;	
	}
	else
	{
		self->front++;
	}

	return element;
}

#define MIN(a, b) (a < b) ? a : b
uint16_t circularQueuePopArray(circularQueue * self, uint8_t* buffer, uint16_t numElements) {
//	sendQ->blockState = Q_BLOCKED;
	uint16_t totalBytesCopied;
	// calculate num of elements to copy from front to end of Q
	uint16_t numElFront;
	numElFront = MIN(self->numel, self->origin + self->maxSize - self->front);
	// Calculate how many elements to copy from the front
	uint16_t elToCopy;
	elToCopy = MIN(numElements, numElFront);
	memcpy(buffer, self->front, elToCopy); // copy bytes to output buffer
	memset(self->front, 0, elToCopy); // set copied bytes to zero

	self->numel -= elToCopy;
	totalBytesCopied = elToCopy;

	if (numElements > elToCopy) { // we've taken all of the bytes from the front
		// reset front to origin
		self->front = self->origin;
		// This call will copy all of the elements from the origin to the back
		totalBytesCopied += circularQueuePopArray(self, buffer + elToCopy, numElements - elToCopy);
	} else { //
		self->front += elToCopy;
	}
//	sendQ->blockState = Q_UNBLOCKED;
	if (self->front == self->origin + self->maxSize) {
		self->front = self->origin;
	}
	return totalBytesCopied;
}

uint16_t circularQueuePushArray2(circularQueue* self, queueEl* buffer, uint16_t numElementsToCopy)
{
	uint16_t totalBytesCopied;
	// check for overrun
	if ((numElementsToCopy + self->numel) > self->maxSize) {
		printf("circularQueuePushArray2(). There has been an overflow!. Numel: %d, Max numel: %d.\n", numElementsToCopy + self->numel, self->maxSize);
		return 0; // Overflow
	}
	// calculate num of elements to copy to back of Q
	uint16_t numElBack;
	numElBack = MIN(numElementsToCopy, self->origin + self->maxSize - self->back);
	memcpy(self->back, buffer,numElBack);
	self->numel += numElBack;
	totalBytesCopied = numElBack;

	if (numElementsToCopy > numElBack) { // we've pushed bytes to rear but there's still more
		// set back to origin
		self->back = self->origin;
		totalBytesCopied += circularQueuePushArray2(self, buffer+numElBack, numElementsToCopy - numElBack);
	} else { // we're done
		self->back += numElBack;
	}
	return totalBytesCopied;
}

void circularQueuePushArray(circularQueue * self, queueEl * data_in, unsigned int length)
{
    circularQueue* sendQ = self;
    sendQ->blockState = Q_BLOCKED;
    for (queueEl* pByte = data_in; pByte < data_in + length; ++pByte) {
    	if (sendQ->numel < sendQ->maxSize) {
    		sendQ->push(sendQ, *pByte);
    	}
    }
    sendQ->blockState = Q_UNBLOCKED;
}
