/*
 * backwardsCompatibility.c
 *
 *  Created on: Sep 11, 2020
 *      Author: Shaun Barlow
 */


#include "Hardware/backwardsCompatibility.h"

#define MAJOR 0
#define SUBMAJ 1
#define MINOR 2

/**
 * @brief	Check if electrical version matches
 * @param	pDeviceElectricalVersion: pointer to uint8_t[3] from deviceManager_t
 * @param	maj, submaj, min electrical revision to compare with
 * @retval	1: True, 0: False
 */
uint8_t compat_isElectricalVersion(uint8_t* pDeviceElectricalVersion, uint8_t maj, uint8_t submaj, uint8_t min) {
	if (*(pDeviceElectricalVersion + MAJOR) == maj
			&& *(pDeviceElectricalVersion + SUBMAJ) == submaj
			&& *(pDeviceElectricalVersion + MINOR) == min) {
		return 1;
	} else {
		return 0;
	}
}

/**
 * @brief	Init GPIO pin. Use for pins not setup by CubeMX
 */
void compat_GPIO_Init(GPIO_TypeDef * gpio_port, uint16_t gpio_pin, uint8_t pin_state) {
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	HAL_GPIO_WritePin(gpio_port, gpio_pin, pin_state);

	/* Configure GPIO pin */
	GPIO_InitStruct.Pin = gpio_pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(gpio_port, &GPIO_InitStruct);
}

/**
 * @brief	Init ADC2. Use for pins not setup by CubeMX
 */
#ifdef ADC2
void compat_ADC2_Init(ADC_HandleTypeDef * hadc2){
  ADC_ChannelConfTypeDef sConfig = {0};
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) */
  hadc2->Instance = ADC2;
  hadc2->Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc2->Init.Resolution = ADC_RESOLUTION_12B;
  hadc2->Init.ScanConvMode = ENABLE;
  hadc2->Init.ContinuousConvMode = ENABLE;
  hadc2->Init.DiscontinuousConvMode = DISABLE;
  hadc2->Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc2->Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc2->Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc2->Init.NbrOfConversion = 5;
  hadc2->Init.DMAContinuousRequests = ENABLE;
  hadc2->Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(hadc2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_11;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_13;
  sConfig.Rank = 2;
  if (HAL_ADC_ConfigChannel(hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = 3;
  sConfig.SamplingTime = ADC_SAMPLETIME_28CYCLES;
  if (HAL_ADC_ConfigChannel(hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = 4;
  if (HAL_ADC_ConfigChannel(hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_15;
  sConfig.Rank = 5;
  if (HAL_ADC_ConfigChannel(hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
}
#endif
