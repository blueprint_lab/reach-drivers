#include "Communication/linuxFifo.h"
#include "Communication/comsprocess.h"
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/ioctl.h>

uint8_t fifo_construct(fifo_comms_link_t* self, 
                        char* txFifoName, char* rxFifoName, 
                        uint txQLen, uint rxQLen) {
    if (strlen(txFifoName) > MAX_FIFO_NAME_LEN) {
        fprintf(stderr, "fifo_construct() txFifoName longer the MAX_FIFO_NAME_LEN. Got %ld. Expected %d or less.\n", 
                            strlen(txFifoName), MAX_FIFO_NAME_LEN);
        exit(1);
    }
    if (strlen(rxFifoName) > MAX_FIFO_NAME_LEN) {
        fprintf(stderr, "fifo_construct() rxFifoName longer the MAX_FIFO_NAME_LEN. Got %ld. Expected %d or less.\n", 
                            strlen(txFifoName), MAX_FIFO_NAME_LEN);
        exit(1);
    }

    memset(self->txFifoName, 0, sizeof(self->txFifoName));
    memset(self->rxFifoName, 0, sizeof(self->rxFifoName));
    strcpy(self->txFifoName, txFifoName);
    strcpy(self->rxFifoName, rxFifoName);
    // printf("fifo_construct() txFifoName %s rxFifoName %s\n", 
    //         self->txFifoName, self->rxFifoName);

    // Try to make the fifos in case they haven't yet been created.
    mkfifo(self->txFifoName, 0666); // RW permissions for all 
    mkfifo(self->rxFifoName, 0666); // RW permissions for all

    circularQueueConstruct(&self->txQ, txQLen);
    circularQueueConstruct(&self->rxQ, rxQLen);
}

uint8_t fifo_Read(coms_link* parent) {
    fifo_comms_link_t* self = parent->fifoLinuxLink;

    int bytesAvailable;
    int err = ioctl(self->rxFileDesc, FIONREAD, &bytesAvailable);
    // printf("fifo_Read bytesAvailable %d, err %d\n", bytesAvailable, err);

    if (err != 0) { 
        return 0; 
    }

    if (bytesAvailable > 0) {
        uint8_t rxBuff[bytesAvailable];
        read(self->rxFileDesc, rxBuff, bytesAvailable);
        // printf("fifo_Read read bytes:");
        // for (int i = 0; i < bytesAvailable; i++) {
        //     printf(" %d", rxBuff[i]);
        //     if (rxBuff[i] == 0) { printf("\n"); }
        // }
        // printf("\n");
        self->rxQ.pushArray(&self->rxQ, rxBuff, bytesAvailable);
    }
    return coms_readPacketFromQ(&parent->currPacket, &self->rxQ);
}

uint8_t fifo_Transmit(coms_link* parent, packet_t* packet) {
    fifo_comms_link_t* self = parent->fifoLinuxLink;

    // uint8_t txLen = self->txQ.numel;
    // uint8_t txBuff[txLen];
    // self->txQ.popArray(&self->txQ, packet->transmitData, packet->length);
    //printf("fifo_Transmit write bytes:");
//    for (int i = 0; i < packet->length; i++) {
//        printf(" %d", packet->transmitData[i]);
//    }
//    printf("\n");
    write(self->txFileDesc, packet->transmitData, packet->length);
    return 0;
}

uint8_t fifo_enableRead(coms_link* parent) {
    fifo_comms_link_t* self = parent->fifoLinuxLink;

    self->rxFileDesc = open(self->rxFifoName, O_RDONLY | O_NONBLOCK);
    printf("fifo_enableRead() waiting for txFifo to opened for reading...\n");
    self->txFileDesc = open(self->txFifoName, O_WRONLY);
    if (self->txFileDesc > 0) {
        printf("fifo_enableRead() txFifo opened\n");
    } else {
        fprintf(stderr, "fifo_enableRead() txFifo not opened\n");
    }
    
}
