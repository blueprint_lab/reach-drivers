/*
 * scheduler.c
 *
 * A simple scheduler.
 *
 *  Created on: 08-29-2022
 *      Author: Kyle McLean
 *
 *  Change Log:
 *  	- Added single-shot method - 25/09/23
 *  	- Re-factored execution code for clarity - 25/09/23
 */

#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "DataStructures/scheduler.h"

/* Private functions */
void scheduler_run(scheduler_t *self, uint32_t ticks_ms);
void scheduler_execute(task_t *task, uint32_t ticks_ms);
void scheduler_register_task(scheduler_t *self, void (*Execute)(void),
		uint32_t *pFrequency, uint32_t Period);
void scheduler_register_singleshot(scheduler_t *self, void (*Execute)(void), uint32_t Period);
void scheduler_unregister_task(scheduler_t *self, void (*Execute)(void));
void scheduler_unregister_singleshot(task_t *task);
bool scheduler_isTaskReady(uint32_t ticks_now, uint32_t ticks_prev,
		uint32_t period);

/*
 * @brief Construct the scheduler interface.
 *
 * @param[in] self : scheduler structure
 *
 * @return None
 */
void scheduler_construct(scheduler_t *self) {
	self->Run = scheduler_run;
	self->RegisterTask = scheduler_register_task;
	self->RegisterSingleshot = scheduler_register_singleshot;
	self->UnregisterTask = scheduler_unregister_task;
}

/*
 * @brief Run scheduler
 *
 * @param[in] self : scheduler structure
 * @param[in] ticks : current processor ticks [ms]
 *
 * @return None
 */
void scheduler_run(scheduler_t *self, uint32_t ticks_ms) {
	self->ticks = ticks_ms;

	for (uint8_t i = 0; i < MAX_TASKS; ++i) {
		if (self->tasks[i].Registered) {
			if (self->tasks[i].Execute == NULL) { continue; }

			if (self->tasks[i].Period_ms
					&& scheduler_isTaskReady(ticks_ms,
							self->tasks[i].LastExecuteTick,
							self->tasks[i].Period_ms)) {
				scheduler_execute(&self->tasks[i], ticks_ms);
				continue;
			}

			if (self->tasks[i].pFrequency == NULL) { continue; }
			if (*self->tasks[i].pFrequency
					&& scheduler_isTaskReady(ticks_ms,
							self->tasks[i].LastExecuteTick,
							(uint32_t) (1000 / (*self->tasks[i].pFrequency)))) {
				scheduler_execute(&self->tasks[i], ticks_ms);
				continue;
			}
		}
	}
}

/*
 * @brief Execute task helper
 *
 * @param[in] task : task structure
 * @param[in] ticks : current processor ticks [ms]
 *
 * @return None
 */
void scheduler_execute(task_t *task, uint32_t ticks_ms) {
	task->measuredFrequency = 1000.0f
			/ ((float) (ticks_ms) - (float) (task->LastExecuteTick));
	task->Execute();
	task->LastExecuteTick = ticks_ms;

	if (task->IsSingleShot) { scheduler_unregister_singleshot(task); }
}

/*
 * @brief Register scheduler task
 *
 * Note 1: If period is 0, then registered task will execute at the specified frequency.
 *
 * @param[in] self : scheduler structure
 * @param[in] Execute : task function pointer
 * @param[in] Period : task period [ms]
 *
 * @return None
 */
void scheduler_register_task(scheduler_t *self, void (*Execute)(void),
		uint32_t *pFrequency, uint32_t Period) {
	/* Find first un-registered task and register function pointer */
	for (uint8_t i = 0; i < MAX_TASKS; ++i) {
		if (!self->tasks[i].Registered) {
			self->tasks[i].Execute = Execute;
			self->tasks[i].LastExecuteTick = self->ticks;
			self->tasks[i].Period_ms = Period;
			self->tasks[i].pFrequency = pFrequency;
			self->tasks[i].Registered = 1;
			self->tasks[i].IsSingleShot = 0;

			break;
		}
	}
}

/*
 * @brief Register scheduler single shot task
 *
 * @param[in] self : scheduler structure
 * @param[in] Execute : task function pointer
 * @param[in] Period : task period [ms]
 *
 * @return None
 */
void scheduler_register_singleshot(scheduler_t *self, void (*Execute)(void), uint32_t Period) {
	/* Find first un-registered task and register function pointer */
	for (uint8_t i = 0; i < MAX_TASKS; ++i) {
		if (!self->tasks[i].Registered) {
			self->tasks[i].Execute = Execute;
			self->tasks[i].LastExecuteTick = self->ticks;
			self->tasks[i].Period_ms = Period;
			self->tasks[i].pFrequency = NULL;
			self->tasks[i].Registered = 1;
			self->tasks[i].IsSingleShot = 1;

			break;
		}
	}
}

/*
 * @brief Unregister all scheduler tasks with matching fp
 *
 * @param[in] self : scheduler structure
 * @param[in] Execute : task function pointer
 *
 * @return None
 */
void scheduler_unregister_task(scheduler_t *self, void (*Execute)(void)) {
	/* Unregister all tasks with matching function pointer */
	for (uint8_t i = 0; i < MAX_TASKS; ++i) {
		if (self->tasks[i].Execute == Execute) {
			self->tasks[i].Execute = NULL;
			self->tasks[i].LastExecuteTick = 0;
			self->tasks[i].Period_ms = 0;
			self->tasks[i].pFrequency = NULL;
			self->tasks[i].Registered = 0;
			self->tasks[i].IsSingleShot = 0;
		}
	}
}

/* @brief Unregister single scheduler task
*
* @param[in] task : task structure
*
* @return None
*/
void scheduler_unregister_singleshot(task_t *task) {
	task->Execute = NULL;
	task->LastExecuteTick = 0;
	task->Period_ms = 0;
	task->pFrequency = NULL;
	task->Registered = 0;
	task->IsSingleShot = 0;
}

/*
 * @brief scheduler_isTaskReady Check if task is ready to execute
 *
 * @param[in] ticks_now : scheduler structure
 * @param[in] ticks_prev : task function pointer
 * @param[in] ticks_now : scheduler structure
 *
 * @return bool : true if task is ready
 */
bool scheduler_isTaskReady(uint32_t ticks_now, uint32_t ticks_prev,
		uint32_t period) {
	return (ticks_now - ticks_prev >= period);
}

