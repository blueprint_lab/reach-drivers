/*H**********************************************************************
* FILENAME :        AMC25A8M.c             DESIGN REF: RS1
*
* DESCRIPTION :
*       Driver for Advanced Motion COntrol Brush pWM Servo Amplifier
*
* PUBLIC FUNCTIONS :
*
*
*
* NOTES :
*       These functions are a part of the FM suite;
*       See IMS FM0121 for detailed description.
*
*       Copyright TheBlueprintLabs
*
* AUTHOR :   Paul Phillips        START DATE :   16/8/16
*
* CHANGES :
*
* REF NO  VERSION DATE    WHO     DETAIL
*
*
*H*/
//#include "stm32f3xx_hal_tim.h"

//#include "stm32_device_select.h"
#include "Hardware/AMC25A8M.h"



/*
 * Constructor for the AMC25A28M motor driver abstraction.
 * requires two pins set up for pwm output and a GPIO enable pin.
 *
 *
 */
void AMC25A28M_Construct(AMC25A8M_t* self, GPIO_TypeDef* enablePort, uint16_t enablePin, TIM_HandleTypeDef pwm1Timer,
		uint32_t pwm1_channel, TIM_HandleTypeDef pwmRefTimer, uint32_t pwmRef_channel){

	self->Enable = AMC25A8M_Enable;
	self-> Disable = AMC25A8M_Disable;
	self->setOutput  = AMC25A8M_setOutput;

	self->enablePin = enablePin;
	self->enablePort = enablePort;

	self->PWM1Timer = pwm1Timer;
	self->PWM1Channel = pwm1_channel;
	self->PWM1Config.OCMode = TIM_OCMODE_PWM1;
	self->PWM1Config.Pulse = 500;
	self->PWM1Config.OCPolarity = TIM_OCPOLARITY_HIGH;
	self->PWM1Config.OCFastMode = TIM_OCFAST_DISABLE;

	self->PWMRefTimer = pwmRefTimer;
	self->PWMRefChannel = pwmRef_channel;
	self->PWMRefConfig.OCMode = TIM_OCMODE_PWM1;
	self->PWMRefConfig.Pulse = 500;
	self->PWMRefConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
	self->PWMRefConfig.OCFastMode = TIM_OCFAST_DISABLE;

}


void AMC25A8M_setOutput(AMC25A8M_t* self, float out){


	if( out > 1 ){
		out = 1;
	}
	if(out < -1){
		out = -1;
	}

	if(out != 0 ){
		float period1 = (float)self->PWM1Timer.Init.Period;
		float period2 = (float)self->PWMRefTimer.Init.Period;

		float out2 = 0.5 - out/2;
		float out1 = 0.5 + out/2;

		self->PWM1Config.Pulse = (uint32_t)(period1*out1);
		__HAL_TIM_SetCompare(&self->PWM1Timer, self->PWM1Channel, self->PWM1Config.Pulse );

		self->PWMRefConfig.Pulse = (uint32_t)(period2*out2);
		__HAL_TIM_SetCompare(&self->PWMRefTimer, self->PWMRefChannel, self->PWMRefConfig.Pulse );
	}
	else{
		__HAL_TIM_SetCompare(&self->PWM1Timer, self->PWM1Channel, 0);

		__HAL_TIM_SetCompare(&self->PWMRefTimer, self->PWMRefChannel, 0 );
	}

}



void AMC25A8M_Enable(AMC25A8M_t * self){

	self->PWM1Config.OCMode = TIM_OCMODE_PWM1;
	self->PWM1Config.Pulse = 500;
	self->PWM1Config.OCPolarity = TIM_OCPOLARITY_HIGH;
	self->PWM1Config.OCFastMode = TIM_OCFAST_DISABLE;
	HAL_GPIO_TogglePin(self->enablePort, self->enablePin);

	self->PWMRefConfig.OCMode = TIM_OCMODE_PWM1;
	self->PWMRefConfig.Pulse = 500;
	self->PWMRefConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
	self->PWMRefConfig.OCFastMode = TIM_OCFAST_DISABLE;

	if(HAL_TIM_PWM_Start_IT(&self->PWM1Timer, self->PWM1Channel) != HAL_OK){

	}
	if(HAL_TIM_PWM_Start_IT(&self->PWMRefTimer, self->PWMRefChannel) != HAL_OK){

	}

}

void AMC25A8M_Disable(AMC25A8M_t * AMC25A8Mcontext){
	HAL_GPIO_TogglePin(AMC25A8Mcontext->enablePort, AMC25A8Mcontext->enablePin);
}
