/*
 * analogThermistor.c
 *
 * DESCRIPTION :
 *       Driver for the analog thermistor used with DRV8353H
 *  Created on: 2023-05-30
 *      Author: YC
 */

#include "Hardware/analogueThermistor.h"



float AnalogueThermistor_Curve(AnalogueSensor_t* self, float input) {
	self->prev_value = input;

	float filtered_value = lpFilter(self->prev_value, input, 0.1);

	return -149.0*filtered_value + 133;
}
