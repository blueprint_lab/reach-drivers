/*H**********************************************************************
* FILENAME :        DRV8313_Brushless.c             DESIGN REF: RS1
*
* DESCRIPTION :
*       Driver for DRV8313 Brushless Motor Controller
*
* PUBLIC FUNCTIONS :
*
*
*
* NOTES :
*       These functions are a part of the FM suite;
*       See IMS FM0121 for detailed description.
*
*       Copyright TheBlueprintLabs
*
* AUTHOR :   Max Revay START DATE :   16/8/16
*
* CHANGES :
*
* REF NO  VERSION DATE    WHO     DETAIL
*
*
*H*/

/*
 * 	void (*Initialise)();
	void (*Enable)();
	void (*Disable)();
	int (*GetStatus)();
	void (*SetOutput)(float motorTorque);
	void (*SetDirection)(uint8_t motorDirection);
 */
#include "stm32_device_select.h"
#include <Hardware/DRV8313_Brushless.h>
#include "DataStructures/auxMath.h"


#define OUTPUT_RANGE 1.0

#define SQRT3 (sqrtf(3))
#define ONEONSQRT3 (1.0/sqrtf(3))
#define ONEON2SQRT3 (0.5/sqrtf(3))

#define SVM

void DRV8313_Brushless_Construct(DRV8313_Brushless_t *self,
		TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel, uint16_t enableA_pin, GPIO_TypeDef* enableA_port, TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel,uint16_t enableB_pin, GPIO_TypeDef* enableB_port,TIM_HandleTypeDef PWMCTimer, uint32_t PWMCChannel, uint16_t enableC_pin, GPIO_TypeDef* enableC_port,
		uint16_t resetPin, GPIO_TypeDef* resetPort, uint16_t sleepPin, GPIO_TypeDef* sleepPort, uint16_t faultPin, GPIO_TypeDef* faultPort){

	self->Enable 			= DRV8313_Brushless_Enable;
	self->Check				= DRV8313_Brushless_Check;
	self->Init 				= DRV8313_Brushless_Init;
	self->Disable 			= DRV8313_Brushless_Disable;
	self->SetOutputBrushed 	= DRV8313_SetOutput_Brushed;
	self->SetOutputFOC 		= DRV8313_SetOutput_FOC;
	self->Calibrate 		= DRV8313_FindZeroAngle;
	self->StallDrive 		= DRV8313_CalibrateEncoder;
	self->CalibrateTest 	= DRV8313_CalibrateTest;


	self->resetPin 			= resetPin;
	self->resetPort 		= resetPort;
	self->sleepPin 			= sleepPin;
	self->sleepPort 		= sleepPort;
	self->faultPin 			= faultPin;
	self->faultPort 		= faultPort;

	self->PWMATimer 		= PWMATimer;
	self->PWMBTimer 		= PWMBTimer;
	self->PWMCTimer 		= PWMCTimer;

	self->PWMAChannel 		= PWMAChannel;
	self->PWMBChannel 		= PWMBChannel;
	self->PWMCChannel 		= PWMCChannel;

	self->enableA_pin 		= enableA_pin;
	self->enableA_port 		= enableA_port;

	self->enableB_pin 		= enableB_pin;
	self->enableB_port 		= enableB_port;

	self->enableC_pin 		= enableC_pin;
	self->enableC_port 		= enableC_port;

	self->mode = DISABLED;
	self->status = GOOD;

	self->index = 0;

	self->positions[0]		= 0;
	self->positions[1]		= 0;

	self->offsetAngle 		= 0;

	self->numPolePairs 		= 7;
}

uint8_t DRV8313_Brushless_Check(DRV8313_Brushless_t *self)
{
	CLEAR_BIT(self->status, FAULT);

	if(!HAL_GPIO_ReadPin(self->faultPort, self->faultPin))
    {
    	SET_BIT(self->status,FAULT);
    	self->mode=DISABLED;

    	return 1;
    }

    return 0;
}


void DRV8313_Brushless_Init(DRV8313_Brushless_t *self)
{
	HAL_GPIO_WritePin(self->resetPort, self->resetPin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(self->sleepPort, self->sleepPin, GPIO_PIN_RESET);

	HAL_GPIO_WritePin(self->enableA_port, self->enableA_pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(self->enableB_port, self->enableB_pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(self->enableC_port, self->enableC_pin, GPIO_PIN_RESET);

	HAL_TIM_Base_Start_IT(&self->PWMATimer);
	HAL_TIM_Base_Start_IT(&self->PWMBTimer);
	HAL_TIM_Base_Start_IT(&self->PWMCTimer);

	HAL_TIM_PWM_Start_IT(&self->PWMATimer, self->PWMAChannel);
	HAL_TIM_PWM_Start_IT(&self->PWMBTimer, self->PWMBChannel);
	HAL_TIM_PWM_Start_IT(&self->PWMCTimer, self->PWMCChannel);
}

void DRV8313_Brushless_Enable(DRV8313_Brushless_t *self)
{
	self->mode=ENABLED;

	HAL_GPIO_WritePin(self->resetPort, self->resetPin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(self->sleepPort, self->sleepPin, GPIO_PIN_SET);

	HAL_GPIO_WritePin(self->enableA_port, self->enableA_pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(self->enableB_port, self->enableB_pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(self->enableC_port, self->enableC_pin, GPIO_PIN_SET);
}

void DRV8313_Brushless_Disable(DRV8313_Brushless_t *self)
{
	self->mode=DISABLED;
	HAL_GPIO_WritePin(self->enableA_port, self->enableA_pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(self->enableB_port, self->enableB_pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(self->enableC_port, self->enableC_pin, GPIO_PIN_RESET);

	HAL_GPIO_WritePin(self->resetPort, self->resetPin, GPIO_PIN_RESET);

	self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*0);
	self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*0);
	self->PWMCConfig.Pulse = (uint32_t)(self->PWMCTimer.Init.Period*0);

	__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
	__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );
	__HAL_TIM_SetCompare(&self->PWMCTimer, self->PWMCChannel, self->PWMCConfig.Pulse );
}

void DRV8313_SetOutput_Brushed(DRV8313_Brushless_t *self,float output)
{
	float outA=0;
	float outB=0;

	cap(&output,OUTPUT_RANGE,-OUTPUT_RANGE);

	if(self->mode==DISABLED)
	{
		self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*0);
		self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*0);
	}
	else
	{
		if(output>=0)
		{
			outA = fabs(output);
			outB = 0.0;
		}
	    if (output<0)
		{
			 outA = 0.0;
			 outB = fabs(output);
		}

		self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*outA);
		self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*outB);
		__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
		__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );
	}

}

void DRV8313_SetOutput_FOC(DRV8313_Brushless_t *self,float v_d, float v_q, float motorDirection)
{

	float theta = self->electricalAngle;

	float mag = sqrtf(v_d*v_d + v_q*v_q);
	if(mag>1.0){
		v_d /= mag;
		v_q /= mag;
	}


	if(self->mode==DISABLED)
	{
		self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*0);
		self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*0);
		self->PWMCConfig.Pulse = (uint32_t)(self->PWMCTimer.Init.Period*0);

		__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse);
		__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse);
		__HAL_TIM_SetCompare(&self->PWMCTimer, self->PWMCChannel, self->PWMCConfig.Pulse);
}
	else
	{
	    //inverse Park Transform
	    float outAlpha = cosf(theta)*v_d - sinf(theta)*v_q;
	    float outBeta = sinf(theta)*v_d + cosf(theta)*v_q;
	    outBeta *= motorDirection;
	    //inverse power-invariant Clarke Transform
//    	float outA = sqrtf(2/3) * outAlpha;
//    	float outB = -sqrtf(1/6) * outAlpha + sqrtf(1/2)*outBeta;
//    	float outC = -sqrtf(1/6) * outAlpha - sqrtf(1/2)*outBeta;

#ifdef SVM
	    //inverse power-variant Clarke Transform
	    float outA = ONEONSQRT3*outAlpha;
	    float outB = ONEON2SQRT3*(-outAlpha + SQRT3*outBeta);
	    float outC = ONEON2SQRT3*(-outAlpha - SQRT3*outBeta);
	    //neutral voltage shifting for full voltage sine waves
	    float v_neutral = 0.5*(fmaxf(fmaxf(outA,outB),outC)+fminf(fminf(outA,outB),outC));
	    outA -= v_neutral;
	    outB -= v_neutral;
	    outC -= v_neutral;
#else
	    //inverse power-variant Clarke Transform
	    float outA = 0.5*outAlpha;
	    float outB = 0.25*(-outAlpha + SQRT3*outBeta);
	    float outC = 0.25*(-outAlpha - SQRT3*outBeta);
#endif

	    outA += 0.5;
	    outB += 0.5;
	    outC += 0.5;

	    self->outputA = outA;
	    self->outputB = outB;
	    self->outputC = outC;

		self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*outA);
		self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*outB);
		self->PWMCConfig.Pulse = (uint32_t)(self->PWMCTimer.Init.Period*outC);

		__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse);
		__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse);
	    __HAL_TIM_SetCompare(&self->PWMCTimer, self->PWMCChannel, self->PWMCConfig.Pulse);

	}

}

void DRV8313_FindZeroAngle(DRV8313_Brushless_t *self,float amplitude,float mechanicalPosition){

	uint16_t overflow = 1280;
	self->index %= overflow;
	float position = (float)(self->index) * (2*M_PI/overflow);
	self->electricalAngle = sinf(position);

	if(self->index == (overflow>>2)){
		self->positions[0] = lpFilter(self->positions[0],mechanicalPosition,0.1);
	} else if(self->index == ((3*overflow)>>2)){
		self->positions[1] = lpFilter(self->positions[1],mechanicalPosition,0.1);
	}
	DRV8313_SetOutput_FOC(self,amplitude, 0, 1.0);

	self->offsetAngle = -atan2(sinf(self->positions[0])+sinf(self->positions[1]),cosf(self->positions[0])+cosf(self->positions[1]));
	self->index++;
	return;
}


void DRV8313_CalibrateEncoder(DRV8313_Brushless_t *self,float amplitude,float mechanicalPosition, float motorDirection){

	uint16_t overflow = 60000;
	if(self->index > overflow){
		self->index = 0;
	}
	float position = (float)(self->index) * (2*M_PI/overflow) * signf(amplitude);
	self->electricalAngle = position*self->numPolePairs;
	DRV8313_SetOutput_FOC(self,0,fabsf(amplitude),motorDirection);
	self->index++;
	return;

}

void DRV8313_CalibrateTest(DRV8313_Brushless_t *self,float amplitude,float motorDirection){
	DRV8313_SetOutput_FOC(self,amplitude,0,motorDirection);
	return;

}
