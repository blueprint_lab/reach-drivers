#include "Communication/canbusLinux.h"
// #include "run.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <errno.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#define FRAME_Q_LEN 100
#define PACKET_Q_LEN 32
#define CAN_DEVICE_NAME "can0"
#define MAX_ATTEMPTS 100

void canbusLinux_Construct(canbus_linux_link_t* self, uint16_t frameBuffLen, uint8_t* pDeviceID) {
	// Initialize the rxFrameQs (two queues are used to ensure one is always available)
	rxFrameQ_Construct(&self->rxFrameQ,     FRAME_Q_LEN);
    rxFrameQ_Construct(&self->rxFrameQ_2,   FRAME_Q_LEN);

    // Initialize the txFrameQ (two queues are used to ensure one is always available)
    txFrameQ_Construct(&self->txFrameQ,     FRAME_Q_LEN);
    txFrameQ_Construct(&self->txFrameQ_2,   FRAME_Q_LEN);

    // Initialize the rxQueue
    canbus_packetQ_Construct(&self->rxQueue, PACKET_Q_LEN);

    // Start with no pending frames
    self->pendingFrames = 0;
    self->pDeviceID = pDeviceID;
}

uint8_t canbusLinux_Init(canbus_linux_link_t* self) {
    int s;
    struct can_filter rfilter[3];
    
    // Open the CAN socket
    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("Error while opening CAN socket.\n");
        return 1;
    }

    // Pass forward allow allow packets
    rfilter[0].can_id   = (0x02 << 24);
    rfilter[0].can_mask = CAN_EFF_MASK & 0xFF000000; // Option byte filter 
    
    // Pass all packets for my Device ID
    rfilter[1].can_id   = ((*self->pDeviceID) << 16);
    rfilter[1].can_mask = CAN_EFF_MASK & 0x00FF0000; // Device ID byte filter
    
    // Only pass bus state packets if they're for me
    rfilter[2].can_id   = (0x95 << 8) | CAN_INV_FILTER;
    rfilter[2].can_mask = CAN_EFF_MASK & 0x0000FF00; // Packet ID byte filter 
    
    // Apply fiter options
    setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));
    
    // Add socket structure to canLink
    self->sock = s;

    struct ifreq ifr;
    strcpy(ifr.ifr_name, CAN_DEVICE_NAME);
    ioctl(s, SIOCGIFINDEX, &ifr);

    struct sockaddr_can addr;

    memset(&addr, 0, sizeof(addr));
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    // Bind the CAN socket
    if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        perror("Error binging CAN socket.\n");
        return 2;
    }
    return 0;
}

uint8_t canbusLinux_ReadFrame(canbus_linux_link_t* self, rxFrame_t* frameOut) {
    /*
    Non-blocking CAN read
    https://stackoverflow.com/questions/21135392/socketcan-continuous-reading-and-writing
    while(1) {
        FD_ZERO(&rdfs);
        FD_SET(s, &rdfs);

        tv.tv_sec = 0;
        tv.tv_usec = 10000; // microseconds

        rc = select(s + 1, &rdfs, NULL, NULL, &tv);

        // rc == 0 - timeout
        if (!rc) {
            // write your CAN frame
        }

        if (FD_ISSET(s, &rdfs)) {
            // read CAN frames
        }
    }
    */

    struct can_frame frame;
    int nbytes;
    
    nbytes = read(self->sock, &frame, sizeof(struct can_frame));

    if (nbytes < 0) {
        perror("Read");
        return 1;
    }

    // Print received frame 
    // printf("0x%03X [%d] ", frame.can_id, frame.can_dlc);
    // for (uint8_t i = 0; i < frame.can_dlc; i++) {
    //     printf("%02X ", frame.data[i]);
    // }
    // printf("\r\n");

    // Copy received frame into rxFrame_t
    if (frame.can_id & CAN_EFF_FLAG) { // extended id
        frameOut->header.ExtId = frame.can_id & CAN_EFF_MASK;
        frameOut->header.IDE = CAN_ID_EXT;
    } else {
        frameOut->header.StdId = frame.can_id & CAN_SFF_MASK;
        frameOut->header.IDE = CAN_ID_STD;
    }

    if (frame.can_id & CAN_RTR_FLAG) {
        frameOut->header.RTR = CAN_RTR_REMOTE;
    } else {
        frameOut->header.RTR = CAN_RTR_DATA;
    }

    frameOut->header.DLC = frame.can_dlc;
    
    memcpy(frameOut->data, frame.data, CAN_MAX_DLC);

    // Check for bus state frame
    if (canbus_checkForBusStateFrame(self->parent->pBusStateMgr, frameOut)) {
		return 0; // BUS_STATE frame already handled
	}

    // Append to an rxFrameQ
    if(!self->rxFrameQ.locked) {
		self->rxFrameQ.Push(&self->rxFrameQ, frameOut);
	} else if(!self->rxFrameQ_2.locked) {
		self->rxFrameQ_2.Push(&self->rxFrameQ_2, frameOut);
	} else {
		self->fifoError++;
	}
    return 0;    
}

uint8_t canbusLinux_Close(canbus_linux_link_t* self) {
    if (close(self->sock) < 0) {
        perror("Close");
        return 1;
    }
    return 0;
}

canbus_tx_state_t canbusLinux_TransmitFrame(coms_link* parent, txFrame_t* pFrame) {
    // If RX_ONLY, caller handles as if the canbus was busy
	if (parent->pBusStateMgr->state == bus_RX_ONLY) return CANBUS_TX_BUSY;
    
    // Get the canbus link from the parent
    canbus_linux_link_t * self = parent->canbusLinuxLink;

    // Flag that the bus is currently busy
	parent->pBusStateMgr->busy = 1;

    // Translate txFrame_t to can_frame
    struct can_frame frameOut;

    frameOut.can_id = pFrame->TxHeader.ExtId;
    if (pFrame->TxHeader.IDE == CAN_ID_EXT) {
        frameOut.can_id |= CAN_EFF_FLAG;
    }
    frameOut.can_dlc = pFrame->TxHeader.DLC;
    memcpy(frameOut.data, pFrame->data, BYTES_PER_FRAME * sizeof(uint8_t));

    canbus_tx_state_t canbus_tx_state = CANBUS_TX_OK;
    uint8_t attemptCounter = 0;
    // Attempt to write to the output buffer. After 1ms, fail.
    do { 
        attemptCounter++;

        if (write(parent->canbusLinuxLink->sock, &frameOut, sizeof(frameOut)) != sizeof(frameOut)) {
            if (errno != ENOBUFS || attemptCounter == MAX_ATTEMPTS) {
                perror("Write canbusLinux_TransmitFrame()");
                canbus_tx_state = CANBUS_TX_ERROR;
            }
        } else {
            break;
        }

        // Sleep for 10 us
        // Ref: ???
        usleep(10);
    } while (errno == ENOBUFS && attemptCounter != MAX_ATTEMPTS);
    
	// If there is still frames before RxOnly, decrement. Don't care if transmission was successful, this prevents getting stuck.
	if (parent->pBusStateMgr->state == bus_TX_QUEUED && self->pendingFrames > 0) {
		// Decrement the number of frames until we get back to the RX_only state
		self->pendingFrames--;
	}

	// If last frame to be transmitted, set back to RX_ONLY
	if(parent->pBusStateMgr->state == bus_TX_QUEUED && self->pendingFrames == 0) {
		parent->pBusStateMgr->busy = 0;  // Bus is not busy anymore
		parent->pBusStateMgr->state = bus_RX_ONLY;  // I'm back in RX_ONLY
	}

    return canbus_tx_state;
}

uint8_t canbusLinux_enableRead(coms_link* parent) {
    canbus_linux_link_t* self = parent->canbusLinuxLink;
    return canbusLinux_Init(self);
}
