/*
 * TMotor.c
 *
 *  Created on: 4 Oct. 2022
 *      Author: Jamie Jacobson & Kyle Mclean
 *
 * This module is a driver for the t-motor/CubeMars AK Dynamic Motor Series.
 *
 * Data-sheet: https://store.cubemars.com/images/file/20211201/1638329381542610.pdf
 *
 * The Driver supports CAN communications using 3rd-party protocols to send and recieve data. Communcation occurs via
 * standard CAN packets with 8-bit commands and 6-bit transmission value. The motor will always send a state packet
 * When a message is successfully acknowledged by the motor.
 *
 * The Module drives the actuator in MIT motor mode and allows a combination of the following control types:
 * 	1. Torque
 * 	2. Velocity
 * 	3. Position
 *
 * The driver also contains the following special functions:
 *
 * 1. Enter MIT Motor Mode
 * 2. Exit MIT Motor Mode
 * 3. Zero Position
 *
 * Control Demands can be sent through either of the following commands:
 *
 * 1. TMotor_SendMITControlDemand <- for Velocity, Positional and Torque Control
 * 2. TMotor_SendTorqueDemand <- For Torque only control
 *
 * Functions convert demands from floating point value to unsigned integer demands before transmission. You do not
 * need to calculate these values. Maximum and minimum limits are defined below.
 */

#include "Hardware/TMotor.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/* Private function prototypes */
TMotor_ErrorCode_t 			TMotor_SendMITControlDemand(TMotor_t* self, float position, float speed, float kp, float kd, float torque);
TMotor_ErrorCode_t 			TMotor_SendTorqueDemand(TMotor_t* self, float torque);
TMotor_ErrorCode_t 			TMotor_SetZeroPosition(TMotor_t* self);
TMotor_ErrorCode_t 			TMotor_EnterMotorMode(TMotor_t* self);
TMotor_ErrorCode_t 			TMotor_ExitMotorMode(TMotor_t* self);

void 						TMotor_DeviceHandler_AddDevice(TMotor_t* device);
static TMotor_ErrorCode_t 	TMotor_TransmitData(uint8_t id, uint8_t* command);
static uint8_t 				TMotor_parseFeedback(protocol3P_t* self, CAN_RxHeaderTypeDef* RxHeader, uint8_t* pData);
static float 				TMotor_uint_to_float(uint16_t val, float min_val, float max_val, uint16_t res);
static uint16_t 			TMotor_CalculateDemand(float val, float min_val, float max_val, uint16_t bits);

/* T-motor special commands */
uint8_t commandExitMotor[MOTOR_COMMAND_BUFFER_LENGTH] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFD};
uint8_t commandEnterMotor[MOTOR_COMMAND_BUFFER_LENGTH] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC};
uint8_t commandSetZeroPos[MOTOR_COMMAND_BUFFER_LENGTH] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE};

/* Private variables */
TMotor_DeviceHandler_t* 	_device_handler;  /* Single device handler object */

/**
 * @brief Create an AK motor device handler
 *
 * @param[in, out] self Pointer to the device handler
 * @param[in] id device CAN ID
 * @return None
 */
void TMotor_Device_Construct(TMotor_t* self, uint8_t id, float direction, float gearRatio, float offset)
{
	self->SetZeroPosition		= TMotor_SetZeroPosition;
	self->EnterMotorMode		= TMotor_EnterMotorMode;
	self->ExitMotorMode			= TMotor_ExitMotorMode;

	self->SendMITControlDemand 	= TMotor_SendMITControlDemand;
	self->SendTorqueDemand		= TMotor_SendTorqueDemand;

	/* Set the can ID */
	self->id = id;

	/* Set state and demand memory to zero */
	memset((void*)&self->demand, 0x00, sizeof(self->demand));
	memset((void*)&self->state, 0x00, sizeof(self->state));

	/* Add device to the device handler */
	TMotor_DeviceHandler_AddDevice(self);

	/* Motor parameters */
	self->param.Kt = 0.408;
	self->param.gearRatio = gearRatio;
	self->param.direction = direction;
	self->param.pos_offset = offset;

	/* Internal limits */
	self->limit.I_max = 1.5;
	self->limit.I_min = -1.5;

	self->limit.T_max = 1.5;
	self->limit.T_min = -1.5;

	/* Scaling parameters */
	self->scale.T_max = 15.0f;
	self->scale.T_min = -15.0f;

	self->scale.I_max = self->scale.T_max / self->param.gearRatio;
	self->scale.I_min = self->scale.T_min / self->param.gearRatio;

	self->scale.P_max = 12.5f;
	self->scale.P_min = -12.5f;

	self->scale.V_max = 45.0f;
	self->scale.V_min = -45.0f;

	self->scale.Kp_max = 500.0f;
	self->scale.Kp_min = 0.0f;

	self->scale.Kd_max = 5.0f;
	self->scale.Kd_min = 0.0f;

	/* Flag motor as initialized */
	self->is_initialised = true;
}

/**
 * @brief Create a t-motor device handler instance
 *
 * @param[in, out] self Pointer to t-motor device handler object
 * @param[in] pProtocolManager 3rd comm protocol manager for  can
 * @param[in] comms_link comm_link handler for CAN communication
 * @return None
 */
void TMotor_DeviceHandler_Construct(TMotor_DeviceHandler_t* self, protocolManager_t* pProtocolManager, canbus_comms_link* comms_link)
{
	self->AddDevice = TMotor_DeviceHandler_AddDevice;

	self->comms_link = comms_link;
	pProtocolManager->Add(pProtocolManager, &self->TMotorProtocol);
	self->TMotorProtocol.Read = TMotor_parseFeedback;

	_device_handler = self;

	/* Flag device handler as initialized */
	self->is_initialised = true;
}

void TMotor_DeviceHandler_AddDevice(TMotor_t* device)
{
	/* Check that the device handler is initialized */
	if (!_device_handler->is_initialised) { return; }

	if (_device_handler->device_count == MAX_AK_DEVICES) { return; }
	_device_handler->devices[_device_handler->device_count] = device;
	_device_handler->device_count++;
}

/* ********************* Motor Control ********************* */

/**
 * @brief sends an MIT demand to control the position, velocity,
 * 		  Kp, kd and torque state of the motor. Calculates demand values
 * 		  and packs data into a recognizable packet.
 *
 * @note Set parameter to zero if not using
 * @param[in, out] self Pointer to t-motor handler object
 * @param[in] position motor position in rad
 * @param[in] speed rad/s
 * @param kp Proportional Gain
 * @param kd Differential Gain
 * @param torque desired torque in N.m
 * @return TMotor_ERROR_OK if successful
 * @note: see TMotor.h for min and max values
 */
TMotor_ErrorCode_t TMotor_SendMITControlDemand(TMotor_t* self, float position, float speed, float kp, float kd, float torque)
{
	/* Check that the device is initialized */
	if (!self->is_initialised) { return TMotor_ERROR_NOT_INIT; }

	/* Limit output torque demand */
	(void)cap(&torque, self->limit.T_max, self->limit.T_min);

	/* Store requested demands */
	self->demand.positionDemand = position;
	self->demand.velocityDemand = speed;
	self->demand.torqueDemand 	= torque;
	self->demand.kpDemand 		= kp;
	self->demand.kdDemand 		= kd;

	/* Compute demands at motor output */
	torque 		= (torque /self->param.gearRatio) * self->param.direction;
	speed 		= speed * self->param.gearRatio * self->param.direction;
	position 	= (position - self->param.pos_offset) * self->param.gearRatio * self->param.direction;

	/* Calculate Current from torque demand */
	float current = torque / self->param.Kt;

	/* Limit current demand */
	(void)cap(&current, self->limit.I_max, self->limit.I_min);

	/* Calculate demand scaled value */
	uint16_t _positionDemand 	= TMotor_CalculateDemand(position, self->scale.P_min, self->scale.P_max, 16);
	uint16_t _velocityDemand 	= TMotor_CalculateDemand(speed, self->scale.V_min, self->scale.V_max, 12);
	uint16_t _currentDemand 	= TMotor_CalculateDemand(current, self->scale.I_min, self->scale.I_max, 12);
	uint16_t _kpDemand 			= TMotor_CalculateDemand(kp, self->scale.Kp_min, self->scale.Kp_max, 12);
	uint16_t _kdDemand 			= TMotor_CalculateDemand(kd, self->scale.Kd_min, self->scale.Kd_max, 12);

	/* Pack and send t-motor demand */
	uint8_t command[MOTOR_COMMAND_BUFFER_LENGTH];
	command[0] = 	(uint8_t)((_positionDemand >> 8) & 0xFF);
	command[1] = 	(uint8_t)(_positionDemand & 0xFF);
	command[2] = 	(uint8_t)((_velocityDemand >> 4) & 0xFF);
	command[3] = 	(uint8_t)(((_velocityDemand &0x0F) << 4));
	command[3] |= 	(uint8_t)((_kpDemand >> 8) & 0x0F);
	command[4] = 	(uint8_t)((_kpDemand) & 0xFF);
	command[5] = 	(uint8_t)((_kdDemand >> 4) & 0xFF);
	command[6] = 	(uint8_t)((_kdDemand &0x0F) << 4);
	command[6] |= 	(uint8_t)((_currentDemand >> 8) & 0x0F);
	command[7] = 	(uint8_t)(_currentDemand & 0xFF);

	return TMotor_TransmitData(self->id, command);
}

/**
 * @brief Calculate and send Torque command to motor
 *
 * @param[in, out] self Pointer to t-motor handler object
 * @param[in] torque Desired torque in N.m
 * @return TMotor_ERROR_OK if successful
 *
 * Torque Control is done by sending a 12-bit current value
 * to the motor in MIT mode.
 */
TMotor_ErrorCode_t TMotor_SendTorqueDemand(TMotor_t* self, float torque)
{
	/* Check that the device is initialized */
	if (!self->is_initialised) { return TMotor_ERROR_NOT_INIT; }

	/* Send torque via MIT demand */
	return TMotor_SendMITControlDemand(self, 0.0f, 0.0f, 0.0f, 0.0f, torque);
}

/**
 * @brief Set current encoder position as zero
 * @param[in, out] self Pointer to t-motor handler object
 * @return TMotor_ERROR_OK if successful
 */
TMotor_ErrorCode_t TMotor_SetZeroPosition(TMotor_t* self)
{
	/* Check that the device is initialized */
	if (!self->is_initialised) { return TMotor_ERROR_NOT_INIT; }

	return TMotor_TransmitData(self->id, commandSetZeroPos);
}

/**
 * @brief Enter MIT motor control mode
 * @param[in, out] self Pointer to t-motor handler object
 * @return TMotor_ERROR_OK if successful
 */
TMotor_ErrorCode_t TMotor_EnterMotorMode(TMotor_t* self)
{
	/* Check that the device is initialized */
	if (!self->is_initialised) { return TMotor_ERROR_NOT_INIT; }

	return TMotor_TransmitData(self->id, commandEnterMotor);
}

/**
 * @brief Exit motor mode (only if already in MIT control motor mode)
 * @param[in, out] self Pointer to t-motor handler object
 * @return TMotor_ERROR_OK if successful
 */
TMotor_ErrorCode_t TMotor_ExitMotorMode(TMotor_t* self)
{
	/* Check that the device is initialized */
	if (!self->is_initialised) { return TMotor_ERROR_NOT_INIT; }

	return TMotor_TransmitData(self->id, commandExitMotor);
}

/* ********************* Device Handler Communication ********************* */

static TMotor_ErrorCode_t TMotor_TransmitData(uint8_t id, uint8_t* command)
{
	TMotor_ErrorCode_t status = TMotor_ERROR_OK;

	/* Check that the device handler is initialized */
	if (!_device_handler->is_initialised) { return TMotor_ERROR_NOT_INIT; }

	/* Pack CAN bus RX header */
	_device_handler->canRxHeader.StdId 	= id;
	_device_handler->canRxHeader.IDE 	= CAN_ID_STD;
	_device_handler->canRxHeader.ExtId 	= 0x00;
	_device_handler->canRxHeader.DLC 	= MOTOR_COMMAND_BUFFER_LENGTH;

	/* Send data on CAN 3rd party protocol */
	_device_handler->TMotorProtocol.Send(&_device_handler->TMotorProtocol, &_device_handler->canRxHeader, command);

	/* Transmit pending CAN frames */
	if (canbus_thirdPartyProtocol_Transmit(_device_handler->comms_link) == 0) { status = TMotor_ERROR_CAN_TRANSMIT; }

	return status;
}

/**
 * @fn TMotor_parseFeedback
 * @brief read a CAN frame using 3rd party protocol and parse the data
 * @note overrides canbus_NOP_thirdPartyProtocol_Read
 * @param[in, out] self TMotor handler to store data
 * @param[in] RxHeader CAN header
 * @param pData[in] Rx Queue Frame data buffer
 * @return 1 if successful
 */
static uint8_t TMotor_parseFeedback(protocol3P_t* self, CAN_RxHeaderTypeDef* RxHeader, uint8_t* pData)
{
	uint16_t _position, _speed, _current;

	/* Check that the device handler is initialized */
	if (!_device_handler->is_initialised) { return 1; }

	/* Unpack message ID */
	uint8_t message_id = pData[0];

	/* Get device structure from CAN ID */
	TMotor_t* device;
	for (uint8_t device_idx = 0; device_idx < _device_handler->device_count; device_idx++)
	{
		if (_device_handler->devices[device_idx]->id == message_id)
		{
			device = _device_handler->devices[device_idx];
			break;
		}
	}

	/* Parse feedback */
	_position 	= (uint16_t) (pData[1] << 8) | (uint16_t)pData[2];
	_speed 		= ((uint16_t)(pData[3] << 8) | (uint16_t)(pData[4] & 0xF0)) >> 4;
	_current 	= (uint16_t) (pData[4] & 0x0F) << 8 | (uint16_t)(pData[5]);

	/* Convert data	and place in the correct device structure */
	device->state.position 		= ((device->param.direction * TMotor_uint_to_float(_position, device->scale.P_min, device->scale.P_max, 16)) / device->param.gearRatio) + device->param.pos_offset;
	device->state.velocity 		= (device->param.direction * TMotor_uint_to_float(_speed, device->scale.V_min, device->scale.V_max, 12)) / device->param.gearRatio;
	device->state.torque 		= (device->param.direction * device->param.Kt * TMotor_uint_to_float(_current, device->scale.I_min, device->scale.I_max, 12)) * device->param.gearRatio;
	device->state.motorStatus 	= (TMotor_MotorStatus_t)pData[7];

	/* TODO: Double check return types and usage */
	return 1;
}

/* ********************* Helpers ********************* */

/**
 * @fn uint_to_float
 * @brief convert an unsigned int value to a float bounded between a max and min
 * @param val value to be converted
 * @param min_val minimum possible value
 * @param max_val maximum possible value
 * @param res number of bits on the result
 * @return converted result
 * @note: Calculation:
 *  				  	   val x span
 * float value = min_val < ---------- + min_val < max_val
 *					   	    2^res - 1
 */
static float TMotor_uint_to_float(uint16_t val, float min_val, float max_val, uint16_t res)
{
	float span = max_val - min_val;
	float resolution = (float)(0b1 <<res);

	return ((float)val) * span / resolution + min_val;
}

/**
 * @fn  TMotor_CalculateDemand
 * @brief Converts a float to a bit-bounded, unsigned int demand
 * @param[in] val value to be converted
 * @param[in] min_val minimum boounded value
 * @param[in] max_val maximum bounded value
 * @param[in] bits Number of bits in the full scale resolution
 * @return unsigned 16 bit integer demand
 */
static uint16_t TMotor_CalculateDemand(float val, float min_val, float max_val, uint16_t bits)
{
	float span = max_val - min_val;

	if (val < min_val) { val = min_val; }
	if (val > max_val) { val = max_val; }

	float resolution = (float)(0b1 << bits);

	return (uint16_t)(((val - min_val)*resolution) /span );
}

