/*
 * canPacketQ.c
 *
 *  Created on: 12 Mar 2021
 *      Author: Kyle Mclean
 */

#ifdef TEST_CANPACKETQ
/***************** Test only *******************/
#include "../inc/Communication/canPacketQ.h"

#define BYTES_PER_FRAME 8
#define FRAMES_PER_PACKET 0xF
#define BYTES_PER_PACKET (BYTES_PER_FRAME * FRAMES_PER_PACKET)
#define HEADER_SIZE 4
#define UART_PACKET_HEADER_SIZE HEADER_SIZE
#include <stdio.h>

#else
/***************** Build only ******************/
#include "Communication/canPacketQ.h"
#endif
/***************** Common ***********************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define TIME2ERASE 12

/**
 * @brief
 * @param
 */
void canbus_packetQ_Construct(canPacketQ_t* self, size_t size) {
	self->Deconstruct 	 = canbus_packetQ_Deconstruct;
	self->Reset 		 = canbus_packetQ_Reset;
	self->Push 			 = canbus_packetQ_Push;
	self->EraseFrontNode = canbus_packetQ_EraseFrontNode;
	self->getNextNode    = canbus_packetQ_getNextNode;

	self->origin = (packet_node*)calloc(size, sizeof(packet_node));
	self->front = self->origin;
	self->back = self->origin;
	self->maxel = size;
	self->numel = 0;
}

/**
 * @brief
 * @param
 */
void canbus_packetQ_Deconstruct(canPacketQ_t* self) {
	free(self->origin);
	self->origin = NULL;
	self->back= NULL;
	self->front= NULL;

	self->numel = 0;
	self->maxel = 0;
}

/**
 * @brief
 * @param
 */
void canbus_packetQ_Reset(canPacketQ_t* self) {
	self->front = self->origin;
	self->back = self->origin;
	self->numel = 0;
}

/**
 * @brief
 * @param
 */
uint8_t canbus_packetQ_Push(canPacketQ_t* self, packet_node* elem) {
	if (self->numel > self->maxel) {
		printf("canbus_packetQ_Push(). There has been an overflow!\n");
		return 0; // Overflow
	} else if (self->numel == self->maxel) { // Overflow
		// Remove node from front of queue
		canbus_packetQ_EraseFrontNode(self);

		if (self->back >= (&self->origin[self->maxel-1])) {
			//printf("Max queue size reached and wrapping around!\n");

			// Elem is added to back of queue
			memcpy(self->back, elem, sizeof(packet_node));

			// Reached end of queue, back moved to origin and numel is incremented
			self->back = self->origin;
			self->numel++;

		} else {
			//printf("Max queue size reached!\n");

			// Elem is added to back of queue
			memcpy(self->back, elem, sizeof(packet_node));

			// Back moves forward by one and numel is incremented
			self->back++;
			self->numel++;
		}
	} else if (self->back == (&self->origin[self->maxel-1])) {  // reached the end and must rap around
		//printf("Adding packet to back of queue and wrapping!\n");
		memcpy(self->back, elem, sizeof(packet_node));
		self->back = self->origin;
		self->numel++;
	} else {
		//printf("Adding packet to back of queue!\n");
		memcpy(self->back, elem, sizeof(packet_node));
		self->back++;
		self->numel++;
	}

	// Success
	return 1;
}


/**
 * @brief
 * @param
 */
uint8_t canbus_packetQ_EraseFrontNode(canPacketQ_t* self) {
	if (self->numel == 0) {
		printf("canbus_packetQ_EraseFrontNode(). There has been an underflow!\n");
		return 0; //underflow
	}

	// Remove and clear node at front
	memset(self->front, 0, sizeof(packet_node));
	self->numel--;

	if (self->front >= &self->origin[self->maxel - 1]) { // wrap around
		self->front = self->origin;
	} else {
		self->front++;
	}

	return 1;  // Success
}

/**
 * @brief
 * @param
 */
packet_node* canbus_packetQ_getNextNode(canPacketQ_t* self, packet_node* pElem) {
	if ((pElem < (self->origin + self->maxel - 1)) && !(pElem < self->origin)) {
		return ++pElem;
	} else {
		return self->origin;
	}
}

/**
 * @brief	Populate elements in the supplied packet
 * @param	pointer to packet
 * @param	device ID for destination
 * @param	packet ID
 * @param	length is number of bytes in the pData array
 * @param	pointer to data for transmission
 * @param	option PACKET_OPT_x value from comslink.h
 */
int8_t canbus_PopulatePacket(packet_t* packet, uint8_t deviceID, uint16_t packetID,
		uint8_t length, uint8_t* pData, uint8_t option)
{
	packet->length = length-4;	// serial packets have length of 4 extra bytes
	packet->address = deviceID;
	packet->code = packetID;
	memcpy(&packet->data, pData, packet->length);
	packet->option = option;
	return 0;
}

/**
 * @brief 	Append the provided packet to the end of the queue
 * @param 	canPacketQ_t* pointer to queue
 * @param 	packet_t* pointer to packet to be appended
 * @retval 	pointer to packet_node added to queue
 */
packet_node* canbus_AddPacketToQueue(canPacketQ_t* q, packet_t* packet) {
	// Create temp packet node and populate
	packet_node tmp;
	memcpy(&tmp.packet, packet, sizeof(packet_t));
	tmp.timeToErase = TIME2ERASE;

	// Push temp packet node to back of queue and return the new node
	q->Push(q, &tmp);

	// if back == origin, return origin + size - 1
	if (q->back == q->origin) {
		return q->origin + q->maxel - 1;
	} else {
		return q->back - 1;
	}
}

/**
 * @brief	Iterate over packet_nodes and return pointer to first incomplete packet_node
 * 			with matching packet_id and device_id and empty data slot at frameIndex position.
 * @param	canPacketQ_t* pointer to queue to iterate over
 * @param	deviceID to match
 * @param	packetID to match
 * @param	frameIndexMask returned packet_node will have an empty data slot at the position
 * 			of the 1 bit in frameIndexMask
 * @param	totalFrames value from incoming frame. Packet returned will have matching number of frames.
 * @retval	packet_node* pointer to matching packet_node or NULL if none match
 */
packet_node* canbus_getPacketNodeWithDeviceIDPacketID(canPacketQ_t* q, canbus_headerData_t* pDecodedHeaderData, uint16_t frameIndexMask) {
	packet_node* current_node = q->front;
	packet_t* current_packet;

	uint16_t packetsToCheck = q->numel;

	// Iterate over packet_nodes in queue
	while (packetsToCheck) {
		// Get the current packet and decrement packetsToCheck
		current_packet = &current_node->packet;
		packetsToCheck--;

		// Test for matching packet id
		if (current_packet->code == pDecodedHeaderData->packetID
				&& current_packet->address == pDecodedHeaderData->deviceID	// test for matching device id
				&& (current_packet->receiveRegister & frameIndexMask) == 0) { 	// test for empty data slot
			return current_node;
		} else { 	// move to next node
			current_node = q->getNextNode(q, current_node);
		}
	}
	return NULL; 	// no matching packet_nodes, return NULL
}



#ifdef TEST_CANPACKETQ
/*
 * To run this test -
 * Open terminal, run:
 * > cd /to/this/directory
 * > gcc canPacketQ.c -o test -D TEST_CANPACKETQ
 * > ./test.exe
 *
 * If gcc is not found, install on Windows with MinGW.
 */
#define PACKETQ_SIZE 32

#define TEST(num, eq)eq ? printf("\tTEST %d: PASS\n", num) : printf("\tTEST %d: FAIL\n", num)
#define NEW_SET(num) printf("\n\nSET %d\n", num)

uint8_t main() {
	printf("TEST_CANPACKETQ\n\n");

	uint8_t result;
	canPacketQ_t q;
	canbus_packetQ_Construct(&q, PACKETQ_SIZE); // Construct packet queue with PACKETQ_SIZE packet nodes

	NEW_SET(0);
	TEST(000, q.maxel == PACKETQ_SIZE);
	TEST(001, q.numel == 0);
	TEST(003, q.front == q.back);
	TEST(004, q.front == q.origin);

	// Create packet and packet node
	packet_t packet;
	packet_node node;

	// Fill packet node
	node.packet = packet;   // An empty packet is used here
	node.next = NULL; 		// Note: next pointer not needed in this implementation. Remove
	node.timeToErase = 5; 	// Time to erase starts as 5

	// Push node to queue and check that numel is updated to 1
	result = q.Push(&q, &node);
	NEW_SET(1);
	TEST(001, result == 1);
	TEST(002, q.numel == 1);

	// Push node to queue and check that numel is updated to 2
	result = q.Push(&q, &node);
	NEW_SET(2);
	TEST(001, result == 1);
	TEST(002, q.numel == 2);
	TEST(003, q.front != q.back);

	// Remove element from front of the queue
	result = q.EraseFrontNode(&q);
	NEW_SET(3);
	TEST(001, result == 1);
	TEST(002, q.numel == 1);
	TEST(003, q.front != q.back);

	// Remove element from front of the queue
	result = q.EraseFrontNode(&q);
	NEW_SET(4);
	TEST(001, result == 1);
	TEST(002, q.numel == 0);
	TEST(003, q.front == q.back);

	// Push PACKETQ_SIZE - 1 packet nodes to queue
	NEW_SET(5);
	for(uint8_t i = 0; i < PACKETQ_SIZE-1; i++){
		result = q.Push(&q, &node);
	}
	TEST(001, result == 1);
	TEST(002, q.numel == PACKETQ_SIZE-1);
	//printf("q.numel = %d q.front idx = %d q.back idx = %d\t", q.numel, (q.front-q.origin) % sizeof(packet_node), (q.back-q.origin) % sizeof(packet_node));

	// Remove PACKETQ_SIZE - 1 packet nodes from front of the queue
	NEW_SET(6);
	for(uint8_t i = 0; i < PACKETQ_SIZE-1; i++){
		result = q.EraseFrontNode(&q);
	}

	TEST(001, result == 1);
	TEST(002, q.numel == 0);
	TEST(003, q.front == q.back);

	// Push PACKETQ_SIZE packet nodes to queue
	NEW_SET(7);
	for(uint8_t i = 0; i < PACKETQ_SIZE; i++){
		result = q.Push(&q, &node);
	}
	TEST(001, result == 1);
	TEST(002, q.numel == PACKETQ_SIZE);

	// Remove PACKETQ_SIZE packet nodes from front of the queue
	NEW_SET(8);
	for(uint8_t i = 0; i < PACKETQ_SIZE; i++){
		result = q.EraseFrontNode(&q);
	}

	TEST(001, result == 1);
	TEST(002, q.numel == 0);
	TEST(003, q.front == q.back);


	// Push PACKETQ_SIZE + 1 packets to queue
	NEW_SET(9);
	for(uint8_t i = 0; i < PACKETQ_SIZE + 1; i++){
		result = q.Push(&q, &node);
	}
	TEST(001, result == 1);
	TEST(002, q.numel == PACKETQ_SIZE);
	//printf("q.numel = %d\n", q.numel);

	// Remove PACKETQ_SIZE + 1 packet nodes from front of the queue
	NEW_SET(10);

	for(uint8_t i = 0; i < PACKETQ_SIZE + 1; i++){
		result = q.EraseFrontNode(&q);
	}

	TEST(001, result == 0);
	TEST(002, q.numel == 0);
	TEST(003, q.front == q.back);
	//printf("q.numel = %d q.front idx = %d q.back idx = %d\t", q.numel, (q.front-q.origin) % sizeof(packet_node), (q.back-q.origin) % sizeof(packet_node));

	// Push PACKETQ_SIZE * 2 packets to queue
	NEW_SET(11);
	for(uint8_t i = 0; i < PACKETQ_SIZE * 2; i++){
		result = q.Push(&q, &node);
	}
	TEST(001, result == 1);
	TEST(002, q.numel == PACKETQ_SIZE);

	// Remove PACKETQ_SIZE packet nodes from front of the queue
	NEW_SET(12);

	for(uint8_t i = 0; i < PACKETQ_SIZE; i++){
		result = q.EraseFrontNode(&q);
	}

	TEST(001, result == 1);
	TEST(002, q.numel == 0);
	TEST(003, q.front == q.back);

	// Push PACKETQ_SIZE packets to queue with corresponding time to erase
	NEW_SET(13);
	for(uint8_t i = 0; i < PACKETQ_SIZE; i++){
		node.timeToErase = (int16_t) i;
		result = q.Push(&q, &node);
	}
	TEST(001, result == 1);
	TEST(002, q.numel == PACKETQ_SIZE);

	// With the packets on the queue, starting at front get next packet and check timeToErase is correct
	{
		packet_node* pCurrentElem = q.front;
		uint8_t i;
		for(i = 0; i < PACKETQ_SIZE * 2; i++){
			printf("pCurrentElem->timeToErase = % d i = %d\t", pCurrentElem->timeToErase, i % (PACKETQ_SIZE));
			TEST(i + 3, pCurrentElem->timeToErase == i % (PACKETQ_SIZE));
			pCurrentElem = canbus_packetQ_getNextNode(&q, pCurrentElem);
		}

		// Remove packets off queue
		for(uint8_t j = 0; j < PACKETQ_SIZE; j++){
			result = q.EraseFrontNode(&q);
		}

		TEST(i + 4, result == 1);
		TEST(i + 5, q.numel == 0);
		TEST(i + 6, q.front == q.back);
	}

	// Push PACKETQ_SIZE packets to queue with corresponding time to erase. Check that overflow overwrites data correctly
	NEW_SET(14);
	for(uint8_t i = 0; i < PACKETQ_SIZE * 2; i++){
		node.timeToErase = (int16_t) i;
		result = q.Push(&q, &node);
	}
	TEST(001, result == 1);
	TEST(002, q.numel == PACKETQ_SIZE);


	// With the packets on the queue, starting at front get next packet and check timeToErase is correct
	{
		packet_node* pCurrentElem = q.front;
		uint8_t i;
		for(i = 0; i < PACKETQ_SIZE * 2; i++){
			//printf("pCurrentElem->timeToErase = % d i = %d\t", pCurrentElem->timeToErase, PACKETQ_SIZE + i % (PACKETQ_SIZE));
			TEST(i + 3, pCurrentElem->timeToErase == PACKETQ_SIZE + i % (PACKETQ_SIZE));
			pCurrentElem = canbus_packetQ_getNextNode(&q, pCurrentElem);
		}

		// Remove packets off queue
		for(uint8_t j = 0; j < PACKETQ_SIZE; j++){
			result = q.EraseFrontNode(&q);
		}

		TEST(i + 4, result == 1);
		TEST(i + 5, q.numel == 0);
		TEST(i + 6, q.front == q.back);
	}

	// Set back or other tests
	node.timeToErase = TIME2ERASE;

	// Use canbus_AddPacketToQueue to populate queue with packets, check all memebers have correct time to erase
	NEW_SET(15);
	for(uint8_t i = 0; i < PACKETQ_SIZE * 2; i++){
		canbus_AddPacketToQueue(&q, &packet);
	}
	TEST(002, q.numel == PACKETQ_SIZE);

	// Check that canbus_getPacketNodeWithDeviceIDPacketID returns null when no packets match
	{
		packet_node* pCurrentElem = q.front;
		uint8_t i;
		for(i = 0; i < PACKETQ_SIZE * 2; i++){
			//printf("pCurrentElem->timeToErase = % d i = %d\t", pCurrentElem->timeToErase, PACKETQ_SIZE + i % (PACKETQ_SIZE));
			TEST(i + 3, pCurrentElem->timeToErase == TIME2ERASE);
			pCurrentElem = canbus_packetQ_getNextNode(&q, pCurrentElem);
		}

		// Check bad packets, should not remove any packets from the queue and return NULL
		pCurrentElem =  canbus_getPacketNodeWithDeviceIDPacketID(&q, 0x10, 0x10, 0, 1);
		TEST(i + 4, pCurrentElem == NULL);
		TEST(i + 5, q.numel == PACKETQ_SIZE);

		// Remove packets off queue manulally
		for(uint8_t j = 0; j < PACKETQ_SIZE; j++){
			result = q.EraseFrontNode(&q);
		}

		TEST(i + 6, result == 1);
		TEST(i + 7, q.numel == 0);
		TEST(i + 8, q.front == q.back);
	}

	// Add good packets to queue and get back with canbus_getPacketNodeWithDeviceIDPacketID
	NEW_SET(16);

	// Fill in packet data
	packet.address = 0x10;  			// Device id
	packet.code = 0x10;     			// Packet id
	packet.receiveRegister = 0; 		// Used in receive queue to track which frames have been received
	packet.totalFrames = 1; 			// Total number of CAN frames that complete this packet

	// What needs to be set -> uint8_t deviceID, uint8_t packetID, uint16_t frameIndexMask, uint8_t totalFrames

	for(uint8_t i = 0; i < PACKETQ_SIZE * 2; i++){
		canbus_AddPacketToQueue(&q, &packet);
	}
	TEST(1, q.numel == PACKETQ_SIZE);


	// With the packets on the queue, starting at front get next packet and check timeToErase is correct
	{
		packet_t packetOut;
		packet_node* pCurrentElem = q.front;
		packet_node* pCurrentElemTmp = q.front;
		uint8_t i;
		for(i = 0; i < PACKETQ_SIZE * 2; i++){
			TEST(i + 2, pCurrentElem->timeToErase == TIME2ERASE);
			pCurrentElem = canbus_packetQ_getNextNode(&q, pCurrentElem);
		}

		// Check returned packet is first on queue
		pCurrentElem =  canbus_getPacketNodeWithDeviceIDPacketID(&q, 0x10, 0x10, 0, 1);
		TEST(i + 2, pCurrentElem == pCurrentElemTmp);
		TEST(i + 3, q.numel == PACKETQ_SIZE);

		// Using canbus_getFirstFullPacketFromQueue 5 times in a row, check that all packet nodes removed after 5 iterations
		result = canbus_getFirstFullPacketFromQueue(&q, &packetOut);
		result = canbus_getFirstFullPacketFromQueue(&q, &packetOut);
		result = canbus_getFirstFullPacketFromQueue(&q, &packetOut);
		result = canbus_getFirstFullPacketFromQueue(&q, &packetOut);
		result = canbus_getFirstFullPacketFromQueue(&q, &packetOut);
		result = canbus_getFirstFullPacketFromQueue(&q, &packetOut);
		TEST(i + 4, q.numel == 0);
		TEST(i + 5, q.front == q.back);
		printf("q.numel = %d", q.numel);

		// Add more packets to queue but with receive register set to 1
		packet.receiveRegister = 1;
		for(uint8_t j = 0; j < PACKETQ_SIZE * 2; j++){
			canbus_AddPacketToQueue(&q, &packet);
		}

		// Run canbus_getFirstFullPacketFromQueue until all packet nodes are removed from the queue
		uint8_t packetsOutCount = 0;
		for(uint8_t j = 0; j < PACKETQ_SIZE * 2; j++){
			result = canbus_getFirstFullPacketFromQueue(&q, &packetOut);
			if(result == 1) {
				packetsOutCount++;
			}
		}
		TEST(i + 5, packetsOutCount == PACKETQ_SIZE);
	}


	return 0;
}

#endif /* TEST_CANPACKETQ */





///**
// * @brief
// * @param
// */
//int32_t canbus_packetQ_Find(canPacketQ_t* self, packet_node target) {
//	packet_node* source = self->front;
//	for (uint16_t i=0; i<self->numel; i++) {
//		if ((*source++) == target) return i; // match found
//
//		if (source >= self->origin + self->maxel) {
//			source = self->origin; // reached the back, search from origin
//		}
//	}
//	return -1; // not found
//}
