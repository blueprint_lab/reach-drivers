/*
 * canbus.c
 *
 *  Created on: Jul 24, 2019
 *      Author: Shaun Barlow
 *
 *	Modified by Kyle McLean - April 2021
 *		- Implemented canbus state management
 *		- Added second txFrameQ and rxFrameQ to allow for ISR's on different priorities
 *		- canbus file is now common between stm32 and linux implimentations
 */

#ifdef TEST_CANBUS
/***************** Test only *******************/
#include "../inc/Communication/canbus.h"

#else
/***************** Build only ******************/
#include "Communication/canbus.h"
#include <stdio.h>
#endif

#define DEFAULT_BUS_TIMEOUT 20  // 200 US
#define ALL_DEVICES 0xFF

/* Common canbus variables */
uint8_t fifoError = 0;

/* Common canbus private function prototypes */
void canbus_busStateMgr(bus_state_mgr_t* self);
void canbus_busStateMgrSendBusState(bus_state_mgr_t* self, uint8_t deviceid, busState_t busState);
void canbus_busStateMgrNextDev(bus_state_mgr_t* self);
void canbus_parseFrame(canPacketQ_t* self, rxFrame_t* newFrame);
void canbus_decodeHeader(canbus_headerData_t* pdecodedHeaderData, uint32_t encodedHeader);
uint32_t canbus_encodeHeader(canbus_headerData_t* pHeaderData);

uint8_t canbus_getFirstFullPacketFromQueue(canPacketQ_t* q, packet_t* packetOut);
uint8_t canbus_PopulateFrame(txFrame_t* frame, canbus_headerData_t* pHeaderData,uint8_t* pData, uint8_t length);

busState_t canbus_handleBusState(coms_link* parent, busState_t new_state);
canbus_tx_state_t canbus_TransmitFrame(coms_link* parent, txFrame_t* pFrame);

/**
 * @brief	Constructor for canbus state manager
 * @param	coms_link parent
 * @param	busStateMgr self
 * @param	busState_t pBusState
 * @param	uint8_t pDeviceID
 * @param	uint8_t busStatePID
 * @param	uint8_t numdev
 * @retval 	returns zero
 */
uint8_t canbus_construct_busStateMgr(coms_link* parent, bus_state_mgr_t* self, busState_t* pBusState, uint8_t* pDeviceID, uint16_t busStatePID,  uint8_t numdev) {
	// Bind functions
	self->BusStateMgr = canbus_busStateMgr;
	self->HandleBusState = canbus_handleBusState;
	self->NextValidDevice = canbus_busStateMgrNextDev;
	self->SendBusState = canbus_busStateMgrSendBusState;
	// self->DetectNewDevices = canbus_detectDevices; // TODO: Automatically detect new devices

	// Instantiate instance variables
	self->busy = 0;
	self->timeoutCounter = 0;
	self->parent = parent;
	self->busStatePID = busStatePID;
	self->pDeviceID = pDeviceID;
	self->state = *pBusState;
	self->numdev = numdev;

	// Allocate memory for canbus devices and their timeouts.
	// Note: calloc implicitly sets memory to zero
	self->pCanbusDevices = (uint8_t*)calloc(self->numdev, sizeof(uint8_t));
	self->pDeviceTimeouts = (uint8_t*)calloc(self->numdev, sizeof(uint8_t));  // Must be the same size as CanbusDevices

	// Start at the first element in the array
	self->pCurrentDevice = self->pCanbusDevices;
	self->pCurrentTimeout = self->pDeviceTimeouts;
	return 0;
}

/**
 * @brief	Canbus state manager. Called every 10 us. If timeoutCounter elapses (by transmission finishing or timeout),
 * then the state manager will transition to the next valid device in the scheduled devices list.
 * @param	busStateMgr pointer to self
 */
void canbus_busStateMgr(bus_state_mgr_t* self) {
	// Check if the state is TX_AT_WILL. If so, don't do bus management
	if (self->state == bus_TX_AT_WILL) return;

    // Check for a timeout condition
    if (self->timeoutCounter == *self->pCurrentTimeout) {
        self->timeoutFlag = 1; // Bus has timed out. Flag is cleared externally when it is serviced
    }

    // Check bus manager state
    if (self->timeoutCounter == *self->pCurrentTimeout || self->txCplt) {
		// Reset the timeout counter and txCplt flag
		self->timeoutCounter = 0;
		self->txCplt = 0;

		// Find the next valid device in the array, if no devices -> TX_AT_WILL
		self->NextValidDevice(self);

		// Check if a valid device has been found, if not tell everyone to TX_AT_WILL
		if (*self->pCurrentDevice) {  // Send TX_QUEUED
			// Handled externally, update bus state of the current device
			self->state = bus_MASTER_CTRL;
			self->SendBusState(self, *self->pCurrentDevice, bus_TX_QUEUED);
			self->state = bus_RX_ONLY;

			if(*self->pCurrentDevice == *self->pDeviceID) {  // Handled internally
				self->HandleBusState(self->parent, bus_TX_QUEUED);
			}
		} else {  // Send TX_AT_WILL, this device is already in TX_AT_WILL
			self->SendBusState(self, ALL_DEVICES, bus_TX_AT_WILL);
		}
	} else if(self->busy) {
		self->timeoutCounter = 0; 	// Reset the timeout
		self->busy = 0;  			// Reset the busy flag
	} else {
		self->timeoutCounter++;
	}
}

/**
 * @brief	Send bus state to a specified device. Encodes a packet with the specified bus state.
 * 			The encoded packet is then transmitted via the parent transmit function.
 *
 * 			WARNING: Must not use parent->transmitPacket this will cause hardfault!
 * @param	busStateMgr self
 * @param	uint8_t deviceid
 * @param	busState_t busState
 */
void canbus_busStateMgrSendBusState(bus_state_mgr_t* self, uint8_t deviceid, busState_t busState) {
    packet_t transmitPacket;

    self->parent->encodePacket((packet_t*)&transmitPacket, deviceid, self->busStatePID, 4+sizeof(uint8_t), (uint8_t*)&busState, PACKET_OPT_FWD_DENY);
    self->parent->Transmit(self->parent, (packet_t*)&transmitPacket);
}

/**
 * @brief	Get the next valid device. If no valid devices, set the bus state to TX_AT_WILL.
 * @param	self pointer to the bus_state_mgr_t structure
 */
void canbus_busStateMgrNextDev(bus_state_mgr_t* self) {
	uint8_t* tmp = self->pCurrentDevice;
	do {
		if ((self->pCurrentDevice < (self->pCanbusDevices + self->numdev - 1))
				&& !(self->pCurrentDevice < self->pCanbusDevices)) {
			self->pCurrentDevice++;
			self->pCurrentTimeout++;
		} else {
			self->pCurrentDevice = self->pCanbusDevices;
			self->pCurrentTimeout = self->pDeviceTimeouts;
		}

		if (*self->pCurrentDevice != 0) return;
	} while(tmp != self->pCurrentDevice);

	// If no valid devices, go to TX_AT_WILL
	self->state = bus_TX_AT_WILL;
}

/**
 * @brief	Populate the self->currPacket with the next packet in the rxQueue
 * @param	pointer to canbus_comms_link
 * @retval	uint8_t 1: packet read, 0: no packet read
 */
uint8_t canbus_Read(coms_link* parent) {
    #ifdef HAL_CAN_MODULE_ENABLED
	canbus_comms_link* self = parent->canbusLink;
    #endif
    #ifdef LINUX
    canbus_linux_link_t* self = parent->canbusLinuxLink;
    #endif
	rxFrame_t newFrame;

	// Parse frames on rxFrameQ_2
	uint16_t numFrames = self->rxFrameQ_2.numel;
	while(numFrames--) {
		if(self->rxFrameQ_2.Pop(&self->rxFrameQ_2, &newFrame)) {
			canbus_parseFrame(&self->rxQueue, &newFrame);
        } else {
            break;
		}
	}

	// Parse frames on rxFrameQ
	numFrames = self->rxFrameQ.numel;
	while(numFrames--) {
		if(self->rxFrameQ.Pop(&self->rxFrameQ, &newFrame)) {
			canbus_parseFrame(&self->rxQueue, &newFrame);
        } else {
            break;
		}
	}

	canbus_getFirstFullPacketFromQueue(&self->rxQueue, &parent->currPacket);
	if (parent->currPacket.length > 0) {
		return 1;
	} else {
		return 0;
	}
}

/**
 * @brief	Iterate over queue. Pop and return the first fully received packet.
 * @param	canbus_packet_queue* pointer to queue
 * @param	pointer to packet_t
 */
uint8_t eraseError = 0;
uint8_t canbus_getFirstFullPacketFromQueue(canPacketQ_t* q, packet_t* packetOut) {
	packet_node* current_node = q->front; // Current node is the front of the queue
	packet_t* current_packet;

	while(current_node->timeToErase == 0 && q->numel > 0) {
		// Erase node at front of queue
		q->EraseFrontNode(q);

		// Current node is the new front of queue
		current_node = q->front;


		eraseError++;
	}

	// Packets to check is the number of packets in the queue
	uint16_t packetsToCheck = q->numel;

	// Iterate over packet_nodes in queue
	while (packetsToCheck > 0) {
		current_packet = &current_node->packet;			// Extract the current nodes packet
		
		// targetMask has a high bit set for each frame expected
		uint16_t targetMask = 0xFFFF >> (0x10 - current_packet->totalFrames);

		// If packet frames all received and timeToErase is not 0
		if (targetMask == current_packet->receiveRegister && current_node->timeToErase > 0) {
			// Copy packet into packetOut, set time to erase node to 0, and return
			memcpy(packetOut, current_packet, sizeof(packet_t));
			current_node->timeToErase = 0;


			eraseError--;
			return 1;
		} else {
			current_node = q->getNextNode(q, current_node); // move to next node
		}

		packetsToCheck--;								// Decrement the number of packet nodes to check
		if (current_node->timeToErase > 0) {
			current_node->timeToErase--; 					// Decrement node's time to erase (sort of iterations to erase?)
		}
	}

	// No matching packet_nodes, set length of packetOut to zero and return
	packetOut->length = 0;
	return 0;
}

/**
 * @brief	Parse the new canbus_frame_t frame.
 * @param	cabus packet queue structure
 * @param	new frame to parse
 */
void canbus_parseFrame(canPacketQ_t* self, rxFrame_t* newFrame) {
	// Standard ID Headers are not used for RS Protocol. Discard.
	if (newFrame->header.IDE == CAN_ID_STD) return;

	// Check if frame is flaged as protocol V3 (bit 27)
	if ((newFrame->header.ExtId >> 27) & 0x01) return;

	/* Extract frame length and decode the header ID */
	canbus_headerData_t decodedHeader;
	canbus_decodeHeader(&decodedHeader, newFrame->header.ExtId);
	uint8_t frameDataLength = newFrame->header.DLC;

	/* Create a mask to AND with packet's frame receive register */
	uint16_t frameIndexMask = 1 << (decodedHeader.frameNumber - 1);

	/* Get the packet_node matching this frame */
	packet_node* matchingPacketNode;
	matchingPacketNode = canbus_getPacketNodeWithDeviceIDPacketID(self, &decodedHeader, frameIndexMask);

	/* There is no existing packet for this frame. Make a new one */
    if (matchingPacketNode == NULL) {
		packet_t newPacket;
		memset(&newPacket, 0, sizeof(packet_t));

		newPacket.address = decodedHeader.deviceID;						// Get device ID from the header
		newPacket.code = decodedHeader.packetID;						// Get packet ID from the header
		newPacket.totalFrames = decodedHeader.totalFrames;  			// Get total frames from the header
		newPacket.option = decodedHeader.option;						// Get options from the header
		newPacket.receiveRegister = 0;									// Reset the recieve register
		newPacket.length = UART_PACKET_HEADER_SIZE; 					// Start packet length as the header size

		/* Add the new packet to the queue */
		matchingPacketNode = canbus_AddPacketToQueue(self, &newPacket);
    }

    /* Set the total frames if available */
    if (decodedHeader.totalFrames < DATA_BYTES_PER_PACKET/BYTES_PER_FRAME) {
    	matchingPacketNode->packet.totalFrames = decodedHeader.totalFrames;
    }

	/* Mark this frame as received in the packet node */
	matchingPacketNode->packet.receiveRegister |= frameIndexMask;

	/* Fill in the packet_node with data from this frame */
	uint8_t dataPosition = (decodedHeader.frameNumber - 1) * BYTES_PER_FRAME;

	/* If dataPosition is valid */
	if (dataPosition < DATA_BYTES_PER_PACKET) {
		uint8_t* dataDest = &matchingPacketNode->packet.data[dataPosition];

		/* Copy data into the packet */
		memcpy(dataDest, newFrame->data, frameDataLength);

		/* Increment the frame data length by the current frame data length */
		matchingPacketNode->packet.length += frameDataLength;
	}
}

/**
 * @brief	Populates the canbus_frame_t frame with the supplied parameters
 * @param   Header data to encode as extended ID
 * @param	pData pointer to the data array containing payload of the Tx frame. 8 bytes or less.
 * @param	length: number of bytes in pData <= 8.
 * @retval	zero
 */
uint8_t canbus_PopulateFrame(txFrame_t* frame, canbus_headerData_t* pHeaderData ,uint8_t* pData, uint8_t length) {
	uint32_t encodedHeader = canbus_encodeHeader(pHeaderData);

	frame->TxHeader.DLC = length;
	frame->TxHeader.ExtId = encodedHeader;
	frame->TxHeader.IDE = CAN_ID_EXT;
	frame->TxHeader.RTR = CAN_RTR_DATA;

	memcpy(frame->data, pData, length);

	return (encodedHeader == 0) ? 1 : 0;
}


/**
 * @brief	Encode canbus header. Supports V1 and V2 headers.
 * @param	Decoded header struct
 * @retval  uint32_t encoded header
 */
uint32_t canbus_encodeHeader(canbus_headerData_t* pHeaderData) {
	uint32_t encoded_header = 0;

	uint8_t useProcotolV2 = (pHeaderData->option >> 2) & 0x01;

	if (useProcotolV2) {
		if (pHeaderData->frameNumber == 1) {
			encoded_header 	|= (uint32_t) 1 << 3; 									// Set total frames flag 
			encoded_header 	|= (uint32_t) (pHeaderData->totalFrames & 0x07) << 0;  	// Set total frames bits
		} else {
			encoded_header 	|= (uint32_t) (pHeaderData->frameNumber & 0x07) << 0;  	// Set number frames bits
		}
		
		encoded_header 		|= (uint32_t) (pHeaderData->packetID & 0x07FF)	<< 4;
		encoded_header 		|= (uint32_t) (pHeaderData->deviceID & 0xFF)	<< 16;
		encoded_header 		|= (uint32_t) (pHeaderData->option & 0x1F) 	  	<< 24;
	} else {
		if (pHeaderData->packetID > 0xFF) return 0; 								// Packet is invalid

		encoded_header 		|= (uint32_t) (pHeaderData->totalFrames & 0xF) 	<< 0;
		encoded_header 		|= (uint32_t) (pHeaderData->frameNumber & 0xF) 	<< 4;
		encoded_header 		|= (uint32_t) pHeaderData->packetID 			<< 8;
		encoded_header 		|= (uint32_t) pHeaderData->deviceID 			<< 16;
		encoded_header 		|= (uint32_t) (pHeaderData->option & 0x1F) 	  	<< 24;
	}

	return encoded_header;
}

/**
 * @brief	Decode canbus header. Supports V1 and V2 headers.
 * @param	uint32_t encoded header
 * @retval  Decoded header struct
 */
void canbus_decodeHeader(canbus_headerData_t* pdecodedHeaderData, uint32_t encodedHeader) {
	uint8_t option = (encodedHeader >> 24) & 0x1F;
	uint8_t useProcotolV2 = (option >> 2) & 0x01;
	
	if (useProcotolV2) {
		uint8_t totalFramesFlag = (uint8_t) (encodedHeader >> 3) & 0x01;
		
		if (totalFramesFlag) {
			pdecodedHeaderData->totalFrames = (encodedHeader >> 0) & 0x07;
			pdecodedHeaderData->frameNumber = 1; // First frame implied
		} else {
			pdecodedHeaderData->totalFrames = DATA_BYTES_PER_PACKET/BYTES_PER_FRAME;
			pdecodedHeaderData->frameNumber = (encodedHeader >> 0) & 0x07;
		}
		
		pdecodedHeaderData->packetID 		= (encodedHeader >> 4) & 0x7FF;
		pdecodedHeaderData->deviceID 		= (encodedHeader >> 16) & 0xFF;
		pdecodedHeaderData->option 			= option;
	} else {
		pdecodedHeaderData->totalFrames 	= (encodedHeader >> 0) & 0x0F;
		pdecodedHeaderData->frameNumber 	= (encodedHeader >> 4) & 0x0F;
		pdecodedHeaderData->packetID 		= (encodedHeader >> 8) & 0xFF;
		pdecodedHeaderData->deviceID 		= (encodedHeader >> 16) & 0xFF;
		pdecodedHeaderData->option 			= option;
	}
}

/**
 * @brief	Convert supplied packet to frames and attempt to transmit all frames.
 * 			Where no empty tx mailbox is available, frame is added to txQueue and
 * 			sent when the mailbox empty interrupt fires.
 * @param	coms_link* parent
 * @param	packet_t* packet
 * @retval	total number of frames indicating transmitted and/or added to tx queue.
 */
uint8_t canbus_Transmit(coms_link* parent, packet_t* packet) {
	#ifdef HAL_CAN_MODULE_ENABLED
	canbus_comms_link* self = parent->canbusLink;
	#endif
	#ifdef LINUX
	canbus_linux_link_t* self = parent->canbusLinuxLink;
	#endif
	uint8_t totalFrames;

	// Calculate number of frames required
	totalFrames = packet->length / BYTES_PER_FRAME; // Number of complete frames
	if (packet->length % BYTES_PER_FRAME > 0) {
		totalFrames++; // Add 1 if there is a partial frame
	}

	for (uint8_t frameIndex = 0; frameIndex < totalFrames; frameIndex++) {
		txFrame_t frame;
		canbus_headerData_t headerData;
		uint8_t dataLength = BYTES_PER_FRAME;

		// if there are less than eight bytes to send, calculate the number of bytes
		if (packet->length - (frameIndex * BYTES_PER_FRAME) < BYTES_PER_FRAME) {
			dataLength = packet->length - (frameIndex * BYTES_PER_FRAME);
		}

		// Fill in header data and construct frame
		headerData.option = packet->option;
		headerData.deviceID = packet->address;
		headerData.packetID = packet->code;
		headerData.totalFrames = totalFrames;
		headerData.frameNumber = frameIndex + 1;

		if (canbus_PopulateFrame(&frame, &headerData, packet->data + (frameIndex * BYTES_PER_FRAME), dataLength)) return 0;

		// Attempt to transmit the frame
		#ifdef HAL_CAN_MODULE_ENABLED
		if (canbus_TransmitFrame(parent, &frame) != CANBUS_TX_OK) {
		#endif
		#ifdef LINUX
		if (canbusLinux_TransmitFrame(parent, &frame) != CANBUS_TX_OK) {
		#endif

			// If the packet was not transmitted, add it a queue
			if(!self->txFrameQ.locked && self->txFrameQ.numel != self->txFrameQ.maxel) {
				self->txFrameQ.Push(&self->txFrameQ, &frame);
			} else if(!self->txFrameQ_2.locked && self->txFrameQ_2.numel != self->txFrameQ_2.maxel) {
				self->txFrameQ_2.Push(&self->txFrameQ_2, &frame);
			} else {
				fifoError++;  // Frame has been dropped!
			}
		}
	}

	// returns total number of frames indicating all have been transmitted
	return totalFrames;
}


/**
 * @brief	Acts on the new bus state.
 * @param	coms_link* parent
 * @param	busState_t new_state
 * @retval
 */
busState_t canbus_handleBusState(coms_link* parent, busState_t new_state) {
#ifdef HAL_CAN_MODULE_ENABLED
	canbus_comms_link* self = parent->canbusLink;  // Get the canbus link ptr
#endif
#ifdef LINUX
	canbus_linux_link_t* self = parent->canbusLinuxLink;  // Get the canbus link ptr
#endif
	bus_state_mgr_t* pBusStateMgr = parent->pBusStateMgr; 				// Get the bus state manager

	if(new_state == pBusStateMgr->state) return pBusStateMgr->state;  	// Only handle changing busStates

	switch(new_state) {
		case bus_RX_ONLY:
			// Set number of frames to send and the new state
			self->pendingFrames = 0;
			pBusStateMgr->state = new_state;
			break;
		case bus_TX_QUEUED:
			{
				txFrameQ_t* freeTxQ;

				pBusStateMgr->scheduleCount++;
				if (pBusStateMgr->scheduleCount == 0xFF) {
					pBusStateMgr->scheduleCount = 0;
				}

				// If both queues at maxel, erase the front node of first unlocked queue
				if (self->txFrameQ.numel == self->txFrameQ.maxel && self->txFrameQ_2.numel == self->txFrameQ_2.maxel) {
					if(!self->txFrameQ.locked) {
						self->txFrameQ.Erase(&self->txFrameQ);
						freeTxQ = &self->txFrameQ;
					} else if(!self->txFrameQ_2.locked) {
						self->txFrameQ_2.Erase(&self->txFrameQ_2);
						freeTxQ = &self->txFrameQ_2;
					} else {
						freeTxQ = &self->txFrameQ; // Set to an arbitrary queue, we will probably lose this frame
						fifoError++;
					}
				} else { // Get the free queue
					if(!self->txFrameQ.locked) {
						freeTxQ = &self->txFrameQ;
					} else if(!self->txFrameQ_2.locked) {
						freeTxQ = &self->txFrameQ_2;
					} else {
						freeTxQ = &self->txFrameQ; // Set to an arbitrary queue, we will probably lose this frame
						fifoError++;
					}
				}

				// Encode BUS_STATE: bus_RX_ONLY -> this will be appended to the end of one of the txQueues via canbus_Transmit()
				pBusStateMgr->SendBusState(pBusStateMgr, *pBusStateMgr->pDeviceID, pBusStateMgr->state);

				// Set number of frames to send. Note: we might not be able to access all of these frames rn.
				if (!self->txFrameQ.locked && !self->txFrameQ_2.locked) {
					self->pendingFrames = self->txFrameQ.numel + self->txFrameQ_2.numel; // We can send all frames now!
				} else if (!self->txFrameQ.locked) {
					self->pendingFrames = self->txFrameQ.numel;
				} else if (!self->txFrameQ_2.locked) {
					self->pendingFrames = self->txFrameQ_2.numel;
				} else {
					self->pendingFrames = 0;
				}

				// Set the new busState
				pBusStateMgr->state = new_state;

				// If there are pending frames, send the first on the queue
				txFrame_t frame;
				if (self->pendingFrames) {
#ifdef HAL_CAN_MODULE_ENABLED
					if (freeTxQ->PopPreview(freeTxQ, (txFrame_t*)&frame)) {
						// Attempt to transmit the first frame on the free queue 
						if (canbus_TransmitFrame(parent, (txFrame_t*)&frame) == CANBUS_TX_OK) {
							// Only remove frame from the queue if the transmit is successful
							freeTxQ->Erase(freeTxQ);
						}
					}
#endif					
#ifdef LINUX
					// Transmit all other frames if the system is running linux
					while(self->pendingFrames > 0) {
						if (self->txFrameQ.numel) {
							if (self->txFrameQ.PopPreview(&self->txFrameQ, (txFrame_t*)&frame)) {
								// Attempt to transmit
								if (canbusLinux_TransmitFrame(parent, (txFrame_t*)&frame) == CANBUS_TX_OK) {
									// Only remove frame from the queue if the transmit is successful
									self->txFrameQ.Erase(&self->txFrameQ);
								}
							}
						}
						
						if (self->txFrameQ_2.numel) {
							if (self->txFrameQ_2.PopPreview(&self->txFrameQ_2, (txFrame_t*)&frame)) {
								// Attempt to transmit
								if (canbusLinux_TransmitFrame(parent, (txFrame_t*)&frame) == CANBUS_TX_OK) {
									// Only remove frame from the queue if the transmit is successful
									self->txFrameQ_2.Erase(&self->txFrameQ_2);
								}
							}
						}
					}

					// Transition back to rx_only
					pBusStateMgr->state = bus_RX_ONLY;
#endif
				}	
			}
			break;
		case bus_TX_AT_WILL:
			// Set number of frames to send and the new busState
			self->pendingFrames = 0;
			pBusStateMgr->state = new_state;
			break;
		case bus_MASTER_CTRL:
			// Set number of frames to send and the new busState
			self->pendingFrames = 0;
			pBusStateMgr->state = new_state;
			break;
	}

	return pBusStateMgr->state;  // Return new state
}

uint8_t canbus_checkForBusStateFrame(bus_state_mgr_t* pBusStateMgr, rxFrame_t* newFrame) {
	if (newFrame->header.IDE != CAN_ID_EXT) return 0;

	canbus_headerData_t decodedHeader;
	canbus_decodeHeader(&decodedHeader, newFrame->header.ExtId);

	if(decodedHeader.totalFrames == 1 && decodedHeader.packetID == pBusStateMgr->busStatePID
			&& decodedHeader.deviceID == *pBusStateMgr->pDeviceID && newFrame->data[0] == bus_TX_QUEUED) {
		pBusStateMgr->HandleBusState(pBusStateMgr->parent, newFrame->data[0]);
		return 1; // BUS_STATE frame already handled
	}
	return 0;
}
