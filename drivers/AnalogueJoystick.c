/*
 * AnalogueJoystick.c
 *
 *  Created on: Sep 1, 2016
 *      Author: p.phillips
 */
#define ADC_MAX_VALUE 4096.0
#define ADC_BUFF_SIZE 10
#define ADC_NUM_SAMPLES 1

#include <stdlib.h>
//#include "stm32_device_select.h"
#include "Hardware/AnalogueJoystick.h"


uint16_t ADC_BUFFER[ADC_BUFF_SIZE];
uint8_t numAnalogChannels = 0;


void Joystick_Error_Handler(){

}

/*
 * Initialize data in the AnalogueJoystick struct.
 * Arguments:
 * 		AnalogueJoystick_t* self: Pointer to self
 * 		ADC_HandleTypeDef AdcHandle:
 * 		uint8_t rank: the rank of the joystick is set up in the device configuration.
 *
 * 		 float (*mapping)(AnalogueJoystick_t* self): A points to a function that maps from a pot to a
 * 		 			a joystick in some way. If this is left as NULL then it behaves just like a pot.
 *
 * 		 float input: I don't know what this does. Probably to be deleted.
 */

void AnalogueJoystick_Construct(AnalogueJoystick_t* self, ADC_HandleTypeDef AdcHandle,
		uint8_t rank, float (*mapping)(AnalogueJoystick_t* self, float input)){

	self->Enable = AnalogueJoystick_Initialise;
	self->Read = AnalogueJoystick_Read;
	self->Deconstruct = AnalogueJoystick_Deconstruct;
	self->callibrate = AnalogueJoystick_callibrate;

	self->AdcHandle = AdcHandle;

	self->ADC_Buff = ADC_BUFFER;
	self->numSamples = ADC_NUM_SAMPLES;
	self->rank = rank;

	++numAnalogChannels;
	self->mapping = mapping;
}



void AnalogueJoystick_Deconstruct(AnalogueJoystick_t* self){
	self->numSamples = 0;
	self->mapping = NULL;
	--numAnalogChannels;
}


/*
 * Enable the analogue joystick's adc.
 * BUG: calling this twice raises an error
 */
void AnalogueJoystick_Initialise(AnalogueJoystick_t * self)
{
#ifdef DEVICE_STM32F3
	assert_param( HAL_ADC_Start(&(self->AdcHandle)) == HAL_OK);
#endif
#ifdef DEVICE_STM32F3
	  if (HAL_ADCEx_Calibration_Start(&self->AdcHandle, ADC_SINGLE_ENDED) !=  HAL_OK)
	  {
		  Joystick_Error_Handler();
	  }
#endif

	//set up pot
	  uint8_t dmaStartStatus = HAL_ADC_Start_DMA(&self->AdcHandle, (uint32_t*)self->ADC_Buff, self->numSamples*numAnalogChannels*sizeof(uint16_t));
	  if ( dmaStartStatus != HAL_OK)
	  {
		  Joystick_Error_Handler();
	  }
}



/*
 * Return the most recent read from the ADC.
 */

float AnalogueJoystick_Read(AnalogueJoystick_t * self)
{
	uint32_t ADCvalue=0;
	uint8_t rank_id = self->rank-1;

	uint8_t endOfRead = self->numSamples*numAnalogChannels + rank_id;  // maybe not correct. +rank_id?
	for (int i = rank_id; i < endOfRead; i+= numAnalogChannels)
	{
		ADCvalue += self->ADC_Buff[i];
	}

	float output = (float)(ADCvalue/(float)self->numSamples)/ADC_MAX_VALUE;
	if(self->mapping == NULL){
		return output;
	}else{
		return self->mapping(self, output);
	}
}

/*
 * Set the center, deadband and upper and lower limits of the analogue joystick.
 */

void AnalogueJoystick_callibrate(AnalogueJoystick_t* self, float center, float deadband, float upperLim, float lowerLim){
	self->lowerLim = lowerLim;
	self->center = center;
	self->upperLim = upperLim;
	self->deadBand = deadband;
}
