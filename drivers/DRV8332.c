/*H**********************************************************************
* FILENAME :        DRV8332.c             DESIGN REF: RS1
*
* DESCRIPTION :
*       Driver for DRV8332 Motor Controller
*
* PUBLIC FUNCTIONS :
*
*
*
* NOTES :
*       These functions are a part of the FM suite;
*       See IMS FM0121 for detailed description.
*
*       Copyright TheBlueprintLabs
*
* AUTHOR :   BlueprintLabs        START DATE :   28/02/2019
*
* CHANGES :
*
* REF NO  VERSION DATE    WHO     DETAIL
*
*
*H*/

/*
 * 	void (*Initialise)();
	void (*Enable)();
	void (*Disable)();
	int (*GetStatus)();
	void (*SetOutput)(float motorTorque);
	void (*SetDirection)(uint8_t motorDirection);
 */
#include "stm32_device_select.h"
#include "Hardware/DRV8332.h"
#include "DataStructures/auxMath.h"

#define SQRT3 (sqrtf(3))
#define ONEONSQRT3 (1.0/sqrtf(3))
#define ONEON2SQRT3 (0.5/sqrtf(3))


#define SVM

uint32_t DRV8332_SetPWMFrequency(DRV8332_t *self, uint32_t frequency);

uint8_t wait = 0;

void DRV8332_Construct(DRV8332_t* self,
		TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel, uint16_t ResetA_pin, GPIO_TypeDef* ResetA_port, TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel, uint16_t ResetB_pin, GPIO_TypeDef* ResetB_port, TIM_HandleTypeDef PWMCTimer, uint32_t PWMCChannel, uint16_t ResetC_pin, GPIO_TypeDef* ResetC_port,
		uint16_t otwPin, GPIO_TypeDef* otwPort,uint16_t faultPin, GPIO_TypeDef* faultPort ){

	self->Enable = DRV8332_Enable;
	self->Check=DRV8332_Check;
	self->Init = DRV8332_Init;
	self->Disable = DRV8332_Disable;
	self->SetOutputBrushed = DRV8332_SetOutput_Brushed;
	self->SetOutputFOC = DRV8332_SetOutput_FOC;
	self->SetOutputSin = DRV8332_SetOutput_DirectSinDrive;
	self->Calibrate = DRV8332_FindZeroAngle;
	self->StallDrive = DRV8332_CalibrateEncoder;
	self->CalibrateTest = DRV8332_CalibrateTest;
	self->SetPWMFrequency = DRV8332_SetPWMFrequency;

	self->otwPin = otwPin;
	self->otwPort = otwPort;
	self->faultPin = faultPin;
	self->faultPort = faultPort;

	self->PWMATimer = PWMATimer;
	self->PWMBTimer = PWMBTimer;
	self->PWMCTimer = PWMCTimer;

	self->PWMAChannel = PWMAChannel;
	self->PWMBChannel = PWMBChannel;
	self->PWMCChannel = PWMCChannel;

	self->ResetA_pin = ResetA_pin;
	self->ResetA_port = ResetA_port;
	self->ResetB_pin = ResetB_pin;
	self->ResetB_port = ResetB_port;
	self->ResetC_pin = ResetC_pin;
	self->ResetC_port = ResetC_port;

	self->mode = DISABLED;
	self->status = GOOD;

	self->index = 0;

	self->positions[0]=0;
	self->positions[1]=0;
	self->offsetAngle = 0;

	self->numPolePairs = 13;

}


uint8_t DRV8332_Check(DRV8332_t *self)
{
    if(!HAL_GPIO_ReadPin(self->faultPort, self->faultPin))
    {
    	SET_BIT(self->status,FAULT);
    	self->mode=DISABLED;
    	return 1;
    }
    else
    {
    	CLEAR_BIT(self->status,FAULT);
    }
    if(!HAL_GPIO_ReadPin(self->otwPort, self->otwPin))
    {
    	SET_BIT(self->status,OTW);

    	return 1;
    }
    else
    {
    	CLEAR_BIT(self->status,OTW);
    }
    return 0;

}

void DRV8332_Init(DRV8332_t *self)
{

	HAL_GPIO_WritePin(self->ResetA_port, self->ResetA_pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(self->ResetB_port, self->ResetB_pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(self->ResetC_port, self->ResetC_pin, GPIO_PIN_RESET);

	HAL_TIM_Base_Start_IT(&self->PWMATimer);
	HAL_TIM_Base_Start_IT(&self->PWMBTimer);
	HAL_TIM_Base_Start_IT(&self->PWMCTimer);
	HAL_TIM_PWM_Start_IT(&self->PWMATimer, self->PWMAChannel);
	HAL_TIM_PWM_Start_IT(&self->PWMBTimer, self->PWMBChannel);
	HAL_TIM_PWM_Start_IT(&self->PWMCTimer, self->PWMCChannel);

}


//Uses output value of between 1 and -1
void DRV8332_SetOutput_Brushed(DRV8332_t *self,float output)
{

	float outA=0;
	float outB=0;
	cap(&output,OUTPUT_RANGE,-OUTPUT_RANGE);

	if(self->mode==DISABLED)
	{
		self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*0);
		self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*0);
		__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
		__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );
	}
	else
	{
		if(output>=0)
		{
			outA = fabs(output);
			outB = 0.0;
		}
	    if (output<0)
		{
			 outA = 0.0;
			 outB = fabs(output);
		}

		self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*outA);
		self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*outB);
		__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
		__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );
	}

}

void DRV8332_SetOutput_DutyCycleZero(DRV8332_t *self) {
	self->PWMAConfig.Pulse = 0;
	self->PWMBConfig.Pulse = 0;
	self->PWMCConfig.Pulse = 0;
	__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse);
	__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse);
    __HAL_TIM_SetCompare(&self->PWMCTimer, self->PWMCChannel, self->PWMCConfig.Pulse);
}

//Uses output value of between 1 and -1
void DRV8332_SetOutput_FOC(DRV8332_t *self,float v_d, float v_q, float motorDirection)
{

	float theta = self->electricalAngle;
	float mag = sqrtf(v_d*v_d + v_q*v_q);
	if(mag>1.0){
		v_d /= mag;
		v_q /= mag;
	}


	if(self->mode==DISABLED)
	{
		self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*0);
		self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*0);
		self->PWMCConfig.Pulse = (uint32_t)(self->PWMCTimer.Init.Period*0);
		__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse);
		__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse);
	    __HAL_TIM_SetCompare(&self->PWMCTimer, self->PWMCChannel, self->PWMCConfig.Pulse);
	}
	else
	{

	    //inverse Park Transform
	    float outAlpha = cosf(theta)*v_d - sinf(theta)*v_q;
	    float outBeta = sinf(theta)*v_d + cosf(theta)*v_q;
	    outBeta *= motorDirection;
	    //inverse power-invariant Clarke Transform
//    	float outA = sqrtf(2/3) * outAlpha;
//    	float outB = -sqrtf(1/6) * outAlpha + sqrtf(1/2)*outBeta;
//    	float outC = -sqrtf(1/6) * outAlpha - sqrtf(1/2)*outBeta;

#ifdef SVM
	    //inverse power-variant Clarke Transform
	    float outA = ONEONSQRT3*outAlpha;
	    float outB = ONEON2SQRT3*(-outAlpha + SQRT3*outBeta);
	    float outC = ONEON2SQRT3*(-outAlpha - SQRT3*outBeta);
	    //neutral voltage shifting for full voltage sine waves
	    float v_neutral = 0.5*(fmaxf(fmaxf(outA,outB),outC)+fminf(fminf(outA,outB),outC));
	    outA -= v_neutral;
	    outB -= v_neutral;
	    outC -= v_neutral;
#else
	    //inverse power-variant Clarke Transform
	    float outA = 0.5*outAlpha;
	    float outB = 0.25*(-outAlpha + SQRT3*outBeta);
	    float outC = 0.25*(-outAlpha - SQRT3*outBeta);
#endif

	    outA += 0.5;
	    outB += 0.5;
	    outC += 0.5;

	    self->outputA = outA;
	    self->outputB = outB;
	    self->outputC = outC;

		self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*outA);
		self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*outB);
		self->PWMCConfig.Pulse = (uint32_t)(self->PWMCTimer.Init.Period*outC);
		__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse);
		__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse);
	    __HAL_TIM_SetCompare(&self->PWMCTimer, self->PWMCChannel, self->PWMCConfig.Pulse);

	}

}


//void DRV8332_SetOutput_FOC(DRV8332_t *self,float v_d, float v_q, float motorDirection)
//{
//	if(v_q>1)
//	{
//		v_q=1;
//	}
//
//	if(v_q<-1)
//	{
//		v_q=-1;
//	}
//
//	float theta = self->electricalAngle;
//    self->outputA = 0.5+0.5*v_q*sinf(theta);
//    self->outputC = 0.5+0.5*v_q*sinf(theta+2*PI/3);
//    self->outputB = 0.5+0.5*v_q*sinf(theta+4*PI/3);
//
//	self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*self->outputA);
//	self->PWMCConfig.Pulse = (uint32_t)(self->PWMCTimer.Init.Period*self->outputB);
//	self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*self->outputC);
//
//	__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse);
//	__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse);
//    __HAL_TIM_SetCompare(&self->PWMCTimer, self->PWMCChannel, self->PWMCConfig.Pulse);
//
//}

void DRV8332_SetOutput_DirectSinDrive(DRV8332_t *self, float amplitude)
{
	if(amplitude>1)
	{
		amplitude=1;
	}

	if(amplitude<-1)
	{
		amplitude=-1;
	}

    self->outputA = 0.5+0.5*amplitude*sinf(self->electricalAngle);
    self->outputB = 0.5+0.5*amplitude*sinf(self->electricalAngle+2*PI/3);
    self->outputC = 0.5+0.5*amplitude*sinf(self->electricalAngle+4*PI/3);

	self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*self->outputA);
	self->PWMCConfig.Pulse = (uint32_t)(self->PWMCTimer.Init.Period*self->outputB);
	self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*self->outputC);

	__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse);
	__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse);
    __HAL_TIM_SetCompare(&self->PWMCTimer, self->PWMCChannel, self->PWMCConfig.Pulse);
}

void DRV8332_Enable(DRV8332_t *self)
{
	self->mode=ENABLED;
	HAL_GPIO_WritePin(self->ResetA_port, self->ResetA_pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(self->ResetB_port, self->ResetB_pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(self->ResetC_port, self->ResetC_pin, GPIO_PIN_SET);

}

void DRV8332_Disable(DRV8332_t *self)
{
	self->mode=DISABLED;
	HAL_GPIO_WritePin(self->ResetA_port, self->ResetA_pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(self->ResetB_port, self->ResetB_pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(self->ResetC_port, self->ResetC_pin, GPIO_PIN_RESET);
	self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*0);
	self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*0);
	self->PWMCConfig.Pulse = (uint32_t)(self->PWMCTimer.Init.Period*0);
	__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
	__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );
	__HAL_TIM_SetCompare(&self->PWMCTimer, self->PWMCChannel, self->PWMCConfig.Pulse );

}

void DRV8332_FindZeroAngle(DRV8332_t *self,float amplitude,float mechanicalPosition){

	uint16_t overflow = 1280;
	self->index %= overflow;
	float position = (float)(self->index) * (2*M_PI/overflow);
	self->electricalAngle = sinf(position); //-(M_PI/2);

	if(self->index == (overflow>>2)){
		self->positions[0] = lpFilter(self->positions[0],mechanicalPosition,0.1);
	} else if(self->index == ((3*overflow)>>2)){
		self->positions[1] = lpFilter(self->positions[1],mechanicalPosition,0.1);
	}
	DRV8332_SetOutput_FOC(self,amplitude,0,1.0);
	//self->offsetAngle = -0.5*(self->positions[0]+self->positions[1]);
	self->offsetAngle = -atan2(sinf(self->positions[0])+sinf(self->positions[1]),cosf(self->positions[0])+cosf(self->positions[1]));
	self->index++;
	return;


	/*
	if((self->index%500) == 0){
		self->positions[0] = lpFilter(self->positions[0],mechanicalPosition,0.1);
		self->electricalAngle = 0.2;
		DRV8332_SetOutput_FOC(self,0,-amplitude);
	} else if((self->index%250) == 0){
		self->positions[1] = lpFilter(self->positions[1],mechanicalPosition,0.1);
		self->electricalAngle = -0.2;
		DRV8332_SetOutput_FOC(self,0,amplitude);
	} else if(self->index >= 5000){
		self->index = 0;
	}
	self->offsetAngle = -0.5*(self->positions[0]+self->positions[1]);
	self->index++;
	return;
	*/
}


//void DRV8332_FindCoggingProfile(DRV8332_t *self,uint8_t axis){
//
//	if(self->index >= 720){
//		self->index = 0;
//	}
//
//	//run postion controller where current setpoint = index
//	positionControl[axis].setpoint = (float)(self->index) * (M_PI/360.0);
//	stateEstimator[axis].velocityDemand=positionControl[axis].run(&positionControl[axis]);
//	float decelerationLimit = sqrt(fabs(2*stateEstimator[axis].maxAcceleration*positionControl[axis].error));
//
//
//	cap(&stateEstimator[axis].velocityDemand,velocityControl[axis].maxSetpoint,velocityControl[axis].minSetpoint);
//
//	//velocityControl[axis].setpoint = stateEstimator[axis].velocityDemand;
//	velocityControl[axis].setpoint = stateEstimator[axis].limitAcceleration(&stateEstimator[axis], velocityControl[axis].setpoint);
//	cap(&velocityControl[axis].setpoint,decelerationLimit,-decelerationLimit);
//
//
//	uint8_t status=stateEstimator[axis].check(&stateEstimator[axis]);
//	if(CHK_FLAG(stateEstimator[axis].status,NEAR_UPPER_LIMIT) || CHK_FLAG(stateEstimator[axis].status,NEAR_LOWER_LIMIT)){
//		cap(&velocityControl[axis].setpoint,fabsf(stateEstimator[axis].velocityDemand),-fabsf(stateEstimator[axis].velocityDemand));
//	}
//
//	currentControl[axis].setpoint= velocityControl[axis].run(&velocityControl[axis]);
//
//	openloopControl[axis].setpoint=currentControl[axis].run(&currentControl[axis]);
//
//	stateEstimator[axis].output=openloopControl[axis].run(&openloopControl[axis]);
//
//	stateEstimator[axis].output=stateEstimator[axis].output*stateEstimator[axis].motorVoltage/stateEstimator[axis].supplyVoltage*stateEstimator[axis].motorDirection;
//
//	//motor[axis].SetOutput(&motor[axis],0,stateEstimator[axis].output,(stateEstimator[A].position+motor[axis].offsetAngle)*7);
//
//	if(fabs(positionControl[axis].measurement - positionControl[axis].setpoint)<0.005){
//		//self->coggingProfile[self->index] = lpFilter(self->coggingProfile[self->index],stateEstimator[axis].i_q,0.2);
//		self->coggingProfile[self->index] = lpFilter(self->coggingProfile[self->index],stateEstimator[axis].output,0.2);
//		wait++;
//		if(wait>=10){
//			self->index++;
//			wait = 0;
//		}
//
//	}
//
//
//	return;
//}


void DRV8332_CalibrateEncoder(DRV8332_t *self,float amplitude,float mechanicalPosition,float motorDirection, uint16_t overflow){

    // uint16_t overflow = 15000;
	if(self->index > overflow){
		self->index = 0;
	}
	float position = (float)(self->index) * (2*M_PI/overflow) * signf(amplitude);
	self->electricalAngle = position*self->numPolePairs;
	DRV8332_SetOutput_FOC(self,0,fabsf(amplitude),motorDirection);
	//DRV8332_SetOutput_DirectSinDrive(self, fabsf(amplitude));
	self->index++;
	return;

}

void DRV8332_CalibrateTest(DRV8332_t *self,float amplitude,float motorDirection){

	DRV8332_SetOutput_FOC(self,amplitude,0,motorDirection);
	return;

}

#define DRV_PWM_PERIOD_MIN 100
#define DRV_PWM_PERIOD_MAX 1500


uint32_t DRV8332_SetPWMFrequency(DRV8332_t *self, uint32_t period) {
	TIM_HandleTypeDef handles[3] = {self->PWMATimer, self->PWMBTimer, self->PWMCTimer};

	period = min(period, DRV_PWM_PERIOD_MAX);
	period = max(period, DRV_PWM_PERIOD_MIN);

	__disable_irq();

	for (uint8_t i = 0; i < 3; i++) {
		handles[i].Init.Prescaler = period;
		if (HAL_TIM_Base_Init(&handles[i]) != HAL_OK)
		{
			Error_Handler();
		}
	}

	__enable_irq();

	return period;
}

