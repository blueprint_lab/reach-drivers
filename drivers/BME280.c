/*
 * BME280.c
 *
 *  Created on: 23 Jun 2020
 *      Author: Shaun Barlow
 */

#include "Hardware/BME280.h"
#include "Hardware/bme280_api.h"
#include "string.h"
#include "Communication/I2C.h"

float dummy;

I2C_HandleTypeDef* pSelected_hi2c;

int8_t _I2C_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len);
int8_t _I2C_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);

void I2C_set(BME280_t* self) {
	pSelected_hi2c = self->phi2c;
}

int8_t I2C_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len) {
	uint16_t DevAddress = (uint16_t) dev_id << 1;
	uint16_t Size = 1;
	uint32_t Timeout = 100;
	HAL_StatusTypeDef status;
	status = HAL_I2C_Master_Transmit(pSelected_hi2c, DevAddress, &reg_addr, Size, Timeout);
	if (status != HAL_OK)
		return (int8_t) status;
	status = HAL_I2C_Master_Receive(pSelected_hi2c, DevAddress, data, len, Timeout);
	if (status != HAL_OK)
		return (int8_t) status;
	return 0;
}

int8_t _I2C_readDMA(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t *data, uint16_t len) {
	uint16_t DevAddress = (uint16_t) dev_id << 1;
	HAL_StatusTypeDef status;
	status = HAL_I2C_Master_Receive_DMA(phi2c, DevAddress, data, len);
	if (status != HAL_OK)
		return (int8_t) status;
	return 0;
}

int8_t I2C_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len) {
	uint16_t DevAddress = (uint16_t) dev_id << 1;
	uint32_t Timeout = 100;
	uint8_t buffer[len + 1];
	HAL_StatusTypeDef status;
	buffer[0] = reg_addr;
	memcpy(buffer + 1, reg_data, len);
	status = HAL_I2C_Master_Transmit(pSelected_hi2c, DevAddress,  buffer, len + 1, Timeout);
	if (status != HAL_OK)
		return (int8_t) status;
	return 0;
}

int8_t _I2C_writeDMA(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t reg_addr, uint16_t len) {
	uint16_t DevAddress = (uint16_t) dev_id << 1;
	uint16_t txSize = 1;
	HAL_StatusTypeDef status;
	status = HAL_I2C_Master_Transmit_DMA(phi2c, DevAddress, &reg_addr, txSize);
	if (status != HAL_OK)
		return (int8_t) status;
	return 0;
}


void BME280_Destroy(BME280_t* self) {
	self->phi2c = BME280_NOP;
	self->Init = BME280_NOP;
	self->Read = BME280_NOP;
	self->ReadDMA = BME280_NOP;

	self->TxCpltCallback = BME280_NOP;
	self->RxCpltCallback = BME280_NOP;

	self->GetPressure = BME280_NOP;
	self->GetTemperature = BME280_NOP;
	self->GetHumidity = BME280_NOP;

	self->pTemperatureAlias = &dummy;
	self->pPressureAlias = &dummy;
	self->pHumidityAlias = &dummy;
}

uint8_t BME280_NOP(BME280_t * self)
{
  return 0;
}

void BME280_Construct(BME280_t* self, I2C_HandleTypeDef* phi2c) {
	self->phi2c = phi2c;
	self->Init = BME280_Init;
	self->Read = BME280_Read;
	self->ReadDMA = BME280_ReadDMA;

	self->TxCpltCallback = BME280_DMAReceiveData;
	self->RxCpltCallback = BME280_DMAProcessData;

	self->GetPressure = BME280_GetPressure;
	self->GetTemperature = BME280_GetTemperature;
	self->GetHumidity = BME280_GetHumidity;

	// Setup API device
	self->device.read = I2C_read;
	self->device.write = I2C_write;
	self->device.dev_id = BME280_I2C_ADDR_PRIM;
	self->device.intf = BME280_I2C_INTF;
	self->device.delay_ms = HAL_Delay;
	self->pTemperatureAlias = &dummy;
	self->pPressureAlias = &dummy;
	self->pHumidityAlias = &dummy;
	self->readingPeriod = 1000;
	self->nextReadingTime = 0;
}

uint8_t BME280_Init(BME280_t* self) {
	int8_t rslt;
	I2C_set(self);
	rslt = bme280_init(&self->device);

	/* MODE OF OPERATION */
	uint8_t settings_sel;

	/* Mode of operation most suitable for indoor navigation */
	self->device.settings.osr_h = BME280_OVERSAMPLING_1X;
	self->device.settings.osr_p = BME280_OVERSAMPLING_16X;
	self->device.settings.osr_t = BME280_OVERSAMPLING_2X;
	self->device.settings.filter = BME280_FILTER_COEFF_16;
	self->device.settings.standby_time = BME280_STANDBY_TIME_62_5_MS;

	settings_sel = BME280_OSR_PRESS_SEL;
	settings_sel |= BME280_OSR_TEMP_SEL;
	settings_sel |= BME280_OSR_HUM_SEL;
	settings_sel |= BME280_STANDBY_SEL;
	settings_sel |= BME280_FILTER_SEL;
	/* End indoor navigation mode */
	I2C_set(self);
	rslt = bme280_set_sensor_settings(settings_sel, &self->device);
	rslt = bme280_set_sensor_mode(BME280_NORMAL_MODE, &self->device);

	self->commState = CS_READY;
	return (uint8_t)rslt;
}

uint8_t BME280_Read(BME280_t* self) {
	int8_t rslt;
	I2C_set(self);
	rslt = bme280_get_sensor_data(BME280_ALL, &self->comp_data, &self->device);
	return (uint8_t) rslt;
}

/**
 * @brief	Send the "read" command to the BME.
 * 			Transmits on DMA.
 * 			Make sure to set a callback in HAL_I2C_MasterTxCpltCallback
 * 			that receives the data (BME280_ReceiveData) on DMA.
 *
 */
uint8_t BME280_ReadDMA(BME280_t* self) {
	// Check that another device does not have the lock.
	if (self->phi2c->Devaddress != 0) { return 0; }

	if (self->commState != CS_READY) {
		return 1; // NOT ready. Don't start a transfer.
	}

	// Not ready for a reading yet.
	if (self->nextReadingTime > HAL_GetTick()) { return 0; }

	self->commState = CS_TX_IN_PROGRESS;
	int8_t rslt;

	/* Begin reading the BME data registers by sending the "read" command. */
	rslt = _I2C_writeDMA(self->phi2c, self->device.dev_id, BME280_DATA_ADDR, BME280_P_T_H_DATA_LEN);
	return (uint8_t) rslt;
}


/**
 * @brief	Receive requested data via DMA.
 * 			This must be called in the HAL_I2C_MasterTxCpltCallback() when
 * 			using DMA transmit to start the transfer.
 */
uint8_t BME280_DMAReceiveData(BME280_t* self, I2C_HandleTypeDef *hi2c) {
	if (self->phi2c != hi2c) {
		return 1;  // Not my callback
	}
	if ((self->phi2c->Devaddress >> 1) != self->device.dev_id) { return  I2C_NOT_MY_CALLBACK; } // Not my callback

	if (self->commState != CS_TX_IN_PROGRESS) {
		return 1; // NOT ready. Don't start receiving data.
	}

	self->commState = CS_RX_IN_PROGRESS;
	int8_t rslt;

	/* Read the pressure, temperature and humidity data from the sensor */
	rslt = _I2C_readDMA(self->phi2c, self->device.dev_id,
			self->DMA_buffer, BME280_P_T_H_DATA_LEN);
	return (uint8_t) rslt;
}

/**
 * @brief	Process data after DMA receive has completed.
 * 			This must can be called in the HAL_I2C_MasterRxCpltCallback() when
 * 			using DMA receive to get the data.
 */
uint8_t BME280_DMAProcessData(BME280_t* self, I2C_HandleTypeDef *hi2c) {

	if (self->phi2c != hi2c) {
		return 1;  // Not my callback
	}
	if ((self->phi2c->Devaddress >> 1) != self->device.dev_id) { return  I2C_NOT_MY_CALLBACK; } // Not my callback

	self->commState = CS_DATA_PENDING;

	int8_t rslt;
	struct bme280_uncomp_data uncomp_data = { 0 };

	/* Parse the read data from the sensor */
	bme280_parse_sensor_data(self->DMA_buffer, &uncomp_data);

	/* Compensate the pressure and/or temperature and/or
	 * humidity data from the sensor
	 */
	rslt = bme280_compensate_data(BME280_ALL, &uncomp_data, &(self->comp_data), &self->device.calib_data);
	self->commState = CS_READY;
	self->pressure = self->comp_data.pressure / 100000;
	self->humidity = self->comp_data.humidity;
	self->temperature = self->comp_data.temperature;
	*self->pTemperatureAlias = self->temperature;
	*self->pPressureAlias = self->pressure;
	*self->pHumidityAlias = self->humidity;

	self->phi2c->Devaddress = 0;
	self->nextReadingTime = HAL_GetTick() + self->readingPeriod;
	return (uint8_t) rslt;
}

float BME280_GetPressure(BME280_t* self) {
	return self->pressure;
}

float BME280_GetTemperature(BME280_t* self) {
	return self->temperature;
}

float BME280_GetHumidity(BME280_t* self) {
	return self->humidity;
}

void BME280_SetAliases(BME280_t* self, float* pTemperatureAlias, float* pPressureAlias, float* pHumidityAlias) {
	self->pTemperatureAlias = pTemperatureAlias;
	self->pPressureAlias = pPressureAlias;
	self->pHumidityAlias = pHumidityAlias;
}

