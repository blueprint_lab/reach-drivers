/*
 * protocolManager.c
 *
 *  Created on: Oct 14, 2020
 *      Author: Shaun Barlow
 */


#include "Communication/protocolManager.h"

void protocolManager_addProtocol(protocolManager_t* self, protocol3P_t* pProtocol);
uint8_t protocolManager_readAll(protocolManager_t* self,
								CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pData);


void protocolManager_construct(protocolManager_t* self) {
	self->ReadAll = protocolManager_readAll;
	self->Add = protocolManager_addProtocol;
	self->protocolCount = 0;
}

/**
 * @brief	Add a protocol to the protocol pointer.
 * 			Protocol must be initialised before adding to manager.
 */
void protocolManager_addProtocol(protocolManager_t* self, protocol3P_t* pProtocol) {
	construct_protocol(pProtocol);
	self->protocols[self->protocolCount] = pProtocol;
	self->protocolCount++;
}

/**
 * @brief	Pass frame to each protocol.
 * 			Return 1 if frame is read by one of the protocols.
 */
uint8_t protocolManager_readAll(protocolManager_t* self,
				CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pData) {
	for (uint8_t i=0; i < self->protocolCount; i++) {
		protocol3P_t* pProtocol = self->protocols[i];
		if (pProtocol->Read(pProtocol, pRxHeader, pData)) {
			return 1;
		}
	}
	return 0;
}

/**
 * @brief	Get next queued frame for all protocols.
 * @retval	Number of frames taken from queue. 0 indicates no frames left.
 */
uint8_t protocolManager_getQueuedFrame(protocolManager_t* self, canbus_frame_t* frame) {
	protocol3P_t* pProtocol;
	for (uint8_t i = 0; i < self->protocolCount; i++) {
		pProtocol = self->protocols[i];
		if (pProtocol->txFrameQ.count) {
			canbus_PopFrameFromQueue(&pProtocol->txFrameQ, frame);
			return 1;  // Frame popped and copied
		} else {
			continue;  // Q is empty, check next protocol
		}
	}
	return 0;  // No frames in any protocol's Q
}
