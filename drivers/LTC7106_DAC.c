/*
 * LTC7106_DAC.c
 *
 *  Created on: 18 Mar 2021
 *      Author: Shaun Barlow
 */


#include "Hardware/LTC7106_DAC.h"
#include <math.h>

uint8_t LTC7106_VoltageToIOutCommand(float voltage);


void LTC7106_Construct(LTC7106_t* self, I2C_HandleTypeDef* phi2c) {
	self->phi2c = phi2c;
	self->Init = LTC7106_Init;
	self->setVoltage = LTC7106_SetVoltage;
	self->CheckAlert = LTC7106_CheckAlert;
}

uint8_t LTC7106_Init(LTC7106_t* self) {
	// Set max sink current
	uint8_t txData[2] = {LTC_MFR_IOUT_MAX, LTC_SINK_MAX_CURRENT};
	uint8_t status = HAL_I2C_Master_Transmit(self->phi2c, LTC_ADDRESS, txData, 2, 100);
	if (status != HAL_OK) {
		// Handle error
	}
	return status;
}

uint8_t LTC7106_SetVoltage(LTC7106_t* self, float voltage) {
	if (voltage == 0) {
		// do nothing
	} else if (voltage < 12) {
		voltage = 12.0;
	} else if (voltage > 24) {
		voltage = 24.0;
	}
	self->voltage = voltage;

	if (voltage == 0) {
		HAL_GPIO_WritePin(EN_LTC7106_GPIO_Port, EN_LTC7106_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(EN_LM76003_GPIO_Port, EN_LM76003_Pin, GPIO_PIN_RESET);
		return 0;
	}

	uint8_t iOut = LTC7106_VoltageToIOutCommand(voltage);
	uint8_t txData[2] = {LTC_MFR_IOUT_COMMAND, iOut};
	uint8_t status = HAL_I2C_Master_Transmit(self->phi2c, LTC_ADDRESS, txData, 2, 100);
	if (status != HAL_OK) {
		// handle error
	}

	HAL_GPIO_WritePin(EN_LTC7106_GPIO_Port, EN_LTC7106_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(EN_LM76003_GPIO_Port, EN_LM76003_Pin, GPIO_PIN_SET);
	return status;
}

uint8_t LTC7106_CheckAlert(LTC7106_t* self) {
	uint8_t status = HAL_GPIO_ReadPin(I2C1_SMBA_ALERT_LTC7106_GPIO_Port, I2C1_SMBA_ALERT_LTC7106_Pin);
	return status;
}

uint8_t LTC7106_VoltageToIOutCommand(float voltage) {
	if (voltage < 12) {
		voltage = 12.0;
	} else if (voltage > 24) {
		voltage = 24.0;
	}

	if (voltage <= 18.0) {
		// 12V 		LTC_DAC_CODE_63_NOM 		0x3F
		// 18V 		LTC_DAC_CODE_0_NOM  		0x00
		return round(0x3F * (3 - (voltage / 6)));
	} else {
		// 18.1V 	LTC_DAC_CODE_NEG_1_NOM  	0x7F
		// 24V 		LTC_DAC_CODE_NEG_64_NOM 	0x40
		return round(0x40 * (5 - (voltage / 6)));
	}
}


