/*
 * udp.c
 *
 *  Created on: Nov 9, 2020
 *      Author: Shaun Barlow
 */

#include "Communication/udp.h"
#include "Communication/comsprocess.h"
#include "Communication/W5500_api.h"

uint8_t udp_construct(udp_comms_link* self, W5500_socket_t* sock) {
	self->sock = sock;
	return 0;
}


uint8_t udp_Read(coms_link* parent) {
	udp_comms_link* self = parent->udpLink;
	W5500_Read(self->sock);
	return coms_readPacketFromQ(&parent->currPacket, &self->sock->rxQ);
}


uint8_t udp_enableRead(coms_link* parent) {
	W5500_InitSocket(parent->udpLink->sock);
	return 0;
}

uint8_t udp_QForTransmit(coms_link* parent, packet_t* packet) {
	udp_comms_link* self = parent->udpLink;
	self->sock->txQ.pushArray(&self->sock->txQ, packet->transmitData, packet->length);
	return 0;
}
uint8_t udp_Transmit(coms_link* parent) {
	udp_comms_link* self = parent->udpLink;
	W5500_SendUDP(self->sock);
	return 0;
}
