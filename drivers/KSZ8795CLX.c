/*
 * KSZ8795CLX.c
 *
 *  Created on: Feb 21, 2021
 *      Author: shaun
 */

#include "Communication/KSZ8795CLX.h"


void KSZ_Construct(KSZ8795CLX_t* self, SPI_HandleTypeDef* phspi) {
	self->phspi = phspi;
}


void KSZ_Init(KSZ8795CLX_t* self) {

}

void KSZ_ReadPortOperationModes(KSZ8795CLX_t* self) {
	uint8_t regList[KSZ_NUM_STD_PORTS] = {KSZ_REG_31_P1_CTL11_ST3,
							KSZ_REG_47_P2_CTL11_ST3,
							KSZ_REG_63_P3_CTL11_ST3,
							KSZ_REG_79_P4_CTL11_ST3};

	uint8_t rxBuff[3], txBuff[3];
	txBuff[0] = KSZ_READ_DATA;
	for (uint8_t i=0; i<KSZ_NUM_STD_PORTS; i++) {
		txBuff[1] = regList[i];
		rxBuff[2] = 0;
		if (HAL_SPI_TransmitReceive(self->phspi, txBuff, rxBuff, 3, 100) == HAL_OK) {
			self->port[i].opMode = 0b111 & rxBuff[2];
		}
	}
}
