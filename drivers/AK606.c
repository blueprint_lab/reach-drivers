/*
 * AK606.c
 *
 *  Created on: 4 Oct. 2022
 *      Author: JamieJacobson
 *
 * This module is a driver for the Ak60-6 CubeMarse Motor.
 *
 * Datasheet: https://store.cubemars.com/images/file/20211201/1638329381542610.pdf
 *
 *
 * The Driver supports CAN communications using 3rd-party protocols to send and recieve data. Communcation occurs via
 * standard CAN packets with 8-bit commands and 6-bit transmission value. The motor will always send a state packet
 * When a message is successfully acknowledged by the motor.
 *
 * The Module drives the actuator in MIT motor mode and allows a combination of the following control types:
 * 	1. Torque
 * 	2. Velocity
 * 	3. Position
 *
 * The driver also contains the following special functions:
 *
 * 1. Enter MIT Motor Mode
 * 2. Exit MIT Motor Mode
 * 3. Zero Position
 *
 * Control Demands can be sent through either of the following commands:
 *
 * 1. AK606_sendMITControlDemand <- for Velocity, Positional and Torque Control
 * 2. AK606_sendTorqueDemand <- For Torque only control
 *
 * Functions convert demands from floating point value to unsigned integer demands before transmission. You do not
 * need to calculate these values. Maximum and minimum limits are defined below.
 *
 *  Initialisation routine:
 * 	declare a handle object and initialise it using the AK606_Construct method. Note: when assigning a CAN id, ensure
 * 	that it matches with the ID on the motor.
 * 	Initialise the motor by sending the "Enter Motor" command (AK606_EnterMotorMode)
 * 	Reset the Encoder by sending the "Zero Position" Command
 */

#include "Hardware/AK606.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * @brief command motor to Leave MIT control mode
 */
static const uint8_t   commandExitMotor[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFD};

/**
 * @brief command motor to enter MIT control mode
 */
static const uint8_t  commandEnterMotor[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC};

/**
 * @brief zero the onboard encoder
 */
static const uint8_t  commandSetZeroPos[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE};

/**
 * @brief Ak60-6 communication slot.
 */
AK606_t* ak606CommSlot;


static uint16_t AK606_CalculateDemand(float val, float min_val, float max_val, uint16_t bits);
static uint8_t Ak606_CanRead(protocol3P_t* self, CAN_RxHeaderTypeDef* RxHeader, uint8_t* pData);
static float uint_to_float(uint16_t val, float min_val, float max_val, uint16_t res);

/**
 * @fn uint_to_float
 * @brief convert an unsigned int value to a float bounded between a max and min
 * @param val value to be converted
 * @param min_val minimum possible value
 * @param max_val maximum possible value
 * @param res number of bits on the result
 * @return converted result
 * @note: Calculation:
 *  				  	   val x span
 * float value = min_val < ---------- + min_val < max_val
 *					   	    2^res - 1
 */
static float uint_to_float(uint16_t val, float min_val, float max_val, uint16_t res)
{
	float span = max_val - min_val;
	float resolution = (float)(0b1 <<res);

	return ((float)val) * span / resolution + min_val;
}

/**
 * @fn Ak606_CanRead
 * @brief read a CAN frame using 3rd party protocol and parse the data
 * @note overrides canbus_NOP_thirdPartyProtocol_Read
 * @param[in, out] self ak606 handler to store data
 * @param[in] RxHeader CAN header
 * @param pData[in] Rx Queue Frame data buffer
 * @return 1 if successful
 */
static uint8_t Ak606_CanRead(protocol3P_t* self, CAN_RxHeaderTypeDef* RxHeader, uint8_t* pData)
{
	uint16_t pos, speed, current;

	ak606CommSlot->motorId = pData[0];
	pos = (uint16_t)(pData[1] << 8) | (uint16_t)pData[2];
	speed = ((uint16_t)(pData[3] << 8) | (uint16_t)(pData[4] & 0xF0)) >> 4;
	current = (uint16_t)(pData[4] & 0x0F) << 8 | (uint16_t)(pData[5]);
	ak606CommSlot->motorState.motorStatus = (AK606_MotorStatus_t)pData[7];

	/* Convert to float	*/
	ak606CommSlot->motorState.position = uint_to_float(pos, AK606_P_MIN, AK606_P_MAX, 16);
	ak606CommSlot->motorState.velocity = uint_to_float(speed, AK606_V_MIN, AK606_V_MAX, 12);
	ak606CommSlot->motorState.torque = AK606_KT * uint_to_float(current, AK606_TOR_MIN/AK606_KT, AK606_TOR_MAX/AK606_KT, 12);


	return 1;
}


void AK606_Construct(AK606_t* self, protocolManager_t* pProtocolManager, canbus_comms_link* comms_link, uint8_t canId)
{
	self->canId = canId;
	self->comms_link = comms_link;
	memset((void*)&self->motorDemand, 0x00, sizeof(self->motorDemand));
	memset((void*)&self->motorState, 0x00, sizeof(self->motorState));
	memset((void*)&self->motorCommandBuffer, 0x00, sizeof(self->motorCommandBuffer));

	pProtocolManager->Add(pProtocolManager, &self->ak606Protocol);
	self->ak606Protocol.Read = Ak606_CanRead;

	ak606CommSlot = self;
}

/**
 * @fn  AK606_CalculateDemand
 * @brief Converts a float to a bit-bounded, unsigned int demand
 * @param[in] val value to be converted
 * @param[in] min_val minimum boounded value
 * @param[in] max_val maximum bounded value
 * @param[in] bits Number of bits in the full scale resolution
 * @return unsigned 16 bit integer demand
 */
static uint16_t AK606_CalculateDemand(float val, float min_val, float max_val, uint16_t bits)
{
	float span = (max_val - min_val);

	if (val < min_val)
	{
		val = min_val;
	}
	if (val > max_val)
	{
		val = max_val;
	}

	float resolution = (float)(0b1 << bits);

	return (uint16_t)(((val - min_val)*resolution) /span );
}

AK606_ErrorCode_t AK606_EnterMotorMode(AK606_t* self)
{
	AK606_ErrorCode_t status = AK606_ERROR_OK;

	assert(self != NULL);
	assert(&self->ak606Protocol != NULL);

	self->canRxHeader.StdId = self->canId;
	self->canRxHeader.IDE = CAN_ID_STD;
	self->canRxHeader.ExtId = 0X00;
	self->canRxHeader.DLC = MOTOR_COMMAND_BUFFER_LENGTH;

	memcpy(self->motorCommandBuffer, commandEnterMotor, sizeof(self->motorCommandBuffer));
	self->ak606Protocol.Send(&self->ak606Protocol,
							 (CAN_RxHeaderTypeDef *)&self->canRxHeader,
							 self->motorCommandBuffer);
	uint8_t frames = canbus_thirdPartyProtocol_Transmit(self->comms_link);
	if (frames == 0)
	{
		status = AK606_ERROR_CAN_TRANSMIT;
	}

	return status;
}


/**
 * Torque Control is done by sending a 12-bit current value
 * to the motor in MIT mode. This value represents the range
 * -15 - 15 N.m or -60 - 60 Amps
 */
AK606_ErrorCode_t AK606_sendTorqueDemand(AK606_t* self, float torque)
{
	AK606_ErrorCode_t status = AK606_ERROR_OK;

	/* Calculate Current */
	float current = torque / AK606_KT;

	/* determine demand  value*/
	uint16_t currentDemand = AK606_CalculateDemand(current, AK606_I_MIN, AK606_I_MAX , 12);

	self->canRxHeader.StdId = self->canId;
	self->canRxHeader.IDE = CAN_ID_STD;
	self->canRxHeader.ExtId = 0x00;
	self->canRxHeader.DLC = MOTOR_COMMAND_BUFFER_LENGTH;

	memset(self->motorCommandBuffer, 0x00, MOTOR_COMMAND_BUFFER_LENGTH);
	self->motorCommandBuffer[6] = (currentDemand >> 8) & 0x0F;
	self->motorCommandBuffer[7] = currentDemand &0xFF;

	self->ak606Protocol.Send(&self->ak606Protocol,
							 (CAN_RxHeaderTypeDef *)&self->canRxHeader,
							 self->motorCommandBuffer);

	uint8_t frames = canbus_thirdPartyProtocol_Transmit(self->comms_link);
	if (frames == 0)
	{
		status = AK606_ERROR_CAN_TRANSMIT;
	}

	return status;
}

/**
 * @note: see AK606.h for min and max values
 */
AK606_ErrorCode_t AK606_sendMITControlDemand(AK606_t* self, float position, float speed, float kp, float kd, float torque)
{
	AK606_ErrorCode_t status = AK606_ERROR_OK;

	assert(self != NULL);

	memset(self->motorCommandBuffer, 0x00, MOTOR_COMMAND_BUFFER_LENGTH);

	/* calculate demands */
	self->motorDemand.positionDemand = AK606_CalculateDemand(position,AK606_P_MIN, AK606_P_MAX, 16);
	self->motorDemand.velocityDemand = AK606_CalculateDemand(speed, AK606_V_MIN, AK606_V_MAX, 12);
	self->motorDemand.torqueDemand = AK606_CalculateDemand(torque / AK606_KT, AK606_I_MIN, AK606_I_MAX, 12);
	self->motorDemand.kpDemand = AK606_CalculateDemand(kp, AK606_KP_MIN, AK606_KP_MAX, 12);
	self->motorDemand.kdDemand = AK606_CalculateDemand(kd, AK606_KD_MIN, AK606_KD_MAX, 12);

	/* set CAN header */
	self->canRxHeader.StdId = self->canId;
	self->canRxHeader.IDE = CAN_ID_STD;
	self->canRxHeader.ExtId = 0x00;
	self->canRxHeader.DLC = MOTOR_COMMAND_BUFFER_LENGTH;

	memset(self->motorCommandBuffer, 0x00, MOTOR_COMMAND_BUFFER_LENGTH);

	self->motorCommandBuffer[0] = (uint8_t)((self->motorDemand.positionDemand >> 8) & 0xFF);
    self->motorCommandBuffer[1] = (uint8_t)(self->motorDemand.positionDemand & 0xFF);
	self->motorCommandBuffer[2] = (uint8_t)((self->motorDemand.velocityDemand >> 4) & 0xFF);
	self->motorCommandBuffer[3] = (uint8_t)(((self->motorDemand.velocityDemand &0x0F) << 4));
	self->motorCommandBuffer[3] |= (uint8_t)((self->motorDemand.kpDemand >> 8) & 0x0F);
	self->motorCommandBuffer[4] = (uint8_t)((self->motorDemand.kpDemand) & 0xFF);
	self->motorCommandBuffer[5] = (uint8_t)((self->motorDemand.kdDemand >> 4) & 0xFF);
	self->motorCommandBuffer[6] = (uint8_t)((self->motorDemand.kdDemand &0x0F) << 4);
	self->motorCommandBuffer[6] |= (uint8_t)((self->motorDemand.torqueDemand >> 8) & 0x0F);
	self->motorCommandBuffer[7] = self->motorDemand.torqueDemand & 0xFF;

	self->ak606Protocol.Send(&self->ak606Protocol,
							 (CAN_RxHeaderTypeDef *)&self->canRxHeader,
							 self->motorCommandBuffer);

	uint8_t frames = canbus_thirdPartyProtocol_Transmit(self->comms_link);
	if (frames == 0)
	{
		status = AK606_ERROR_CAN_TRANSMIT;
	}

	return status;
}

AK606_ErrorCode_t AK606_SetZeroPosition(AK606_t* self)
{
	AK606_ErrorCode_t status = AK606_ERROR_OK;

	assert(self != NULL);

	self->canRxHeader.StdId = self->canId;
	self->canRxHeader.IDE = CAN_ID_STD;
	self->canRxHeader.ExtId = 0x00;
	self->canRxHeader.DLC = MOTOR_COMMAND_BUFFER_LENGTH;

	memcpy(self->motorCommandBuffer, commandSetZeroPos, MOTOR_COMMAND_BUFFER_LENGTH);
	self->ak606Protocol.Send(&self->ak606Protocol,
							 (CAN_RxHeaderTypeDef *)&self->canRxHeader,
							 self->motorCommandBuffer);

	uint8_t frames = canbus_thirdPartyProtocol_Transmit(self->comms_link);
	if (frames == 0)
	{
		status = AK606_ERROR_CAN_TRANSMIT;
	}

	return status;

}

AK606_ErrorCode_t ExitMotorMode(AK606_t* self)
{
	AK606_ErrorCode_t status = AK606_ERROR_OK;

	assert(self != NULL);
	assert(&self->ak606Protocol != NULL);

		self->canRxHeader.StdId = self->canId;
		self->canRxHeader.IDE = CAN_ID_STD;
		self->canRxHeader.ExtId = 0x00;
		self->canRxHeader.DLC = MOTOR_COMMAND_BUFFER_LENGTH;

		memcpy(self->motorCommandBuffer, commandExitMotor, MOTOR_COMMAND_BUFFER_LENGTH);

		self->ak606Protocol.Send(&self->ak606Protocol,
								 (CAN_RxHeaderTypeDef *)&self->canRxHeader,
								 self->motorCommandBuffer);

		uint8_t frames = canbus_thirdPartyProtocol_Transmit(self->comms_link);
		if (frames == 0)
		{
			status = AK606_ERROR_CAN_TRANSMIT;
		}

		return status;
}
