/*
 * controller2D.c
 *
 *  Created on: 25Jan.,2017
 *      Author: TheBlueprintLabs
 */

#include "DataStructures/controller2D.h"
#include "DataStructures/auxMath.h"

//// limits in test
//#define	LOWER_LIM	0.33		// 20 deg
//#define UPPER_LIM	5.6		// 310 deg
//#define	LOWER_CUT	0.7			// arbitrary test val
//#define UPPER_CUT	4.6			// arbitrary test val
//#define TOLERANCE	0.03491		// 2 deg

#define	LOWER_LIM	1.57	// 90 deg
#define UPPER_LIM	0.07		// 310 deg
#define	LOWER_CUT	1.8		// arbitrary test val
#define UPPER_CUT	6.4			// arbitrary test val
#define TOLERANCE	0.03491		// 2 deg

float tmpPositionError = 0;
float positionError = 0;

uint8_t constructPositionController(controller_t* self,controlMode_t controlType, float timestep, float Kp, float Kd, float Ki, float tauP, float tauD){
	uint8_t success = 1;

	/*
	 *Assign function pointers
	 */

	// Control loop functions

		self->calculateControl = controller_Position;
		self->setGains = controller_setGains;
		self->calculateVelocityControl = controller_Velocity;
		self->calculateOpenloopControl = controller_Openloop;
		self->setpoint = positionController_setpoint;
		self->updateStateEst = positionController_updateStateEst;


	// controller monitoring
	self->readKd = positionController_readKd;
	self->readKp = positionController_readKp;
	self->readKi = positionController_readKi;
	self->readKd = positionController_readKd;
	self->readPositionEst = positionController_readPositionEst;
	self->readVelocityEst= positionController_readVelocityEst;

	// controller configuration
	self->setKp = positionController_setKp;
	self->setKi = positionController_setKi;
	self->setKd = positionController_setKd;

	self->setMaxPosition= positionController_setMaxPosition;
	self->setMinPosition= positionController_setMinPosition;
	self->setMaxVelocity = positionController_setMaxVelocity;
	self->setMinVelocity = positionController_setMinVelocity;
	self->setMaxAcceleration = positionController_setMaxAcceleration;
	self->setMinAcceleration = positionController_setMinAcceleration;

	//Fill in Gains
	self->Kd = Kd;
	self->Kp = Kp;
	self->Ki = Ki;
	self->Kvi = 0.01; //Integral Velocity Gain
	self->Kvp = 4;   //Proportional Velocity Gain
	self->Kvf = 1.0; //Feedforward Velocity Gain
	self->timestep = timestep;
	self->tauP = tauP;
	self->tauD = tauD;

	// Acceleration, Velocity and position limits
	self->vel_max = 1.0;
	self->vel_min = -1.0;
	self->voltage_max = 1.0;
	self->voltage_min = -1.0;
	self->acc_max = 1.0;
	self->acc_min= -1.0;
	self->maxPosition= UPPER_LIM;
	self->minPosition= LOWER_LIM;

	// initial estimates
	self->Velocity = 0.0;
	self->Position = 0.0;
	self->IntegralError = 0.0;
	self->velocityIntegralError=0.0;
	self->PositionOffset = 0.279;

	self->output=0;

	self->controllerStatus = 0x00;

	return success;
}

void positionController_setpoint(controller_t* self, float PosSetPoint, float velSetPoint){
	self->positionSetPoint = PosSetPoint;
	self->velocitySetPoint = velSetPoint;
	self->IntegralError = 0.0;
}

/*
 * Implementation of an alpha beta filter.
 */

float positionController_updateStateEst(controller_t* self, float measurement){
//	float alpha = self->tauP;
//	float beta = self->tauD;
//
	float dt = self->timestep;
//	float velocity = self->Velocity;
//	float position = self->Position + dt* velocity;
//
//	float innovation = wrapAngle(measurement - position);
//	self->Position = wrapAngle(position + alpha*innovation);
//	self->Velocity = velocity + beta*innovation/dt;

//	self->Position = self->Position + self->Velocity*dt + wrapAngle(measurement - self->Position)/self->tauP;
	self->Velocity = wrapAngle(measurement - self->Position) / dt;
//	self->Position = measurement;
	self->Position =measurement; // wrapAngle(measurement-self->PositionOffset); //Apply offset

	//float positionError = wrapAngle(self->positionSetPoint - self->Position);


	return 0.0;
}

float lambda = 1000;
float eta = 1000;
float F = 1;
float w = 0.0;
float epsilon = 0.01;

float controller_Position(controller_t* self){
	// find x_d, dx_d, ddx_d
	// u = Bhat(x) (ddx_d + Kp (x_d - x) + Kd (dx_d - dx) + Ki \int{xd-x} dt

	// if poisition error < 0.5*vmax^2/amin
	//		xd = setpoint, velocity = 0, ddx = u_nominal
//   else if dx >= vmax
	//		xd = x + dt*vmax, velocity = vmax, ddx = u_nominal
//   else if dx< vmax
	//		xd = x + 0.5*v*dt^2, velocity = v + amax*dt, ddx = unom + amax

	//self->maxPosition

/* Begin Limiting Code under test */

	float tmpSetPos = self->positionSetPoint;
	float tmpCurPos = self->Position;
	float correctedPositionSetPoint = 0;

	if (tmpSetPos == tmpCurPos){
		return 0;
	}

	// If the positionSetPoint is in the no-go zone, we modify it to be the closest limit to that point
	correctedPositionSetPoint = correctPositionSetPoint(tmpSetPos,self->maxPosition,self->minPosition,TOLERANCE);

	if (correctedPositionSetPoint != tmpSetPos){
		// set some flag to indicate a limit has been reached
	}

	tmpPositionError = wrapAngle(correctedPositionSetPoint - tmpCurPos);// shortest distance to corrected position set point

	// First, check if the current position is:

	// Between Lower Lim and Lower Cut-off?
	if (((LOWER_CUT > self->minPosition) && (tmpCurPos > self->minPosition && tmpCurPos < LOWER_CUT)) || ((LOWER_CUT < self->minPosition) && (tmpCurPos > self->minPosition || tmpCurPos < LOWER_CUT))){
		// positionError must be a negative value (i.e. we need to move in the negative direction through self->minPosition)
		if (tmpPositionError > 0){
			positionError = 2*M_PI - tmpPositionError;
		}
	}
	// Or between Upper Lim and Upper Cut-off?
	else if (((UPPER_CUT < self->maxPosition) && (tmpCurPos < self->maxPosition && tmpCurPos > UPPER_CUT)) || ((UPPER_CUT > self->maxPosition) && (tmpCurPos < self->maxPosition || tmpCurPos > UPPER_CUT))){
		// positionError must be a positive value
		if (tmpPositionError < 0){
			positionError = 2*M_PI + tmpPositionError;
		}
	}
	// Or in an impossible point between the cut-offs (still check to account for small errors)
	else if (((UPPER_CUT > LOWER_CUT) && (tmpCurPos < UPPER_CUT && tmpCurPos > LOWER_CUT)) || ((UPPER_CUT < LOWER_CUT) && (tmpCurPos < UPPER_CUT || tmpCurPos > LOWER_CUT))){

		// set flag

	}
	// Else, we are in an OK area and proceed as normal
	else {
		// Check if the no-go zone (zone between upper and lower limit) is between the shortest distance from current to corrected set position (we really just check for one of the limits)
		float tmpLimAngle = wrapAngle(self->minPosition - tmpCurPos); // shortest distance to lower limit
		// if tmpLimAngle is in the same direction and less than tmpPositionError, it will be traversed - so we must go the other way
		if ((tmpPositionError * tmpLimAngle > 0) && (fabs(tmpLimAngle) < fabs(tmpPositionError))) {
			if (tmpPositionError > 0){
				positionError = -(2 * M_PI - tmpPositionError);
			}
			else {
				positionError = 2 * M_PI - fabs(tmpPositionError);
			}
		}
		else {
			positionError = tmpPositionError;
		}

	}


/* End Limiting Code under test */



//	float positionError = wrapAngle(self->positionSetPoint - self->Position);

	float velocityError = self->velocitySetPoint - self->Velocity;

	self->IntegralError += self->timestep*positionError;
	if( self->IntegralError > IntegralErrorLim ){self->IntegralError = IntegralErrorLim;}
	if( self->IntegralError < -IntegralErrorLim ){self->IntegralError = -IntegralErrorLim;}

//	float s = lambda*lambda*self->IntegralError + velocityError + 2*lambda*positionError;
//	float s = lambda*lambda*self->IntegralError + velocityError + 2*lambda*positionError;
//	float s = velocityError + lambda*positionError;
//	float signS = s/fabs(s);

//	w = -w + self->timestep*1.1*eta*signS;

//	float output = 0.0;

//	output = sqrt(eta)*sqrt(fabs(s))*signS - w;
//	output = sqrt(eta)*s/(fabs(s)+epsilon) ;

	float output = self->Kp*positionError +
					self->Kd*velocityError +
					self->Ki*self->IntegralError;

	return output;
}

float correctPositionSetPoint(float tmpPositionSetPoint, float upperLim, float lowerLim, float tolerance){

	float correctedPositionSetPoint = 0;

	if ((upperLim > lowerLim) && (tmpPositionSetPoint > lowerLim && tmpPositionSetPoint < upperLim)){
		// Go to closest position limit to positionSetPoint
		if (smallestAngleBetween(tmpPositionSetPoint,lowerLim) < smallestAngleBetween(tmpPositionSetPoint,upperLim)){
			correctedPositionSetPoint = lowerLim - tolerance;
		}
		else {
			correctedPositionSetPoint = upperLim + tolerance;
		}
	}
	else if ((upperLim < lowerLim) && (tmpPositionSetPoint > lowerLim || tmpPositionSetPoint < upperLim)){
		// Go to closest position limit to positionSetPoint
		if (smallestAngleBetween(tmpPositionSetPoint,lowerLim) < smallestAngleBetween(tmpPositionSetPoint,upperLim)){
			correctedPositionSetPoint = lowerLim - tolerance;
		}
		else {
			correctedPositionSetPoint = upperLim + tolerance;
		}
	}
	else {
		correctedPositionSetPoint = tmpPositionSetPoint;
	}

	return correctedPositionSetPoint;

}


// Open loop controller
//Checks limits and stes output to open loop setpoint
float controller_Openloop(controller_t* self){

	float output=self->openloopsetpoint;
//	if((self->Position > self->maxPosition && output>0) || (self->Position < self->minPosition && output<0))
//		{
//		output=0;
//		}

	float tmpUpperLimitDistance=wrapAngle(self->Position-self->maxPosition);
	float tmpLowerLimitDistance=wrapAngle(self->Position-self->minPosition);

	if(tmpUpperLimitDistance<0 && output<0)
	{
		output =0 ;
	}
	if(tmpLowerLimitDistance>0 && output>0)
	{
		output=0;
	}

	if (output > self->voltage_max){
		output = self->voltage_max;
	}
	else if (output < self->voltage_min){
		output = self->voltage_min;
	}


	return output;
}

// Velocity Controller
float controller_Velocity(controller_t* self){

	  float output=0;

	  float velocityError= self->velocitySetPoint-self->Velocity;
	  self->velocityIntegralError+=velocityError;
	  output=velocityError*self->Kvp+self->velocityIntegralError*self->Kvi+self->velocitySetPoint*self->Kvf;

	float tmpUpperLimitDistance=wrapAngle(self->Position-self->maxPosition);
	float tmpLowerLimitDistance=wrapAngle(self->Position-self->minPosition);

	if(tmpUpperLimitDistance<0 && output<0)
	{
		output =0 ;
		self->velocityIntegralError=0;
	}
	if(tmpLowerLimitDistance>0 && output>0)
	{
		output=0;
		self->velocityIntegralError=0;
	}

  return output;

}

float controller_Current(controller_t* self)
{
	return 0;
}


void controller_setGains(controller_t* self, float Kp, float Kd, float Ki,float Kvp, float Kvd, float Kvf)
{
	self->Kd = Kd;
	self->Kp = Kp;
	self->Ki = Ki;
	self->Kvi = 0.01; //Integral Velocity Gain
	self->Kvp = 4;   //Proportional Velocity Gain
	self->Kvf = Kvf; //Feedforward Velocity Gain
}

void positionController_setKp(controller_t* self, float Kp){
	self->Kp = Kp;
}
void positionController_setKi(controller_t* self, float Ki){
	self->Ki = Ki;
}
void positionController_setKd(controller_t* self, float Kd){
	self->Kd = Kd;
}
float positionController_readKp(controller_t* self){
	return self->Kp;
}
float positionController_readKi(controller_t* self){
	return self->Ki;
}
float positionController_readKd(controller_t* self){
	return self->Kd;
}

float positionController_readPositionEst(controller_t* self){
	return self->Position;
}

float positionController_readVelocityEst(controller_t* self){
	return self->Velocity;
}

// acceleration and velocity limits
void positionController_setMaxVelocity(controller_t* self, float val){
	 self->vel_max = val;
}
void positionController_setMinVelocity(controller_t* self, float val){
	 self->vel_min = val;
}
void positionController_setMaxAcceleration(controller_t* self, float val){
	 self->acc_max = val;
}
void positionController_setMinAcceleration(controller_t* self, float val){
	 self->acc_min = val;
}
void positionController_setMaxPosition(controller_t* self, float val){
	self->maxPosition = val;
}
void positionController_setMinPosition(controller_t* self, float val){
	self->minPosition = val;
}
void positionController_positionOffset(controller_t* self, float val){
	self->PositionOffset = val;
}
