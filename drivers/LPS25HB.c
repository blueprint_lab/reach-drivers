/*
 * LPS25HB.c
 *
 *  Created on: Jun 19, 2020
 *      Author: Shaun Barlow
 */


#include "Hardware/LPS25HB.h"

#define TEMP_OFFSET 42.5

// Private functions
// Functions that outside modules do not need to access.s
int32_t computeTwosComp(uint32_t val, uint8_t bits);
void setPressure(LPS25HB_t* self, uint32_t rawPressure);
void setTemperature(LPS25HB_t* self, uint16_t rawTemperature);
void LPS25HB_error(LPS25HB_t* self);


void LPS25HB_construct(LPS25HB_t* self, I2C_HandleTypeDef* phi2c) {
	self->phi2c = phi2c;
	self->address = ONE;
}

void LPS25HB_init(LPS25HB_t* self) {
	uint8_t timeout = 200;
	uint8_t dataLength = 2;
	uint8_t data[dataLength];

	// setup continuous update
	data[0] = CTRL_REG1;
	data[1] = 0x90;
	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		LPS25HB_error(self);
	}
}

void LPS25HB_read(LPS25HB_t* self) {
	uint8_t timeout = 200;
	uint8_t dataLength = 10;
	uint8_t data[dataLength];

	// read pressure registers
	dataLength = 1;
	data[0] = PRESS_OUT_XL;
	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		LPS25HB_error(self);
	}

	dataLength = 3;
	if (HAL_I2C_Master_Receive(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		LPS25HB_error(self);
	}

	uint32_t pressureRegs = (data[2] << 16) | (data[1] << 8) | data[0];
	setPressure(self, pressureRegs);

	// read temperature registers
	dataLength = 1;
	data[0] = TEMP_OUT_L;
	if (HAL_I2C_Master_Transmit(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		LPS25HB_error(self);
	}

	dataLength = 2;
	if (HAL_I2C_Master_Receive(self->phi2c, self->address, data, dataLength, timeout) != HAL_OK) {
		LPS25HB_error(self);
	}

	uint32_t temperatureRegs = (data[1] << 8) | data[0];
	setTemperature(self, temperatureRegs);
}

void LPS25HB_readDMA(LPS25HB_t* self) {
}

void LPS25HB_onRxCplt(LPS25HB_t* self, uint8_t address) {
}

void setPressure(LPS25HB_t* self, uint32_t rawPressure) {
	uint8_t numBits = 24;
	int32_t signedRaw = computeTwosComp(rawPressure, numBits);

	// Convert data to Bar
	float pressure = 0.001 * signedRaw / 4096.0;
	self->pressure = pressure;
}

float LPS25HB_getPressure(LPS25HB_t* self) {
	return self->pressure;
}

void setTemperature(LPS25HB_t* self, uint16_t rawTemperature) {
	uint8_t numBits = 16;
	int32_t signedRaw = computeTwosComp(rawTemperature, numBits);

	// Convert data to degrees C
	float temperature = TEMP_OFFSET + (signedRaw / 480.);
	self->temperature = temperature;
}

float LPS25HB_getTemperature(LPS25HB_t* self) {
	return self->temperature;
}

void LPS25HB_error(LPS25HB_t* self) {
}

/**
 * @brief	Compute the twos complement of int value val
 */
int32_t computeTwosComp(uint32_t val, uint8_t bits) {
	if ((val & (1 << (bits - 1))) != 0) {  	// if sign bit is set e.g., 8bit: 128-255
		val = val - (1 << bits);			// compute negative value
	}
	return val;
}
