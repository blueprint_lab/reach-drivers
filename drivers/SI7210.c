/*
 * SI7210.c
 *
 * SI7210 Sensor driver
 *
 * Data sheet: https://www.silabs.com/documents/public/data-sheets/si7210-datasheet.pdf
 * App. notes: https://www.silabs.com/documents/public/application-notes/an1018-si72xx-sensors.pdf
 *
 * Driver adapated: https://github.com/FARLY7/si7210-driver
 *
 *  Created on: 08-11-2021
 *      Author: Kyle McLean
 */



#include <stddef.h>
#include <string.h>
#include <math.h>

#include "DataStructures/auxMath.h"
#include "Communication/I2C.h"
#include "Hardware/SI7210.h"

#define SCALE_THRSHOLD 5

/* Si7210 Register addresses */
#define SI72XX_HREVID       0xC0U
#define SI72XX_DSPSIGM      0xC1U
#define SI72XX_DSPSIGL      0xC2U
#define SI72XX_DSPSIGSEL    0xC3U
#define SI72XX_POWER_CTRL   0xC4U
#define SI72XX_ARAUTOINC    0xC5U
#define SI72XX_CTRL1        0xC6U
#define SI72XX_CTRL2        0xC7U
#define SI72XX_SLTIME       0xC8U
#define SI72XX_CTRL3        0xC9U
#define SI72XX_A0           0xCAU
#define SI72XX_A1           0xCBU
#define SI72XX_A2           0xCCU
#define SI72XX_CTRL4        0xCDU
#define SI72XX_A3           0xCEU
#define SI72XX_A4           0xCFU
#define SI72XX_A5           0xD0U
#define SI72XX_OTP_ADDR     0xE1U
#define SI72XX_OTP_DATA     0xE2U
#define SI72XX_OTP_CTRL     0xE3U
#define SI72XX_TM_FG        0xE4U

/* Si7210 Register bit masks */
#define CHIP_ID_MASK        0xF0U
#define REV_ID_MASK         0x0FU
#define DSP_SIGSEL_MASK     0x07U
#define MEAS_MASK           0x80U
#define USESTORE_MASK       0x08U
#define ONEBURST_MASK       0x04U
#define STOP_MASK           0x02U
#define SLEEP_MASK          0x01U
#define ARAUTOINC_MASK      0x01U
#define SW_LOW4FIELD_MASK   0x80U
#define SW_OP_MASK          0x7FU
#define SW_FIELDPOLSEL_MASK 0xC0U
#define SW_HYST_MASK        0x3FU
#define SW_TAMPER_MASK      0xFCU
#define SL_FAST_MASK        0x02U
#define SL_TIMEENA_MASK     0x01U
#define DF_BURSTSIZE_MASK   0xE0U
#define DF_BW_MASK          0x1EU
#define DF_IIR_MASK         0x01U
#define OTP_READ_EN_MASK    0x02U
#define OTP_BUSY_MASK       0x01U
#define TM_FG_MASK          0x03U

#define DSP_SIGM_DATA_FLAG      0x80U
#define DSP_SIGM_DATA_MASK      0x7FU
#define DSP_SIGSEL_TEMP_MASK    0x01U
#define DSP_SIGSEL_FIELD_MASK   0x04U

/* Burst sizes */
#define DF_BW_1             0x0U << 1
#define DF_BW_2             0x1U << 1
#define DF_BW_4             0x2U << 1
#define DF_BW_8             0x3U << 1
#define DF_BW_16            0x4U << 1
#define DF_BW_32            0x5U << 1
#define DF_BW_64            0x6U << 1
#define DF_BW_128           0x7U << 1
#define DF_BW_256           0x8U << 1
#define DF_BW_512           0x9U << 1
#define DF_BW_1024          0xAU << 1
#define DF_BW_2048          0xBU << 1
#define DF_BW_4096          0xCU << 1
#define DF_BURSTSIZE_1      0x0U << 5
#define DF_BURSTSIZE_2      0x1U << 5
#define DF_BURSTSIZE_4      0x2U << 5
#define DF_BURSTSIZE_8      0x3U << 5
#define DF_BURSTSIZE_16     0x4U << 5
#define DF_BURSTSIZE_32     0x5U << 5
#define DF_BURSTSIZE_64     0x6U << 5
#define DF_BURSTSIZE_128    0x7U << 5

/* Private funtions */
si7210_status_t 		si7210_blocking_read(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len);
si7210_status_t 		si7210_blocking_write(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len);

si7210_status_t 		si7210_dma_read(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);
si7210_status_t 		si7210_dma_write(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);

si7210_status_t 		si7210_DMARxTxCplt(si7210_t *self, I2C_HandleTypeDef *hi2c, I2C_txRxDirection_t transferDirection);

si7210_status_t 		si7210_config_field_measurement(si7210_t *self);

static si7210_status_t 	si7210_read_temperature_calib_values(si7210_t *self);
static si7210_status_t 	si7210_load_compensation_values(si7210_t *self);
static si7210_status_t 	null_ptr_check(const si7210_t *self);


/*
 * @brief Construct the device interface.
 *
 * @param[in] self : Si7210 structure
 *
 * @return status
 * @retval si7210_status
 */
si7210_status_t si7210_construct(si7210_t *self, I2C_HandleTypeDef *phi2c) {
    si7210_status_t rslt 		= SI7210_OK;

    self->Init 					= si7210_init;

    self->settings.range        = SI7210_200mT;
    self->settings.compensation = SI7210_COMPENSATION_TEMP_NEO;
    self->settings.output_pin   = SI7210_OUTPUT_PIN_HIGH;

    self->dev_id   				= 0xFF;

    self->phi2c     			= phi2c;

    self->calculateRange		= si7210_calculate_range;
    self->calculateTemp			= si7210_calculate_temperature;

    self->read     				= si7210_blocking_read;
    self->write    				= si7210_blocking_write;

    self->readDMA   			= si7210_dma_read;
    self->writeDMA  			= si7210_dma_write;

    self->RxTxCpltCallback 		= si7210_DMARxTxCplt;

    self->state 				= SI7210_IDLE;

	self->rangeAlpha			= 0.3;

    self->initialized 			= 0;

    return rslt;
}

/*
 * @brief Initialise the device and check if responding.
 *
 * @param[in] self : Si7210 structure
 *
 * @return status
 * @retval si7210_status
 */
si7210_status_t si7210_init(si7210_t *self) {
    si7210_status_t rslt;

    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt;  												/* Check for null pointer in device structure 					*/

    rslt = si7210_check(self);  																				/* Check if device is responding 								*/

    if(rslt != SI7210_OK) return SI7210_E_DEV_NOT_FOUND;  														/* Error: Timeout / device not found 							*/
    if((rslt = si7210_set_sensor_settings(self)) != SI7210_OK) return rslt;  									/* Set device and internal driver settings 						*/
    if((rslt != si7210_write_reg(self, SI72XX_CTRL3, (uint8_t) ~SL_TIMEENA_MASK, 0)) != SI7210_OK) return rslt; /* Disable periodic auto-wakeup by device, and tamper detect. 	*/

    rslt |= si7210_write_reg(self, SI72XX_CTRL3, SL_FAST_MASK | SL_TIMEENA_MASK, 63 << 2);  					/* Disable tamper detection by setting sw_tamper to 63 			*/

    if((rslt = si7210_config_field_measurement(self)) != SI7210_OK) return SI7210_E_IO;							/* configure measurements 										*/

    self->initialized = 1;

    self->enabled = 1;

    return rslt;
}

/*
 * @brief De-initialise the device and put it to sleep mode.
 *
 * @param[in] self : Si7210 device structure
 *
 * @return status
 * @retval si7210_status
 */
si7210_status_t si7210_deinit(si7210_t *self) {
    si7210_status_t rslt;

    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt;  												/* Check for null pointer in device structure 	*/


    rslt |= si7210_write_reg(self, SI72XX_CTRL3, (uint8_t) ~SL_TIMEENA_MASK, 0);  								/* Disable sleep timer 							*/
    rslt |= si7210_write_reg(self, SI72XX_POWER_CTRL, MEAS_MASK, SLEEP_MASK);     								/* Disable user store of values, and enter sleep mode with no measurements */

    if(rslt != SI7210_OK) return SI7210_E_IO;

    return rslt;
}

/*
 * @brief Change the settings of the device and internal driver.
 *
 * @param[in] self : Si7210 device structure
 *
 * @return result of API execution status
 * @retval si7210_status
 */
si7210_status_t si7210_set_sensor_settings(si7210_t *self) {
    si7210_status_t rslt;

    /* Range - used internally, no need to change device. */
    /* Compensation - used internally, no need to change device. */

    /* Set sw_low4field bit if output pin is in LOW configuration.
     * Clear sw_low4field bit if output is in HIGH configuration.*/
    uint8_t ctrl1 = 0;  /* Output pin */
    switch(self->settings.output_pin) {
        case SI7210_OUTPUT_PIN_LOW:  ctrl1 |= SW_LOW4FIELD_MASK;  break;
        case SI7210_OUTPUT_PIN_HIGH: ctrl1 &= ~SW_LOW4FIELD_MASK; break;
    }

    /* Set SW_LOW4FIELD */
    rslt = si7210_write_reg(self, SI72XX_CTRL1, (uint8_t) ~SW_LOW4FIELD_MASK, ctrl1);

    return rslt;
}


/*
 * @brief Configure device for feild strength measumrents.
 *
 * @param[in]  self : Si7210 device structure.
 *
 * @return status
 * @retval si7210_status
 */
si7210_status_t si7210_config_field_measurement(si7210_t *self) {
    si7210_status_t rslt;

    /* Check for null pointer in device structure */
    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt;

    /* Stop the control loop by setting stop bit - WARNING: Removed USE_STORE MASK*/
    if((rslt = si7210_write_reg(self, SI72XX_POWER_CTRL, MEAS_MASK | USESTORE_MASK, STOP_MASK)) != SI7210_OK) return rslt;

    /* Load compensation values from OTP registers, to be used in measurement. */
    if((rslt = si7210_load_compensation_values(self)) != SI7210_OK) return rslt;

    /* Use a burst size of 1/4096 samples FIR mode */
    rslt = si7210_write_reg(self, SI72XX_CTRL4, 0, DF_BURSTSIZE_128 | DF_BW_4096);

    return rslt;
}


float si7210_compute_distance(float x, float x2, float x1, float y2, float y1) {
	return sqrtf(1/(((1/(y2*y2) - 1/(y1*y1)) / (x2 - x1) ) * (x - x1) + 1/(y1*y1)));
}

/*
 * @brief Get the last measured field strength from the device. The value is correctly compensated.
 *
 * @param[in]  self : Si7210 device structure.
 *
 * Note: set to optimisation level zero to prevent removal of NaN check
 *
 * @return status
 * @retval si7210_status
 */
si7210_status_t __attribute__((optimize("O0")))si7210_calculate_range(si7210_t *self) {
	static uint32_t startTicks = 0;

	si7210_status_t rslt;
    uint8_t val = 0;

    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt; 											/* Check for null pointer in device structure 	*/

    switch(self->state) {
		case SI7210_IDLE:
			rslt |= si7210_write_reg(self, SI72XX_DSPSIGSEL, 0, DSP_SIGSEL_FIELD_MASK);						/* Selet field strength measurement 			*/
			rslt |= si7210_write_reg(self, SI72XX_POWER_CTRL, MEAS_MASK | USESTORE_MASK, ONEBURST_MASK);	/* Start measurement 							*/

			if(rslt != SI7210_OK) return SI7210_E_IO;  														/* Error starting measurement     				*/

			self->state = SI7210_CALCULATE_FIELD;
		case SI7210_CALCULATE_FIELD:
			/*==========================================================================================================
			 * For calcualtions see data sheet: https://www.silabs.com/documents/public/data-sheets/si7210-datasheet.pdf
			 * =========================================================================================================*/

			if((rslt = si7210_read_reg(self, SI72XX_DSPSIGM, &val)) != SI7210_OK) return rslt; 				/* Read most-significant byte 					*/
			if((val & DSP_SIGM_DATA_FLAG) != DSP_SIGM_DATA_FLAG) return SI7210_OK;  						/* Timeout error 								*/

			int32_t value = 256 * (val & DSP_SIGM_DATA_MASK);

			/* Read least-significant byte of data */
			if((rslt = si7210_read_reg(self, SI72XX_DSPSIGL, &val)) != SI7210_OK) return rslt;

			value += val;
			value -= 16384U;

			float raw_field = fabsf((float) value);

			float max_m = self->max_flux;
			float min_m = self->min_flux;

			float min_d = self->min_distance + self->offset;
			float max_d = self->max_distance + self->offset;

			float tmp_prevFlux = self->flux;

			if(self->settings.range == SI7210_20mT) self->flux = raw_field * 0.00125; 						/* rawField * 1.25 								*/
			else if(self->settings.range == SI7210_200mT) self->flux = raw_field * 0.0125; 					/* rawField * 12.5 								*/
			else rslt = SI7210_E_INVALID_ARG;

			/* Calculate the filter frequency based on the sample frequency and controller frequency */
			double tmp_dt = ((double)getElapsedTicks(startTicks, uwTick) / (double)(HAL_TICK_FREQ_DEFAULT * 1000));
			self->conversionFreq = 1/tmp_dt;
		    startTicks = uwTick;
		    self->rangeAlpha = (float)1/((0.5 * self->conversionFreq / 1000) + 1);

		    /* Calculate the range from flux measument */
		    self->prevRange = self->range;

			self->range = lpFilter(self->range, si7210_compute_distance(self->flux, min_m, max_m, max_d, min_d) - self->offset, self->rangeAlpha);
			if (!(self->range == self->range)) self->range = self->max_distance; // Check for NAN values

			self->velocity = lpFilter((self->range - self->prevRange)/tmp_dt, self->velocity, self->rangeAlpha/10);

			/* Calcualte rate of change in flux */
			self->deltaSqrtFlux = lpFilter(self->deltaSqrtFlux, sqrt(self->flux - tmp_prevFlux)  / (float)tmp_dt, self->rangeAlpha/100);

			rslt |= si7210_read_reg(self, SI72XX_CTRL1, &val);  											/* Set SW_OP and SW_LOW4FIELD 					*/
			rslt |= si7210_read_reg(self, SI72XX_CTRL2, &val);  											/* Set SW_HYST (Hysterisis) to 0 				*/

			self->state = SI7210_IDLE;
			break;
		default:
			break;
    }

//    if (self->flux < SCALE_THRSHOLD && self->settings.range == SI7210_200mT) {
//    	self->settings.range = SI7210_20mT;
//    	if((rslt = si7210_config_field_measurement(self)) != SI7210_OK) return SI7210_E_IO;					/* configure measurements 										*/
//    } else if (self->flux >= SCALE_THRSHOLD && self->settings.range == SI7210_20mT) {
//    	self->settings.range = SI7210_200mT;
//    	if((rslt = si7210_config_field_measurement(self)) != SI7210_OK) return SI7210_E_IO;					/* configure measurements 										*/
//    }


    return rslt;
}

/*
 * @brief Get the last measured temperature from the device.
 *        The value is correctly compensated.
 *
 * @param[in]  self         : Si7210 device structure.
 * @param[out] temperature : Pointer to value which is to be written.
 *
 * @return Success of operation
 * @retval si7210_status
 */
si7210_status_t si7210_calculate_temperature(si7210_t *self) {
    uint8_t val = 0;
    si7210_status_t rslt;

    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt;												/* Check for null pointer in device structure */

    switch(self->state) {
    	case SI7210_IDLE:
			rslt |= si7210_write_reg(self, SI72XX_DSPSIGSEL, 0, DSP_SIGSEL_TEMP_MASK);						/* Select temperature measurement 	*/
			rslt |= si7210_write_reg(self, SI72XX_POWER_CTRL, (uint8_t) ~(STOP_MASK), ONEBURST_MASK);		/* Start temperature measurement  	*/

			if(rslt != SI7210_OK) return SI7210_E_IO;  														/* Error starting measurement     	*/

			self->state = SI7210_CALCULATE_TEMP;
			break;
		case SI7210_CALCULATE_TEMP:
			/*==========================================================================================================
			 * For calcualtions see data sheet: https://www.silabs.com/documents/public/data-sheets/si7210-datasheet.pdf
			 * =========================================================================================================*/

			if((rslt = si7210_read_reg(self, SI72XX_DSPSIGM, &val)) != SI7210_OK) return rslt; 				/* Read most-significant byte 		*/
			if((val & DSP_SIGM_DATA_FLAG) != DSP_SIGM_DATA_FLAG) return SI7210_OK;  						/* Timeout error 					*/

			int32_t value = 32 * (val & DSP_SIGM_DATA_MASK);

			if((rslt = si7210_read_reg(self, SI72XX_DSPSIGL, &val)) != SI7210_OK) return rslt;  			/* Read the least-significant byte 	*/

			value += (val >> 3);

			/* If no offset and gain values exist, read them now. */
			if (self->calib_data.temperature_offset == 0 && self->calib_data.temperature_gain == 0) {
				if((rslt = si7210_read_temperature_calib_values(self)) != SI7210_OK) return rslt;
			}

			/* Error reading OTP values */
			if(rslt != SI7210_OK) return SI7210_E_IO;

			float temp_c = (float) value;
			float offset = self->calib_data.temperature_offset / 16.0;
			float gain   = 1 + (self->calib_data.temperature_gain / 2048.0);

			temp_c = gain * (-3.83e-6F * temp_c * temp_c + 0.16094F * temp_c - 279.80F - 0.222F * 3.3F) + offset;

			self->temperature = temp_c;

			self->state = SI7210_IDLE;
			break;
		default:
			break;

    }

    return rslt;
}

/*
 * @brief Put the device into it's independent sleep/measure cycle.
 *        The device will continue to periodically measure the field and
 *        trigger the output pin when the threshold is crossed.
 *
 * @param[in]  self : Si7210 device structure.
 *
 * @return status
 * @retval si7210_status
 */
si7210_status_t si7210_start_periodic_measurement(si7210_t *self) {
    si7210_status_t rslt;

    /* Check for null pointer in device structure */
    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt;

    /* Enable periodic wakeup */
    if((rslt = si7210_write_reg(self, SI72XX_CTRL3, (uint8_t) ~SL_TIMEENA_MASK, SL_TIMEENA_MASK)) != SI7210_OK) return rslt;

    /* Start measurement - Change to ~STOP_MASK with STOP_MASK */
    if((rslt = si7210_write_reg(self, SI72XX_POWER_CTRL, MEAS_MASK | USESTORE_MASK, 0)) != SI7210_OK) return rslt;

    return rslt;
}

/*
 * @brief Reads a register from Si7210 device.
 *
 * @param[in] self : Si7210 device structure.
 * @param[in] reg : Register number to read from.
 * @param[out] data : Pointer to where register value is to be stored.
 *
 * @return status
 * @retval si7210_status
 */
si7210_status_t si7210_read_reg(si7210_t *self, uint8_t reg, uint8_t *val) {
    si7210_status_t rslt;

    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt; 	/* Check for null pointer in device structure */
    rslt = self->read(self->phi2c, self->dev_id, reg, val, 1);		/* Call user implemented method for I2C read */

    return rslt;
}

/*
 * @brief Blocking read from Si7210 device.
 */
si7210_status_t si7210_blocking_read(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len) {
	uint16_t DevAddress = ((uint16_t) dev_id) << 1;
	uint32_t Timeout = 100;

	HAL_StatusTypeDef status;

	status = HAL_I2C_Master_Transmit(phi2c, DevAddress, &reg_addr, sizeof(reg_addr), Timeout);
	if (status != HAL_OK) return SI7210_E_IO;

	status = HAL_I2C_Master_Receive(phi2c, DevAddress, data, len, Timeout);
	if (status != HAL_OK) return SI7210_E_IO;

	return SI7210_OK;
}

/*
 * @brief Non-blocking read from Si7210 device.
 */
si7210_status_t si7210_dma_read(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len) {
//	HAL_StatusTypeDef status;
//
//	status = I2C_writeDMA(phi2c, dev_id, &reg_addr, len);
//
//	status = I2C_readDMA(phi2c, dev_id, &reg_data, len);
//
//	if (status != HAL_OK) return SI7210_E_IO;
	return SI7210_OK;
}

/*
 * @brief Write a value to a register of Si7210 device.
 *
 * @param[in] self : Si7210 device structure.
 * @param[in] reg : Register number to write to.
 * @param[in] mask : 1-byte mask used to keep original register bits.
 * @param[in] data : 1-byte data to write to register.
 *
 * @return status
 * @retval si7210_status
 */
si7210_status_t si7210_write_reg(si7210_t *self, uint8_t reg, uint8_t mask, uint8_t val) {
    si7210_status_t rslt;
    uint8_t temp_val = 0;

    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt;  /* Check for null pointer in device structure */

    /* A mask has been used, must read original value from register. */
    if(mask != 0) {
        /* Call user implemented method for I2C read */
        if((rslt = self->read(self->phi2c, self->dev_id, reg, &temp_val, 1)) != SI7210_OK) return rslt;
        temp_val &= mask;
    }

    temp_val |= val;

    /* Call method for I2C write */
    if((rslt = self->write(self->phi2c, self->dev_id, reg, &temp_val, 1)) != SI7210_OK) return rslt;

    return rslt;
}

/*
 * @brief Blocking write to Si7210 device.
 */
si7210_status_t si7210_blocking_write(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len) {
	uint8_t buffer[len + sizeof(reg_addr)];

	uint16_t DevAddress = (uint16_t) dev_id << 1;
	uint32_t Timeout = 100;

	HAL_StatusTypeDef status;

	memcpy(buffer, &reg_addr, sizeof(reg_addr));				/* Copy address to start of buffer */
	memcpy(buffer + sizeof(reg_addr), reg_data, len);		    /* Copy data into buffer 		   */

	status = HAL_I2C_Master_Transmit(phi2c, DevAddress,  buffer, len + sizeof(reg_addr), Timeout);

	if (status != HAL_OK) return SI7210_E_IO;

	return SI7210_OK;
}

/*
 * @brief Non-blocking write to Si7210 device.
 */
#define MAX_BUF 10
si7210_status_t si7210_dma_write(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len) {
	static uint8_t buffer[MAX_BUF + sizeof(reg_addr)];		/* Create a static buffer so we can use the pointer */

	HAL_StatusTypeDef status;

	if (len > MAX_BUF) return SI7210_E_IO;					/* Limit buffer data to 10 bytes   */

	memcpy(buffer, &reg_addr, sizeof(reg_addr));			/* Copy address to start of buffer */
	memcpy(buffer + sizeof(reg_addr), reg_data, len);		/* Copy data into buffer 		   */

	status = I2C_writeDMA(phi2c, dev_id, buffer, len);

	if (status != HAL_OK) return SI7210_E_IO;

	return SI7210_OK;
}

/*
 * @brief	To be called in both HAL_I2C_MasterTxCpltCallback() and HAL_I2C_MasterRxCpltCallback().
 * @param	transferDirection determines whether this is from the Tx or Rx callbacks.
 * @retval	I2C_CallbackStatus indicates whether callback is for this module instance.
 */
si7210_status_t si7210_DMARxTxCplt(si7210_t *self, I2C_HandleTypeDef *hi2c, I2C_txRxDirection_t transferDirection) {
	if (self->phi2c != hi2c) { return I2C_NOT_MY_CALLBACK; } // Not my callback
	if ((self->phi2c->Devaddress >> 1) != self->dev_id) { return  I2C_NOT_MY_CALLBACK; } // Not my callback


	return SI7210_OK;
}

/*
 * @brief Check if the device is responding.
 *
 * @param[in] self : Si7210 device structure.
 *
 * @return status
 * @retval si7210_status
 */
si7210_status_t __attribute__((optimize("O0")))si7210_check(si7210_t *self) {
    si7210_status_t rslt = SI7210_OK;
    uint8_t temp;

    /* Check for null pointer in device structure */
    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt;

    for (uint8_t i = 0; i < 0xFF; i++) {
    	/* Read the device ID and revision fields to check comms. 		*/
		if ((rslt = self->read(self->phi2c, SI7210_ADDRESS_0, SI72XX_HREVID, &temp, 1)) == SI7210_OK) self->dev_id=SI7210_ADDRESS_0;
		else if ((rslt = self->read(self->phi2c, SI7210_ADDRESS_1, SI72XX_HREVID, &temp, 1)) == SI7210_OK) self->dev_id=SI7210_ADDRESS_1;
		else if ((rslt = self->read(self->phi2c, SI7210_ADDRESS_2, SI72XX_HREVID, &temp, 1)) == SI7210_OK) self->dev_id=SI7210_ADDRESS_2;
		else if ((rslt = self->read(self->phi2c, SI7210_ADDRESS_3, SI72XX_HREVID, &temp, 1)) == SI7210_OK) self->dev_id=SI7210_ADDRESS_3;

		if (self->dev_id != 0xFF) break;

		/* Wake device up incase it is sleeping. 						*/
		if((rslt = si7210_wakeup(self)) != SI7210_OK) return rslt;
    }

    return rslt;
}


/*
 * @brief Put the device into SLEEP mode.
 *
 * @param[in] self : Si7210 device structure.
 *
 * @return Success of operation.
 * @retval si7210_status
 */
si7210_status_t si7210_sleep(si7210_t *self) {
    uint8_t temp;

    si7210_status_t rslt;

    /* Check for null pointer in device structure */
    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt;


    rslt |= si7210_read_reg(self, SI72XX_CTRL3, &temp); 				/* Read CTRL3 register */

    temp &= 0xFEU; 														/* Clear SLTIMENA bit of CTRL3 register */
    rslt |= si7210_write_reg(self, SI72XX_CTRL3, 0, temp); 				/* Write back new CTRL3 register value  */
    rslt |= si7210_read_reg(self, SI72XX_POWER_CTRL, &temp);			/* Read POWER_CTRL register */

    temp = (temp & 0xF8U) | 0x01; 										/* Clear STOP and set SLEEP bits */
    rslt |= si7210_write_reg(self, SI72XX_POWER_CTRL, MEAS_MASK, temp); /* Write back POWER_CTRL register value */

    if(rslt != SI7210_OK) return SI7210_E_IO;

    return rslt;
}

/*
 * @brief Wake the device from SLEEP mode.
 *
 * @param[in] self : Si7210 device structure.
 * @param[in] reg : Register number to write to.
 * @param[in] data : 1-byte data to write to register.
 *
 * @return Success of operation.
 * @retval si7210_status
 */
si7210_status_t si7210_wakeup(si7210_t *self) {
    uint8_t temp = 0;
    si7210_status_t rslt;

    /* Check for null pointer in device structure */
    if((rslt = null_ptr_check(self)) != SI7210_OK) return rslt;

    /* Wake the device up by sending a WRITE request. call will fail as we are unable
     * to write to memory address 0x00, however it will wake the part up. */
    self->write(self->phi2c, SI7210_ADDRESS_0, 0x00, &temp, 1);
    self->write(self->phi2c, SI7210_ADDRESS_1, 0x00, &temp, 1);
    self->write(self->phi2c, SI7210_ADDRESS_2, 0x00, &temp, 1);
    self->write(self->phi2c, SI7210_ADDRESS_3, 0x00, &temp, 1);

    return rslt;
}

/*
 * @brief Self-test sequence offered by the device.
 *        It uses an internal coil to generate and test the + and - field.
 *
 * @note Important: There must not be any external magnetic field in the vicinity
 *       for the test to run successfully.
 *       The user must also ensure the correct Vdd value is defined in si7210_defs.h.
 *
 * @param[in] self : Si7210 device structure.
 *
 * @return Success of operation.
 * @retval si7210_status
 */
si7210_status_t si7210_self_test(si7210_t *self) {
    float field_pos, field_neg;
    si7210_status_t rslt;

    /* Check for null pointer in device structure */
    if((rslt = null_ptr_check(self)) != SI7210_OK)
        return rslt;

    /* Enable test field generator coil in POSITIVE direction. */
    rslt |= si7210_write_reg(self, SI72XX_TM_FG, 0, 1);

    /* Measure field strength */
    rslt |= si7210_calculate_range(self); // 200mT
    field_pos = self->flux;

    /* Enable test field generator coil in POSITIVE direction. */
    rslt |= si7210_write_reg(self, SI72XX_TM_FG, 0, 2);

    /* Measure field strength */
    rslt |= si7210_calculate_range(self); // 200mT
    field_neg = self->flux;

    /* Disable test field generator coil. */
    rslt |= si7210_write_reg(self, SI72XX_TM_FG, 0, 0);

    /* Error in performing self-test measurement. */
    if(rslt != SI7210_OK)
        return SI7210_E_IO;

    float b_out = 1.16 * SI7210_VDD;
    float b_upper = b_out + (b_out * 0.25); /* +25% */
    float b_lower = b_out - (b_out * 0.25); /* -25% */

    if( (field_pos <= b_upper) &&
        (field_pos >= b_lower) &&
        (field_neg >= (b_upper * -1)) &&
        (field_neg <= (b_lower * -1)))
    {
        rslt = SI7210_OK;
    }
    else
    {
        rslt = SI7210_E_SELF_TEST_FAIL;
    }

    return rslt;
}

/*
 * @brief Read the factory programmed temperature offset and gain adjustment values.
 */
static si7210_status_t si7210_read_temperature_calib_values(si7210_t *self) {
    si7210_status_t rslt = SI7210_OK;

    /* Read out compensation values */
    rslt |= si7210_write_reg(self, SI72XX_OTP_ADDR, 0, 0x1DU);
    rslt |= si7210_write_reg(self, SI72XX_OTP_CTRL, 0, OTP_READ_EN_MASK);
    rslt |= si7210_read_reg(self, SI72XX_OTP_DATA, (uint8_t*) &self->calib_data.temperature_offset);

    rslt |= si7210_write_reg(self, SI72XX_OTP_ADDR, 0, 0x1EU);
    rslt |= si7210_write_reg(self, SI72XX_OTP_CTRL, 0, OTP_READ_EN_MASK);
    rslt |= si7210_read_reg(self, SI72XX_OTP_DATA, (uint8_t*) &self->calib_data.temperature_gain);

    if(rslt != SI7210_OK)
        return SI7210_E_IO;

    return rslt;
}

/*
 * @brief Load the factory compensation data from OTP registers into run-time registers.
 */
static si7210_status_t si7210_load_compensation_values(si7210_t *self) {
    si7210_status_t rslt = SI7210_OK;
    uint8_t base_addr;
    uint8_t val;

    switch(self->settings.compensation)
    {
        case SI7210_COMPENSATION_NONE:     { base_addr = 0x21; break; }
        case SI7210_COMPENSATION_TEMP_NEO: { base_addr = 0x2D; break; }
        case SI7210_COMPENSATION_TEMP_CER: { base_addr = 0x39; break; }
        default: { return SI7210_E_SETTINGS; }
    }

    if(self->settings.range == SI7210_200mT)
        base_addr += 6;

    /* Load A0 register */
    rslt |= si7210_write_reg(self, SI72XX_OTP_ADDR, 0, base_addr);
    rslt |= si7210_write_reg(self, SI72XX_OTP_CTRL, 0, OTP_READ_EN_MASK);
    rslt |= si7210_read_reg (self, SI72XX_OTP_DATA, &val);
    rslt |= si7210_write_reg(self, SI72XX_A0, 0, val);

    /* Load A1 register */
    rslt |= si7210_write_reg(self, SI72XX_OTP_ADDR, 0, base_addr + 1);
    rslt |= si7210_write_reg(self, SI72XX_OTP_CTRL, 0, OTP_READ_EN_MASK);
    rslt |= si7210_read_reg (self, SI72XX_OTP_DATA, &val);
    rslt |= si7210_write_reg(self, SI72XX_A1, 0, val);

    /* Load A2 register */
    rslt |= si7210_write_reg(self, SI72XX_OTP_ADDR, 0, base_addr + 2);
    rslt |= si7210_write_reg(self, SI72XX_OTP_CTRL, 0, OTP_READ_EN_MASK);
    rslt |= si7210_read_reg (self, SI72XX_OTP_DATA, &val);
    rslt |= si7210_write_reg(self, SI72XX_A2, 0, val);

    /* Load A3 register */
    rslt |= si7210_write_reg(self, SI72XX_OTP_ADDR, 0, base_addr + 3);
    rslt |= si7210_write_reg(self, SI72XX_OTP_CTRL, 0, OTP_READ_EN_MASK);
    rslt |= si7210_read_reg (self, SI72XX_OTP_DATA, &val);
    rslt |= si7210_write_reg(self, SI72XX_A3, 0, val);

    /* Load A4 register */
    rslt |= si7210_write_reg(self, SI72XX_OTP_ADDR, 0, base_addr + 4);
    rslt |= si7210_write_reg(self, SI72XX_OTP_CTRL, 0, OTP_READ_EN_MASK);
    rslt |= si7210_read_reg (self, SI72XX_OTP_DATA, &val);
    rslt |= si7210_write_reg(self, SI72XX_A4, 0, val);

    /* Load A5 register */
    rslt |= si7210_write_reg(self, SI72XX_OTP_ADDR, 0, base_addr + 5);
    rslt |= si7210_write_reg(self, SI72XX_OTP_CTRL, 0, OTP_READ_EN_MASK);
    rslt |= si7210_read_reg (self, SI72XX_OTP_DATA, &val);
    rslt |= si7210_write_reg(self, SI72XX_A5, 0, val);

    if(rslt != SI7210_OK) return SI7210_E_IO;

    return rslt;
}

/*
 * @brief Used to validate the device pointer for null conditions.
 */
static si7210_status_t null_ptr_check(const si7210_t *self) {
    si7210_status_t rslt;

    if (self == NULL || self->read == NULL || self->write == NULL) {
        /* Device structure pointer is not valid */
        rslt = SI7210_E_NULL_PTR;
    } else {
        /* Device structure is fine */
        rslt = SI7210_OK;
    }

    return rslt;
}
