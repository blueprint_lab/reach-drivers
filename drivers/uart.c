/*
 * uart.c
 *
 *  Created on: 6 Jan 2017
 *      Author: p.phillips
 *
 *  Re-factored and modified by Kyle McLean - April 2021
 *		- Removal of redundant functionality
 *		- Added documentation
 */

#include <string.h>
#include <stdlib.h>
#include "Communication/uart.h"
#include "Communication/tm_stm32_crc.h"
#include "DataStructures/circularQueue.h"
#include "Communication/packetid.h"
#include "Communication/comsprocess.h"

coms_link* uart_GetLinkFromHuart(UART_HandleTypeDef* huart);
uint16_t uart_comms_GetTxQLength(coms_link* parent);
uint8_t uart_comms_handleBusState(coms_link* parent);

/**
 * @brief	Uart error callback implimentation
 * @param	huart pointer to UART_HandleTypeDef
 */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart) {
	uint32_t oldNDTR = huart->hdmarx->Instance->NDTR;
	HAL_UART_Receive_DMA(huart, (uint8_t*) huart->pRxBuffPtr, huart->RxXferSize);
	huart->hdmarx->Instance->NDTR = oldNDTR;
}

/**
 * @brief	Uart constructor
 * @param	self pointer to uart_comms_link
 * @param	huart pointer to UART_HandleTypeDef
 * @param	rxBuffLen rx buffer length to be allocated
 * @param	txBuffLen tx buffer length to be allocated
 * @param	protocolType used to select the correct function bindings
 * @retval 	returns zero 0n fail
 */
uint8_t uart_comms_construct(uart_comms_link* self, UART_HandleTypeDef* huart, uint16_t rxBuffLen, uint16_t txBuffLen, protocol_t protocolType) {
	if(protocolType==BPSS_PROTOCOL) { // TODO: Is this needed now?
//		self->Read = BPSS_protocol_decodePacket;
//		self->Transmit = uart_comms_TransmitData;
//		self->reset = uart_comms_reset;
//		self->enableRead = uart_comms_enableRead;
//		self->encodePacket = BPSS_protocol_encodePacket;
//		self->numPackets = BPSS_protocol_numPackets;
//		self->transmitDMA = uart_comms_TransmitDMA;
	}

	self->writePointer=0;
	self->readPointer=0;
	self->errorCount=0;
	self->rxBuffLen = rxBuffLen;
	self->txBuffLen = txBuffLen;
	self->huart = huart;
	self->serialType=RS232;
	self->status=0;
	self->packetEnd=0;

	// Allocate the data for the send and receive buffers
	self->rxBuff = (uint8_t*)calloc(rxBuffLen, sizeof(uint8_t));
	for(int i = 0; i< rxBuffLen; ++i){
		self->rxBuff[i] = 0xFF;
	}

	self->txBuff = (uint8_t*)calloc(txBuffLen, sizeof(uint8_t));

	// If memory failed to allocate
	if(self->rxBuff == NULL || self->txBuff == NULL){
		return 0;
	}

	// Initialize the send queue
	circularQueueConstruct(&(self->transmitQueue), txBuffLen);
	circularQueueConstruct(&(self->transmitQueue2), txBuffLen);

	return 1;
}

/**
 * @brief	Sets the usart/uart to RS485 and initialises pins and sets serialType to RS485
 * @param	self pointer to uart_comms_link
 * @param	RXE_Pin
 * @param	RXE_Port
 * @param	TXE_Pin
 * @param	TXE_Port
 */
void uart_comms_setRS485(uart_comms_link* self, uint16_t RXE_Pin, GPIO_TypeDef* RXE_Port, uint16_t TXE_Pin, GPIO_TypeDef* TXE_Port) {
	self->RXE_Pin = RXE_Pin;
	self->RXE_Port = RXE_Port;
	self->TXE_Pin = TXE_Pin;
	self->TXE_Port = TXE_Port;

	self->serialType = RS485;
}


/**
 * @brief	Enables reading from the UART specified in parent's uartLink.
 * 			Note: UART idle line interrupt has been disabled. Instead the DMA is
 * 			constantly shifting data from the uart to the rxBuff.
 *
 * @param	parent pointer to coms_link structure
 */
uint8_t uart_comms_enableRead(coms_link* parent) {
	uart_comms_link* self = parent->uartLink;

	/* Required because bootloader enables it and HAL does not clear it. */
	__HAL_UART_DISABLE_IT(self->huart, UART_IT_IDLE);
	if (HAL_UART_Receive_DMA(self->huart, (uint8_t*)self->rxBuff, self->rxBuffLen) == HAL_ERROR) {
		return 0;
	}

	return 1;
}

/* 	 Reads the next packet into the currPacket structure inside the object.
	 The packet is cleared from the receive buffer.

	 Returns 1 if a packet was read out successfully.
	 returns 0 if the receive buffer was empty or there was an error
*/


uint16_t uart_getInWaiting(coms_link* parent) {
	uart_comms_link* self = parent->uartLink;


	uint16_t inWaiting = 0;
	uint8_t writePointer = self->rxBuffLen - self->huart->hdmarx->Instance->NDTR;
	if (self->readPointer > writePointer) {
		// Buffer has wrapped around
		// Count number of bytes between readPointer and end
		inWaiting = self->rxBuffLen - self->readPointer;
		// PLUS
		// Number of bytes between start and writePointer
		inWaiting += writePointer;
	} else {
		// Buffer has not wrapped
		// Count number of bytes between readPointer and writePointer
		inWaiting = writePointer - self->readPointer;
	}
	return inWaiting;
}

uint8_t uart_ReadBytes(coms_link* parent, uint8_t* buffer, uint8_t numBytes) {
	uart_comms_link* self = parent->uartLink;
	// Refactor uart.c to use a circularQueue for rx.

	self->writePointer = self->rxBuffLen - self->huart->hdmarx->Instance->NDTR;
	uint16_t writePointCopy = self->writePointer;
	if (writePointCopy == self->readPointer) {
		return 0;
	}
	uint16_t i;
	for (i=0; i<numBytes; i++) {
		if(self->readPointer==writePointCopy) // end of rxBuff
		{
			return i; // Works because we come around the for loop once more after completing.
		}

		buffer[i] = self->rxBuff[self->readPointer];

		self->readPointer++;

		if(self->readPointer>=self->rxBuffLen)
		{
			self->readPointer=0;
		}

	}
	return i;
}


/**
 * @brief	Reads the next packet into the currPacket structure inside the object.
 *			The packet is cleared from the receive buffer.
 *
 * 			Logic:
 * 				- Write pointer is specified by the the DMA's location in the rx buffer
 * 				- Read pointer is the current read location.
 * 				- If read pointer = write point -> No packet data
 * 				- While the current value of packetEnd isn't 0 -> Keep trying to find the next packet
 * 				- If the end of a packet is found. Set it back to 0xFF to signal that it has been read.
 * 				- Fetch data from the packet.
 * 				- Check if data is valid, decode, check cobs... return packet.
 * @param	parent pointer to coms_link structure
 * @retval  Returns 1 if a packet was read out successfully.
 *   	 	returns 0 if the receive buffer was empty or there was an error
 */
uint8_t decodedData[MAX_PACKET_DATA_SIZE] = {0};
uint8_t crcError=0;
uint8_t uart_comms_Read_Packet_IT(coms_link* parent){
	uart_comms_link* self = parent->uartLink;

	uint8_t unwrappedBuffer[MAX_PACKET_DATA_SIZE];
	memset(unwrappedBuffer, 0, MAX_PACKET_DATA_SIZE);

	// Set the write pointer to the buffer length - number of data register
	self->writePointer = self->rxBuffLen - self->huart->hdmarx->Instance->NDTR;

	// Get a copy of the current writePointer
	uint16_t writePointCopy = self->writePointer;

	if (writePointCopy == self->readPointer) {
		return 0;
	}

	while(self->rxBuff[self->packetEnd] != 0) {
		self->packetEnd++;
		self->readPointer++;

		if(self->readPointer>=self->rxBuffLen) {
			self->readPointer=0;
		}

		if(self->packetEnd>=self->rxBuffLen) {
			self->packetEnd=0;
		}

		if(self->readPointer==writePointCopy) { // No packet in data
			parent->currPacket.length = 0;
			parent->currPacket.code = 0;
			parent->currPacket.address = 0;
			return 0;
		}
	}

	if(self->readPointer==writePointCopy) { // No packet in data
		parent->currPacket.length = 0;
		parent->currPacket.code = 0;
		parent->currPacket.address = 0;
		return 0;
	}

	self->rxBuff[self->packetEnd] = 0xFF;

	// Fetch header data. None of these are naturally zero and so are unaffected by COBS
	uint8_t length;
	if (self->packetEnd < 2) {
		length = fetchByte(self->rxBuff, self->rxBuffLen, self->rxBuffLen + self->packetEnd - 2) & 0x7F;
	} else {
		length = fetchByte(self->rxBuff, self->rxBuffLen, self->packetEnd - 2) & 0x7F;
	}

	// Check to see if the data is valid
	if(self->packetEnd >= self->rxBuffLen || length < 4 || length >= MAX_PACKET_DATA_SIZE) {
		parent->currPacket.length = 0;
		parent->currPacket.code = 0;
		parent->currPacket.address = 0;
		self->errorCount++;
		return 0;
	}

	// Copy out the data into an unwrapped buffer so that it can be decoded.
	for (int i = 0; i < length + 1; ++i) {
		uint8_t byte;
		if (self->packetEnd < length + i + 1) {
			byte = fetchByte(self->rxBuff, self->rxBuffLen, self->rxBuffLen + self->packetEnd - length-1 + i);
		} else {
			byte = fetchByte(self->rxBuff, self->rxBuffLen, self->packetEnd - length-1 + i);
		}

		/* If index is not out of bounds, write byte */
		if (i < MAX_PACKET_DATA_SIZE) unwrappedBuffer[i] = byte;
	}

	// reset packet
	parent->currPacket.length = 0;
	parent->currPacket.code = 0;
	parent->currPacket.address = 0;
	parent->currPacket.option = 0;

	return coms_decodePacket(&parent->currPacket, unwrappedBuffer, length + 2);
}


uint8_t cdc_comms_Read_Packet_IT(coms_link* parent){
	uart_comms_link* self = parent->uartLink;

	uint8_t unwrappedBuffer[MAX_PACKET_DATA_SIZE];
	memset(unwrappedBuffer, 0, MAX_PACKET_DATA_SIZE);

	// Set the write pointer to the buffer length - number of data register
	//self->writePointer = self->rxBuffLen - self->huart->hdmarx->Instance->NDTR;

	// Get a copy of the current writePointer
	uint16_t writePointCopy = self->writePointer;

	if (writePointCopy == self->readPointer) {
		return 0;
	}

	while(self->rxBuff[self->packetEnd] != 0) {
		self->packetEnd++;
		self->readPointer++;

		if(self->readPointer>=self->rxBuffLen) {
			self->readPointer=0;
		}

		if(self->packetEnd>=self->rxBuffLen) {
			self->packetEnd=0;
		}

		if(self->readPointer==writePointCopy) { // No packet in data
			parent->currPacket.length = 0;
			parent->currPacket.code = 0;
			parent->currPacket.address = 0;
			return 0;
		}
	}

	if(self->readPointer==writePointCopy) { // No packet in data
		parent->currPacket.length = 0;
		parent->currPacket.code = 0;
		parent->currPacket.address = 0;
		return 0;
	}

	self->rxBuff[self->packetEnd] = 0xFF;

	// Fetch header data. None of these are naturally zero and so are unaffected by COBS
	uint8_t length;
	if (self->packetEnd < 2) {
		length = fetchByte(self->rxBuff, self->rxBuffLen, self->rxBuffLen + self->packetEnd - 2) & 0x7F;
	} else {
		length = fetchByte(self->rxBuff, self->rxBuffLen, self->packetEnd - 2) & 0x7F;
	}

	// Check to see if the data is valid
	if(self->packetEnd >= self->rxBuffLen || length < 4 || length > MAX_PACKET_DATA_SIZE) {
		parent->currPacket.length = 0;
		parent->currPacket.code = 0;
		parent->currPacket.address = 0;
		self->errorCount++;
		return 0;
	}

	// Copy out the data into an unwrapped buffer so that it can be decoded.
	for (int i = 0; i < length + 1; ++i) {
		uint8_t byte;
		if (self->packetEnd < length + i + 1) {
			byte = fetchByte(self->rxBuff, self->rxBuffLen, self->rxBuffLen + self->packetEnd - length-1 + i);
		} else {
			byte = fetchByte(self->rxBuff, self->rxBuffLen, self->packetEnd - length-1 + i);
		}
		writeByte(unwrappedBuffer, length+1, i, byte);
	}

	// reset packet
	parent->currPacket.length = 0;
	parent->currPacket.code = 0;
	parent->currPacket.address = 0;
	parent->currPacket.option = 0;

	return coms_decodePacket(&parent->currPacket, unwrappedBuffer, length + 2);
}


/**
 * @brief	Send data in the transmit queue
 * @param	parent pointer to coms_link structure
 */
uint8_t uart_comms_TransmitDMA(coms_link* parent) {
	uart_comms_link* self = parent->uartLink;
	if (parent->busState == bus_RX_ONLY) return 0; // Do not transmit if we're RX_ONLY

	circularQueue* sendQ = &(self->transmitQueue);

	uart_comms_handleBusState(parent);

#ifdef DEVICE_STM32F4
	if(self->huart->gState == HAL_UART_STATE_READY
			&& self->huart->hdmatx->State == HAL_DMA_STATE_READY
			&& uart_comms_GetTxQLength(parent) > 0) {
#endif
		if (self->serialType == RS485) {
			if (self->writePointer != self->prevWritePointer) {
				self->prevWritePointer = self->writePointer;
				return 0;
			}
		}

		size_t index = 0;
		while(sendQ->numel > 0 && sendQ->blockState == Q_UNBLOCKED){
			uint8_t byte = sendQ->pop(sendQ);
			writeByte(self->txBuff, self->txBuffLen, index, byte);
			++index;
		}

		/* Repeat for second Q: transmitQueue2 */
		sendQ = &(self->transmitQueue2);
		while(sendQ->numel > 0 && sendQ->blockState == Q_UNBLOCKED){
			uint8_t byte = sendQ->pop(sendQ);
			writeByte(self->txBuff, self->txBuffLen, index, byte);
			++index;
		}

		if (self->serialType == RS485) {  // change USART instance
			// Transmit complete handle RS-485 related stuff
			HAL_GPIO_WritePin(self->TXE_Port, self->TXE_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(self->RXE_Port, self->RXE_Pin, GPIO_PIN_SET);
		}

		if (HAL_UART_Transmit_DMA(self->huart, (uint8_t*)self->txBuff, index) != HAL_OK) {
			return 0;
		}
	} else {
		if (sendQ->numel > 0) {
			return 0;
		}
	}
	return 1;
}

/**
 * @brief	Handle the bus state packet.
 * @param	parent pointer to coms_link structure
 */
uint8_t uart_comms_handleBusState(coms_link* parent) {
	if (parent->busState == bus_TX_QUEUED) {
		if (uart_comms_GetTxQLength(parent) == 0) {
			// Only proceed if the uart is ready
			if (parent->uartLink->huart->gState != HAL_UART_STATE_READY) {
				return 0;
			}

			// Only proceed if the dma is ready
			if (parent->uartLink->huart->hdmatx->State != HAL_DMA_STATE_READY) {
				return 0;
			}

			uint8_t data = bus_RX_ONLY;
			parent->encodePacket(
					&parent->transmitPacket,
					*parent->pDeviceID,
					BUS_STATE,
					UART_PACKET_HEADER_SIZE+sizeof(uint8_t),
					&data,
					PACKET_OPT_FWD_DENY
			);

			parent->Transmit(parent, &parent->transmitPacket);

			uint8_t txCount = 0;
			while (!parent->transmitDMA(parent)) {
				// For some reason we need to call transmitDMA twice to
				// only send one copy of the bus_state packet. Otherwise, we send two.
				// Watchdog here just in case we ever get stuck in this loop.
				if (txCount++ > 10) { break; }
			}
			parent->busState = bus_RX_ONLY;
		}
	}
	return 0;
}

/**
 * @brief	If using 485, handle pin state.
 * @param	parent pointer to coms_link structure
 */
void uart_comms_TxCpltCallback(coms_link* parent) {
	uart_comms_link* self = parent->uartLink;

	if (self->serialType == RS485) {
		// Transmit complete handle RS-485 related stuff
		HAL_GPIO_WritePin(self->RXE_Port, self->RXE_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(self->TXE_Port, self->TXE_Pin, GPIO_PIN_RESET);
	}
}

/**
 * @brief	Impliments the HAL tx complete callback
 * @param	huart pointer to UART_HandleTypeDef structure
 */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef* huart) {
	coms_link* thisLink;
	thisLink = uart_GetLinkFromHuart(huart);
	if (thisLink != NULL) {
		uart_comms_TxCpltCallback(thisLink);
	}
}

/**
 * @brief	Returns pointer to the coms_link containing the
 * 			UART_HandleTypeDef at huart
 * @param	huart pointer to UART_HandleTypeDef
 * @retval	pointer to the relevant coms_link or NULL if none found
 */
coms_link* uart_GetLinkFromHuart(UART_HandleTypeDef* huart) {
	coms_link* thisLink;
	for (uint8_t i = 0; i < uart_list_length; i++) {
		thisLink = uart_list[i];
		if (thisLink->uartLink->huart == huart) {
			return thisLink;
		}
	}
	return NULL;
}

uint16_t uart_comms_GetTxQLength(coms_link* parent) {
	return parent->uartLink->transmitQueue.numel + parent->uartLink->transmitQueue2.numel;
}

uint8_t uart_comms_TransmitData(coms_link* parent, packet_t* packet ) {
	uart_comms_link* self = parent->uartLink;

	circularQueue* sendQ = &(self->transmitQueue);
	if (sendQ->blockState == Q_BLOCKED) {
		sendQ = &(self->transmitQueue2);
	}

	sendQ->blockState = Q_BLOCKED;
	for (int i = 0; i < packet->length ; ++i){
		if (parent->busState == bus_RX_ONLY) {
			// If Q is full and we're RX_ONLY, throw away old data to fit new data.
			if (sendQ->numel == sendQ->maxSize - 1) {
				sendQ->pop(sendQ);
			}
		}
		sendQ->push(sendQ, packet->transmitData[i]);
	}
	sendQ->blockState = Q_UNBLOCKED;

	return 0;
}

uint8_t uart_comms_AddBytesToTxQ(coms_link* parent, uint8_t* data, uint8_t len) {
	uart_comms_link* self = parent->uartLink;
	circularQueue* sendQ = &(self->transmitQueue);
	if (sendQ->blockState == Q_BLOCKED) {
		sendQ = &(self->transmitQueue2);
	}

	sendQ->blockState = Q_BLOCKED;
	for (uint8_t i = 0; i < len; i++) {
		sendQ->push(sendQ, data[i]);
	}
	sendQ->blockState = Q_UNBLOCKED;

	return 0;
}
/*
 * count the number of zeros in the receive buffer.
 * This corresponds to the number of complete packets received.
 */

size_t uart_comms_numpackets(uart_comms_link* self){
	int i = 0;
	size_t count = 0;
	for(i = 0; i < self->rxBuffLen; ++i){
		if(self->rxBuff[i] == 0){
			++count;
		}
	}
	return count;
}

void uart_comms_reset(uart_comms_link* self){

}


uint32_t crc=0;
uint8_t uart_comms_encodePacket(packet_t* packet, uint8_t address ,uint8_t code, uint8_t length, uint8_t* data, uint8_t priority)
{
	packet->address = address;
	packet->code = code;
	packet->length = length;

	memcpy(packet->data, data, length-HEADER_SIZE);

	if(packet->length < 3 || packet->length > MAX_PACKET_DATA_SIZE+3){
			return 0;
		}

		for(int i = 0; i < packet->length-4; ++i) {
			uint8_t byte  = fetchByte(packet->data, packet->length, i);
			writeByte(packet->transmitData, length, i, byte);
		}

		writeByte(packet->transmitData, length, length-4, code);
		writeByte(packet->transmitData, length, length-3, address);
		writeByte(packet->transmitData, length, length-2, length);

		uint8_t stuffedPacket[length + 2];
		crc=crc8(0xff, packet->transmitData, length-1);
		writeByte(packet->transmitData, length, length-1,(uint8_t)( crc));

		cobs_encode(packet->transmitData, length, stuffedPacket);
		stuffedPacket[packet->length + 1] = 0;
		packet->length=packet->length+2;
		memcpy(packet->transmitData,stuffedPacket,packet->length);
	return 1;
}

/*
 * fetches the element at index while dealing with wraparound.
 * ie.  the buffer[0] is the frist element
 * 			buffer[-1] is the last element
 * 			buffer[buffLen+1] is the first element
 */
uint8_t fetchByte(volatile uint8_t* buffer, size_t buffLen, size_t index){
	return buffer[index%buffLen];
}

void writeByte(uint8_t* buffer, size_t buffLen, size_t index, uint8_t val){
	buffer[index%buffLen] = val;
}

