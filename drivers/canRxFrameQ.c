/*
 * canRxFrameQ.c
 *
 *  Created on: 12 Mar 2021
 *      Author: Shaun Barlow
 */

#ifdef TEST_RX_FRAME_CIRC_Q
/***************** Test only ********************/
#include "../inc/Communication/canRxFrameQ.h"

#else
/***************** Build only *******************/
#include "Communication/canRxFrameQ.h"

#endif

/***************** Common ***********************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


void rxFrameQ_Deconstruct(rxFrameQ_t* self);
void rxFrameQ_Reset(rxFrameQ_t* self);
uint8_t rxFrameQ_Push(rxFrameQ_t* self, rxFrame_t* elem);
uint8_t rxFrameQ_Pop(rxFrameQ_t* self, rxFrame_t* elem);

void rxFrameQ_Construct(rxFrameQ_t* self, size_t size) {
	self->Deconstruct 	= rxFrameQ_Deconstruct;
	self->Reset 		= rxFrameQ_Reset;
	self->Push 			= rxFrameQ_Push;
	self->Pop 			= rxFrameQ_Pop;

	self->origin = (rxFrame_t*)calloc(size, sizeof(rxFrame_t));
	self->front = self->origin;
	self->back = self->origin;
	self->maxel = size;
	self->numel = 0;
	self->locked = 0;
}

void rxFrameQ_Deconstruct(rxFrameQ_t* self) {
	free(self->origin);
	self->origin = NULL;
	self->back= NULL;
	self->front= NULL;

	self->numel = 0;
	self->maxel = 0;
}


void rxFrameQ_Reset(rxFrameQ_t* self) {
	self->front = self->origin;
	self->back = self->origin;
	self->numel = 0;
}

uint8_t rxFrameQ_Push(rxFrameQ_t* self, rxFrame_t* elem) {
	// Lock the frame queue to prevent memory access violation via CAN-ISR.
	// Note: the pop functionality must be locked since it can be preempted by the other functions that access its members
	self->locked = 1;

	if (self->numel >= self->maxel)
	{
		printf("rxFrameQ_Push(). There has been an overflow!\n");
		self->locked = 0;
		return 0; // Overflow
	}
	else if (self->back >= (&self->origin[self->maxel - 1]))
	{  // reached the end and must rap around
//		printf("Push() Wrapping buffer self->origin: %d, self->back: %d, last: %d\n", self->origin, self->back, self->origin + (sizeof(rxFrame_t) * (self->maxel - 1)));
		memcpy(self->back, elem, sizeof(rxFrame_t));
		self->back = self->origin;
		self->numel++;
	}
	else
	{
		memcpy(self->back, elem, sizeof(rxFrame_t));
		self->back++;
		self->numel++;
	}

	// Unlock the queue
	self->locked = 0;

	return 1;
}

uint8_t rxFrameQ_Pop(rxFrameQ_t* self, rxFrame_t* elem) {
	// Lock the frame queue to prevent memory access violation via CAN-ISR.
	// Note: the pop functionality must be locked since it can be preempted by the other functions that access its members
	self->locked = 1;

	if (self->numel <= 0 )
	{
		printf("rxFrameQ_Pop(). There has been an underflow!\n");
		self->locked = 0;
		return 0; //underflow
	}

	// remove and clear element at front
	memcpy(elem, self->front, sizeof(rxFrame_t));
	memset(self->front, 0, sizeof(rxFrame_t));

	// Decrement the number of elements on the queue
	self->numel--;

	if (self->front == &self->origin[self->maxel - 1]) { // wrap around
		self->front = self->origin;
	}
	else {
		self->front++;
	}

	// Unlock the queue
	self->locked = 0;

	return 1;
}


#ifdef TEST_RX_FRAME_CIRC_Q
/*
 * To run this test -
 * Open terminal, run:
 * > cd /to/this/directory
 * > gcc canRxFrameQ.c -o test -D TEST_RX_FRAME_CIRC_Q
 * > ./test.exe
 *
 * If gcc is not found, install on Windows with MinGW.
 */
#define TEST(num, eq) eq ? printf("TEST %d: PASS\n", num) : printf("TEST %d: FAIL\n", num)

uint8_t main() {
	printf("TEST_RX_FRAME_CIRC_Q\n\n");

	rxFrameQ_t q;
	rxFrameQ_Construct(&q, 4);

	TEST(100, q.maxel == 4);
	TEST(101, q.numel == 0);

	// pop from queue should return 0 and not change elem
	rxFrame_t dest;
	dest.header.ExtId = 0x1234567;
	uint8_t result = q.Pop(&q, &dest);
	TEST(201, result == 0);
	TEST(202, dest.header.ExtId == 0x1234567);

	// push to queue and check that numel is updated
	rxFrame_t source;
	source.header.ExtId = 0x2345678;
	result = q.Push(&q, &source);
	TEST(301, result == 1);
	TEST(302, q.numel == 1);

	// Push again
	source.header.ExtId = 0x2334455;
	result = q.Push(&q, &source);
	TEST(303, result == 1);
	TEST(304, q.numel == 2);

	// Pop and check the first elem
	result = q.Pop(&q, &dest);
	TEST(401, dest.header.ExtId == 0x2345678);
	TEST(402, result == 1);

	// Pop again
	result = q.Pop(&q, &dest);
	TEST(403, dest.header.ExtId == 0x2334455);
	TEST(404, result == 1);
	TEST(405, q.numel == 0);

	// Push three more to test the wrapping
	source.header.ExtId = 1;
	result = q.Push(&q, &source);
	TEST(501, result == 1);
	TEST(502, q.numel == 1);

	source.header.ExtId = 2;
	result = q.Push(&q, &source);
	TEST(503, result == 1);
	TEST(504, q.numel == 2);

	source.header.ExtId = 3;
	result = q.Push(&q, &source);
	TEST(505, result == 1);
	TEST(506, q.numel == 3);

	// Pop all three from the buffer and check the ExtIds
	result = q.Pop(&q, &dest);
	TEST(603, dest.header.ExtId == 1);
	TEST(604, result == 1);
	TEST(605, q.numel == 2);

	result = q.Pop(&q, &dest);
	TEST(703, dest.header.ExtId == 2);
	TEST(704, result == 1);
	TEST(705, q.numel == 1);

	result = q.Pop(&q, &dest);
	TEST(803, dest.header.ExtId == 3);
	TEST(804, result == 1);
	TEST(805, q.numel == 0);

	// Pop again, ensure that Q is empty and does fill in dest
	dest.header.ExtId = 0x1234567;
	result = q.Pop(&q, &dest);
	TEST(901, result == 0);
	TEST(902, dest.header.ExtId == 0x1234567);
	return 0;
}

#endif /* TEST_RX_FRAME_CIRC_Q */
