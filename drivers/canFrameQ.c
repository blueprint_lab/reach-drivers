/*
 * canFrameQ.c
 *
 *  Created on: 11 Nov 2020
 *      Author: Shaun Barlow
 */

#include "Communication/canFrameQ.h"
#include <stdlib.h>
#include <string.h>


/**
 * @brief	Initialise the count and locked to zero, front and rear pointers to NULL
 * @param	canbus_frame_queue* q to initiliase
 */
void canbus_InitialiseFrameQueue(canbus_frame_queue* q) {
	q->count = 0;
	q->front = NULL;
	q->rear = NULL;
	q->locked = 0;
}

/**
 * @brief	Return whether queue is empty
 * @param	canbus_frame_queue* q to check
 * @retval	uint8_t 1 = empty, 0 = not empty
 */
uint8_t canbus_FrameQueueIsEmpty(canbus_frame_queue* q) {
	return (q->front == NULL);
}

/**
 * @brief	Adds the supplied frame to the supplied frame queue
 * @param	canbus_frame_queue* q pointer to frame queue
 * @param	canbus_frame_t* frame
 */
void canbus_AddFrameToQueue(canbus_frame_queue* q, canbus_frame_t* frame) {
	q->locked = 1;
	frame_node* tmp;
	tmp = malloc(sizeof(frame_node));
	memcpy(&tmp->frame, frame, sizeof(canbus_frame_t));
	tmp->next = NULL;

	if (!canbus_FrameQueueIsEmpty(q)) {
		q->rear->next = tmp;
		q->rear = tmp;
	} else {
		q->front = q->rear = tmp;
	}
	q->count++;
	q->locked = 0;
}

/**
 * @brief	Populates destFrame with the first frame in a frame queue
 * 			then removes frame from queue.
 * @param	canbus_frame_queue* q pointer to the frame queue
 * @param	canbus_frame_t* destFrame pointer to the frame to be populated
 */
void canbus_PopFrameFromQueue(canbus_frame_queue* q, canbus_frame_t* destFrame) {
	frame_node* tmp;
	tmp = q->front;

	// copy data from the queue frame to the provided frame
	canbus_frame_t* sourceFrame;
	sourceFrame = &q->front->frame;
	memcpy(destFrame, sourceFrame, sizeof(canbus_frame_t));

	q->front = q->front->next; // set pointer to q front to the next frame_node
	q->count--; // decrement the number of nodes in the queue

	free(tmp); // release the memory
}
