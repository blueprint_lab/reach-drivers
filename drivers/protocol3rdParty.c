/*
 * tpprotocol.c
 *
 *  Created on: Oct 13, 2020
 *      Author: Shaun Barlow
 */

#include "Communication/protocol3rdParty.h"
#include <string.h>

void protocol_send(protocol3P_t* self, CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pRxData);
uint8_t protocol_read_NOP(protocol3P_t* self,
		CAN_RxHeaderTypeDef* RxHeader, uint8_t* pRxData);

uint8_t construct_protocol(protocol3P_t* self) {
	self->Send = protocol_send;
	self->Read = protocol_read_NOP;
	canbus_InitialiseFrameQueue(&self->txFrameQ);
	return 0;
}

void protocol_send(protocol3P_t* self, CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pRxData) {
	canbus_frame_t frame;
	memcpy(&frame.TxHeader, pRxHeader, sizeof(CAN_RxHeaderTypeDef));
	memcpy(frame.data, pRxData, sizeof(uint8_t) * pRxHeader->DLC);
	canbus_AddFrameToQueue(&self->txFrameQ, &frame);
}

uint8_t protocol_read_NOP(protocol3P_t* self,
		CAN_RxHeaderTypeDef* RxHeader, uint8_t* pRxData) {
	return 0;
}
