/*
 * ICPZ.c
 *
 *  Created on: 20-05-2021
 *      Author: Kyle McLean
 *
 *  Position evaluation notes:
 *  	The absolute position information is provided by the sampled Pseudo Random Code (PRC) track.
 *  	The sampled incremental track is interpolated to increase the resolution of the single turn
 *  	position. An internal counter is implemented that is incremented with each new sample of the
 *  	incremental track. That counter is initialized during startup with the first sampled absolute
 *  	position. During operation, each sampled absolute position is compared to the internally
 *  	counted position. In case both values do not match, the counted position is replacing the
 *  	sampled position. This way the system provides a mechanism to mask single misreading during
 *  	PRC sampling. An error is reported in DIAG(11) as soon as the tolerance for mismatches of the
 *  	sampled and counted position set via RAN_TOL is exceeded.
 *
 *  Calibration notes (see pg. 67 & 71 https://www.ichaus.de/upload/pdf/PZ_AN1_appnote_B1en.pdf):
 *  	- For very slow systems, the minimum speed can be reduced by factor 10 activating AC_ETO (not required at this point in time)
 *
 *  	INNER:
 *  		- Minimum analog calibration velocity:
 *  			2^(AC_COUNT)/FCL * 1.25 [rad/s]
 *  			= 2^(8)/446 * 1.2
 *  			= 0.69 rad/s
 *  		- Minimum analog calibration time:
 *  			2^(AC_COUNT)/FCL * (AC_SEL1-AC_SEL2)/(3/4 * n)
 *  			= 2^(8)/446 * (15-0)/(n)
 *  			= 12.47 s
 *			- Minimum digital calibration velocity:
 *  			2^(AC_COUNT)/FCL * 1.5 [rad/s]
 *  			= 2^(8)/446 * 1.5
 *  			= 0.86 rad/s
 *  		- Minimum digital calibration time:
 *  			2^(AC_COUNT)/FCL * (AC_SEL1-AC_SEL2)/(3/4 * n)
 *  			= 2^(8)/446 * (15-0)/(3/4*n)
 *  			= 13.34 s
 *  		- Minimum eccentricity calibration velocity:
 *				30 RPM (all calibrations)
 *			- Minimum eccentricity calibration time:
 *				2^(AC_COUNT)*1/(n) = 2^(8)*1/(30/9.5) = 8s (manual says 17?)
 *  		- To achieve the minimum velocity (30 RPM) on the inner encoder, the overflow value for stall drive must be set as follows
 *  			Overflow value = (2 * pi * loop-freq) / (min-velocity [rad/s]) = (2 * pi * 8000) / (30 [RPM] / 9.5) = 15917
 *
 *		OUTER:
 *  		- Minimum analog calibration velocity:
 *  			2^(AC_COUNT)/FCL * 1.25 [rad/s]
 *  			= 2^(8)/302 * 1.25
 *  			= 1.06 rad/s
 *  		- Minimum analog calibration time:
 *  			2^(AC_COUNT)/FCL * (AC_SEL1-AC_SEL2)/(3/4 * n)
 *  			= 2^(8)/302 * (15-0)/(n)
 *  			= 12 s
 *			- Minimum digital calibration velocity:
 *  			2^(AC_COUNT)/FCL * 1.5 [rad/s]
 *  			= 2^(8)/302 * 1.5
 *  			= 1.27 rad/s
 *  		- Minimum digital calibration time:
 *  			2^(AC_COUNT)/FCL * (AC_SEL1-AC_SEL2)/(3/4 * n)
 *  			= 2^(8)/302 * (15-0)/(3/4*n)
 *  			= 13.34 s
 *  		- Minimum eccentricity calibration velocity:
 *				30 RPM (all calibrations)
 *
 *			- For very slow systems, the minimum speed can be reduced by factor 10 activating AC_ETO this is necessary for the outer encoder
 *			since 30 RPM (180 deg/s) is 6 times the maximum velocity
 *			- To achieve the minimum velocity (12 / 10 [RPM]) on the outer encoder, the inner velocity must be set as follows
 *				inner-velocity = outer-velocity * gear-ratio = 7 deg/s * 120 = 840 deg/s = 88 rad/s
 */

/* Imports */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Hardware/ICPZ.h"
#include "DataStructures/auxMath.h"

/* Definitions */
#define SPI_TIMEOUT 10

#define ECC_CAL_TIME 17

#define POSITION_BYTES 6
#define STATUS_BYTES 10

#define GET_MSB(value) ((value >> 8) & 0xFF)
#define GET_LSB(value) ((value) & 0xFF)

/* Interface functions */
void	 	ICPZ_Enable(encoder_t* self);
void 		ICPZ_SetChannelState(encoder_t* self, ICPZ_activateBits_t state);
void 		ICPZ_Disable(encoder_t* self);
void 		__attribute__((optimize("O0")))ICPZ_Init(encoder_t* self);  // Note: optimizations turned off to ensure all operations complete sequentially
void 		ICPZ_Destroy(encoder_t* self);
void 		ICPZ_Callibrate(encoder_t* self);
float 		ICPZ_Read(encoder_t* self);
void 		ICPZ_ReadDma(encoder_t* self);
void 		ICPZ_checkStatus(encoder_t* self);
void	 	ICPZ_readRegisterValue(encoder_t * self, uint8_t addr);
void	 	ICPZ_writeRegisterValue(encoder_t * self, uint8_t addr, uint8_t value);
void 		ICPZ_SetDirection(encoder_t* self, uint8_t direction);
void 		ICPZ_SetZero(encoder_t* self);
uint8_t		ICPZ_absoluteFaultClassifier(encoder_t* self, float prevEncoderAngle, float velocityLimit, float dt);
uint8_t 	ICPZ_GetDirection(encoder_t* self);

/* Initialization functions */

void ICPZ_setDataLength(encoder_t* self, uint8_t data_len);
void ICPZ_setRandomEvaluationBehaviour(encoder_t* self, uint8_t ranTol, uint8_t ranVal);
void ICPZ_setDynamicSelection(encoder_t* self, uint8_t aiScaleSel, uint8_t aiPhaseSel);

/* DMA callback functions */
void 		ICPZ_DmaReadCallback(encoder_t * self);
void 		ICPZ_DmaStatusCallback(encoder_t* self);

/* Register interface functions */
void 		ICPZ_ReadWriteTarget(encoder_t * self, uint8_t ICPZ_opCode, uint8_t addr, uint8_t value);
void	 	ICPZ_WriteRegisterBit(encoder_t* self, uint8_t Reg, uint8_t bit_index, uint8_t bit_value);

/* Transmit functions */
uint32_t 	ICPZ_TransmitQueue(encoder_t* self, uint8_t* receiveBuff);
uint32_t 	ICPZ_TransmitQueueDma(encoder_t* self);

/* Other functions */
float 		ICPZ_CalculateEncoderAngle(encoder_t* self, uint8_t db1, uint8_t db2, uint8_t db3, uint8_t db4);
void 		ICPZ_setRegisterBank(encoder_t* self, uint8_t bank_idx);
void 		ICPZ_handleErrors(encoder_t* self, const uint8_t* errorBuf);
void 		ICPZ_setFlex(encoder_t* self);
uint8_t 	ICPZ_checkFlex(encoder_t* self);
void 		ICPZ_WriteRegBankVal(encoder_t* self, uint16_t reg_bank, uint8_t value);
void 		ICPZ_ReadRegBankVal(encoder_t* self, uint16_t reg_bank);
void 		ICPZ_Reset(encoder_t* self);


/* Global variable definitions */
const uint8_t icpz_posCommand[POSITION_BYTES] 	= {ICPZ_readPosition, 0x00, 0x00, 0x00, 0x00, 0x00};
const uint8_t icpz_statusCommand[STATUS_BYTES] 	= {ICPZ_readStatus, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

/*
 * @brief	Construct the ICPZ interface
 * @param 	self 			pointer to the ICPZ structure
 * @param	serialObj		SPI structure
 * @param	chipSelect		Chip select pin
 * @param	chipSelectPort	Chip select port
 */
void ICPZ_Construct(encoder_t* self, SPI_HandleTypeDef* serialObj, uint16_t chipSelect, GPIO_TypeDef* chipSelectPort){
	/* Initialize public interface */
	self->Init 			= ICPZ_Init;
	self->Enable 		= ICPZ_Enable;
	self->Disable		= ICPZ_Disable;
	self->Destroy		= ICPZ_Destroy;
	self->Callibrate 	= ICPZ_Callibrate;

	self->Read 			= ICPZ_Read;
	self->ReadDMA 		= ICPZ_ReadDma;
	self->CheckStatus	= ICPZ_checkStatus;
	self->WriteRegister	= ICPZ_writeRegisterValue;
	self->ReadRegister	= ICPZ_readRegisterValue;
	self->DMACallback	= ICPZ_DmaReadCallback;

	self->SetDirection	= ICPZ_SetDirection;
	self->SetZero		= ICPZ_SetZero;
	self->GetDirection	= ICPZ_GetDirection;

	self->checkAbsolute = ICPZ_absoluteFaultClassifier;

	/* Initialize hardware and pins */
	self->spiObj 		= serialObj;
	self->chipSelect 	= chipSelect;
	self->chipSelectPort= chipSelectPort ;

	/* Construct buffers and queues */
	circularQueueConstruct(&self->SendQ, ICPZ_TX_BUFF_SIZE);
	self->receiveBuff 	= (uint8_t*) calloc(ICPZ_RX_BUFF_SIZE, sizeof(uint8_t));
	self->sendBuff 		= (uint8_t*) calloc(ICPZ_TX_BUFF_SIZE, sizeof(uint8_t));

	/* Initialize status variables */
	self->readCount		= 0;
	self->status 		= encoderValid;
	self->encoderState 	= ICPZ_spiReady;

	/* Initialize SPI variables */
	self->lastReadAddr 	= 0xFF;
	self->lastReadByte 	= 0xFF;

	/* Initialize calculation variables */
	self->encoderAngle 	= 0;

	/* Set construct flags */
	self->constructed = 1;
	self->initialized = 0;
	self->absolute = 0;
	self->encoderType = ICPZ;
}

/*
 * @brief	Destroy the ICPZ interface
 * @param 	self 	pointer to the ICPZ structure
 */
void ICPZ_Destroy(encoder_t* self){
	/* ICPZ construct destroyed */
	self->initialized = 0;
	self->constructed = 0;

	/* Destroy the buffers */
	free(self->receiveBuff);
	free(self->sendBuff);

	/* De-init SPI pin */
	HAL_SPI_DeInit(self->spiObj);
	HAL_SPI_MspDeInit(self->spiObj);

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = self->chipSelect ;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(self->chipSelectPort, &GPIO_InitStruct);

	/* Reset the chip select pin */
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);
}


/*
 * @brief	Initialize the encoder.
 * Note:
 * 		Users can evaluate status bit DIAG(6) = ABZ_RDY (via GPIO). Once this bit becomes 1,
 * 		the ABZ startup is finished and valid signals are output henceforth.
 *
 * @param 	self 	pointer to the ICPZ structure
 */
void __attribute__((optimize("O0")))ICPZ_Init(encoder_t* self) {
	if(!self->constructed) return;

    /* Set active mode on P and R channels */
    self->Enable(self);

    /* Set IO pad slew rate to default value */
    ICPZ_WriteRegBankVal(self, ICPZ_IO_SLEW_RATE, 0x6A);

	/* Check if flex codes require updating */
    if (ICPZ_checkFlex(self)) {
		ICPZ_setFlex(self);
		ICPZ_Reset(self);
	}

	/* Set register configuration */
	ICPZ_setDataLength(self, 32); // 32 bit single turn position evaluation
	ICPZ_setRandomEvaluationBehaviour(self, 0x0F, 0x00); // RAN_TOL = 0xF -> mask 15 PRC error, RAN_VAL = 0x00 -> decrement error counter after 8 samples
	ICPZ_setDynamicSelection(self, 0, 0); // Note: Change to (7, 7) for dynamic calibration

	ICPZ_WriteRegBankVal(self, 0x002, 0x21); // Reserved [0x21]
	ICPZ_WriteRegBankVal(self, 0x003, 0xEA); // IPO_FILT1 - default
	ICPZ_WriteRegBankVal(self, 0x004, 0x04); // Reserved [0x00] bit [3-7]
	ICPZ_WriteRegBankVal(self, 0x005, 0xE6); // Reserved [0xE6]
	ICPZ_WriteRegBankVal(self, 0x006, 0x10); // Reserved [0x10]

	/* Encoder finished initialization */
	self->initialized = 1;

	/* Read initial position of encoder (Blocking)*/
	self->Read(self);
}


/*
 * @brief	Set flex data for ICPZ encoder as per manufacture specifications.
 *
 * Note 1: this is currently done every time the system is started. Move this to a
 * production once off procedure in the future?
 *
 * Note 2: One disk was found to be inverted. This is meant position data was inverted.
 * Solution was to set the invert bit on the disk.
 *
 * @param 	self pointer to the ICPZ structure
 */
void ICPZ_setFlex(encoder_t* self) {
	/* Set the SYS over-ride bits*/
	ICPZ_WriteRegBankVal(self, ICPZ_SYS_OVR_INV, ((self->sysOveride << 4) | self->invertFlex));

	/* Set the Flex length */
	ICPZ_WriteRegBankVal(self, ICPZ_FCL_LSB, (self->flexLength & 0xFF));
	ICPZ_WriteRegBankVal(self, ICPZ_FCL_MSB, ((self->flexLength >> 8) & 0xFF));

	/* Set the Flex id */
	ICPZ_WriteRegBankVal(self, ICPZ_FCS_LSB, (self->flexId & 0xFF));
	ICPZ_WriteRegBankVal(self, ICPZ_FCS_MSB, ((self->flexId >> 8) & 0xFF));

	/* Write RAM to EEPROM - required before reboot! */
	self->WriteRegister(self, ICPZ_CMD, ICPZ_writeAll);
	do {
		self->ReadRegister(self, ICPZ_CMD);
	} while(self->lastReadByte != 0x00);
}

/*
 * @brief	Check flex data for ICPZ encoder as per manufacture specifications.
 *
 * @param 	self pointer to the ICPZ structure
 */
uint8_t ICPZ_checkFlex(encoder_t* self) {
	uint8_t status = 0;

	/* Set the SYS over-ride bits*/
	ICPZ_ReadRegBankVal(self, ICPZ_SYS_OVR_INV);
	if (self->lastReadByte != ((self->sysOveride << 4) | self->invertFlex)) {status = 1;}

	/* Set the Flex length */
	ICPZ_ReadRegBankVal(self, ICPZ_FCL_LSB);
	if (self->lastReadByte != (self->flexLength & 0xFF)) {status = 1;}
	ICPZ_ReadRegBankVal(self, ICPZ_FCL_MSB);
	if (self->lastReadByte != ((self->flexLength >> 8) & 0xFF)) {status = 1;}

	/* Set the Flex id */
	ICPZ_ReadRegBankVal(self, ICPZ_FCS_LSB);
	if (self->lastReadByte != (self->flexId & 0xFF)) {status = 1;}
	ICPZ_ReadRegBankVal(self, ICPZ_FCS_MSB);
	if (self->lastReadByte != ((self->flexId >> 8) & 0xFF)) {status = 1;}

	return status;
}


/*
 * @brief	Set length of single turn SPI data. Multi-turn data is disabled
 */
void ICPZ_setDataLength(encoder_t* self, uint8_t data_len) {
	ICPZ_WriteRegBankVal(self, ICPZ_ST_PDL, data_len);
	ICPZ_WriteRegBankVal(self, ICPZ_SPI_ST_DL, data_len);

	ICPZ_WriteRegBankVal(self, ICPZ_MT_PDL, 0);
	ICPZ_WriteRegBankVal(self, ICPZ_SPI_MT_DL, 0);
}

/*
 * @brief	Set the pseudo-random track behavior. If encoder is set to absolute
 * mode then absolute re-calculations are automatically disabled.
 */
void ICPZ_setRandomEvaluationBehaviour(encoder_t* self, uint8_t ranTol, uint8_t ranVal) {
	ICPZ_setRegisterBank(self, GET_MSB(ICPZ_RAN_FLD_TOL_VAL));

	ICPZ_WriteRegisterBit(self, GET_LSB(ICPZ_RAN_FLD_TOL_VAL), 0, (ranTol >> 0) & 1);
	ICPZ_WriteRegisterBit(self, GET_LSB(ICPZ_RAN_FLD_TOL_VAL), 1, (ranTol >> 1) & 1);
	ICPZ_WriteRegisterBit(self, GET_LSB(ICPZ_RAN_FLD_TOL_VAL), 2, (ranTol >> 2) & 1);
	ICPZ_WriteRegisterBit(self, GET_LSB(ICPZ_RAN_FLD_TOL_VAL), 3, (ranTol >> 3) & 1);

	ICPZ_WriteRegisterBit(self, GET_LSB(ICPZ_RAN_FLD_TOL_VAL), 4, (ranVal >> 0) & 1);
	ICPZ_WriteRegisterBit(self, GET_LSB(ICPZ_RAN_FLD_TOL_VAL), 5, (ranVal >> 1) & 1);
	ICPZ_WriteRegisterBit(self, GET_LSB(ICPZ_RAN_FLD_TOL_VAL), 6, (ranVal >> 2) & 1);

	// ABZ is recalculated after startup if self->absolute is set
	ICPZ_WriteRegisterBit(self, GET_LSB(ICPZ_RAN_FLD_TOL_VAL), 7, self->absolute & 1);
}

/*
 * @brief	Set dynamic selection bits. This changes filter coefficient for dynamic calibration.
 * Note: Nominal value to use is dynamic calibration is desired is 7 for each filter (0x77).
 */
void ICPZ_setDynamicSelection(encoder_t* self, uint8_t aiScaleSel, uint8_t aiPhaseSel) {
	uint8_t data = (aiScaleSel << 4) | aiPhaseSel;

	ICPZ_WriteRegBankVal(self, ICPZ_SC_GAIN_OFF_SEL, 0x00); // Dynamic analog adjustment
	ICPZ_WriteRegBankVal(self, ICPZ_SC_PHASE_SEL, 0x00); // Dynamic analog adjustment
	ICPZ_WriteRegBankVal(self, ICPZ_AI_S_P_SEL, data);  // Dynamic digital adjustment

	ICPZ_WriteRegBankVal(self, 0x202, 0x00); // Reserved [0x00]
}


/*
 * @brief	Enable the encoder. Sets the ICPZ chip to active state. Assumes lock has been checked.
 * @param 	self 	pointer to the ICPZ structure
 */
void ICPZ_Enable(encoder_t* self){
	if(!self->constructed) return;

	uint8_t receiveBuff[ICPZ_RX_BUFF_SIZE];
	memset(receiveBuff, 0xFF, ICPZ_RX_BUFF_SIZE * sizeof(uint8_t));

	/* Try 255 times to enable encoder */
	for (uint8_t i = 0; i < 255; ++i){
		/* Transmit ICPZ_ACTIVATE opCode followed by the 0b11 to activate RACTIVE and PACTIVE channels */
		self->SendQ.push(&self->SendQ, ICPZ_activate);
		self->SendQ.push(&self->SendQ, ICPZ_RA_PA);
		ICPZ_TransmitQueue(self, receiveBuff);

		/* Check for active opcode returned */
		if(receiveBuff[1] == 0x00 && receiveBuff[0] == ICPZ_activate) {
			CLR_FLAG(self->status, encoderNotDetected);
			return;
		}
	}

	/* Encoder not detected */
	SET_FLAG(self->status, encoderNotDetected);
}

/*
 * @brief	Set the communication channel state. Assumes lock has been checked.
 * @param 	self 	pointer to the ICPZ structure
 */
void ICPZ_SetChannelState(encoder_t* self, ICPZ_activateBits_t state){
	if(!self->constructed) return;

	uint8_t receiveBuff[ICPZ_RX_BUFF_SIZE];
	memset(receiveBuff, 0xFF, ICPZ_RX_BUFF_SIZE * sizeof(uint8_t));

	/* Try 255 times to enable encoder */
	for (uint8_t i = 0; i < 255; ++i){
		/* Transmit ICPZ_ACTIVATE opCode followed by the 0b11 to activate RACTIVE and PACTIVE channels */
		self->SendQ.push(&self->SendQ, ICPZ_activate);
		self->SendQ.push(&self->SendQ, state);
		ICPZ_TransmitQueue(self, receiveBuff);

		/* Check for active opcode returned */
		if(receiveBuff[1] == 0x00 && receiveBuff[0] == ICPZ_activate){
			CLR_FLAG(self->status, encoderNotDetected);
			return;
		}
	}

	/* Encoder not detected */
	SET_FLAG(self->status, encoderNotDetected);
}

/*
 * @brief	Disable the encoder.
 * @param 	self 	pointer to the ICPZ structure
 */
void ICPZ_Disable(encoder_t* self){
	if(!self->constructed) return;

	/* Not implemented */
}

/*
 * @brief	Reset the encoder. Send reboot command to ICPZ.
 * @param 	self 	pointer to the ICPZ structure
 */
void ICPZ_Reset(encoder_t* self) {
	if(!self->constructed) return;

	/* Send reboot command */
	self->WriteRegister(self, ICPZ_CMD, ICPZ_reboot);
	do {
		self->ReadRegister(self, ICPZ_CMD);
	} while(self->lastReadByte != 0x00);

	/* Wait for reboot via ABZ_RDY... */
	do {
		self->ReadRegister(self, 0x6F);
	} while(self->lastReadByte != 0x00);

    /* Set active mode on P and R channels */
    self->Enable(self);
}

/*
 * @brief	Implements the ICPZ_readPosition read functionality in for encoder position measurements.
 * Note: this function is called a 8kHz for commutation and 5kHz for absolute position.
 *
 * @param 	self 	pointer to the ICPZ structure
 */
void ICPZ_ReadDma(encoder_t * self) {
	if(!self->constructed || !self->initialized) return;
	if(self->encoderState != ICPZ_spiReady) return;
	self->encoderState = ICPZ_spiBusy;

	/* CMD length - static to make persist*/
	static uint16_t cmdLen;

	/* Check if we should request status register yet */
	self->readCount++;
	if (self->readCount > (8000-2)) {
		/* Re-set the read counter */
		self->readCount = 0;

		/* Set the callback */
		self->DMACallback = ICPZ_DmaStatusCallback;

		/* Copy the encoder read tx command into the send buffer */
		cmdLen = (uint16_t)sizeof(uint8_t) * STATUS_BYTES;
		memcpy(self->sendBuff, icpz_statusCommand, cmdLen);
	} else {
		/* Set the callback */
		self->DMACallback = ICPZ_DmaReadCallback;

		/* Copy the encoder read tx command into the send buffer */
		cmdLen = (uint16_t)sizeof(uint8_t) * POSITION_BYTES;
		memcpy(self->sendBuff, icpz_posCommand, cmdLen);
	}

	/* Chips select (NCS -> GPIO_PIN_RESET) */
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);

	/* Transmit on SPI in non-blocking mode, received data is read via ICPZ_DmaReadCallback() */
	if (HAL_SPI_TransmitReceive_DMA(self->spiObj, self->sendBuff, self->receiveBuff, cmdLen) != HAL_OK) {
		/* If HAL error, set the encoder back to ready and chips de-select (NCS -> GPIO_PIN_SET) */
		HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
		self->encoderState = ICPZ_spiReady;
	}
}

/*
 * @brief 	Callback for ICPZ_ReadDma. Parses data from the read buffer, if data is in valid encoderNotDetected is given
 * @param 	self 	pointer to the ICPZ structure
 */
void ICPZ_DmaReadCallback(encoder_t* self) {
	if(!self->constructed) return;

	/* Chips de-select (NCS -> GPIO_PIN_SET) */
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);

	/* Check if data matches target opCode  and calulate encoder angle */
	if(self->receiveBuff[0] != ICPZ_readPosition) {
		SET_FLAG(self->status, encoderNotDetected);
	} else if(((self->receiveBuff[5] >> 7) & 1) || !self->absolute || 1) {
		/* Compute the encoder angle from 32 bit data */
		self->encoderAngle = ICPZ_CalculateEncoderAngle(self, self->receiveBuff[1], self->receiveBuff[2], self->receiveBuff[3], self->receiveBuff[4]);

		CLR_FLAG(self->status, encoderFault);
		CLR_FLAG(self->status, encoderNotDetected);
	} else {
		/* An error has occured preventing data aquistion - see Pg 48/82 for errors that are flagged */
		SET_FLAG(self->status, encoderFault);
	}

	/* Set the SPI state to ready */
	self->encoderState = ICPZ_spiReady;
}

/*
 * @brief 	Callback for DMA status.
 * @param 	self 	pointer to the ICPZ structure
 */
void ICPZ_DmaStatusCallback(encoder_t* self) {
	if(!self->constructed) return;

	/* Chips de-select (NCS -> GPIO_PIN_SET) */
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
	self->encoderState = ICPZ_spiReady;

	/* Check if data matches target opCode */
	CLR_FLAG(self->status, encoderNotDetected);
	if(self->receiveBuff[0] != ICPZ_readStatus) {
		SET_FLAG(self->status, encoderNotDetected);
	}

	/* Check errors and warnings */
	ICPZ_handleErrors(self, self->receiveBuff);
}

/*
 * @brief 	Set read and get status of ICPZ register value
 * @param 	self 	pointer to the ICPZ structure
 */
void ICPZ_readRegisterValue(encoder_t* self, uint8_t addr){
	if(!self->constructed) return;

	while(self->encoderState != ICPZ_spiReady) {};
	self->encoderState = ICPZ_spiBusy;

	/* Block the sendQ */
	self->encoderState = ICPZ_spiBusy;

	/* Clear the last read byte and address */
	self->lastReadByte = 0xFF;
	self->lastReadAddr = 0xFF;

	/* Read value from target address where value is set to 0x00 for this operation since no value is being set */
	ICPZ_ReadWriteTarget(self, ICPZ_readRegisters, addr, 0x00);

	/* Check ICPZ receive status and if read address matches the target "ICPZ_REG" */
	if((self->receiveBuff[0] == ICPZ_transInfo) && (self->receiveBuff[1] == ICPZ_dataValid)) {
		CLR_FLAG(self->status, encoderNotDetected);

		self->lastReadByte = self->receiveBuff[2];
		self->lastReadAddr = addr;
	} else {
		SET_FLAG(self->status, encoderNotDetected);
	}

	/* Unblock the sendQ */
	self->encoderState = ICPZ_spiReady;

}

/*
 * @brief	Set write and get status of ICPZ register value
 * @param 	self 	pointer to the ICPZ structure
 * @param	addr 	Address to write
 * @param	value	Value to write
 */
void ICPZ_writeRegisterValue(encoder_t* self, uint8_t addr, uint8_t value){
	if(!self->constructed) return;

	while(self->encoderState != ICPZ_spiReady) {};
	self->encoderState = ICPZ_spiBusy;

	/* Write value to target address */
	ICPZ_ReadWriteTarget(self, ICPZ_writeRegisters, addr, value);

	/* Unblock the sendQ */
	self->encoderState = ICPZ_spiReady;

	/* Read data back if adr isn't the CMD register */
	if (addr != ICPZ_CMD) { self->ReadRegister(self, addr); }
}

/*
 * @brief	Sends read or write opcode to the ICPZ and then tries to get valid data until the watchdog times out.
 * Notes:
 * 		- The data stream to be sent on MOSI consists of the opcode 0x81, followed by the address of the first
 * 		register to be read and a delay byte 0x00. Those first three bytes are also transmitted by iC-PZ on MISO,
 * 		before sending the requested data (DATA1) from the register at address (ADR). As long as clock is sent and
 * 		the slave stays active, data (DATA2) from the next register at the incremented address (ADR + 1) is
 * 		transmitted.
 * 		- The data stream to be sent on MOSI consists of the opcode 0xCF, followed by the address of the first
 * 		register to be written and the data. With each data byte the address of the register to be written is
 * 		incremented by one (ADR + 1)
 *
 * @param 	self 		pointer to the ICPZ structure
 * @param 	ICPZ_opCode Operation code to be executed
 * @param	addr 		Address to write
 * @param	value		Value to write
 */
void ICPZ_ReadWriteTarget(encoder_t * self, uint8_t ICPZ_opCode, uint8_t addr, uint8_t value) {
	if(!self->constructed) return;

	/* Set the register read/write state to send */
	ICPZ_regState_t ICPZ_regStatus = ICPZ_SEND_OPCODE;

	/* Clear the buffer */
	memset(self->receiveBuff, 0xFF, ICPZ_RX_BUFF_SIZE * sizeof(uint8_t));

	/* See pg. 39/69 of ICMU (assumed the same) documentation https://www.ichaus.de/upload/pdf/MU_datasheet_F1en.pdf */
	while(1) {
		switch(ICPZ_regStatus){
			case ICPZ_SEND_OPCODE:
				/* Send set READ state with target address */
				self->SendQ.push(&self->SendQ, ICPZ_opCode);
				self->SendQ.push(&self->SendQ, addr);
				self->SendQ.push(&self->SendQ, value);
				ICPZ_TransmitQueue(self, self->receiveBuff);

				/* Fall through */
			case ICPZ_REG_REQUEST:
				/* Send STATUS to receive REG values */
				self->SendQ.push(&self->SendQ, ICPZ_transInfo); // Request ICPZ to push requested data out of SPI
				self->SendQ.push(&self->SendQ, 0x00);
				self->SendQ.push(&self->SendQ, 0x00);
				ICPZ_TransmitQueue(self, self->receiveBuff);

				/* Extract ICPZ status byte */
				uint8_t spiStatus = self->receiveBuff[1];

				if (spiStatus == ICPZ_slaveBusy) {
					/* Send the register request again */
					ICPZ_regStatus = ICPZ_REG_REQUEST;
					break;
				}

				if (spiStatus == ICPZ_dataRequestFailed) {
					/* Re-send the op-code */
					ICPZ_regStatus = ICPZ_SEND_OPCODE;
					break;
				}

				/* Fall through */
			default:
				/* Register read/write finished */
				return;
		}
	}
}

/*
 * @brief	Return the encoder angle as a number between 0 and 0xFFFFFF
 * @param 	self	pointer to the ICPZ structure
 */
float ICPZ_Read(encoder_t* self){
	if(!self->constructed) return self->encoderAngle;
	if(self->encoderState != ICPZ_spiReady) return self->encoderAngle;

	self->encoderState = ICPZ_spiBusy;

	/* Transmit request and wait bytes */
	self->SendQ.push(&self->SendQ, ICPZ_readPosition);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	ICPZ_TransmitQueue(self, self->receiveBuff);

	/* Check if data matches target opCode */
	CLR_FLAG(self->status, encoderNotDetected);
	if(self->receiveBuff[0] != ICPZ_readPosition) {
		SET_FLAG(self->status, encoderNotDetected);
	} else {
		/* Compute the encoder angle from 32 bit data */
		self->encoderAngle = ICPZ_CalculateEncoderAngle(self, self->receiveBuff[1], self->receiveBuff[2], self->receiveBuff[3], self->receiveBuff[4]);
	}

	/* Unblock the sendQ */
	self->encoderState = ICPZ_spiReady;

	return self->encoderAngle;
}

/*
 * @brief	Calculate encoder angle from 32-bit data
 * @param 	db1		(31-17) byte
 * @param 	db2		(23-16) byte
 * @param 	db3		(15-8) 	byte
 * @param 	db4		(7-0) 	byte
 */
float ICPZ_CalculateEncoderAngle(encoder_t* self, uint8_t db1, uint8_t db2, uint8_t db3, uint8_t db4){
	/* Calculate encoder angle */
	uint32_t num = (uint32_t)(db1 << 24) | (uint32_t)(db2 << 16) | (uint32_t)(db3 << 8) | (uint32_t)(db4 << 0);
	uint32_t den = 0xFFFFFFFF;

	return 2 * M_PI * ((float)(self->invert ? num : ~num)) / ((float)den);
}

/*
 * @brief	Start internal calibration sequence on ICPZ chip
 * @param 	self	pointer to the ICPZ structure
 */
void ICPZ_Callibrate(encoder_t* self){
	if(!self->constructed) return;

	switch(self->calibrationState) {
		case ICPZ_calibrateInit:
			/* Set auto-calibration settings */
			self->WriteRegister(self, 0x5C, (0xF | (0 << 4)));   			// Set AC_SEL2 = 0x00, AC_SEL1 = 15 (see pg. 71/82)
			self->WriteRegister(self, 0x5D, (self->ac_count | (1 << 7)));   // Enable AC_ETO, set AC_COUNT = 0x06 (see pg. 71/82)

			self->calibrationState = ICPZ_calibrateAnalog;
			break;
		case ICPZ_calibrateAnalog:
			/* Set analoge filter settings */
			self->WriteRegister(self, ICPZ_BSEL, 0x00);
			self->WriteRegister(self, 0x03, 0x6E); 				  			// Set IPO_FILT1 = 0x6E (see pg. 31/82)
			self->WriteRegister(self, 0x04, 0x04); 				  			// Set IPO_FILT2 = 0x04 (see pg. 31/82)

			/* Start analoge auto-calibration */
			self->WriteRegister(self, ICPZ_CMD, ICPZ_autoAdjAna);
			self->calibrationState = ICPZ_calibrateAnaWait;
		case ICPZ_calibrateAnaWait:
			self->ReadRegister(self, ICPZ_CMD_STAT);
			if(self->lastReadByte == 0x80) SET_FLAG(self->status, calibrationTimeout);

			self->ReadRegister(self, ICPZ_CMD);
			if (self->lastReadByte == 0x00) self->calibrationState = ICPZ_calibrateDigital;
			break;
		case ICPZ_calibrateDigital:
			/* Set digital filter settings */
			self->WriteRegister(self, ICPZ_BSEL, 0x00);
			self->WriteRegister(self, 0x03, 0xEA);				  			// Set IPO_FILT1 = 0xEA (see pg. )

			/* Start digital auto-calibration */
			self->WriteRegister(self, ICPZ_CMD, ICPZ_autoAdjDig);
			self->calibrationState = ICPZ_calibrateDigWait;
		case ICPZ_calibrateDigWait:
			self->ReadRegister(self, ICPZ_CMD_STAT);
			if(self->lastReadByte == 0x80) SET_FLAG(self->status, calibrationTimeout);

			self->ReadRegister(self, ICPZ_CMD);
			if (self->lastReadByte == 0x00) self->calibrationState = ICPZ_calibrateFinish;
			break;
		case ICPZ_calibrateFinish:
			/* Apply settings to finish calibration */
			self->WriteRegister(self, ICPZ_CMD, ICPZ_writeAll);

			if(self->lastReadByte == 0x00) self->calibrationState = ICPZ_calibrateExit;
			break;
		default:
			break;
	}
}

/*
 * @brief	Transmit the ICPZ queue in blocking mode
 * @param 	self	pointer to the ICPZ structure
 */
uint32_t ICPZ_TransmitQueue(encoder_t* self, uint8_t* receiveBuff){
	if(!self->constructed) return 0;

	uint16_t numel = self->SendQ.numel;
	if(numel){
		/* Move data to the send buffer */
		for(int i = 0; i < numel; ++i){
			self->sendBuff[i] = self->SendQ.pop(&self->SendQ);
		}

		/* Chips select (NCS -> GPIO_PIN_RESET) */
		HAL_GPIO_WritePin( self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);

		/* Begin send in blocking mode */
		HAL_SPI_TransmitReceive(self->spiObj, (uint8_t*)self->sendBuff, receiveBuff, (uint16_t)numel, SPI_TIMEOUT);

		/* Chips de-select (NCS -> GPIO_PIN_SET) */
		HAL_GPIO_WritePin( self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
	}

	/* Return  SPI error state */
	return HAL_SPI_GetError(self->spiObj);
}

/*
 * @brief	Transmit the ICPZ queue in non-blocking mode
 * @param 	self	pointer to the ICPZ structure
 */
uint32_t ICPZ_TransmitQueueDma(encoder_t* self) {
	if(!self->constructed) return 0;

	uint16_t numel = self->SendQ.numel;
	if (numel) {
		/* Move data to the buffer */
		for (uint16_t i = 0; i < numel; i++) {
			self->sendBuff[i] = self->SendQ.pop(&self->SendQ);
		}

		/* Transmit in non-blocking mode */
		HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);
		HAL_SPI_TransmitReceive_DMA(self->spiObj, self->sendBuff, self->receiveBuff, (uint16_t)numel);
	}

	/* Return  SPI error state */
	return HAL_SPI_GetError(self->spiObj);
}

/*
 * @brief	Check the ICPZ chip for errors
 * @param 	self	pointer to the ICPZ structure
 */
void ICPZ_checkStatus(encoder_t* self) {
	if(!self->constructed) return;
	if(self->encoderState != ICPZ_spiReady) return;

	self->encoderState = ICPZ_spiBusy;

	/* Transmit request and wait bytes */
	self->SendQ.push(&self->SendQ, ICPZ_readStatus);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	ICPZ_TransmitQueue(self, self->receiveBuff);

	/* Copy buffer */
	uint8_t receiveBuff[ICPZ_RX_BUFF_SIZE];
	memcpy(&receiveBuff, self->receiveBuff, ICPZ_RX_BUFF_SIZE);

	/* Unblock the sendQ */
	self->encoderState = ICPZ_spiReady;

	/* Check if status data returned */
	CLR_FLAG(self->status, encoderNotDetected);
	if(receiveBuff[0] != ICPZ_readStatus) { SET_FLAG(self->status, encoderNotDetected); }

	/* Check errors and warnings */
	ICPZ_handleErrors(self, receiveBuff);
}

/*
 * @brief	Handle reported errors and warnings
 * @param 	self	pointer to the ICPZ structure
 * @param 	errorBuf	Error buffer
 */
uint8_t err1;
uint8_t err2;
uint8_t err3;
uint8_t err4;
uint8_t warn1;
uint8_t warn2;
uint8_t warn3;
uint8_t warn4;
void ICPZ_handleErrors(encoder_t* self, const uint8_t* errorBuf) {
	if (self->calibrationState != ICPZ_calibrateInit) return;

	/* Extract error/warning from buffer */
	err1 	= errorBuf[2];
	err2 	= errorBuf[3];
	err3 	= errorBuf[4];
	err4 	= errorBuf[5];
	warn1 	= errorBuf[6];
	warn2 	= errorBuf[7];
	warn3 	= errorBuf[8];
	warn4 	= errorBuf[9];

	/*
	 * Main error to flag is sync error.
	 * 		- PRC mismatching tolerance set in RAN_TOL exceeded. Most likely the PRC track was not sampled correctly.
	 *
	 * In this case we flag an encoder position error (similar to ICMU)
	 */
	if(CHK_FLAG(err2, ICPZ_syncFailed)) SET_FLAG(self->status, periodConsistancyError);

	/* Clear the diagnosis registers */
	self->WriteRegister(self, ICPZ_CMD, ICPZ_sClear);
}

/*
 * @brief	Set the register bank
 * @param 	self	pointer to the ICPZ structure
 */
void ICPZ_setRegisterBank(encoder_t* self, uint8_t bank_idx) {
	if(!self->constructed) return;

	/* Write the bank index to the selected bank register */
	self->WriteRegister(self, ICPZ_BSEL, bank_idx);
}

/*
 * @brief	Transmit the ICPZ queue in blocking mode
 * @param 	self			pointer to the ICPZ structure
 * @param 	Reg				ICPZ register to write to
 * @param 	bit_index		[0-7] index of bit to be set
 * @param 	bit_value		[0,1] value of bit to be set
 * @retval 	lastReadByte	Data written to the ICPZ register
 */
void ICPZ_WriteRegisterBit(encoder_t* self, uint8_t Reg, uint8_t bit_index, uint8_t bit_value) {
	if(!self->constructed) return;

	/* Get the data from the ICPZ at the register specified (Reg) */
	self->ReadRegister(self, Reg);
	uint8_t output_reg = 0;

	if (bit_value != 0) {
		/* If writing bit_value == 1, create a mask for bitwise OR operation */
		output_reg = self->lastReadByte | (1 << bit_index);
	} else {
		/*	else writing bit_value == 0, create a mask for bitwise AND operation */
		output_reg = self->lastReadByte &  ~(1 << bit_index);
	}

	/* Write to the register and return the data written */
	self->WriteRegister(self, Reg, output_reg);
}

/*
 * @brief	Set the zero position for the ICPZ chip
 * @param 	self	pointer to the ICPZ structure
 */
void ICPZ_SetZero(encoder_t* self) {
	if(!self->constructed || self->calibrationState != ICPZ_calibrateInit) return;

	/* Set current position as zero */
	self->WriteRegister(self, ICPZ_CMD, ICPZ_MtStPresetStore);
}

/*
 * @brief	Set the direction for the ICPZ chip
 * @param 	self	pointer to the ICPZ structure
 */
void ICPZ_SetDirection(encoder_t* self, uint8_t direction) {
	if(!self->constructed) return;

	// not implemented
}

/*
 * @brief	Get the direction for the ICPZ chip
 * @param 	self	pointer to the ICPZ structure
 */
uint8_t ICPZ_GetDirection(encoder_t* self) {
	if(!self->constructed) return 0;

	// not implemented

	return 0x00;
}

/*
 * @brief	Set register bank and value
 * @param 	self Pointer to the ICPZ structure
 */
void ICPZ_WriteRegBankVal(encoder_t* self, uint16_t reg_bank, uint8_t value) {
	if(!self->constructed) return;

	/* Set the register bank */
	ICPZ_setRegisterBank(self, GET_MSB(reg_bank));

	/* Write direction to RAM */
	self->WriteRegister(self, GET_LSB(reg_bank), value);
}

/*
 * @brief	Set register bank and value
 * @param 	self Pointer to the ICPZ structure
 */
void ICPZ_ReadRegBankVal(encoder_t* self, uint16_t reg_bank) {
	if(!self->constructed) return;

	/* Set the register bank */
	ICPZ_setRegisterBank(self, GET_MSB(reg_bank));

	/* Read register */
	self->ReadRegister(self, GET_LSB(reg_bank));
}

/*
 * @brief	A classifier to determine the likelihood of a correct absolute position.
 *
 * 			This is a Bayesian classifier that determines the log-likelihood of a correct
 * 			encoder reading based on an accumulation of evidence.
 *
 * 			The log-likelihood of a binary condition is given by;
 * 				- ln(P/(1-P))
 *
 * 			In this case we look at several conditions and their estimated probability
 * 			of giving a no-fault condition.
 * 				- The encoder is stationary 		-> P = 0.5  -> Equal probability of fault or no-fault
 * 				- The encoder is inside limits  	-> P = 0.55 -> Probability favors no-fault
 * 				- The encoder is outside limits  	-> P = 1e-2 -> Probability favors fault
 * 				- Encoder error flag			  	-> P = 1e-9 -> Equal probability of flipping between fault and no-fault
 *
 * 			When the cumulative log-likelihood reaches the confidence threshold (20 db),
 * 			we are sure that the absolute value is correct. In this case,
 * 				- RAN_FLD -> 0
 *
 * 			References:
 * 				- https://www.ling.upenn.edu/courses/ling052/BinaryClassification.html
 *
 * @param 	self Pointer to the ICPZ structure
 */
#define ICPZ_ABSOLUTE_THRESHOLD 20.0f
#define ICPZ_MIN_VEL_THRESHOLD 0.2f
#define ICPZ_DISABLE_THRESHOLD 100

//static const float P_static 	= logf(0.5 / (1 - 0.5));
//static const float P_inLimits 	= logf(0.55  / (1 - 0.55)); /* 50 reading on startup, 100 from fault detected */
//static const float P_outLimits 	= logf(0.45  / (1 - 1e-2));
//static const float P_error 		= logf(1e-9  / (1 - 1e-9));
uint8_t ICPZ_absoluteFaultClassifier(encoder_t* self, float prevEncoderAngle, float velocityLimit, float dt) {
	if(!self->constructed || !self->absolute) return 0;

//	static uint16_t faultCount = 0;
//	static float likelihood = 0.0f;
//
//	float velocity = fabsf(prevEncoderAngle - self->encoderAngle) / dt;
//
//	if (velocity < ICPZ_MIN_VEL_THRESHOLD) { 				/* Check for static condition */
//		likelihood += P_static;
//	} else if (velocity < velocityLimit) { 					/* Check for in limits condition */
//		likelihood += P_inLimits;
//	} else {												/* Must be outside the specified limit */
//		likelihood += P_outLimits;
//	}
//
//	if (CHK_FLAG(self->status, periodConsistancyError)) {	/* Check for error condition */
//		likelihood = P_error;
//	}
//
//
//	if (likelihood <= -ICPZ_ABSOLUTE_THRESHOLD && faultCount < ICPZ_DISABLE_THRESHOLD) {
//		likelihood = -ICPZ_ABSOLUTE_THRESHOLD; 				/* Cap likelihood */
//		faultCount++;
//	} else if (likelihood > ICPZ_ABSOLUTE_THRESHOLD) {
//		likelihood = ICPZ_ABSOLUTE_THRESHOLD;  				/* Cap likelihood */
//	} else if (faultCount > 0) {							/* Reduce fault count while transitioning */
//		faultCount--;
//	}
//
//	if (faultCount > ICPZ_DISABLE_THRESHOLD) {
//		faultCount = 0;
//		SET_FLAG(self->status, encoderFault);
//		return 1; 											/* Disable motor */
//	}

	return 0;
}

