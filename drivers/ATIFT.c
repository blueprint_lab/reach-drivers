/*
 * ATIFT.c
 *
 *  Created on: Oct 14, 2020
 *      Author: Shaun Barlow
 */

#include "Hardware/ATIFT.h"
#include <string.h>

void MX_CAN1_Init_250kbps(void); // implemented in main.c
void MX_CAN1_Init_1000kbps(void);

#define SWAP_ENDIAN_32(pInput) __builtin_bswap32(*(uint32_t*)pInput)
#define SWAP_ENDIAN_16(pInput) __builtin_bswap16(*(uint16_t*)pInput)

uint8_t ATIFT_CAN_read(protocol3P_t* protocol,
					CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pData);
void ATIFT_CAN_parse(ATIFT_t* thisATIFT, ATIFT_packetID_t packetID, uint8_t* pData);
ATIFT_t* ATIFT_getFromProtocolAddr(protocol3P_t* protocol);
uint8_t ATIFT_CAN_encode(ATIFT_t* self, ATIFT_txPacketID_t packetID, uint8_t* pData);

#define TIMEOUT 100
#define MAX_ATIFT_COUNT 3
uint8_t atiftCount = 0;
ATIFT_t* atiftList[MAX_ATIFT_COUNT];

void ATIFT_construct(ATIFT_t* self, protocolManager_t* pProtocolManager,
		uint8_t baseIDAddress, void (*fnChangeCANBaud250kbps), void (*fnZeroBusStateSched)) {
	pProtocolManager->Add(pProtocolManager, &(self->protocol));
	self->protocol.Read = ATIFT_CAN_read;
	self->CANIDMask = (uint32_t) (baseIDAddress << 4) & 0b111111110000;

	self->setCANBaud250kbps = fnChangeCANBaud250kbps;
	self->zeroBusStateSched = fnZeroBusStateSched;
	// If more the MAX_ATIFT_COUNT, increase the #define value.
	atiftList[atiftCount] = self;
	atiftCount++;

	self->SGData.numRows = 6;
	self->SGData.numCols = 1;
	self->runtimeMatrix.numRows = 6;
	self->runtimeMatrix.numCols = 6;
	self->forceTorqueValues.numRows = 6;
	self->forceTorqueValues.numRows = 1;
	self->runtimeCountdown = 0;
	self->runtimePeriodMs = 10;
	self->tare[0] = 0;
	self->tare[1] = 0;
	self->tare[2] = 0;
	self->tare[3] = 0;
	self->tare[4] = 0;
	self->tare[5] = 0;
}

void ATIFT_init(ATIFT_t* self, uint8_t calibIndex) {
	self->calibIndex = calibIndex;
}

/**
 * To obtain force and torque data the strain gage values
 * (signed 16-bit format) have to be multiplied with the
 * calibration matrix (4 byte floating point format) of
 * the transducer. Figure7.1 illustrates the generic
 * F/T matrix calculation procedure.
 * The result of the matrix multiplication must be divided
 * by Counts per Force (CpF) and Counts per Torque (CpT),
 * respectively.
 * CpF and CpT values can be obtained from the transducer’s
 * calibration file, with NETCANOEM firmware revision 3.7
 * and higher over the CAN interface.
 * For any MINI45 calibration, the value of CpF and CpT is
 * 1000000.
 *
 * Rather than dividing the result of the matrix calculation,
 * the matrix values can be divided by the CpF and CpT values.
 * This may save computing time since it would only have to be
 * done once at the beginning of the measurement and not with
 * every sample during the measurement phase.
 *
 * The run time matrix columns are ordered: sg0, sg2, sg4, sg1, sg3, sg5
 * to reduce computation during runtime.
 */
void ATIFT_calculateRuntimeMatrix(ATIFT_t* self) {
	uint8_t numRows = 6;
	for (uint8_t row = 0; row < numRows; row++) {
//		self->runtimeMatrix.matrix[row][0] = self->calibMatrix[row][0] / (float)self->countsPerForce;
//		self->runtimeMatrix.matrix[row][1] = self->calibMatrix[row][2] / (float)self->countsPerForce;
//		self->runtimeMatrix.matrix[row][2] = self->calibMatrix[row][4] / (float)self->countsPerTorque;
//		self->runtimeMatrix.matrix[row][3] = self->calibMatrix[row][1] / (float)self->countsPerForce;
//		self->runtimeMatrix.matrix[row][4] = self->calibMatrix[row][3] / (float)self->countsPerTorque;
//		self->runtimeMatrix.matrix[row][5] = self->calibMatrix[row][5] / (float)self->countsPerTorque;

		self->runtimeMatrix.matrix[row][0] = self->calibMatrix[row][0] / (float)self->countsPerForce;
		self->runtimeMatrix.matrix[row][1] = self->calibMatrix[row][1] / (float)self->countsPerForce;
		self->runtimeMatrix.matrix[row][2] = self->calibMatrix[row][2] / (float)self->countsPerForce;
		self->runtimeMatrix.matrix[row][3] = self->calibMatrix[row][3] / (float)self->countsPerTorque;
		self->runtimeMatrix.matrix[row][4] = self->calibMatrix[row][4] / (float)self->countsPerTorque;
		self->runtimeMatrix.matrix[row][5] = self->calibMatrix[row][5] / (float)self->countsPerTorque;
	}
}

/**
 * @brief	Calculate Force Torque values by multiplying run time
 * 			matrix and strain gauge data.
 * 			The run time matrix columns are ordered: sg0, sg2, sg4, sg1, sg3, sg5
 * 			to reduce computation during runtime.
 * @retval	Pointer to forceTorqueValues in order Fx, Fz, Ty, Fy, Tx, Tz
 */
Matrix* ATIFT_calculateFTValues(ATIFT_t* self) {
//	self->SGData.matrix[0][0] = (float)self->SG0_2_4[0];
//	self->SGData.matrix[1][0] = (float)self->SG0_2_4[1];
//	self->SGData.matrix[2][0] = (float)self->SG0_2_4[2];
//	self->SGData.matrix[3][0] = (float)self->SG1_3_5[0];
//	self->SGData.matrix[4][0] = (float)self->SG1_3_5[1];
//	self->SGData.matrix[5][0] = (float)self->SG1_3_5[2];

	self->SGData.matrix[0][0] = (float)self->SG0_2_4[0];
	self->SGData.matrix[1][0] = (float)self->SG1_3_5[0];
	self->SGData.matrix[2][0] = (float)self->SG0_2_4[1];
	self->SGData.matrix[3][0] = (float)self->SG1_3_5[1];
	self->SGData.matrix[4][0] = (float)self->SG0_2_4[2];
	self->SGData.matrix[5][0] = (float)self->SG1_3_5[2];


	matrixMultiply(&self->runtimeMatrix, &self->SGData, &self->forceTorqueValues);
	self->fx = self->forceTorqueValues.matrix[0][0] - self->tare[0];// * (float)self->countsPerForce;
	self->fy = self->forceTorqueValues.matrix[1][0] - self->tare[1];// * (float)self->countsPerForce;
	self->fz = self->forceTorqueValues.matrix[2][0] - self->tare[2];// * (float)self->countsPerTorque;
	self->tx = self->forceTorqueValues.matrix[3][0] - self->tare[3];// * (float)self->countsPerForce;
	self->ty = self->forceTorqueValues.matrix[4][0] - self->tare[4];// * (float)self->countsPerTorque;
	self->tz = self->forceTorqueValues.matrix[5][0] - self->tare[5];// * (float)self->countsPerTorque;
	return &self->forceTorqueValues;
}

void ATIFT_run(ATIFT_t* self) {
	if (self->enabled == ATIFT_DISABLED) return;

	switch(self->state) {
	case ATI_DISABLED:
		break;
	case ATI_SET_CAN_BAUD:
		// Assuming that DIP switch 1 on ATI PCB is set to ON ("default" settings mode).
		// And board has been power cycled.
		// Check that green LED is flashing.
//		self->setCANBaud250kbps();

		self->baudRate = 1;
		ATIFT_CAN_encode(self, SET_BAUD_RATE, &self->baudRate);
		self->state = ATI_DISABLED;
		break;
	case ATI_SET_CAN_ID:
	{
		uint8_t id = 0x20;
		ATIFT_CAN_encode(self, SET_BASE_ID, &id);
		//self->state = ATI_ERROR;
		self->state = ATI_DISABLED;
		break;
	}
	case ATI_DO_RESET:
		ATIFT_CAN_encode(self, DO_FT_RESET, &self->baudRate);
		break;
	case ATI_TEST:
		ATIFT_CAN_encode(self, GET_FT_FIRMWARE_VERSION, NULL);
//		self->state = ATI_START;
		break;
	// INIT SEQUENCE
	case ATI_START:
//		self->baudRate = MBPS_1;
//		ATIFT_CAN_encode(self, SET_BAUD_RATE, &self->baudRate);
		self->state = ATI_SET_CALIB;
		break;
	case ATI_SET_CALIB:
		/**
		 * Select the correct calibration. With the “Set Active Calibration”
		 * command a calibration-slot number between 0 and 15 is sent to the
		 * NETCANOEM. If the transducer has several calibrations (up to 16
		 * are possible), then the calibration-slot will be set to the new
		 * number. If there is no valid calibration in the selected slot,
		 * then bit 6 “Bad active calibration” in the status register
		 * will be on.
		 */
		ATIFT_CAN_encode(self, SET_CALIB_INDEX, &self->calibIndex);
		self->state = ATI_VERIFY_CALIB;
		self->timeoutCounter = TIMEOUT; // set a count down to timeout
		break;
	case ATI_VERIFY_CALIB:
		/**
		 * Verify that the correct calibration is selected: The “Set Active
		 * Calibration” command echoes the selected calibration slot. Verify
		 * that it matches the desired calibration slot number.
		 */
		if (self->timeoutCounter == 0) {
			self->state = ATI_SET_CALIB;
		} else {
			self->timeoutCounter--;
		}
		break;
	case ATI_GET_MTRX_FX:
	case ATI_GET_MTRX_FY:
	case ATI_GET_MTRX_FZ:
	case ATI_GET_MTRX_TX:
	case ATI_GET_MTRX_TY:
	case ATI_GET_MTRX_TZ:
		/**
		 * Read the active calibration matrix with the “Read Active
		 * Calibration” command.
		 */
		self->calibRow = (calibIndex_t) (self->state - ATI_GET_MTRX_FX);
		if (self->timeoutCounter == 0) {
			ATIFT_CAN_encode(self, GET_CALIB_MTRX, &self->calibRow);
			self->timeoutCounter = TIMEOUT;
		} else {
			self->timeoutCounter--;
		}
		break;
	case ATI_GET_CPFT:
		/**
		 * Read the Counts per Force and Counts per Torque values
		 * (only available with firmware versions 3.7 and later.
		 * Older firmware use 1000000 for both values)
		 */
		if (self->timeoutCounter == 0) {
			ATIFT_CAN_encode(self, GET_CP_FORCE_TORQUE, NULL);
			ATIFT_CAN_encode(self, GET_FT_UNITS, NULL);
			self->timeoutCounter = TIMEOUT;
		} else {
			self->timeoutCounter--;
		}
		break;
	case ATI_INIT_COMPLETE:
		/**
		 * CALCULATE RUNTIME MATRIX
		 */
		ATIFT_calculateRuntimeMatrix(self);
		self->state = ATI_GET_SG_DATA;
		break;
	// RUN SEQUENCE
	case ATI_GET_SG_DATA:
		ATIFT_CAN_encode(self, GET_STRAIN_GAUGE_DATA, NULL);
		self->timeoutCounter = TIMEOUT;
		self->state = ATI_WAITING_FOR_SG_DATA;
		break;
	case ATI_WAITING_FOR_SG_DATA:
		self->timeoutCounter--;
		if (self->timeoutCounter == 0) {
			self->state = ATI_TIMEOUT;
		}
		break;
	case ATI_SG_DATA_COMPLETE:
		// check status bytes
		/* STATUS CHECKS HERE */
		ATIFT_calculateFTValues(self);
		self->state = ATI_PAUSE_BETWEEN_SG_REQUESTS;
		self->runtimeCountdown = self->runtimePeriodMs;
		break;
	case ATI_PAUSE_BETWEEN_SG_REQUESTS:
		if (self->runtimeCountdown == 0) {
			self->state = ATI_GET_SG_DATA;
		}
		self->runtimeCountdown--;
		break;
	case ATI_TIMEOUT:
		self->numTimeouts++;
		self->state = ATI_GET_SG_DATA;
		break;
	case ATI_ERROR:
		break;
	case ATI_PROG_FT:
		self->baudReturn = ATI_FLAG_UNSET;
		self->idReturn = ATI_FLAG_UNSET;

		self->baudRate = 1;
		ATIFT_CAN_encode(self, SET_BAUD_RATE, &self->baudRate);
		uint8_t id = 0x20;
		ATIFT_CAN_encode(self, SET_BASE_ID, &id);

		self->state = ATI_START;

		break;
	default:
		// what to do???
		return;
	}
}

uint8_t ATIFT_CAN_read(protocol3P_t* protocol,
					CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pData) {
	if (pRxHeader->IDE != CAN_ID_STD) // Must be STD_ID
		return 0;

	ATIFT_t* thisATIFT = ATIFT_getFromProtocolAddr(protocol);
	if (thisATIFT == NULL)
		return 0; // ATIFT not set up correctly

	// Check ATIFT's base ID matches this frame's header
	if ((pRxHeader->StdId & 0b111111110000) != (thisATIFT->CANIDMask))
		return 0;

	ATIFT_packetID_t packetID = (ATIFT_packetID_t) (pRxHeader->StdId & 0b1111);

	ATIFT_CAN_parse(thisATIFT, packetID, pData);
	return 1; // frame is for this ATIFT. Return 1.
}

ATIFT_t* ATIFT_getFromProtocolAddr(protocol3P_t* protocol) {
	for (uint8_t i=0; i < atiftCount; i++) {
		if (&atiftList[i]->protocol == protocol) {
			return atiftList[i];
		}
	}
	return (ATIFT_t*) NULL;  // Error the ATIFT protocol was not set up correctly.
}

void ATIFT_CAN_parse(ATIFT_t* self, ATIFT_packetID_t packetID, uint8_t* pData) {
	switch (packetID) {
	case STRAIN_GAUGE_DATA_SG0_SG2_SG4:
		/* Contains the two byte status code, followed by the two byte values
		 * for sg0, sg2, and sg4 (total of eight bytes)
		 * */
		memcpy(&self->status, pData, sizeof(uint16_t));
		*(uint16_t*)&(self->SG0_2_4[0]) = SWAP_ENDIAN_16(&pData[2]);
		*(uint16_t*)&(self->SG0_2_4[1]) = SWAP_ENDIAN_16(&pData[4]);
		*(uint16_t*)&(self->SG0_2_4[2]) = SWAP_ENDIAN_16(&pData[6]);
		break;
	case STRAIN_GAUGE_DATA_SG1_SG3_SG5:
		/*contains the three 2-byte values sg1, sg3, and sg5 (total of six bytes).
		 * It will be necessary to reorder the strain gauges to the order:
		 * sg0, sg1, sg2, sg3, sg4, sg5 – before performing the matrix multiplication.
		 * Alternatively, you can rearrange the order of the columns in the
		 * matrix to match the (sg0, sg2, sg4, sg1, sg3, sg5) ordering in this response
		 * */
		*(uint16_t*)&self->SG1_3_5[0] = SWAP_ENDIAN_16(&pData[0]);
		*(uint16_t*)&self->SG1_3_5[1] = SWAP_ENDIAN_16(&pData[2]);
		*(uint16_t*)&self->SG1_3_5[2] = SWAP_ENDIAN_16(&pData[4]);
		self->state = ATI_SG_DATA_COMPLETE;
		break;
	case MTRX_COEFF_SG0_SG1:
		/* SG0 and SG1 coefficients for the requested axis */
		*(uint32_t*)&self->calibMatrix[self->calibRow][G0] = SWAP_ENDIAN_32(&pData[0]);
		*(uint32_t*)&self->calibMatrix[self->calibRow][G1] = SWAP_ENDIAN_32(&pData[sizeof(float)]);
		break;
	case MTRX_COEFF_SG2_SG3:
		/* SG2 and SG3 coefficients */
		*(uint32_t*)&self->calibMatrix[self->calibRow][G2] = SWAP_ENDIAN_32(&pData[0]);
		*(uint32_t*)&self->calibMatrix[self->calibRow][G3] = SWAP_ENDIAN_32(&pData[sizeof(float)]);
		break;
	case MTRX_COEFF_SG4_SG5:
		/* SG4 and SG5 coefficients */
		*(uint32_t*)&self->calibMatrix[self->calibRow][G4] = SWAP_ENDIAN_32(&pData[0]);
		*(uint32_t*)&self->calibMatrix[self->calibRow][G5] = SWAP_ENDIAN_32(&pData[sizeof(float)]);
		// Advance the state when receiving the
		switch (self->state) {
		case ATI_GET_MTRX_FX:
		case ATI_GET_MTRX_FY:
		case ATI_GET_MTRX_FZ:
		case ATI_GET_MTRX_TX:
		case ATI_GET_MTRX_TY:
		case ATI_GET_MTRX_TZ:
			self->state++;
			self->timeoutCounter = 0;
			break;
		default:
			break;
		}
		break;
	case FT_SERIAL_NUMBER:
		memcpy(self->serialNumberASCII, pData, 8);
		break;
	case CALIB_INDEX:
		/* Check if we've received matching calib index */
		if ((self->calibIndex == pData[0])
				&& (self->state == ATI_VERIFY_CALIB)) {
			/* Move to next state */
			self->state = ATI_GET_MTRX_FX;
			self->timeoutCounter = 0;
		}
		break;
	case CP_FORCE_TORQUE:
		self->countsPerForce = SWAP_ENDIAN_32(&pData[0]);
		self->countsPerTorque = SWAP_ENDIAN_32(&pData[sizeof(float)]);
		if (self->state == ATI_GET_CPFT) {
			self->state++;
			self->timeoutCounter = 0;
		}
		break;
	case FT_UNITS:
		self->forceUnit = (forceUnit_t) pData[0];
		self->torqueUnit = (torqueUnit_t) pData[1];
		break;
	case ADC_VOLTAGE:
		*(uint16_t*)&self->adcVoltage[self->adcVoltageIndex] = SWAP_ENDIAN_16(&pData[0]);
		break;
	case FT_RESET:
		break;
	case BASE_ID:
		self->idReturn = ATI_FLAG_SET;
		break;
	case BAUD_RATE:
		self->baudReturn = ATI_FLAG_SET;
		break;
	case FT_FIRMWARE_VERSION:
		memcpy(self->firmwareVersion, pData, sizeof(uint8_t) * 4);
		break;
	default:
		break;
	}
}

uint8_t ATIFT_CAN_encode(ATIFT_t* self, ATIFT_txPacketID_t packetID, uint8_t* pData) {
	CAN_RxHeaderTypeDef txHeader;

	txHeader.StdId = self->CANIDMask | (0b1111 & packetID);
	txHeader.IDE = CAN_ID_STD;
	txHeader.RTR = CAN_RTR_DATA;

	switch (packetID) {
	case GET_STRAIN_GAUGE_DATA:
		txHeader.DLC = 0;
		break;
	case GET_CALIB_MTRX:
		txHeader.DLC = 1;
		self->calibRow = pData[0];
		break;
	case GET_FT_SERIAL_NUMBER:
		break;
	case SET_CALIB_INDEX:
		txHeader.DLC = 1;
		self->calibIndex = pData[0];
		break;
	case GET_CP_FORCE_TORQUE:
		txHeader.DLC = 0;
		break;
	case GET_FT_UNITS:
		txHeader.DLC = 0;
		break;
	case GET_ADC_VOLTAGE:
		txHeader.DLC = 1;
		self->adcVoltageIndex = pData[0];
		break;
	case DO_FT_RESET:
		txHeader.DLC = 0;
		break;
	case SET_BASE_ID:
		self->idReturn = ATI_FLAG_UNSET;
		txHeader.DLC = 1;
		// TODO: save base id to use when ATIFT restarts
		break;
	case SET_BAUD_RATE:
		self->baudReturn = ATI_FLAG_UNSET;
		txHeader.DLC = 1;
		break;
	case GET_FT_FIRMWARE_VERSION:
		txHeader.DLC = 0;
		break;
	default:
		return 0;
	}
	self->protocol.Send(&self->protocol, &txHeader, pData);
	return 1;
}

void ATIFT_setTare(ATIFT_t* self, float* tareValues) {
	for (uint8_t i = 0; i < 6; i++) {
		self->tare[i] = self->forceTorqueValues.matrix[i][0];
	}
	//	memcpy(&self->tare, self->forceTorqueValues.matrix[0], sizeof(float) * 6);
}

void ATIFT_initProgramming(ATIFT_t* self) {
	// set 708 can baudrate to match the FT sensor default
	self->setCANBaud250kbps();

	// clear the bus state scheduler to ensure the FT sensor has enough time to respond
	self->zeroBusStateSched();

	// enable the ATIFT_run loop
	self->enabled = ATIFT_ENABLED;

}

void ATIFT_messageHandler(ATIFT_t* self, uint8_t packetID, uint8_t data) {
	// Check if this packet id matches with one of the ATI commands.
	if (ATIFT_CAN_encode(self, (ATIFT_txPacketID_t)packetID, &data) == 0) {
		// packetID was not in range to transmit to sensor.
		// This is a request. Respond with requested data.

		switch((txDataID_t)packetID) {
		case atiTx_STRAIN_GAUGE_DATA:
			self->txData[0] = packetID;
			memcpy(&self->txData[1], self->SG0_2_4, sizeof(self->SG0_2_4));
			memcpy(&self->txData[1 + sizeof(self->SG0_2_4)], self->SG1_3_5, sizeof(self->SG1_3_5));
			self->txDataLen = 1 + sizeof(self->SG0_2_4) + sizeof(self->SG1_3_5);
			break;
		case atiTx_STATUS:
			self->txData[0] = packetID;
			self->txData[1] = self->status;
			self->txDataLen = 2;
			break;
		case atiTx_CALIB_MTRX:
			self->txData[0] = packetID;
			self->txData[1] = data; // data indicates matrix row (0 through 5)
			memcpy(&self->txData[2], &self->calibMatrix[data], sizeof(float) * 6); // data indicates matrix row
			self->txDataLen = 26; // 26 bytes: packet id, matrix row, 6 floats from calib matrix row
			break;
		case atiTx_SERIAL_NUMBER:
			self->txData[0] = packetID;
			memcpy(&self->txData[1], self->serialNumberASCII, ATI_SERIAL_NUM_LENGTH);
			self->txDataLen = 1 + ATI_SERIAL_NUM_LENGTH;
			break;
		case atiTx_CALIB_INDEX:
			self->txData[0] = packetID;
			self->txData[1] = self->calibIndex;
			self->txDataLen = 2;
			break;
		case atiTx_COUNTS:
			self->txData[0] = packetID;
			memcpy(&self->txData[1], &self->countsPerForce, sizeof(self->countsPerForce));
			memcpy(&self->txData[1 + sizeof(self->countsPerForce)], &self->countsPerTorque, sizeof(self->countsPerTorque));
			self->txDataLen = 1 + sizeof(self->countsPerForce) + sizeof(self->countsPerTorque);
			break;
		case atiTx_UNITS:
			self->txData[0] = packetID;
			self->txData[1] = self->forceUnit;
			self->txData[2] = self->torqueUnit;
			self->txDataLen = 3;
			break;
		case atiTx_ADC_VOLTAGE:
			self->txData[0] = packetID;
			memcpy(&self->txData[1], &self->adcVoltage, sizeof(self->adcVoltage));
			self->txDataLen = 1 + sizeof(self->adcVoltage);
			break;
		case atiTx_RESET:
			self->txData[0] = packetID;
			self->txDataLen = 1;
			break;
		case atiTx_BASE_ID:
			self->txData[0] = packetID;
			self->txData[1] = (self->CANIDMask >> 4);
			self->txDataLen = 2;
			break;
		case atiTx_BAUD_RATE:
			self->txData[0] = packetID;
			self->txData[1] = self->baudRate;
			self->txDataLen = 2;
			break;
		case atiTx_FIRMWARE_VERSION:
			self->txData[0] = atiTx_FIRMWARE_VERSION;
			self->txData[1] = self->firmwareVersion[0];
			self->txData[2] = self->firmwareVersion[1];
			self->txData[3] = self->firmwareVersion[2];
			self->txData[4] = self->firmwareVersion[3];
			self->txDataLen = 5;
			break;
		case atiCmd_SET_STATE:
			if (data < ATI_LAST_STATE) {
				self->state = data;
			}
		case atiCmd_GET_STATE:
			self->txData[0] = self->state;
			self->txDataLen = 1;
			break;
		case atiCmd_SET_ENABLE:
			if (data > 0) {
				self->enabled = ATIFT_ENABLED;
			} else {
				self->enabled = ATIFT_DISABLED;
			}
		case atiCmd_GET_ENABLE:
			self->txData[0] = self->enabled;
			self->txDataLen = 1;
			break;
		case atiCmd_SET_BAUD_AND_ID:
			self->state = ATI_PROG_FT;
			break;
		case atiCmd_START_PROGRAM:
			ATIFT_initProgramming(self);
			break;
		case atiCmd_GET_BAUD_RESPONSE:
			self->txData[0] = packetID;
			self->txData[1] = self->baudReturn;
			self->txDataLen = 2;
			break;
		case atiCmd_GET_ID_RESPONSE:
			self->txData[0] = packetID;
			self->txData[1] = self->idReturn;
			self->txDataLen = 2;
			break;

		}
	}
}
