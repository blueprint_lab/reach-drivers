/*
 * stm32f4_otp.c
 *
 *  Created on: 21 Aug 2020
 *      Author: blueprintlab
 */

#include "DataStructures/stm32f4_otp.h"

extern void FLASH_Program_Byte(uint32_t Address, uint8_t Data);

uint32_t timeout = 20;

// clear flag not declared
//TM_OTP_Result_t TM_OTP_Write(uint8_t block, uint8_t byte, uint8_t data) {
//	HAL_StatusTypeDef status;
//
//	/* Check input parameters */
//	if (
//		block >= OTP_BLOCKS ||
//		byte >= OTP_BYTES_IN_BLOCK
//	) {
//		/* Invalid parameters */
//
//		/* Return error */
//		return TM_OTP_Result_Error;
//	}
//
//	/* Unlock FLASH */
//	HAL_FLASH_Unlock();
//
//	/* Clear pending flags (if any) */
//	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
//					FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
//
//	/* Wait for last operation */
//	status = FLASH_WaitForLastOperation(timeout);
//
//	/* If it is not success, return error */
//	if (status != HAL_OK) {
//		/* Lock FLASH */
//		HAL_FLASH_Lock();
//
//		/* Return error */
//		return TM_OTP_Result_Error;
//	}
//
//	/* Write byte */
//	FLASH_Program_Byte(OTP_START_ADDR + block * OTP_BYTES_IN_BLOCK + byte, data);
//	status = FLASH_WaitForLastOperation(timeout);
//
//	/* Lock FLASH */
//	HAL_FLASH_Lock();
//
//	/* Check status */
//	if (status == HAL_OK) {
//		/* Return OK */
//		return TM_OTP_Result_Ok;
//	} else {
//		/* Return error */
//		return TM_OTP_Result_Error;
//	}
//}

uint8_t TM_OTP_Read(uint8_t block, uint8_t byte) {
	uint8_t data;

	/* Check input parameters */
	if (
		block >= OTP_BLOCKS ||
		byte >= OTP_BYTES_IN_BLOCK
	) {
		/* Invalid parameters */
		return 0;
	}

	/* Get value */
	data = *(__IO uint8_t *)(OTP_START_ADDR + block * OTP_BYTES_IN_BLOCK + byte);

	/* Return data */
	return data;
}

TM_OTP_Result_t TM_OTP_BlockLock(uint8_t block) {
	HAL_StatusTypeDef status;

	/* Check input parameters */
	if (block >= OTP_BLOCKS) {
		/* Invalid parameters */

		/* Return error */
		return TM_OTP_Result_Error;
	}

	/* Unlock FLASH */
	HAL_FLASH_Unlock();

	/* Wait for last operation */
	status = FLASH_WaitForLastOperation(timeout);

	/* If it is not success, return error */
	if (status != HAL_OK) {
		/* Lock FLASH */
		HAL_FLASH_Lock();

		/* Return error */
		return TM_OTP_Result_Error;
	}

	/* Write byte */
	FLASH_Program_Byte(OTP_LOCK_ADDR + block, 0x00);
	status = FLASH_WaitForLastOperation(timeout);
	/* Lock FLASH */
	HAL_FLASH_Lock();

	/* Check status */
	if (status == HAL_OK) {
		/* Return OK */
		return TM_OTP_Result_Ok;
	}

	/* Return error */
	return TM_OTP_Result_Error;
}
