/*
 * LED.c
 * Implements a LED with settable duty cycle and frequency
 *  Created on: 23 Jan 2018
 *      Author: p.phillips
 */


#include "Hardware/LED.h"
#include "../Inc/stm32_device_select.h"


void LED_construct(LED_t *self,TIM_HandleTypeDef PWMTimer, uint32_t PWMChannel, uint32_t clockSpeed)
{
	self->PWMTimer=PWMTimer;
	self->PWMChannel=PWMChannel;
	self->frequency=1000.0;
	self->duty=0;

	self->setDuty=LED_setDuty;
	self->setFrequency=LED_setFrequency;
	self->LPFTemperature=LED_LPFTemperature;

	self->PWMConfig.OCMode = TIM_OCMODE_PWM1;
	self->PWMConfig.Pulse = 300;
	self->PWMConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
	self->PWMConfig.OCFastMode = TIM_OCFAST_DISABLE;
	self->Mode=led_mode_standby;

	self->ID=0x01;

	if (HAL_TIM_PWM_ConfigChannel(&self->PWMTimer, &self->PWMConfig, self->PWMChannel) != HAL_OK)
	{
	//Error_Handler();
	}

	if (HAL_TIM_PWM_Start_IT(&self->PWMTimer, self->PWMChannel) != HAL_OK)
	{

	//Error_Handler();
	}
}

void LED_setFrequency(LED_t *self,float  frequency)
{

  self->frequency=frequency;

  self->PWMTimer.Init.Period=self->clockSpeed/(frequency*(self->PWMTimer.Init.Prescaler-1));

  if (HAL_TIM_PWM_Init(&self->PWMTimer) != HAL_OK)
  {
//    _Error_Handler(__FILE__, __LINE__);
  }


}

void LED_setDuty(LED_t *self,float  duty)
{
	self->dutyDemand=duty;
	if(self->dutyDemand<0.0 ||self->dutyDemand>1.1 )
	{
		self->dutyDemand=self->duty;
		duty=0;
	}
	if(self->dutyDemand>1.0)
	{
		self->dutyDemand=1.0;
		duty=1;
	}


	if(self->dutyDemand>self->maxBrightness)
	{
		self->duty=self->maxBrightness; //temporary variable so not to change the actual desired duty value.
	}
	else
	{
		self->duty=self->dutyDemand;
	}

	self->PWMConfig.Pulse = (uint32_t)(self->PWMTimer.Init.Period*self->duty);

//	if (HAL_TIM_PWM_ConfigChannel(&self->PWMTimer, &self->PWMConfig, self->PWMChannel) != HAL_OK)
//	{
//	   // _Error_Handler(__FILE__, __LINE__);
//	}

	__HAL_TIM_SET_COMPARE(&self->PWMTimer, self->PWMChannel, self->PWMConfig.Pulse );
}

#define a

float LED_LPFTemperature(LED_t * self,float newReading, float alpha)
{
	float voltage=-149.0*(newReading/4096.0);
	voltage=voltage+133;
//	float newTemperature=(-45.164*(3.3*(newReading/4096))+133.46);

	self->temperature=self->temperature*(1-alpha)+(voltage)*alpha;
	return self->temperature;
}


float LED_TemperatureCheck(LED_t *self)
{
	if(self->temperature<2400.0)
	{
		self->maxBrightness=0.5;
	}
	else if(self->temperature<2250.0)
	{
		self->maxBrightness=0;
	}
	else
	{
		self->maxBrightness=1;
	}

	return 1;

}
