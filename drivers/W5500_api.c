/*
 * W5500_api.c
 *
 *  Created on: 29 Oct 2020
 *      Author: shaun
 */
#include <stdlib.h>
#include "Communication/W5500_api.h"
//#include "Ethernet/socket.h"

#define PASS 0
#define FAIL 1

extern W5500_t eth;

uint8_t W5500_CommTest(W5500_t* self);
uint8_t W5500_PhyConf(W5500_t* self);
uint16_t W5500_RecvUDP(W5500_socket_t* sock);


uint8_t W5500_SPI_ReadByte() {
	uint8_t outByte;
	HAL_SPI_Receive(eth.phspi, &outByte, 1, 100);
	return outByte;
}

void W5500_SPI_WriteByte(uint8_t wb) {
	HAL_StatusTypeDef ret = HAL_ERROR;
	while (ret != HAL_OK) {
		ret = HAL_SPI_Transmit(eth.phspi, &wb, 1, 100);
	}
}

void W5500_SPI_ReadByteBurst(uint8_t* pBuf, uint16_t len) {
	HAL_SPI_Receive(eth.phspi, pBuf, len, 100);
}

void W5500_SPI_WriteByteBurst(uint8_t* pBuf, uint16_t len) {
	HAL_SPI_Transmit(eth.phspi, pBuf, len, 100);
}

void W5500_SPI_ChipSelect() {
	HAL_GPIO_WritePin(eth.pSelectPort, eth.selectPin, GPIO_PIN_RESET);
}
void W5500_SPI_ChipDeselect() {
	HAL_GPIO_WritePin(eth.pSelectPort, eth.selectPin, GPIO_PIN_SET);
}

void W5500_SPI_ResetLow() {
	HAL_GPIO_WritePin(eth.pResetPort, eth.resetPin, GPIO_PIN_RESET);
}
void W5500_SPI_ResetHigh() {
	HAL_GPIO_WritePin(eth.pResetPort, eth.resetPin, GPIO_PIN_SET);
}

void W5500_SPI_CriticalStart() {}
void W5500_SPI_CriticalStop() {}

void W5500_Construct(W5500_t* self, SPI_HandleTypeDef* hspi,
		GPIO_TypeDef* pSelectPort, uint16_t selectPin,
		GPIO_TypeDef* pResetPort, uint16_t resetPin) {

	reg_wizchip_spi_cbfunc(W5500_SPI_ReadByte, W5500_SPI_WriteByte);
	reg_wizchip_spiburst_cbfunc(W5500_SPI_ReadByteBurst, W5500_SPI_WriteByteBurst);
	reg_wizchip_cs_cbfunc(W5500_SPI_ChipSelect, W5500_SPI_ChipDeselect);

	self->phspi = hspi;
	self->pSelectPort = pSelectPort;
	self->selectPin = selectPin;
	self->pResetPort = pResetPort;
	self->resetPin = resetPin;
}

void W5500_Error(W5500_t* self) {
	self->errorCount++;
}

void W5500_Init(W5500_t* self) {
	self->errorState = OK;

	W5500_SPI_ChipDeselect();
	W5500_SPI_ResetLow();
	uint16_t tmp = 10000; // Hold reset pin low for at least 500us
	while(tmp-- > 0);
	HAL_Delay(2);
	W5500_SPI_ResetHigh();
//	tmp = 10000;
//	while(tmp-- > 0); // Wait for W5500 to start after reset.
	HAL_Delay(2);

	if (W5500_CommTest(self) != PASS) {
		W5500_Error(self);
		self->errorState = COMS_ERROR;
		return;
	}

	uint8_t memsize[2][8] = {{2, 2, 2, 2, 2, 2, 2, 2},
							 {2, 2, 2, 2, 2, 2, 2, 2}};

	if(ctlwizchip(CW_INIT_WIZCHIP,(void*)memsize) == -1)
	{
		self->errorState = API_ERROR;
		return;
	}

	W5500_PhyConf(self);

	W5500_NetConf(self);
#ifdef DRAFT
	W5500_RecvUDP(&self->sockets[0]);
	while (1) {
		W5500_SendUDP(&self->sockets[0]);
	}
#endif
	return;
}

uint8_t W5500_CommTest(W5500_t* self) {
	uint8_t txData[10] = {0,};
	uint8_t rxData[10] = {0,};
	uint8_t len;

	txData[0] = 0;
	txData[1] = 0x39; // VERSIONR expect value 0x4
	txData[2] = 0; // COMMON REG block. FDM. Read.
	len = 4;

	W5500_SPI_ChipSelect();
	HAL_SPI_TransmitReceive(self->phspi, txData, rxData, len, 500);
	W5500_SPI_ChipDeselect();
	if (rxData[3] == 4) {
		return PASS;
	}
	return FAIL;
}

uint8_t W5500_NetConf(W5500_t* self) {
#ifdef DRAFT
	wiz_NetInfo gWIZNETINFO = {
		{ 0x11, 0x22, 0x33, 0x44, 0x55, 0x66 },		// Mac address
		{ 192, 168, 2, 4 },							// IP address
		{ 192, 168, 2, 1},							// Gateway
		{ 255, 255, 255, 0},						// Subnet mask
		{ 8, 8, 8, 0},								// DNS Server
	};
	gWIZNETINFO.dhcp = NETINFO_STATIC;

	ctlnetwork(CN_SET_NETINFO, (void*) &gWIZNETINFO);

	wiz_NetInfo ret_gWIZNETINFO;
	ctlnetwork(CN_GET_NETINFO, (void*) &ret_gWIZNETINFO);
#endif
	ctlnetwork(CN_SET_NETINFO, (void*) &self->netInfo);

	wiz_NetInfo ret_NETINFO;
	ctlnetwork(CN_GET_NETINFO, (void*) &ret_NETINFO);

	return 0;
}

uint8_t W5500_PhyConf(W5500_t* self) {
	/* PHY link status check */
	uint16_t tmp;
	do {
		if(ctlwizchip(CW_GET_PHYLINK, (void*)&tmp) == -1) {
			self->errorState = API_ERROR;
			return 1;
		}
	} while (tmp == PHY_LINK_OFF);

	// Configure phy to Auto Negotiation of speed and duplex.
	// We must set this here in case PMODE pins on W5500 have been set incorrectly.
	self->phyConf.by = PHY_CONFBY_SW;
	self->phyConf.mode = PHY_MODE_AUTONEGO;
	//	phyConf.mode = PHY_MODE_MANUAL;
	self->phyConf.speed = PHY_SPEED_100; // Redundant
	self->phyConf.duplex = PHY_DUPLEX_FULL; // Redundant
	ctlwizchip(CW_SET_PHYCONF, &self->phyConf);

	// Read it back and store results in self.
	ctlwizchip(CW_GET_PHYCONF, &self->phyConf);
	return 0;
}

void W5500_ConstructSocket(W5500_t* self, uint8_t sockIndex,
							W5500_socket_type_t sockType)
{
	W5500_socket_t* sock = &self->sockets[sockIndex];
	sock->index = sockIndex;
	sock->type = sockType;
	sock->flag = SF_IO_NONBLOCK;
	sock->port = 6780 + sockIndex;
	sock->txSize = W5500_DEFAULT_TX_SIZE;
	sock->rxSize = W5500_DEFAULT_RX_SIZE;
	sock->remoteIP[0] = 0;
	sock->remoteIP[1] = 0;
	sock->remoteIP[2] = 0;
	sock->remoteIP[3] = 0;
	sock->remotePort = 0;
	sock->bytesToRead = 0;

	circularQueueConstruct(&sock->txQ, sock->txSize);
	circularQueueConstruct(&sock->rxQ, sock->rxSize);
	sock->rxBuff = (uint8_t*)calloc(sock->rxSize, sizeof(uint8_t));
}

void W5500_InitSocket(W5500_socket_t* sock) {
	uint8_t intrMask;
	intrMask = SIK_ALL;
	ctlsocket(sock->index, CS_SET_INTMASK, &intrMask);
	if (socket(sock->index, sock->type, sock->port, sock->flag) != sock->index) {
		W5500_Error(&eth);
		eth.errorState = COMS_ERROR;
	}
}

void W5500_DestroySocket(W5500_socket_t* sock) {
	close(sock->index);
	free(sock->rxBuff);
}

uint32_t W5500_SendUDP(W5500_socket_t* sock) {
	int32_t ret;
	uint16_t txLength = sock->txQ.numel;
	uint8_t buf[txLength];
	sock->txQ.popArray(&sock->txQ, buf, txLength);

	ret = sendto(sock->index, buf, txLength, sock->remoteIP, sock->remotePort);
	return ret;
}

uint16_t W5500_RecvUDP(W5500_socket_t* sock) {
	int32_t ret;

	ret = recvfrom(sock->index, sock->rxBuff, sock->rxSize, sock->remoteIP, &sock->remotePort);

	if (ret > 0) { // bytes read successfully
		sock->bytesToRead = ret;
		return (uint16_t) ret;
	}
	else if (ret<0){ // error or no bytes received
		// TODO: handle errors
		sock->errorCode = ret;
		return 0;
	} else {
		sock->bytesToRead = 0;
		return (uint16_t) 0;
	}
}

uint16_t W5500_Read(W5500_socket_t* sock) {
	uint16_t numBytes = 1; // initialise to non-zero to enter the while loop.
	// The following should probably be a pointer to a function: socket->recv(sock)
	// That would add modularity for swapping UDP/TCP/other
	while (numBytes > 0) {
		numBytes = W5500_RecvUDP(sock);
		if (numBytes) {
			sock->rxQ.pushArray(&sock->rxQ, sock->rxBuff, numBytes);
		}
	}
	return numBytes;
}

uint8_t W5500_EnableSocketInterrupts() {
	intr_kind intrKind;
	intrKind = IK_SOCK_ALL;
	ctlwizchip(CW_SET_INTRMASK, &intrKind);

	return 0;
}

uint8_t W5500_GetInterrupts(W5500_socket_t* sock) {
	uint8_t intrReg = 0;
	// Reads interrupt
	ctlsocket(sock->index, CS_GET_INTERRUPT, &intrReg);
	if (intrReg) {
		// Clears interrupt
		ctlsocket(sock->index, CS_CLR_INTERRUPT, &intrReg);
	}
	return intrReg;
}
