/*
 * canbusStm32.c
 *
 *  Created on: 22 April 2021
 *      Author: Kyle McLean
 *
 *	Broken out of canbus.c
 */

#include "Communication/canbusStm32.h"
#include <stdio.h>

/* STM32 canbus defines */
#define MAX_CANBUS_LINKS 2
#define MAX_RX_FRAMES 100
#define MAX_RX_PACKET_NODES 100

/* Forward define external funtions */
extern void canbus_decodeHeader(canbus_headerData_t* pdecodedHeaderData, uint32_t encodedHeader);

/* STM32 canbus variables */
canbus_comms_link* canbus_list[MAX_CANBUS_LINKS] = {};
uint8_t canbus_list_length = 0;
uint8_t fifoErrorStm32 = 0;

/**
 * @brief	Constructor for a CAN bus device
 * @param	hcan pointer to CAN_HandleTypeDef
 * @retval 	returns zero
 */
uint8_t canbus_construct(canbus_comms_link* self, CAN_HandleTypeDef* hcan) {
	self->hcan = hcan;
	self->filterCount = 0;
	self->pendingFrames = 0;
	self->thirdPartyProtocol_Read = canbus_thirdPartyProtocol_Read;
	protocolManager_construct(&self->protocolManager);

	// Initialize the txFrameQ (two queues are used to ensure one is always available)
	txFrameQ_Construct(&self->txFrameQ, MAX_RX_FRAMES);
	txFrameQ_Construct(&self->txFrameQ_2, MAX_RX_FRAMES);

	// Initialize the rxFrameQs (two queues are used to ensure one is always available)
	rxFrameQ_Construct(&self->rxFrameQ, MAX_RX_FRAMES);
	rxFrameQ_Construct(&self->rxFrameQ_2, MAX_RX_FRAMES);

	// Initialize the rxQueue
	canbus_packetQ_Construct(&self->rxQueue, MAX_RX_PACKET_NODES);

	// Add this link to the global canbus_list
	if (canbus_list_length < MAX_CANBUS_LINKS) {
		canbus_list[canbus_list_length] = self;
		canbus_list_length++;
	}

	// Initialize the canbus with all frames accepted in case no device ID filters are added
	canbus_SetFilterAcceptAll(self);
	return 0;
}

/**
 * @brief	Enable the CAN device to start transmitting and receiving.
 * @param	coms_link pointer to the parent of this canbus_comms_link
 */
uint8_t canbus_enableRead(coms_link* parent) {
	canbus_comms_link* self = parent->canbusLink;

	if (HAL_CAN_ActivateNotification(self->hcan,
			CAN_IT_TX_MAILBOX_EMPTY | CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_BUSOFF)
			!= HAL_OK)
	{
		Error_Handler();
	}

	if (HAL_CAN_Start(self->hcan) != HAL_OK) {
		  Error_Handler();
	}
	return 0;
}

/**
 * @brief	Wrapper function for hcan_TransmitFrame(). Checks the bus_state and if tx_queued
 * adds a single data frame to the first free Tx mailbox.
 * @param	parent pointer to the coms_link
 * @param	frame pointer to canbus_frame_t to transmit
 * @retval 	HAL status
 */
canbus_tx_state_t canbus_TransmitFrame(coms_link* parent, txFrame_t* pFrame) {
	// Get the canbus link from the parent
	canbus_comms_link* self = parent->canbusLink;

	// If RX_ONLY, caller handles as if the canbus was busy
	if (parent->pBusStateMgr->state == bus_RX_ONLY) return CANBUS_TX_BUSY;

	// Flag that the bus is currently busy
	parent->pBusStateMgr->busy = 1;

	// Attempt to transmit frame on canbus
	canbus_tx_state_t canbus_tx_state = hcan_TransmitFrame(self->hcan, pFrame);

	// If there is still frames before RxOnly, decrement. Don't care if transmission was successful, this prevents getting stuck.
	if (parent->pBusStateMgr->state == bus_TX_QUEUED && self->pendingFrames > 0) {
		// Decrement the number of frames until we get back to the RX_only state
		self->pendingFrames--;
	}

	// If last frame to be transmitted, set back to RX_ONLY
	if(parent->pBusStateMgr->state == bus_TX_QUEUED && self->pendingFrames == 0) {
		parent->pBusStateMgr->busy = 0;  // Bus is not busy anymore
		parent->pBusStateMgr->state = bus_RX_ONLY;  // I'm back in RX_ONLY
	}

	return canbus_tx_state;
}

/**
 * @brief	Add a single data frame to the first free Tx mailbox and activate transmission
 * 			request on mailbox. If mailboxes are all full or not available, return HAL_ERROR.
 * @param	hcan pointer to a CAN_HandleTypeDef structure for the specified CAN.
 * @param	frame pointer to txFrame_t to transmit
 * @retval 	HAL status
 */
canbus_tx_state_t hcan_TransmitFrame(CAN_HandleTypeDef* hcan, txFrame_t* frame) {
	uint32_t TxMailbox;

	if (HAL_CAN_GetTxMailboxesFreeLevel(hcan) > 0) {
		/* if a mailbox is free, send immediately */
		if (HAL_CAN_AddTxMessage(hcan, &frame->TxHeader, frame->data, &TxMailbox) != HAL_OK) {
			Error_Handler();
			return CANBUS_TX_ERROR;
		}
	} else { /* mailboxes are all full, return CANBUS_TX_BUSY */
		return CANBUS_TX_BUSY;
	}

	return CANBUS_TX_OK;
}

/**
 * @brief	Attempt to transmit a single can frame.
 * @param	self pointer to a canbus_comms_link structure for the specified CAN.
 * @param	frame pointer to txFrame_t to transmit
 */
void canbus_AttemptTransmitFrame(canbus_comms_link* self, txFrame_t* pFrame) {
	// Attempt to transmit the frame
	if (canbus_TransmitFrame(self->parent, pFrame) != CANBUS_TX_OK) {
		// If the packet was not transmitted, add it a queue
		if(!self->txFrameQ.locked && self->txFrameQ.numel != self->txFrameQ.maxel) {
			self->txFrameQ.Push(&self->txFrameQ, pFrame);
		} else if(!self->txFrameQ_2.locked && self->txFrameQ_2.numel != self->txFrameQ_2.maxel) {
			self->txFrameQ_2.Push(&self->txFrameQ_2, pFrame);
		} else {
			fifoErrorStm32++;  // Frame has been dropped!
		}
	}
}

/**
 * @brief	Sets the first filter on the provided canbus to accept all frames.
 * 			This makes all other filters redundant.
 * 			Should be used only when no filtering is required.
 * @param	canbus_comms_link* self pointer to canbus_comms_link
 */
void canbus_SetFilterAcceptAll(canbus_comms_link* self) {
	CAN_HandleTypeDef* hcan = self->hcan;

	CAN_FilterTypeDef can_filter_init;

	can_filter_init.FilterActivation = ENABLE;
	can_filter_init.FilterBank = 0;
	can_filter_init.FilterFIFOAssignment = CAN_RX_FIFO0;
	can_filter_init.FilterIdHigh = 0x0000;
	can_filter_init.FilterIdLow = 0x0000;
	can_filter_init.FilterMaskIdHigh = 0x0000;
	can_filter_init.FilterMaskIdLow = 0x0000;
	can_filter_init.FilterMode = CAN_FILTERMODE_IDMASK;
	can_filter_init.FilterScale = CAN_FILTERSCALE_32BIT;

	if (HAL_CAN_ConfigFilter(hcan, &can_filter_init) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief	Add a filter to accept frames with specified device ID
 * @param	coms_link* parent pointer to parent coms_link struct
 * @param	uint8_t deviceID to accept
 */
void canbus_AddFilterForDeviceID(coms_link* parent, uint8_t deviceID) {
	if (parent->comsType == COMS_TYPE_CANBUS) {
		canbus_comms_link* self = parent->canbusLink;
		CAN_HandleTypeDef* hcan = self->hcan;
		CAN_FilterTypeDef can_filter_init;

		can_filter_init.FilterActivation = ENABLE;
		can_filter_init.FilterBank = self->filterCount;
		can_filter_init.FilterFIFOAssignment = CAN_RX_FIFO0;
		can_filter_init.FilterIdHigh = (uint32_t) deviceID << 3; // Shift deviceID for filter mapping
		can_filter_init.FilterIdLow = 0x0000 | CAN_ID_EXT; // bit 3 is IDE. High for EXT ID.
		can_filter_init.FilterMaskIdHigh = 0x07F8; // Only compare bits 10 through 3 of the Mask High
		can_filter_init.FilterMaskIdLow = 0x0000 | CAN_ID_EXT; // bit 3 is IDE. High for EXT ID.
		can_filter_init.FilterMode = CAN_FILTERMODE_IDMASK;
		can_filter_init.FilterScale = CAN_FILTERSCALE_32BIT;

		if (HAL_CAN_ConfigFilter(hcan, &can_filter_init) != HAL_OK) {
			Error_Handler();
		}

		self->filterCount++; // increment the number of filters initialised
	}
}

/**
 * @brief	Add a filter to accept frames with FWD_ALLOW bit set
 * @param	coms_link* parent pointer to parent coms_link struct
 */
void canbus_AddFilterForForwardAllow(coms_link* parent) {
	if (parent->comsType == COMS_TYPE_CANBUS) {
		canbus_comms_link* self = parent->canbusLink;
		CAN_HandleTypeDef* hcan = self->hcan;
		CAN_FilterTypeDef can_filter_init;

		can_filter_init.FilterActivation = ENABLE;
		can_filter_init.FilterBank = self->filterCount;
		can_filter_init.FilterFIFOAssignment = CAN_RX_FIFO0;
		can_filter_init.FilterIdHigh = 0x0800; // FWD_ALLOW bit
		can_filter_init.FilterIdLow = 0x0000 | CAN_ID_EXT; // bit 3 is IDE. High for EXT ID.
		can_filter_init.FilterMaskIdHigh = 0x0800; // Only compare bit 11 of the Mask High
		can_filter_init.FilterMaskIdLow = 0x0000 | CAN_ID_EXT; // bit 3 is IDE. High for EXT ID.
		can_filter_init.FilterMode = CAN_FILTERMODE_IDMASK;
		can_filter_init.FilterScale = CAN_FILTERSCALE_32BIT;

		if (HAL_CAN_ConfigFilter(hcan, &can_filter_init) != HAL_OK) {
			Error_Handler();
		}

		self->filterCount++; // increment the number of filters initialised
	}
}

/**
 * @brief	Add a filter to accept frames with FWD_ALLOW bit set
 * @param	coms_link* parent pointer to parent coms_link struct
 */
void canbus_AddFilterForStdFrames(coms_link* parent) {
	if (parent->comsType == COMS_TYPE_CANBUS) {
		canbus_comms_link* self = parent->canbusLink;
		CAN_HandleTypeDef* hcan = self->hcan;
		CAN_FilterTypeDef can_filter_init;

		can_filter_init.FilterActivation = ENABLE;
		can_filter_init.FilterBank = self->filterCount;
		can_filter_init.FilterFIFOAssignment = CAN_RX_FIFO0;
		can_filter_init.FilterIdHigh = 0x0000;
		can_filter_init.FilterIdLow = 0x0000 | CAN_ID_STD; // bit 3 is IDE. Low for STD ID.
		can_filter_init.FilterMaskIdHigh = 0x0000;
		can_filter_init.FilterMaskIdLow = 0x0000 | CAN_ID_EXT; // bit 3 is IDE. Low for STD ID. Mask must be HIGH (EXT).
		can_filter_init.FilterMode = CAN_FILTERMODE_IDMASK;
		can_filter_init.FilterScale = CAN_FILTERSCALE_32BIT;

		if (HAL_CAN_ConfigFilter(hcan, &can_filter_init) != HAL_OK) {
			Error_Handler();
		}

		self->filterCount++; // increment the number of filters initialised
	}
}

/**
 * @brief	Returns pointer to the canbus_comms_link containing the
 * 			CAN_HandleTypeDef at hcan
 * @param	hcan pointer to the CAN_HandleTypeDef
 * @retval	pointer to the relevant canbus_comms_link or NULL if none found
 */
canbus_comms_link* canbus_GetLinkFromHcan(CAN_HandleTypeDef* hcan) {
	canbus_comms_link* thisLink;
	for (uint8_t i = 0; i < canbus_list_length; i++) {
		thisLink = canbus_list[i];
		if (thisLink->hcan == hcan) {
			return thisLink;
		}
	}
	return NULL;
}

/**
 * @brief	Provide NOP for the Protocols Read function.
 * 			Used when no Third Party Protocols implemented.
 * @retval 	Zero indicates frames not parsed by a protocol.
 */
uint8_t canbus_NOP_thirdPartyProtocol_Read(canbus_comms_link* self,
					CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pData) {
	return 0;
}

/**
 * @brief	Send frame to protocol manager. All protocols will inspect the frame
 * 			and return 1 if relevant.
 * @retval 	Zero indicates frames not parsed by a protocol.
 */
uint8_t canbus_thirdPartyProtocol_Read(canbus_comms_link* self,
					CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pData) {

	return self->protocolManager.ReadAll(&self->protocolManager, pRxHeader, pData);
}

/**
 * &brief	Transmit all frames on third party protocol queues.
 */
uint8_t canbus_thirdPartyProtocol_Transmit(canbus_comms_link* self) {
	uint8_t numFrames = 0;
	canbus_frame_t frame;
	while (protocolManager_getQueuedFrame(&self->protocolManager, &frame)) {
		canbus_AttemptTransmitFrame(self, (txFrame_t*) &frame);
		numFrames++;
	}
	return numFrames;
}

/**
 * @brief	Overwrites the default HAL method
 * @param	pointer to the hcan handle throwing the error
 */
void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan) {
	char msg[50];
	sprintf(msg, "CAN Error Detected\r\n");
//	HAL_UART_Transmit(&huart3, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
}


/**
 * @brief	Overwrite default HAL method.
 * 			Called when the mailbox completes transmission.
 * 			Attempts to transmit the next frame in the txQueue
 * @param	hcan pointer to CAN handle
 */
void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan) {
	canbus_comms_link* thisLink;
	thisLink = canbus_GetLinkFromHcan(hcan);
	if (thisLink == NULL) {
		return;
	}

	if (HAL_CAN_GetTxMailboxesFreeLevel(hcan) == 0) {
		return;
	}

	// If we're finished transmitting, then set timeout to indicate next device frame can be sent
	bus_state_mgr_t* pBusStateMgr = thisLink->parent->pBusStateMgr;
	if (!thisLink->pendingFrames && *pBusStateMgr->pCurrentDevice == *pBusStateMgr->pDeviceID) {
		pBusStateMgr->txCplt = 1;
	}

	// Sort the queues
	txFrameQ_t* biggestQ;
	txFrameQ_t* smallestQ;
	if (thisLink->txFrameQ.numel > thisLink->txFrameQ_2.numel) {
		biggestQ = &thisLink->txFrameQ;
		smallestQ = &thisLink->txFrameQ_2;
	} else {
		biggestQ = &thisLink->txFrameQ_2;
		smallestQ = &thisLink->txFrameQ;
	}

	// Try to transmit the next frame on the most full queue first
	txFrame_t frame;
	if (!biggestQ->locked) {
		if (biggestQ->PopPreview(biggestQ, (txFrame_t*)&frame)) {
			// Attempt to transmit the frame
			if (canbus_TransmitFrame(thisLink->parent, &frame) == CANBUS_TX_OK) {
				// Only remove frame from the queue if the transmit is successful
				biggestQ->Erase(biggestQ);
			}
		}
	}

	if (!smallestQ->locked) {
		if (smallestQ->PopPreview(smallestQ, (txFrame_t*)&frame)) {
			// Attempt to transmit the frame
			if (canbus_TransmitFrame(thisLink->parent, &frame) == CANBUS_TX_OK) {
				// Only remove frame from the queue if the transmit is successful
				smallestQ->Erase(smallestQ);
			}
		}
	}
}

void HAL_CAN_TxMailbox1CompleteCallback(CAN_HandleTypeDef *hcan) {
	HAL_CAN_TxMailbox0CompleteCallback(hcan);
}

void HAL_CAN_TxMailbox2CompleteCallback(CAN_HandleTypeDef *hcan) {
	HAL_CAN_TxMailbox0CompleteCallback(hcan);
}

/**
 * @brief	Overwrites the HAL_CAN_RxFifo0MsgPendingCallback() function.
 * 			Called by interrupt on receipt of frame on canbus.
 * @param	CAN_HandleTypeDef* hcan pointer to the handle for can peripheral
 */
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan) {
	canbus_comms_link* thisLink;
	thisLink = canbus_GetLinkFromHcan(hcan);
	if (thisLink == NULL) {
		return;
	}

	bus_state_mgr_t* pBusStateMgr = thisLink->parent->pBusStateMgr;
	rxFrame_t newFrame;

	if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &newFrame.header, newFrame.data) != HAL_OK) {
		Error_Handler();
	}

	// Check if frame is for 3rd party protocol
	// *****Note*****
	// Third party devices are expected to follow the master device (708) in the bus state schedule
	// **************
	if(thisLink->thirdPartyProtocol_Read(thisLink, &newFrame.header, newFrame.data) ) {
		// A new frame has come in, bus is busy
		pBusStateMgr->busy = 1;

		return; // Frame was handled by third party protocol. Do not parse as BPL protocol.
	}

	if (newFrame.header.IDE == CAN_ID_EXT) {
		canbus_headerData_t decodedHeader;
		canbus_decodeHeader(&decodedHeader, newFrame.header.ExtId);

		if(decodedHeader.totalFrames == 1 && decodedHeader.packetID == pBusStateMgr->busStatePID
				&& decodedHeader.deviceID == *pBusStateMgr->pDeviceID && newFrame.data[0] == bus_TX_QUEUED) {
			pBusStateMgr->HandleBusState(thisLink->parent, newFrame.data[0]);
			return; // BUS_STATE frame already handled
		}

		if(decodedHeader.totalFrames == 1 && decodedHeader.packetID == pBusStateMgr->busStatePID
				&& decodedHeader.deviceID == *pBusStateMgr->pCurrentDevice && newFrame.data[0] == bus_RX_ONLY) {
			pBusStateMgr->busy = 0; // Transmission has finished, bus not busy
			pBusStateMgr->txCplt = 1;
			return;	// BUS_STATE frame already handled
		}

		if(decodedHeader.totalFrames == 1 && decodedHeader.packetID == pBusStateMgr->busStatePID
				&& decodedHeader.deviceID == *pBusStateMgr->pDeviceID && newFrame.data[0] == bus_TX_AT_WILL) {
			pBusStateMgr->state = bus_TX_AT_WILL;
			return; // BUS_STATE frame already handled
		}
	}

	// A new frame has come in, bus is busy
	pBusStateMgr->busy = 1;

	if(!thisLink->rxFrameQ.locked) {
		thisLink->rxFrameQ.Push(&thisLink->rxFrameQ, &newFrame);
	} else if(!thisLink->rxFrameQ_2.locked) {
		thisLink->rxFrameQ_2.Push(&thisLink->rxFrameQ_2, &newFrame);
	} else {
		fifoErrorStm32++;
	}
}
