/*
 * udpLinux.c
 *
 *  Created on: April 2021
 *      Author: Kyle McLean
 */

#include "Communication/udpLinux.h"
#include "Communication/comsprocess.h"

uint8_t udpLinux_Construct(udp_linux_link_t* self, uint8_t rxSize, uint16_t port) {
	self->rxSize = rxSize;
    self->port = port;

    // Create a circular queue to match format of -> coms_readPacketFromQ()
    circularQueueConstruct(&self->rxQ, self->rxSize);

	return 0;
}

uint8_t udpLinux_Init(udp_linux_link_t* self) {
	self->slen = sizeof(self->cliaddr);

    // Create a UDP socket
    if ((self->sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // zero out the structures
    memset((char *) &self->cliaddr, 0, sizeof(self->cliaddr));
    memset((char *) &self->servaddr, 0, sizeof(self->servaddr));

    // Server information
    self->servaddr.sin_family = AF_INET;
    self->servaddr.sin_port = htons(self->port);
    self->servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    // bind socket to port
    if (bind(self->sock, (const struct sockaddr*) &self->servaddr, sizeof(self->servaddr)) < 0) {
        perror("bind");
        exit(EXIT_FAILURE);
    }
    
    return 0;
}

uint8_t udpLinux_Read(coms_link* parent) {
	udp_linux_link_t* self = parent->udpLinuxLink;
	
	// Read the first packet from the queue
	return coms_readPacketFromQ(&parent->currPacket, &self->rxQ);
}

/**
 * @brief	Read bytes from the socket file descriptor and push to the RxQ
 */
uint8_t udpLinux_ReadBytes(udp_linux_link_t* self) {
	
    // Read from the socket at the specified file descriptor on to the buffer
	uint8_t recv_len = 0;
    uint8_t* rxBuff = (uint8_t*)calloc(self->rxSize, sizeof(uint8_t));
    
    if ((recv_len = recvfrom(self->sock, rxBuff, self->rxSize, 0, (struct sockaddr *) &self->cliaddr, &self->slen)) < 0) {
        perror("recvfrom()");
        exit(EXIT_FAILURE);
    }

    // Push buffered data into the rx queue.
    if (recv_len) {
        self->rxQ.pushArray(&self->rxQ, rxBuff, recv_len);
    }
    
    // Free the buffer 
    free(rxBuff);
		
	return 0;
}


uint8_t udpLinux_Transmit(coms_link* parent, packet_t* packet) {
	udp_linux_link_t* self = parent->udpLinuxLink;

    if (self->cliaddr.sin_addr.s_addr) {
        if (sendto(self->sock, packet->transmitData, packet->length, 0, (struct sockaddr*) &self->cliaddr, self->slen) < 0) {
            perror("sendto()");
            exit(EXIT_FAILURE);
        }
    }

	return 0;
}

uint8_t udpLinux_Close(udp_linux_link_t* self) {
    self->rxQ.deconstruct(&self->rxQ);
    
    if (close(self->sock) < 0) {
        perror("Close");
        return 1;
    }
    return 0;
}

uint8_t udpLinux_enableRead(coms_link* parent) {
    udp_linux_link_t* self = parent->udpLinuxLink;
    return udpLinux_Init(self);
}

/* ============================================================================  */
uint8_t udpLinuxClient_Construct(udp_linux_link_t* self, uint8_t rxSize, uint16_t port) {
	self->rxSize = rxSize;
    self->port = port;

    // Create a circular queue to match format of -> coms_readPacketFromQ()
    circularQueueConstruct(&self->rxQ, self->rxSize);

	return 0;
}

uint8_t udpLinuxClient_Init(udp_linux_link_t* self) {
	self->slen = sizeof(self->servaddr);

    // Create a UDP socket
    if ((self->sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // zero out the structures
    memset((char *) &self->cliaddr, 0, sizeof(self->cliaddr));
    memset((char *) &self->servaddr, 0, sizeof(self->servaddr));

    // Server information
    self->servaddr.sin_family = AF_INET;
    self->servaddr.sin_port = htons(self->port);
    /*self->servaddr.sin_addr.s_addr = htonl(INADDR_ANY);*/
    /*inet_pton(AF_INET, "192.168.2.4", &(self->servaddr.sin_addr));*/
    inet_pton(AF_INET, "192.168.1.6", &(self->servaddr.sin_addr));
    
    // bind socket to port
    if (connect(self->sock, (const struct sockaddr*) &self->servaddr, sizeof(self->servaddr)) < 0) {
        perror("connect");
        exit(EXIT_FAILURE);
    }
    
    return 0;
}

uint8_t udpLinuxClient_Read(coms_link* parent) {
    udp_linux_link_t* self = parent->udpLinuxLink;
    
    // Read the first packet from the queue
    return coms_readPacketFromQ(&parent->currPacket, &self->rxQ);
}

/**
 * @brief	Read bytes from the socket file descriptor and push to the RxQcoms_link
 */
uint8_t udpLinuxClient_ReadBytes(udp_linux_link_t* self) {
	
    // Read from the socket at the specified file descriptor on to the buffer
	uint8_t recv_len = 0;
    uint8_t* rxBuff = (uint8_t*)calloc(self->rxSize, sizeof(uint8_t));
    
    if ((recv_len = recvfrom(self->sock, rxBuff, self->rxSize, 0, (struct sockaddr *) &self->servaddr, &self->slen)) < 0) {
        perror("recvfrom()");
        exit(EXIT_FAILURE);
    }

    // Push buffered data into the rx queue.
    if (recv_len) {
        self->rxQ.pushArray(&self->rxQ, rxBuff, recv_len);
    }
    
    // Free the buffer 
    free(rxBuff);
		
	return 0;
}


uint8_t udpLinuxClient_Transmit(coms_link* parent, packet_t* packet) {
	udp_linux_link_t* self = parent->udpLinuxLink;

    if (self->servaddr.sin_addr.s_addr) {
        if (sendto(self->sock, packet->transmitData, packet->length, 0, (struct sockaddr*) &self->servaddr, self->slen) < 0) {
            perror("sendto()");
            exit(EXIT_FAILURE);
        }
    }

	return 0;
}

uint8_t udpLinuxClient_Close(udp_linux_link_t* self) {
    self->rxQ.deconstruct(&self->rxQ);
    
    if (close(self->sock) < 0) {
        perror("Close");
        return 1;
    }
    return 0;
}

uint8_t udpLinuxClient_enableRead(coms_link* parent) {
    udp_linux_link_t* self = parent->udpLinuxLink;
    return udpLinuxClient_Init(self);
}
