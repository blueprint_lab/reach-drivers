/*
 * deviceid.c
 *
 *  Created on: 27Nov.,2019
 *      Author: s.barlow
 */

#include "Communication/deviceid.h"
#include "packets.h"
#define DEVICE_ID_CONFLICT_TIMEOUT_DURATION_MS 500


/**
 * @brief	Saves a device id to the flash location accessible by bootloader
 */
uint8_t Bootloader_SaveDeviceID(uint8_t deviceID) {
	uint64_t flash_data = deviceID;

	HAL_FLASH_Unlock();
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );
	FLASH_Erase_Sector(FLASH_SECTOR_3, VOLTAGE_RANGE_3);
	uint8_t status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, DEVICE_ID_FLASH_ADDRESS, flash_data);
//	HAL_FLASH_Lock();
	return status;
}

/**
 * @brief	Returns device id from bootloader flash location
 */
uint8_t Bootloader_ReadDeviceID() {
	uint8_t deviceID;
	deviceID = *(uint8_t*)DEVICE_ID_FLASH_ADDRESS;
	return deviceID;
}

void Bootloader_SaveEthSettings(uint32_t deviceIP, uint16_t devicePort, uint32_t deviceGW, uint32_t deviceSN) {
	uint8_t deviceID;
	deviceID = *(uint8_t*)DEVICE_ID_FLASH_ADDRESS;
	HAL_FLASH_Unlock();
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );
	FLASH_Erase_Sector(FLASH_SECTOR_3, VOLTAGE_RANGE_3);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, DEVICE_ID_FLASH_ADDRESS, deviceID);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, DEVICE_IP_FLASH_ADDRESS, deviceIP);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, DEVICE_PORT_FLASH_ADDRESS, devicePort);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, DEVICE_GW_FLASH_ADDRESS, deviceGW);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, DEVICE_SN_FLASH_ADDRESS, deviceSN);
}

void Bootloader_SaveDeviceIP(uint32_t deviceIP) {
	uint32_t flash_ip = deviceIP;
	uint16_t flash_port = *(uint16_t*)DEVICE_PORT_FLASH_ADDRESS;
	uint32_t flash_gw = *(uint32_t*)DEVICE_GW_FLASH_ADDRESS;
	uint32_t flash_sn = *(uint32_t*)DEVICE_SN_FLASH_ADDRESS;
	Bootloader_SaveEthSettings(flash_ip, flash_port, flash_gw, flash_sn);
}

uint32_t Bootloader_ReadDeviceIP() {
	uint32_t deviceIP;
	deviceIP = *(uint32_t*)DEVICE_IP_FLASH_ADDRESS;
	return deviceIP;
}

void Bootloader_SaveDevicePort(uint16_t devicePort) {
	uint16_t flash_port = devicePort;
	uint32_t flash_ip = *(uint32_t*)DEVICE_IP_FLASH_ADDRESS;
	uint32_t flash_gw = *(uint32_t*)DEVICE_GW_FLASH_ADDRESS;
	uint32_t flash_sn = *(uint32_t*)DEVICE_SN_FLASH_ADDRESS;
	Bootloader_SaveEthSettings(flash_ip, flash_port, flash_gw, flash_sn);
}

uint16_t Bootloader_ReadDevicePort() {
	uint16_t devicePort;
	devicePort = *(uint16_t*)DEVICE_PORT_FLASH_ADDRESS;
	return devicePort;
}

void Bootloader_SaveDeviceGW(uint32_t deviceGW) {
	uint32_t flash_ip = *(uint32_t*)DEVICE_IP_FLASH_ADDRESS;
	uint16_t flash_port = *(uint16_t*)DEVICE_PORT_FLASH_ADDRESS;
	uint32_t flash_gw = deviceGW;
	uint32_t flash_sn = *(uint32_t*)DEVICE_SN_FLASH_ADDRESS;
	Bootloader_SaveEthSettings(flash_ip, flash_port, flash_gw, flash_sn);
}

uint32_t Bootloader_ReadDeviceGW() {
	uint32_t deviceGW;
	deviceGW = *(uint32_t*)DEVICE_GW_FLASH_ADDRESS;
	return deviceGW;
}

void Bootloader_SaveDeviceSN(uint32_t deviceSN) {
	uint32_t flash_ip = *(uint32_t*)DEVICE_IP_FLASH_ADDRESS;
	uint16_t flash_port = *(uint16_t*)DEVICE_PORT_FLASH_ADDRESS;
	uint32_t flash_gw = *(uint32_t*)DEVICE_GW_FLASH_ADDRESS;
	uint32_t flash_sn = deviceSN;
	Bootloader_SaveEthSettings(flash_ip, flash_port, flash_gw, flash_sn);
}

uint32_t Bootloader_ReadDeviceSN() {
	uint32_t deviceSN;
	deviceSN = *(uint32_t*)DEVICE_SN_FLASH_ADDRESS;
	return deviceSN;
}



/**
 * @brief	Construct a decimal number split up into bytes and compare with a
 * 			full uint16_t of the number.
 * @retval	1 for match, 0 for no match.
 */
uint8_t DeviceID_MatchBytesWithInt(uint8_t thousands, uint8_t hundreds,
									uint8_t tens, uint8_t ones, uint16_t full) {
	uint16_t combined = (1000 * thousands) + (100 * hundreds) + (10 * tens) + (1 * ones);
	if (combined == full) {
		return 1;  // Match
	}
	return 0;  // No match
}

/**
 * @brief	Request device id and set a timeout time.
 * 			If we receive a DEVICE_ID packet with both address
 * 			and payload as my device id before timeout time, register a conflict.
 *
 */
uint8_t DeviceID_StartDeviceIDConflictCheck(deviceManager_t* self,
													coms_link* pComsLink) {
	// Clear flag first. It will be set again if a conflict is detected.
	CLR_FLAG(self->monitorStatus, status_deviceIDConflict);
	// Request device id on internal bus
	uint8_t data = DEVICE_ID;
	packets_encodeSingle(pComsLink, REQUEST, self->deviceID,
			&data, sizeof(data), PACKET_OPT_FWD_DENY);
	// Set timeout for conflict check
	self->deviceIDConflictTimeout = HAL_GetTick() + DEVICE_ID_CONFLICT_TIMEOUT_DURATION_MS;
	return 0;
}

/**
 * @brief	Check if the device id packet indicates a conflict.
 * 			That is,
 * 				1) are we within the timeout period?
 * 				2) does packet's address equal my device id?
 * 			 	3) does packet's data equal my device id?
 * @retval	1 for conflict, 0 for no conflict
 */
uint8_t DeviceID_CheckDeviceIDPacketForConflict(deviceManager_t* self,
													packet_t* packet) {
	if (HAL_GetTick() < self->deviceIDConflictTimeout) {
		if (	(packet->data[0] == self->deviceID)
				&& (packet->address == self->deviceID)) {
			// Conflict detected. Set status flag and enter disable mode.
			SET_FLAG(self->monitorStatus, status_deviceIDConflict);
			self->mode = mode_disabled;
			return 1;  // Conflict
		}
	}
	return 0;  // No conflict
}
