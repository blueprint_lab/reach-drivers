/*
 * comslink.c
 *
 *  Created on: Aug 26, 2019
 *      Author: Shaun Barlow
 *
 *	Communications link module
 *
 */

#include "Communication/comslink.h"
#include "Communication/comsprocess.h"
#if defined(HAL_CAN_MODULE_ENABLED) || defined(LINUX)
#include "Communication/canbus.h"
#endif

#include <stdlib.h>

uint8_t comslink_NOP_Transmit(coms_link* self, packet_t* packet);
int8_t comslink_NOP_encodePacket(packet_t* packet, uint8_t address, uint16_t code, uint8_t length, uint8_t* data, uint8_t priority);

#ifdef HAL_UART_MODULE_ENABLED
/**
 * @brief	Create and construct the UART link for this coms_link.
 * 			Sets the type to UART, initialises values and sets function pointers.
 * @param	self
 * @param	huart
 * @param	rxBuffLen
 * @param	txBuffLen
 * @param	protocolType
 * @param	hdmaRX
 * @param	hdmaTX
 * @retval	zero
 */
uint8_t comslink_construct_uart(coms_link* self, UART_HandleTypeDef* huart,
		uint16_t rxBuffLen, uint16_t txBuffLen, protocol_t* pComsProtocol, uint8_t* pDeviceID)
{
	self->comsType = COMS_TYPE_UART;
	self->pDeviceID = pDeviceID;
	self->pComsProtocol = pComsProtocol;

	self->uartLink = (uart_comms_link*) malloc(sizeof(uart_comms_link));
	uart_comms_construct(self->uartLink, huart, rxBuffLen, txBuffLen, 0);

	self->busState = bus_TX_AT_WILL;

	self->Read = uart_comms_Read_Packet_IT;
	self->Transmit = uart_comms_TransmitData;
	self->enableRead = uart_comms_enableRead;
	self->encodePacket = coms_encodePacket;
	self->transmitDMA = uart_comms_TransmitDMA;
	self->Destroy = comslink_destroy;

	// Add this link to the global uart_list
	if (uart_list_length < MAX_UART_LINKS) {
		uart_list[uart_list_length] = self;
		uart_list_length++;
	}
	return 0;
}
#endif

#if defined(HAL_SPI_MODULE_ENABLED) && !defined(G_TEST)
uint8_t comslink_construct_udp(coms_link* self, W5500_socket_t* sock, protocol_t* pComsProtocol) {
	self->comsType = COMS_TYPE_UDP;
	self->pComsProtocol = pComsProtocol;

	self->udpLink = (udp_comms_link*) malloc(sizeof(udp_comms_link));
	udp_construct(self->udpLink, sock);

	self->Read = udp_Read;
	self->Transmit = udp_QForTransmit;
	self->enableRead = udp_enableRead;
	self->encodePacket = coms_encodePacket;
	self->transmitDMA = udp_Transmit;
	self->Destroy = comslink_destroy;
	self->busState = bus_TX_AT_WILL;
	return 0;
}
#endif

#ifdef HAL_PCD_MODULE_ENABLED
/**
 * @brief	Create and construct the UART link for this coms_link.
 * 			Sets the type to CDC, initialises values and sets function pointers.
 * @param	self
 * @param	rxBuffLen
 * @param	txBuffLen
 * @param	protocolType
 * @retval	zero
 */
uint8_t comslink_construct_cdc(coms_link* self, uint16_t rxBuffLen, uint16_t txBuffLen, protocol_t* pComsProtocol) {
	self->comsType = COMS_TYPE_CDC;
	self->pComsProtocol = pComsProtocol;

	self->uartLink = (cdc_comms_link*) malloc(sizeof(cdc_comms_link));
	cdc_comms_construct(self->uartLink, rxBuffLen, txBuffLen);

	self->busState = bus_TX_AT_WILL;

	self->Read = cdc_comms_Read_Packet_IT;
	self->Transmit = cdc_comms_TransmitData;
	self->enableRead = cdc_comms_enableRead;
	self->encodePacket = coms_encodePacket;
	self->transmitDMA = cdc_comms_Transmit;

	return 0;
}
#endif
/**
 * @brief	Create and construct the CANBUS link for this coms_link.
 * 			Sets the type to CANBUS, initialises value and sets the function pointers.
 * @param	self coms_link pointer
 * @param	hcan pointer to CAN_HandleTypeDef
 * @retval	zero
 */
#ifdef HAL_CAN_MODULE_ENABLED
uint8_t comslink_construct_canbus(coms_link* self, CAN_HandleTypeDef* hcan, uint8_t* pDeviceID, protocol_t* pComsProtocol) {
	self->comsType = COMS_TYPE_CANBUS;
	self->pComsProtocol = pComsProtocol;

	self->canbusLink = (canbus_comms_link*) malloc(sizeof(canbus_comms_link));
	canbus_construct(self->canbusLink, hcan);

	self->pBusStateMgr = (bus_state_mgr_t*) malloc(sizeof(bus_state_mgr_t));

	self->busState = bus_TX_AT_WILL;
	canbus_construct_busStateMgr(self, self->pBusStateMgr, &self->busState, pDeviceID, BUS_STATE, MAX_CANBUS_DEVICES);

	self->canbusLink->parent = self;

	self->Read = canbus_Read;
	self->Transmit = canbus_Transmit;
	self->enableRead = canbus_enableRead;
	self->encodePacket = canbus_PopulatePacket;
	self->transmitDMA = comslink_null;
	self->Destroy = comslink_destroy;
	return 0;
}
#endif

#ifdef LINUX
uint8_t comslink_construct_canbus_linux(coms_link* self, uint16_t frameBuffLen, uint8_t* pDeviceID) {
	self->comsType = COMS_TYPE_CAN_LINUX;

	self->canbusLinuxLink = (canbus_linux_link_t*) malloc(sizeof(canbus_linux_link_t));
	canbusLinux_Construct(self->canbusLinuxLink, frameBuffLen, pDeviceID);

	self->pBusStateMgr = (bus_state_mgr_t*) malloc(sizeof(bus_state_mgr_t));
	canbus_construct_busStateMgr(self, self->pBusStateMgr, &self->busState, pDeviceID, BUS_STATE, MAX_CANBUS_DEVICES);

	self->canbusLinuxLink->parent = self;

	self->Read = canbus_Read;
	self->Transmit = canbus_Transmit;
	self->enableRead = canbusLinux_enableRead;
	self->encodePacket = canbus_PopulatePacket;
	self->transmitDMA = comslink_null;
	self->Destroy = comslink_destroy;
	self->busState = bus_TX_AT_WILL;
	return 0;
}

uint8_t comslink_construct_udp_linux(coms_link* self, uint8_t rxQLen, float port) {
	self->comsType = COMS_TYPE_UDP_LINUX;

	self->udpLinuxLink = (udp_linux_link_t*) malloc(sizeof(udp_linux_link_t));
	udpLinux_Construct(self->udpLinuxLink, rxQLen, port);

	self->udpLinuxLink->parent = self;

	self->Read = udpLinux_Read;
	self->Transmit = udpLinux_Transmit;
	self->enableRead = udpLinux_enableRead;
	self->encodePacket = coms_encodePacket;
	self->transmitDMA = comslink_null;
	self->Destroy = comslink_destroy;
	self->busState = bus_TX_AT_WILL;
	return 0;
}

uint8_t comslink_construct_udp_client_linux(coms_link* self, uint8_t rxQLen, float port) {
	self->comsType = COMS_TYPE_UDP_LINUX;

	self->udpLinuxLink = (udp_linux_link_t*) malloc(sizeof(udp_linux_link_t));
	udpLinuxClient_Construct(self->udpLinuxLink, rxQLen, port);

	self->udpLinuxLink->parent = self;

	self->Read = udpLinuxClient_Read;
	self->Transmit = udpLinuxClient_Transmit;
	self->enableRead = udpLinuxClient_enableRead;
	self->encodePacket = coms_encodePacket;
	self->transmitDMA = comslink_null;
	self->Destroy = comslink_destroy;
	self->busState = bus_TX_AT_WILL;
	return 0;
}

uint8_t comslink_construct_fifo_linux(coms_link* self, char* txFifoName, char* rxFifoName, uint txQLen, uint rxQLen) {
	self->comsType = COMS_TYPE_FIFO_LINUX;
	self->fifoLinuxLink = (fifo_comms_link_t*) malloc(sizeof(fifo_comms_link_t));

	fifo_construct(self->fifoLinuxLink, txFifoName, rxFifoName, txQLen, rxQLen);
	self->fifoLinuxLink->parent = self;

	self->Read = fifo_Read;
	self->Transmit = fifo_Transmit;
	self->enableRead = fifo_enableRead;
	self->encodePacket = coms_encodePacket;
	self->transmitDMA = comslink_null;
	self->Destroy = comslink_destroy;
	self->busState = bus_TX_AT_WILL;
	return 0;
}
#endif

uint8_t comslink_null(coms_link* parent) {
	return 0;
}

void comslink_destroy(coms_link* self) {
	self->comsType = COMS_TYPE_NONE;
	self->Read = comslink_null;
	self->Transmit = comslink_NOP_Transmit;
	self->enableRead = comslink_null;
	self->encodePacket = comslink_NOP_encodePacket;
	self->transmitDMA = comslink_null;
}

uint8_t comslink_NOP_Transmit(coms_link* self, packet_t* packet) {
	return 1; // Return 1. If we return 0, this signals an error for the other transmit functions.
}

int8_t comslink_NOP_encodePacket(packet_t* packet, uint8_t address,
		uint16_t code, uint8_t length, uint8_t* data, uint8_t priority) {
	return 0;
}

