/*
 * matrixMath.c
 *
 *  Created on: 22Jun.,2018
 *      Author: TheBlueprintLabs
 */
#include<math.h>
#include<stdio.h>
#include<stdint.h>
#include "DataStructures/matrixMath.h"
#include <stdint.h>

void LUDecomposition(Matrix *A,Matrix *P){

	int n = A->numRows;

	int i,j,k,m,imax;
	float maxA,absA,temp;
	float Tol = 0.00001;

    for (i = 0; i <= n; i++){
        P->matrix[i][1] = i; //Unit permutation matrix, P[N] initialized with N
    }

	//implemented partial pivoting
	for (i = 0; i < n; i++) {
	        maxA = 0.0;
	        imax = i;

	        for (k = i; k < n; k++){
	            if ((absA = fabs(A->matrix[k][i])) > maxA) {
	                maxA = absA;
	                imax = k;
	            }
	        }

	        if (maxA < Tol){
	        	for (i = 0; i <= n; i++){
	        		for (j = 0; j <= n; j++){
						A->matrix[i][j] = 0.0;
	        		}
	        	}
	        	printf("Degenerate matrix\n");
				return; //failure, matrix is degenerate
	        }

	        if (imax != i) {
	            //pivoting P
	            j = P->matrix[i][1];
	            P->matrix[i][1] = P->matrix[imax][1];
	            P->matrix[imax][1] = j;

	            //pivoting rows of A
	            for(m = 0;m<n;m++){
		            temp = A->matrix[i][m];
		            A->matrix[i][m] = A->matrix[imax][m];
		            A->matrix[imax][m] = temp;
	            }


	            //counting pivots starting from N (for determinant)
	            P->matrix[n][1]++;
	        }



	for (j = i + 1; j < n; j++) {
		A->matrix[j][i] /= A->matrix[i][i];

		for (k = i + 1; k < n; k++)
			A->matrix[j][k] -= A->matrix[j][i] * A->matrix[i][k];
			}
	}
}


/*  TO FIND THE INVERSE */

/* to find the inverse we solve [D][y]=[d] with only one element in
the [d] array put equal to one at a time */

void LUInverse(Matrix *A,Matrix *P,Matrix *IA){

	int n = A->numRows;

	for (int j = 0; j < n; j++) {
		for (int i = 0; i < n; i++) {
			if (P->matrix[i][1] == j)
				IA->matrix[i][j] = 1.0;
			else
				IA->matrix[i][j] = 0.0;

			for (int k = 0; k < i; k++)
				IA->matrix[i][j] -= A->matrix[i][k] * IA->matrix[k][j];
		}

		for (int i = n - 1; i >= 0; i--) {
			for (int k = i + 1; k < n; k++)
				IA->matrix[i][j] -= A->matrix[i][k] * IA->matrix[k][j];

			IA->matrix[i][j] = IA->matrix[i][j] / A->matrix[i][i];
		}
	}
}


float LUDeterminant(Matrix *A,Matrix *P){

	int n = A->numRows;

    double det = 1;

    for (int i = 0; i < n; i++){
        det *= A->matrix[i][i];
    }
    if (((int)P->matrix[n][1] - n) % 2 == 0){
        return det;
    } else {
        return -det;
    }
}


void printMatrix(Matrix *A){     //int numRows,int numCols,float(*A)[][numCols]){

	for(int i=0;i<A->numRows;i++){
		for(int j=0;j<A->numCols;j++){
			//printf("%f  ",A->matrix[i][j]);
			//fflush(stdout);
		}
		//printf("\n");
	}
}


void matrixMultiply(Matrix *A,Matrix *B,Matrix *Result){			//float(*A)[3][3],float(*B)[3][3],float(*Mult)[3][3]){

	if(A->numCols != B->numRows){
		printf("Incompatible matrix sizes \n");
		return;
	}

	int i, j, k;

	// Initializing elements of matrix Mult to 0.
//	for(i = 0; i < A->numRows; ++i){
//		for(j = 0; j < B->numCols; ++j){
//			Result->matrix[i][j] = 0;
//		}
//	}
//	uint8_t lenResult = sizeof(Result);
//	memset(Result, 0, sizeof(Result));

	// Multiplying matrix firstMatrix and secondMatrix and storing in array Mult.
	for(i = 0; i < A->numRows; ++i){
		for(j = 0; j < B->numCols; ++j){
			Result->matrix[i][j] = 0;
			for(k=0; k<A->numCols; ++k){
				Result->matrix[i][j] += A->matrix[i][k] * B->matrix[k][j];
			}
		}
	}
}

void matrixAdd(Matrix *A,Matrix *B,Matrix *Result){

	if((A->numCols != B->numCols) || (A->numRows != B->numRows)){
		printf("Incompatible matrix sizes \n");
		return;
	}

	int i, j;

	// Multiplying matrix firstMatrix and secondMatrix and storing in array Mult.
	for(i = 0; i < A->numRows; ++i){

		for(j = 0; j < A->numCols; ++j){

			Result->matrix[i][j] = A->matrix[i][j] + B->matrix[i][j];
		}
	}
}

void matrixMinus(Matrix *A,Matrix *B,Matrix *Result){

	if((A->numCols != B->numCols) || (A->numRows != B->numRows)){
		printf("Incompatible matrix sizes \n");
		return;
	}

	// Subtract matrix firstMatrix and secondMatrix and storing in matrix result.
	for(uint8_t i = 0; i < A->numRows; ++i){
		for(uint8_t j = 0; j < A->numCols; ++j){
			Result->matrix[i][j] = A->matrix[i][j] - B->matrix[i][j];
		}
	}
}



void matrixTranspose(Matrix *A,Matrix *AT){

	int i, j;

	// Multiplying matrix firstMatrix and secondMatrix and storing in array Mult.
	for(i = 0; i < A->numRows; ++i){
		for(j = 0; j < A->numCols; ++j){
			AT->matrix[j][i] = A->matrix[i][j];
		}
	}
}

float matrixTrace(Matrix *A){

	if(A->numRows != A->numCols){
		return 0;
	}
	float trace=0.0;
	for(int i =0;i<A->numRows;i++){
		trace += A->matrix[i][i];
	}
	return trace;
}

void identityMatrix(Matrix *A){

	for(int i = 0; i < A->numRows; i++){
		for(int j = 0; j < A->numRows; j++){
			A->matrix[i][j] = 0;
			A->matrix[i][i] = 1;
		}

	}

}

void matrixScalarMultiply(float scalar, Matrix *A, Matrix *Result){
	int i,j;

	for(i = 0; i < A->numRows; ++i){
		for(j = 0; j < A->numCols; ++j){
			Result->matrix[i][j] = scalar*A->matrix[i][j];
		}
	}
}


void vectorCross(Matrix *A,Matrix *B,Matrix *Result){

	if(A->numRows == 1 && B->numRows == 1 && A->numCols == 3 && B->numCols == 3){

		Result->matrix[0][0] = A->matrix[0][1] * B->matrix[0][2] - B->matrix[0][1] * A->matrix[0][2];
		Result->matrix[0][1] = A->matrix[0][2] * B->matrix[0][0] - B->matrix[0][2] * A->matrix[0][0];
		Result->matrix[0][2] = A->matrix[0][0] * B->matrix[0][1] - B->matrix[0][0] * A->matrix[0][1];


	} else if(A->numCols == 1 && B->numCols == 1 && A->numRows == 3 && B->numRows == 3) {

		Result->matrix[0][0] = A->matrix[1][0] * B->matrix[2][0] - B->matrix[1][0] * A->matrix[2][0];
		Result->matrix[1][0] = A->matrix[2][0] * B->matrix[0][0] - B->matrix[2][0] * A->matrix[0][0];
		Result->matrix[2][0] = A->matrix[0][0] * B->matrix[1][0] - B->matrix[0][0] * A->matrix[1][0];

	} else {
		return;
	}
}

float vectorDot(Matrix *A,Matrix *B){

	float result = 0.0;
	if(A->numRows == 1 && B->numRows == 1 && A->numCols == B->numCols){

		int i;
		for(i = 0; i < A->numCols; ++i){
			result += A->matrix[0][i] * B->matrix[0][i];
		}

	} else if(A->numCols == 1 && B->numCols == 1 && A->numRows == B->numRows) {

		int i;
		for(i = 0; i < A->numRows; ++i){
			result += A->matrix[i][0] * B->matrix[i][0];
		}

	} else {
		result = 0;
	}
	return result;
}

/* Set Matrix Dest equal to Matrix Src
 *
 */
void copyMatrix(Matrix *Dest, Matrix *Src, int destRowStart, int destColStart, int srcRowStart, int srcRowEnd, int srcColStart, int srcColEnd){
	int rowOffset = destRowStart - srcRowStart;
	int colOffset = destColStart - srcColStart;

	for(int i = srcRowStart;i<srcRowEnd;i++){
		for(int j = srcColStart;j<srcColEnd;j++){
			Dest->matrix[i + rowOffset][j + colOffset] = Src->matrix[i][j];
		}
	}
}

float vectorNorm(Matrix *A){

	float result = 0;

	if(A->numRows == 1){
		for(int i=0;i<A->numCols;i++){
			result += A->matrix[0][i] * A->matrix[0][i];
		}
	} else if(A->numCols == 1){
		for(int i=0;i<A->numRows;i++){
			result += A->matrix[i][0] * A->matrix[i][0];
		}
	}

	return sqrtf(result);

}

float axisAngleFromRotationMatrix(Matrix *R,Matrix *v){

	float angle = acosf(0.5*(R->matrix[0][0] + R->matrix[1][1] + R->matrix[2][2] - 1.0));
	float epsilon = 0.001;
	if(angle<epsilon){
		//zero angle, arbitary axis
		v->matrix[0][0] = 1;
		v->matrix[1][0] = 0;
		v->matrix[2][0] = 0;
	} else if(angle>(1-epsilon)*M_PI){
		//pi angle
		float xx = (R->matrix[0][0]+1)/2.0;
		float yy = (R->matrix[1][1]+1)/2.0;
		float zz = (R->matrix[2][2]+1)/2.0;
		float xy = (R->matrix[0][1]+R->matrix[1][0])/4.0;
		float xz = (R->matrix[0][2]+R->matrix[2][0])/4.0;
		float yz = (R->matrix[1][2]+R->matrix[2][1])/4.0;
		if ((xx > yy) && (xx > zz)) { // R[0][0] is the largest diagonal term
			if (xx< epsilon) {
				v->matrix[0][0] = 0;
				v->matrix[1][0] = 0.7071;
				v->matrix[2][0] = 0.7071;
			} else {
				v->matrix[0][0] = sqrtf(xx);
				v->matrix[1][0] = xy/v->matrix[0][0];
				v->matrix[2][0] = xz/v->matrix[0][0];
			}
		} else if (yy > zz) { // m[1][1] is the largest diagonal term
			if (yy< epsilon) {
				v->matrix[0][0] = 0.7071;
				v->matrix[1][0] = 0;
				v->matrix[2][0] = 0.7071;
			} else {
				v->matrix[1][0] = sqrtf(yy);
				v->matrix[0][0] = xy/v->matrix[1][0];
				v->matrix[2][0] = yz/v->matrix[1][0];
			}
		} else { // m[2][2] is the largest diagonal term so base result on this
			if (zz< epsilon) {
				v->matrix[0][0] = 0.7071;
				v->matrix[1][0] = 0.7071;
				v->matrix[2][0] = 0;
			} else {
				v->matrix[2][0] = sqrtf(zz);
				v->matrix[0][0] = xz/v->matrix[2][0];
				v->matrix[1][0] = yz/v->matrix[2][0];
			}
		}

	} else {
		v->matrix[0][0] = (R->matrix[2][1]- R->matrix[2][1])/(2*sinf(angle));
		v->matrix[1][0] = (R->matrix[0][2]- R->matrix[2][0])/(2*sinf(angle));
		v->matrix[2][0] = (R->matrix[1][0]- R->matrix[0][1])/(2*sinf(angle));
	}
	return angle;
}

void RotX(Matrix *Rx,float angle){

	Rx->numCols = 3;
	Rx->numRows = 3;
	Rx->matrix[0][0] = 1;
	Rx->matrix[1][1] = cosf(angle);
	Rx->matrix[2][2] = Rx->matrix[1][1];
	Rx->matrix[1][2] = -sinf(angle);
	Rx->matrix[2][1] = -Rx->matrix[1][2];
	Rx->matrix[0][1] = 0;
	Rx->matrix[0][2] = 0;
	Rx->matrix[1][0] = 0;
	Rx->matrix[2][0] = 0;
	return;
}

void RotY(Matrix *Ry,float angle){

	Ry->numCols = 3;
	Ry->numRows = 3;
	Ry->matrix[0][0] = cosf(angle);
	Ry->matrix[2][2] = Ry->matrix[0][0];
	Ry->matrix[0][2] = sinf(angle);
	Ry->matrix[2][0] = -Ry->matrix[0][2];
	Ry->matrix[1][1] = 1;
	Ry->matrix[0][1] = 0;
	Ry->matrix[1][0] = 0;
	Ry->matrix[1][2] = 0;
	Ry->matrix[2][1] = 0;
	return;
}

void RotZ(Matrix *Rz,float angle){

	Rz->numCols = 3;
	Rz->numRows = 3;
	Rz->matrix[0][0] = cosf(angle);
	Rz->matrix[0][1] = -sinf(angle);
	Rz->matrix[1][1] = Rz->matrix[0][0];
	Rz->matrix[1][0] = -Rz->matrix[0][1];
	Rz->matrix[2][2] = 1;
	Rz->matrix[0][2] = 0;
	Rz->matrix[1][2] = 0;
	Rz->matrix[2][0] = 0;
	Rz->matrix[2][1] = 0;
	return;
}
