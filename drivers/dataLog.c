/*
 *  Created on: 26-07-2022
 *      Author: YC
 */

#include "run.h"

uint32_t dataLog_WriteAddress;
uint16_t savedSize = DATALOG_PARAM_SIZE * DATALOG_NUM_PARAMS;

/*
 * Save the parameters to flash memory
 * */
void datalog_Save()
{
	/* Read the 8 parameters */
	float param_1 = bme280.temperature;
	float param_2 = bme280.pressure;
	float param_3 = bme280.humidity;
	float param_4 = NAN;
	float param_5 = NAN;
	float param_6 = NAN;
	float param_7 = NAN;
	float param_8 = NAN;
	float param_9 = NAN;
	float param_10 = NAN;
	float param_11 = NAN;
	float param_12 = NAN;
	float param_13 = NAN;
	float param_14 = NAN;
	float param_15 = NAN;
	float param_16 = NAN;

	/* If write address is past the last viable address in the sector, erase */
	if (dataLog_WriteAddress >= DATALOG_LAST_ADDRESS)
	{
		return;
	}

	/* Write the data to flash */
	HAL_FLASH_Unlock();
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress, *(uint64_t*) &param_1);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + DATALOG_PARAM_SIZE, *(uint64_t*) &param_2);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 2), *(uint64_t*) &param_3);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 3), *(uint64_t*) &param_4);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 4), *(uint64_t*) &param_5);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 5), *(uint64_t*) &param_6);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 6), *(uint64_t*) &param_7);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 7), *(uint64_t*) &param_8);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 8), *(uint64_t*) &param_9);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 9), *(uint64_t*) &param_10);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 10), *(uint64_t*) &param_11);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 11), *(uint64_t*) &param_12);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 12), *(uint64_t*) &param_13);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 13), *(uint64_t*) &param_14);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 14), *(uint64_t*) &param_15);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dataLog_WriteAddress + (DATALOG_PARAM_SIZE * 15), *(uint64_t*) &param_16);

	/* Update write address */
	dataLog_WriteAddress += savedSize;
}

/*
 * Check the status of each address in the sector:
 * 	erase sector if full
 * 	set write address if empty address found
 * 	*/
void dataLog_VerifySector()
{
	uint8_t status;
	uint32_t address;

	for (address = DATALOG_FLASH_ADDRESS;
			address < DATALOG_FLASH_ADDRESS + DATALOG_PAGE_SIZE;
			address += savedSize)
	{
		status = dataLog_VerifyAddress(address);

		switch(status)
		{
		case DATALOG_LOG_FULL_SECTOR: /* Sector full, erase */
			datalog_EraseSector();
			break;
		case DATALOG_LOG_EMPTY_ADDRESS: /* Address is empty, set this as the address to write to */
			dataLog_WriteAddress = address;
			return;
			break;
		case DATALOG_VALID_DATA: /* Found valid data, continue searching through addresses */
			break;
		}
	}
}

/*
 * Verify the address and its data to check if:
 * 	 the log is full
 * 	 the data is empty
 * 	 */
uint8_t dataLog_VerifyAddress(uint32_t address)
{
	uint16_t data = *(uint16_t*)address;

	if (address >= DATALOG_LAST_ADDRESS)
	{
		return DATALOG_LOG_FULL_SECTOR;
	}
	else if (data == DATALOG_EMPTY_VALUE)
	{
		return DATALOG_LOG_EMPTY_ADDRESS;
	}
	else
	{
		return DATALOG_VALID_DATA;
	}
}

/*
 * Erase the sector, and reset the address to be written to.
 */
void datalog_EraseSector()
{
	HAL_FLASH_Unlock();
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );
	FLASH_Erase_Sector(FLASH_SECTOR_7, VOLTAGE_RANGE_3);
	FLASH_Erase_Sector(FLASH_SECTOR_8, VOLTAGE_RANGE_3);

	/* reset address to write from*/
	dataLog_WriteAddress = DATALOG_FLASH_ADDRESS;
}

/*
 * Populate the device datalog array with:
 *  a set number of parameters, starting at a  specified index
 */
void dataLog_GetParams(DataLog_t* dataLog, uint16_t dataIdx, uint16_t numParams, float* arr)
{
	if (dataIdx >= DATALOG_NUM_PARAMS)
	{
		dataIdx = DATALOG_NUM_PARAMS - 1;
	}

	if (numParams > DATALOG_MAX_READ)
	{
		numParams = DATALOG_MAX_READ;
	}
	else
	{
		numParams = ceil((double) numParams/DALALOG_PARAMS_PER_PKT) * DALALOG_PARAMS_PER_PKT;
	}

	/* Find address of the most recent parameter specified by the index */
	uint32_t address = dataLog_WriteAddress - (DATALOG_PARAM_SIZE*(DATALOG_NUM_PARAMS - dataIdx));

	for (uint16_t i = 0; i <= numParams; i++)
	{
		arr[i] = dataLog_ReadAddress(address);
		address -= savedSize;
		if (address < DATALOG_FLASH_ADDRESS)
		{
			break; /* Stop if the start of the data log sector is reached*/
		}
	}

	dataLog->numParams = numParams;
	dataLog->sentCounter = 0;
	dataLog->dataLogFlag = 1;
}

/*
 * Read and return the data held at a given address
 */
float dataLog_ReadAddress(uint32_t address)
{
	if (address > DATALOG_LAST_READ_ADDRESS)
	{
		return NAN;
	}
	else
	{
		float data = *(float*) address;
		return data;
	}
}

/*
 * Set the save frequency. User input in increments of 10mins.
 */
void dataLog_SetSaveFreq(DataLog_t* dataLog, uint8_t freq)
{
	if (freq > DATALOG_MAX_SAVE_FREQ)
	{
		dataLog->saveFreq = DATALOG_MAX_SAVE_FREQ;
		dataLog->saveCounter = DATALOG_MAX_SAVE_FREQ;
		return;
	}
	else if (freq < DATALOG_MIN_SAVE_FREQ)
	{
		dataLog->saveFreq = DATALOG_MIN_SAVE_FREQ;
		dataLog->saveCounter = DATALOG_MIN_SAVE_FREQ;
		return;
	}
	else
	{
		dataLog->saveFreq = freq;
		dataLog->saveCounter = freq;
	}

}

void dataLog_SetSaveCounter(DataLog_t* dataLog)
{
	dataLog->saveCounter = dataLog->saveFreq;
}




