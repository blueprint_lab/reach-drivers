/*
 * control.c
 *
 *  Created on: 30 May 2017
 *      Author: TheBlueprintLabs
 *
 *  Refactored by Kyle Mclean s18/11/2021
 *  	- Removed redundant code
 *  	- Removed redundant parameters
 *  	- Consolidated PID funtionality to one funtion
 *  	- Fixed bug in differential control for linear estimator
 *  	- Added feedforward term for circular estimator
 */

#include <float.h>
#include "DataStructures/control.h"

/* Private funtion prottypes */
float control_PID(control_t* self);
float correctCircularSetpoint(float tmpPositionSetPoint, float upperLim, float lowerLim, float tolerance);


/*
 * @brief: Creates and instances of control type. Used for running a standard PID controller in either linear or circular mode
 */
uint8_t constructControl(control_t* self, float timestep) {
	self->run = control_runCircular;

	self->setLimits= control_setLimits;
	self->setGains = control_setGains;
	self->reset=control_reset;
	self->setType=control_setType;
	self->setpointLimitScale = 1.0;
	self->timestep = timestep;

	if (fabsf(self->Ko) < FLT_EPSILON) self->Ko = 1.0;

	return 0;
}

void control_setType(control_t* self,controlType_t controlType) {
	self->controlType = controlType;

	if(self->controlType == LINEAR)
	{
		self->run = control_runLinear;
	}
	else
	{
		self->run = control_runCircular;
	}
}

/*
 * @brief: resets controller
 */
void control_reset(control_t* self) {
	self->integralError=0;
	self->error=0;
	self->measurement = 0.0;
	self->setpoint=self->measurement;
	self->output=0;
}

/*
 * @brief: sets PID gains
 */
void control_setGains(control_t* self, float Kp, float Kd, float Ki, float Kf, float Ko) {
	self->Kp=Kp;
	self->Kd=Kd;
	self->Ki=Ki;
	self->Kf=Kf;
	self->Ko=Ko;
}

/*
 * @brief: sets limits, maxOutput, minOutput and integral wind up limit
 */
void control_setLimits(control_t* self, float maxOutput, float minOutput, float maxSetpoint, float minSetpoint, float maxIntegralError) {
	self-> maxSetpoint=maxSetpoint;
	self-> minSetpoint=minSetpoint;
	self-> maxIntegralError=maxIntegralError;
	self-> minOutput=minOutput;
	self-> maxOutput=maxOutput;
}

/*
 * @brief: Run linear PID controller
 */
float control_runLinear(control_t* self) {
	/* Check controller limits */
	if (fabsf(self->maxSetpoint - self->minSetpoint) < ONE_DEGREE) {
		CLR_FLAG(self->controlStatus, MIN_SETPOINT);
		CLR_FLAG(self->controlStatus, MAX_SETPOINT);
	} else if(self->setpoint > self->maxSetpoint * self->setpointLimitScale) {
		self->setpoint = self->maxSetpoint * self->setpointLimitScale;
		SET_FLAG(self->controlStatus, MAX_SETPOINT);
	} else if(self->setpoint < self->minSetpoint * self->setpointLimitScale) {
		self->setpoint = self->minSetpoint * self->setpointLimitScale;
		SET_FLAG(self->controlStatus, MIN_SETPOINT);
	} else {
		CLR_FLAG(self->controlStatus, MIN_SETPOINT);
		CLR_FLAG(self->controlStatus, MAX_SETPOINT);
	}

	self->error=self->setpoint-self->measurement;

	return control_PID(self);
}


/*
 * @brief: Run a PID on a rotating device between 0 and 2PI
 */
float control_runCircular(control_t* self) {
	/* check if min and max setpoints are the same */
	if((self->maxSetpoint > self->minSetpoint-0.01 && self->maxSetpoint < self->minSetpoint+0.01) || self->ignoreSetpointLimits) {
		self->error = wrapAngle(self->setpoint - self->measurement); // If setpoints are the same, continuous rotate mode
	} else {
		capCircularSetpoint(&self->setpoint, self->maxSetpoint, self->minSetpoint, 0.01);
		self->error = capCircularError(self->measurement, self->setpoint, self->maxSetpoint, self->minSetpoint);
	}

	self->ignoreSetpointLimits = 0;

	return control_PID(self);
}

/*
 * @brief: runs a PID controller
 */
float control_PID(control_t* self) {
	static float previousError = 0.0;

	float _kp = self->Kp;
	float _maxIntergralError = self->maxIntegralError;

	if (self->Kp < 0) { _kp = self->autoKp; }
	if (self->maxIntegralError < 0) { _maxIntergralError = self->autoMi; }

	/* Check error tolerance */
	if(fabs(self->error) < self->errorTolerence)
	{
		self->error = 0.0;
	}

	/* Check for zero error */
	if(fabsf(self->error) < FLT_EPSILON) SET_FLAG(self->controlStatus, ZERO_ERROR);

	/* Add integral error */
	self->integralError += self->error * self->timestep;

	/* Clamp integral wind-up */
	if(self->integralError > _maxIntergralError)
	{
		self->integralError = _maxIntergralError;
	}
	if(self->integralError < -_maxIntergralError)
	{
		self->integralError= -_maxIntergralError;
	}

	/* Update output */
	self->output = self->Ko * (self->error * _kp
							+ (self->error - previousError) * (self->Kd / self->timestep)
							+ (self->integralError * self->Ki)
							+ (self->setpoint * self->Kf));

	/* Record error */
	previousError = self->error;

	return self->output;
}



/*
 * @brief: corrects the setpoint to the closest limit
 */
float correctCircularSetpoint(float tmpPositionSetPoint, float upperLim, float lowerLim, float tolerance) {

	float correctedPositionSetPoint = 0;

	if ((upperLim > lowerLim) && (tmpPositionSetPoint > lowerLim && tmpPositionSetPoint < upperLim)){
		// Go to closest position limit to positionSetPoint
		if (smallestAngleBetween(tmpPositionSetPoint,lowerLim) < smallestAngleBetween(tmpPositionSetPoint,upperLim)){
			correctedPositionSetPoint = lowerLim - tolerance;
		}
		else {
			correctedPositionSetPoint = upperLim + tolerance;
		}
	}
	else if ((upperLim < lowerLim) && (tmpPositionSetPoint > lowerLim || tmpPositionSetPoint < upperLim)){
		// Go to closest position limit to positionSetPoint
		if (smallestAngleBetween(tmpPositionSetPoint,lowerLim) < smallestAngleBetween(tmpPositionSetPoint,upperLim)){
			correctedPositionSetPoint = lowerLim - tolerance;
		}
		else {
			correctedPositionSetPoint = upperLim + tolerance;
		}
	}
	else {
		correctedPositionSetPoint = tmpPositionSetPoint;
	}

	return correctedPositionSetPoint;

}

/*
 * @brief: Caps circular setpoint setpoint to the closest limit
 */
void capCircularSetpoint(float* positionSetPoint, float upperLimit, float lowerLimit, float tolerance) {

	float zero = lowerLimit;
	upperLimit = upperLimit - lowerLimit;

	//make sure all angles are between 0 and 2pi
	*positionSetPoint =  wrapTo2Pi(*positionSetPoint - lowerLimit);
	lowerLimit = 0.0f;
	upperLimit =  wrapTo2Pi(upperLimit-tolerance);
	lowerLimit =  wrapTo2Pi(lowerLimit+tolerance);

	float distToUpperLimit = smallestAngleBetween(upperLimit,*positionSetPoint);
	float distToLowerLimit = smallestAngleBetween(lowerLimit,*positionSetPoint);
	if(upperLimit>=lowerLimit && (*positionSetPoint>upperLimit || *positionSetPoint<lowerLimit)){
		//if outside limits set to closest limit
		*positionSetPoint = (distToUpperLimit <= distToLowerLimit)*upperLimit + (distToUpperLimit > distToLowerLimit)*lowerLimit;
	} else if (*positionSetPoint>upperLimit && *positionSetPoint<lowerLimit){
		//if inside restriction, set to closest limit
		*positionSetPoint = (distToUpperLimit <= distToLowerLimit)*upperLimit + (distToUpperLimit > distToLowerLimit)*lowerLimit;
	}

	*positionSetPoint = zero + *positionSetPoint;

	return;
}

float capCircularError(float position,float setpoint, float upperLimit, float lowerLimit) {
	/* Rotate frame to make lower limit 0. This ensures that upperLimit > lowerLimit. */
	position =  wrapTo2Pi(position - lowerLimit);
	setpoint =  wrapTo2Pi(setpoint - lowerLimit);
	upperLimit =  wrapTo2Pi(upperLimit - lowerLimit);
	lowerLimit =  wrapTo2Pi(lowerLimit - lowerLimit);

	float vectorToUpperLimit = upperLimit - position;
	float vectorToLowerLimit = lowerLimit - position;

	/* Find error between -pi and pi */
	float error = wrapAngle(setpoint - position);

	if((error>vectorToUpperLimit && vectorToUpperLimit>0) || (error<vectorToLowerLimit && vectorToLowerLimit<0)){
		error = (error>=0)*(error-2*M_PI) + (error<0)*(error+2*M_PI);  // If error is outside limits, go other way around
	} else if (vectorToUpperLimit < 0 || vectorToLowerLimit > 0){
		// If current position is outside limits
		float distToUpperLimit = smallestAngleBetween(upperLimit,position);
		float distToLowerLimit = smallestAngleBetween(lowerLimit,position);

		if(distToUpperLimit>=distToLowerLimit){
			// If closer to lower limit, error direction should be positive
			if(error < 0) error = (error>=0)*(error-2*M_PI) + (error<0)*(error+2*M_PI);
		} else {
			// If closer to upper limit, error direction should be negative
			if(error > 0) error = (error>=0)*(error-2*M_PI) + (error<0)*(error+2*M_PI);
		}
	}

	return error;
}

/**
 * @brief	Set max and min factory setpoint limits. Check user limits do not exceed factory limits. If so, reduce user limits to factory limits.
 * @params	control_t, float max limit, float min limit
 */
void control_setFactorySetpointLimits(control_t* self, float maxSetpointFactory, float minSetpointFactory) {
	self->maxSetpointFactory = maxSetpointFactory;
	self->minSetpointFactory = minSetpointFactory;

	/* Limit min/max setpoints to factory limits */
	if(self->maxSetpoint > self->maxSetpointFactory) self->maxSetpoint = self->maxSetpointFactory;
	if(self->minSetpoint < self->minSetpointFactory) self->minSetpoint = self->minSetpointFactory;

}
