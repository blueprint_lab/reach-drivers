/*
 * semanticVersion.c
 *
 * Semantic version conversion
 *
 *  Created on: 07-19-2023
 *      Author: Kyle McLean
 */

#include <stdint.h>
#include <math.h>

#include "Communication/semanticVersion.h"


void version_float2int(float f, uint8_t *major, uint8_t *submaj, uint8_t *minor) {
    f = floorf(f);

    *major  = (uint8_t)((int)(f / 100) % 10);
    *submaj = (uint8_t)((int)(f / 10) % 10);
    *minor  = (uint8_t)((int)(f / 1) % 10);
}

float version_int2float(uint8_t major, uint8_t submaj, uint8_t minor) {
    return (float)(major * 1e2 + submaj * 1e1 + minor);
}

