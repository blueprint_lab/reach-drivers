/*
 * IPIDMap.c
 *
 *  Created on: 24 Feb 2021
 *      Author: Shaun Barlow
 */
#ifdef TEST_IPIDMAP
#include "../inc/Communication/IPIDMap.h"
#include <stdio.h>
#else
#include "Communication/IPIDMap.h"
#endif



uint8_t IPIDMap_addEntry(IPIDMap_t* self, uint8_t deviceID, uint8_t* ip, uint16_t port) {
	uint8_t mapIndex;
	mapIndex = deviceID >> 4;
	if (mapIndex >= NUM_MAP_ENTRIES) {
		// Invalid mapIndex. Don't write to map.
		return 0;
	}

	self->map[mapIndex].ip[0] = ip[0];
	self->map[mapIndex].ip[1] = ip[1];
	self->map[mapIndex].ip[2] = ip[2];
	self->map[mapIndex].ip[3] = ip[3];
	self->map[mapIndex].port = port;
	return 1; // Success
}

uint8_t IPIDMap_getIpPort(IPIDMap_t* self, uint8_t deviceID, uint8_t* ip, uint16_t* port) {
	uint8_t mapIndex;
	mapIndex = deviceID >> 4;
	if (mapIndex >= NUM_MAP_ENTRIES) {
		// Invalid mapIndex. Don't write to map.
		return 0;
	}

	ip[0] = self->map[mapIndex].ip[0];
	ip[1] = self->map[mapIndex].ip[1];
	ip[2] = self->map[mapIndex].ip[2];
	ip[3] = self->map[mapIndex].ip[3];
	*port = self->map[mapIndex].port;
	return 1; // Success
}

uint8_t IPIDMap_getDeviceID(IPIDMap_t* self, uint8_t* ip, uint16_t port) {
	for (uint8_t mapIndex=0; mapIndex<NUM_MAP_ENTRIES; mapIndex++) {
		if (*(uint32_t*)ip == *(uint32_t*)self->map[mapIndex].ip
				&& port == self->map[mapIndex].port) {
			return mapIndex << 4;
		}
	}
	return 0; // If none found, return the zero prefix.
}


#ifdef TEST_IPIDMAP
/*
 * To run this test -
 * Open terminal, run:
 * > cd /to/this/directory
 * > gcc IPIDMap.c -o testmap -D TEST_IPIDMAP
 * > ./testmap.exe
 *
 * If gcc is not found, install on Windows with MinGW.
 */
#define TEST(num, eq) eq ? printf("TEST %d: PASS\n", num) : printf("TEST %d: FAIL\n", num)

uint8_t main() {
	IPIDMap_t map;

	// No entries added to map
	// Check that we always get zero from getDeviceID()
	uint8_t ip[4];
	uint16_t port;
	TEST(100, IPIDMap_getDeviceID(&map, ip, port) == 0);

	// Populate map with some entry
	ip[0] = 192;
	ip[1] = 168;
	ip[2] = 1;
	ip[3] = 100;
	port = 7000;
	TEST(200, IPIDMap_addEntry(&map, 0x10, ip, port));

	// Test that we get the correct prefix
	TEST(201, IPIDMap_getDeviceID(&map, ip, port) == 0x10);

	// Test that we get correct ip and port out
	uint8_t ipOut[4];
	uint16_t portOut;
	IPIDMap_getIpPort(&map, 0x10, ipOut, &portOut);
	TEST(300, (ipOut[0] == ip[0]) && (ipOut[1] == ip[1])
			&& (ipOut[2] == ip[2]) && (ipOut[3] == ip[3])
			&& (port == portOut));

	return 0;
}

#endif
