/*
 * ICMU.c
 *
 *  Created on: 21 Nov 2016
 *      Author: Blueprint Lab
 *
 *
 *  This module was modified by Kyle Mclean (k.mclean@blueprintlab.com) on 24/02/2021.
 *  Modifications implement several changes to the way values are read and written to
 *  the ICMU chip via SPI.
 */

#include <math.h>
#include <stdlib.h>
#include "Hardware/ICMU.h"
#include "string.h"
#include "DataStructures/circularQueue.h"

// Private functions
void ICMU_DMA_ReadCallback(encoder_t * self);
uint32_t transmitQueue_DMA(encoder_t* self);
void ICMU_ReadDMA(encoder_t * self);
void ICMU_ReadDMAFast(encoder_t * self);

void ICMU_DMACheck1(encoder_t * self);
void ICMU_DMACheck1Callback(encoder_t * self);
void ICMU_DMACheck2(encoder_t * self);
void ICMU_DMACheck2Callback(encoder_t * self);


/*
 * Set up the serial interface and other necessary hardware.
 */
void ICMU_Construct(encoder_t* self, SPI_HandleTypeDef* serialObj, uint16_t chipSelect, GPIO_TypeDef* chipSelectPort){

	// Initialize function pointers for public interface
	self->Enable = ICMU_Enable;
	self->Disable= ICMU_Disable;
	self->Init = ICMU_Init;
	self->Read = ICMU_Read;
	self->ReadDMA = ICMU_ReadDMAFast;
	self->ReadRaw = ICMU_ReadRaw;
	self->CheckStatus = ICMU_checkEncoder;

	self->Callibrate = ICMU_Callibrate;
	self->Destroy= ICMU_Destroy;
	self->WriteRegister=ICMU_writeRegisterValue;
	self->ReadRegister=ICMU_readRegisterValue;
	self->SetDirection=ICMU_Set_Direction;
	self->SetZero=ICMU_Set_Zero;
	self->GetDirection=ICMU_Get_Direction;
	self->DMACallback=ICMU_DMA_ReadCallback;

	// Initialize hardware and pins
	self->spiObj = serialObj;
	self->chipSelect = chipSelect;
	self->chipSelectPort = chipSelectPort ;
	self->receiveBuff = (uint8_t*)calloc(ICMU_RX_BUFF_SIZE, sizeof(uint8_t));
	self->sendBuff = (uint8_t*)calloc(ICMU_TX_BUFF_SIZE, sizeof(uint8_t));

	// Calibration variables
	self->rawStreamCount = -1;
	self->rawStreamDt = 0;
	self->lastStreamTime = 0;
	self->masterRaw = 0;
	self->noniusRaw = 0;
	self->lastReadAddr = 0xFF;
	self->lastReadByte = 0xFF;

	self->status=0;
	self->encoderState = ready;
	circularQueueConstruct(&self->SendQ, ICMU_TX_BUFF_SIZE);

	self->encoderType = ICMU;
}

void ICMU_Destroy(encoder_t* self){
	self->Enable = ICMU_NOP;
	self->Disable= ICMU_NOP;
	self->Init = ICMU_NOP;
	self->Read = ICMU_NOP;
	self->ReadDMA = ICMU_NOP;
	self->ReadRaw = ICMU_NOP;
	self->CheckStatus = ICMU_NOP;
	self->Callibrate = ICMU_NOP;
	self->Destroy= ICMU_NOP;
	self->WriteRegister=ICMU_NOP;
	self->ReadRegister=ICMU_NOP;
	self->SetDirection=ICMU_NOP;
	self->SetZero=ICMU_NOP;
	self->GetDirection=ICMU_NOP;

	free(self->receiveBuff);
	self->receiveBuff = NULL;
	free(self->sendBuff);
	self->sendBuff = NULL;

	HAL_SPI_DeInit(self->spiObj);
	HAL_SPI_MspDeInit(self->spiObj);

	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = self->chipSelect ;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(self->chipSelectPort, &GPIO_InitStruct);

	HAL_GPIO_WritePin(self->chipSelectPort,self->chipSelect,GPIO_PIN_RESET);
}


/*
 * Initialize the encoder.
 */
void ICMU_Init(encoder_t * self) {
	self->Enable(self); // Set active mode
	self->CheckStatus(self); // Get status and check for position errors
	self->Read(self); // Read initial position of encoder
}

/*
 * This function does no operations.
 */
void ICMU_NOP(encoder_t* self) {
  return;
}

/*
 * Enable the encoder - Sets the ICMU chip to active state
 */
void ICMU_Enable(encoder_t* self){

	uint8_t db1 = 0xFF;
	uint8_t operand = 0xFF;

	// Try 10 times to enable encoder
	for (int i = 0; i < 10; ++i){

		// Transmit ICMU_ACTIVATE opCode followed by the 0b11 to activate RACTIVE and PACTIVE channels
		// (see pg36/69 of https://www.ichaus.de/upload/pdf/MU_datasheet_F1en.pdf)
		self->SendQ.push(&self->SendQ, ICMU_ACTIVATE);
		self->SendQ.push(&self->SendQ, 0x03);
		transmitQueue( self );

		// Extract the operand and return value from the recieve buffer
		operand = self->receiveBuff[0];
		db1 = self->receiveBuff[1];

		if(db1 == 0x00 && operand == ICMU_ACTIVATE){
			CLR_FLAG(self->status, encoderNotDetected);
			break;
		}

		// If we don't succeed in 10 tries, encoder not detected on startup
		if(i == 10){
			SET_FLAG(self->status, encoderNotDetected); // Encoder Not Detected
		}
	}

	// Set the ICMU to accumulate errors
	ICMU_Write_Register_Bit(self, ICMU_REG_ACC_STAT, 7, ICMU_ACC_STAT_ENABLE);
}

void ICMU_Disable(encoder_t * self){
	// Not implemented
}

/*
 *  Implements the ICMU_SDAD read functionality in for encoder position measurements.
 *
 *  Note: this function is called a 8kHz in current control ISR. This function should not be called
 *  when in ICMU calibration modes.
 */
const uint32_t txCommand = ICMU_SDAD;
const uint16_t sizeOf4Bytes = sizeof(uint8_t) * 4;
void ICMU_ReadDMAFast(encoder_t * self) {
	if(self->encoderState==ready ) { // If anything else is using spi then don't interrupt
		self->encoderState=busy;
		self->transmitCount++;

		// Set to the correct callback
		self->DMACallback = ICMU_DMA_ReadCallback;

		// Copy the encoder read tx command into the send buffer
		memcpy(self->sendBuff, &txCommand, sizeOf4Bytes);

		// Chips select (NCS -> GPIO_PIN_RESET)
		HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);

		// Transmit on SPI in non-blocking mode, received data is read via ICMU_DMA_ReadCallback()
		if (HAL_SPI_TransmitReceive_DMA(self->spiObj, self->sendBuff, self->receiveBuff, sizeOf4Bytes) != HAL_OK) {
			// If hal error, set the encoder back to ready and chips deselect (NCS -> GPIO_PIN_SET)
			self->encoderState=ready;
			HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
		}
	}
}

/*
 *  Callback for ICMU_ReadDMAFast. Parses data from the readbuffer, if data is in valid encoderNotDetected is given
 */
void ICMU_DMA_ReadCallback(encoder_t * self) {
	// Check if we should request status1 register yet
	self->readCount++;
	if (self->readCount > (8000-2)) {
		self->ReadDMA = ICMU_DMACheck1;
	}

	// Declare local receive variables
	uint8_t operand = 0;
	uint8_t db1 = 0;
	uint8_t db2 = 0;
	uint8_t db3 = 0;

	// Chips deselect (NCS -> GPIO_PIN_SET)
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);

	// Check if data matches target opCode
	operand = self->receiveBuff[0];
	if(operand != ICMU_SDAD) {
		SET_FLAG(self->status,encoderNotDetected);
	} else {
		CLR_FLAG(self->status,encoderNotDetected);
	}

	// Extract encoder angle data from buffer
	db1 = self->receiveBuff[1];
	db2 = self->receiveBuff[2];
	db3 = self->receiveBuff[3];
	self->encoderAngle = 2*M_PI*(float)((db1<<16) + (db2 << 8) +db3)/((float)0xFFFFFF);
	self->encoderState=ready;
}

const uint16_t sizeOf3Bytes = sizeof(uint8_t) * 3;
void ICMU_DMACheck1(encoder_t * self) {
	if(self->encoderState!=ready ) { return; } // If anything else is using spi then don't interrupt
	self->encoderState=busy;

	// Set the correct DMA callback
	self->DMACallback = ICMU_DMACheck1Callback;

	// Add ICMU_READ_REG, ICMU_REG_STATUS1 to the send buffer (0x00 is for the returned data because SPI required data to be sent both ways per transaction)
	uint8_t txData[3] = {ICMU_READ_REG, ICMU_REG_STATUS1, 0x00};
	memcpy(self->sendBuff, txData, sizeOf3Bytes);

	// Chips select (NCS -> GPIO_PIN_RESET)
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);

	// Transmit on SPI in non-blocking mode, received data is read via ICMU_DMACheck1Callback()
	if (HAL_SPI_TransmitReceive_DMA(self->spiObj, self->sendBuff, self->receiveBuff, sizeOf3Bytes) != HAL_OK) {
		// If there is an error, reset the encoder state and chip deselect (NCS -> GPIO_PIN_SET)
		self->encoderState=ready;
		HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
	}
}

/*
 *  Callback for ICMU_DMACheck1. De-selects ICMU chip for next read and set the ReadDMA funtion to ICMU_DMACheck2
 */
void ICMU_DMACheck1Callback(encoder_t * self) {
	self->readCount++;
	self->ReadDMA = ICMU_DMACheck2;
	// Do nothing
	// Chips deselect (NCS -> GPIO_PIN_SET)
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
	self->encoderState=ready;
}

void ICMU_DMACheck2(encoder_t * self) {
	if(self->encoderState!=ready ) { return; } // If anything else is using spi then don't interrupt
	self->encoderState=busy;

	// Set the correct DMA callback
	self->DMACallback = ICMU_DMACheck2Callback;

	// Add ICMU_REG to the send buffer (0x00 is for the returned data because SPI required data to be sent both ways per transaction)
	uint8_t txData[3] = {ICMU_REG, 0x00, 0x00};
	memcpy(self->sendBuff, txData, sizeOf3Bytes);

	// Chips select (NCS -> GPIO_PIN_RESET)
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);

	// Transmit on SPI in non-blocking mode, received data is read via ICMU_DMACheck2Callback()
	if (HAL_SPI_TransmitReceive_DMA(self->spiObj, self->sendBuff, self->receiveBuff, sizeOf3Bytes) != HAL_OK) {
		// If hal error, set the encoder back to ready and chips deselect (NCS -> GPIO_PIN_SET)
		self->encoderState=ready;
		HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
	}
}

/*
 *  Callback for ICMU_DMACheck2. Parses
 */
void ICMU_DMACheck2Callback(encoder_t * self) {
	self->readCount = 0;
	self->ReadDMA = ICMU_ReadDMAFast;

	// Chips deselect (NCS -> GPIO_PIN_SET)
	HAL_GPIO_WritePin(self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);

	// Check if rxBuff[0] is ICMU_REG
	if (self->receiveBuff[0] != ICMU_REG) {
		// bad transmission with encoder
		SET_FLAG(self->status, encoderNotDetected);
		return;
	}

	if (self->receiveBuff[1] != REG_VALID) {
		// Invalid reading. Exit out.
		self->notValidError++;
		return;
	}

	// Valid reading.
	CLR_FLAG(self->status, encoderNotDetected);

	// Check for period error.
	if(CHK_FLAG(self->receiveBuff[2], NON_CTR)) {
		SET_FLAG(self->status, periodConsistancyError);
	} else {
		CLR_FLAG(self->status, periodConsistancyError);
	}
	self->encoderState=ready;
}

void ICMU_ReadDMA(encoder_t * self) {
	self->SendQ.push(&self->SendQ, ICMU_SDAD);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);

	transmitQueue_DMA(self);
}

/*
 * Set read and get status of ICMU register value
 */
int16_t ICMU_readRegisterValue(encoder_t* self, uint8_t addr){
	if(self->encoderState==ready) { // If anything else is using spi then don't interrupt
		// Declare local register value
		uint8_t reg_value;

		// Block the sendQ - prevent miss matched bytes
		self->encoderState=busy;

		// Clear received buffer
		self->receiveBuff[0] = 0;
		self->receiveBuff[1] = 0;
		self->receiveBuff[2] = 0;
		self->receiveBuff[4] = 0;

		// Read value from target address, value is set to 0x00 for this operation since no value is being set...
		ICMU_ReadWriteTarget(self, ICMU_READ_REG, addr, 0x00);

		// Check ICMU receive status and if read address matches the target "ICMU_REG"
		if((self->receiveBuff[0] == ICMU_REG) && (self->receiveBuff[1] == REG_VALID)) {
			CLR_FLAG(self->status, encoderNotDetected);
			reg_value = self->receiveBuff[2];
			self->lastReadByte = reg_value;
			self->lastReadAddr = addr;
		} else {
			SET_FLAG(self->status, encoderNotDetected);
			reg_value = 0xFF;
			self->lastReadByte = 0xFF;
			self->lastReadAddr = 0xFF;
		}

		// Unblock the sendQ
		self->encoderState=ready;

		return (int16_t) reg_value;
	}
	return -1;
}

/*
 * Set write and get status of ICMU register value
 */
uint8_t ICMU_writeRegisterValue(encoder_t* self, uint8_t addr, uint8_t value){
	if(self->encoderState==ready) { // If anything else is using spi then don't interrupt
		// Block the sendQ
		self->encoderState=busy;

		// Write value to target address
		ICMU_ReadWriteTarget(self, ICMU_WRITE_REG, addr, value);

		// Unblock the sendQ
		self->encoderState=ready;

		// Read data back
		int16_t read_data = ICMU_readRegisterValue(self, addr);

		// Check that the requested data matches the target value
		if (read_data == value) {
			CLR_FLAG(self->status,encoderNotDetected);
			return read_data;
		} else {
			SET_FLAG(self->status,encoderNotDetected);
			return 0xFF;
		}
	}
	return 0xFF;
}

/*
 *  Sends read or write opcode to the ICMU and then tries to get valid data until the watchdog times out.
 */
#define MAX_ICMU_ATTEMPTS 10
void ICMU_ReadWriteTarget(encoder_t * self, uint8_t icmu_opCode, uint8_t addr, uint8_t value){
	// Set the register read/write state to send
	uint8_t ICMU_regStatus = ICMU_SEND_OPCODE;

	for (uint8_t i = 0; i<MAX_ICMU_ATTEMPTS; i++) {
	// See pg. 39/69 of ICMU documentation https://www.ichaus.de/upload/pdf/MU_datasheet_F1en.pdf
		switch(ICMU_regStatus){
		case ICMU_SEND_OPCODE:
			// Send set READ state with target address
			self->SendQ.push(&self->SendQ, icmu_opCode);
			self->SendQ.push(&self->SendQ, addr);
			self->SendQ.push(&self->SendQ, value);
			transmitQueue(self);
		case ICMU_REG_REQUEST:
			// Send STATUS to receive REG values
			self->SendQ.push(&self->SendQ, ICMU_REG); // Request ICMU to push requested data out of SPI
			self->SendQ.push(&self->SendQ, 0x00);     // SPI requires TX operation for each slave RX operation
			self->SendQ.push(&self->SendQ, 0x00);
			transmitQueue(self);

			// Extract ICMU status byte, the
			//uint8_t recivedOpCode = self->receiveBuff[0];
			uint8_t registerStatus = self->receiveBuff[1];

			// Check for ICMU busy or ICMU fail state
			switch(registerStatus) {
			case REG_BUSY:
				ICMU_regStatus = ICMU_REG_REQUEST;
				break;
			case REG_FAIL:
				ICMU_regStatus = ICMU_SEND_OPCODE;
				break;
			default:
				return;
			}
			break;
		}
	}
}

/*
 * Set ACTIVATE status of single IMCU chip
 */
void ICMU_Set_Activate(encoder_t * self, uint8_t activate_set_state){

	// Clear input buffer
	self->receiveBuff[0] = 0;
	self->receiveBuff[1] = 0;
	self->receiveBuff[2] = 0;
	self->receiveBuff[3] = 0;
	self->receiveBuff[4] = 0;

	// Block the sendQ - prevent miss matched bytes
	self->encoderState=busy;

	// Send set WRITE state with target addr and value
	self->SendQ.push(&self->SendQ, ICMU_ACTIVATE);
	self->SendQ.push(&self->SendQ, activate_set_state);
	transmitQueue(self);

	// Unblock the sendQ
	self->encoderState=ready;
}


/*
 * ICMU read raw data
 */
void ICMU_ReadRaw(encoder_t * self){
	if(self->encoderState==ready) { // If anything else is using spi then don't interrupt
		// Set raw and nonius data to null
		self->masterRaw = 0x0;
		self->noniusRaw = 0x0;

		// Clear input buffer
		self->receiveBuff[0] = 0;
		self->receiveBuff[1] = 0;
		self->receiveBuff[2] = 0;
		self->receiveBuff[3] = 0;
		self->receiveBuff[4] = 0;

		// Block the sendQ
		self->encoderState=busy;

		// Send SDAD transmission request using ICMU_SDAD opCode
		self->SendQ.push(&self->SendQ, ICMU_SDAD);
		self->SendQ.push(&self->SendQ, 0x00);
		self->SendQ.push(&self->SendQ, 0x00);
		self->SendQ.push(&self->SendQ, 0x00);
		self->SendQ.push(&self->SendQ, 0x00);
		transmitQueue(self);

		// Unblock the sendQ
		self->encoderState=ready;

		// Check that the request operation has taken effect
		if(self->receiveBuff[0] != ICMU_SDAD){
			SET_FLAG(self->status,encoderNotDetected);
		} else {
			CLR_FLAG(self->status,encoderNotDetected);

			// Extract the raw data from the receive buffer as 32 bit int
			uint32_t data_as_uint32 = self->receiveBuff[4] | (self->receiveBuff[3] << 8) | (self->receiveBuff[2] << 16) | (self->receiveBuff[1] << 24);

			// Extract the master data (bits 4-18) and the nonius data (bits 18-24)
			uint32_t master_data = (data_as_uint32 & 0x0003FFF0) >> 4;  // Using 0x0003FFF0 takes the needed 14 bits and shifts LSB
			uint32_t nonius_data = (data_as_uint32 & 0xFFFC0000) >> 18; // Using 0xFFFC0000 takes the needed 14 bits and shifts LSB

			// Cast to 16 bit integer to remove redundant bytes and pass to the ICMU object
			self->masterRaw = (uint16_t)master_data;
			self->noniusRaw = (uint16_t)nonius_data;
		}
	}
}


/*
 * Return the encoder angle as a number between 0 and 0xFFFFFF
 */
float ICMU_Read(encoder_t * self){

	if(self->encoderState==ready) {
		//float encoderAngle = 0; // parse receive buffer

		uint8_t operand = 0;
		uint8_t db1 = 0;
		uint8_t db2 = 0;
		uint8_t db3 = 0;

		int i;
		for( i = 0; i<1; ++i){
			// Block the sendQ - prevent miss matched bytes
			self->encoderState=busy;

			self->SendQ.push(&self->SendQ, ICMU_SDAD);
			self->SendQ.push(&self->SendQ, 0x00);
			self->SendQ.push(&self->SendQ, 0x00);
			self->SendQ.push(&self->SendQ, 0x00);
			transmitQueue(self);

			// Unblock the sendQ
			self->encoderState=ready;

			operand = self->receiveBuff[0];
			if(operand != ICMU_SDAD)
			{
				SET_FLAG(self->status,encoderNotDetected);
				return self->encoderAngle;

			}
			else
			{
				CLR_FLAG(self->status,encoderNotDetected);
			}
			db1 = self->receiveBuff[1];
			db2 = self->receiveBuff[2];
			db3 = self->receiveBuff[3];
			break;
		}


		self->encoderAngle = 2*M_PI*(float)((db1<<16) + (db2 << 8) +db3)/((float)0xFFFFFF);
	}
	return self->encoderAngle;
}

void ICMU_Callibrate(encoder_t * self){
	// Not implemented
}

// PRIVATE FUNCTIONS
uint32_t transmitQueue(encoder_t* self){

	unsigned int numel = self->SendQ.numel;
	if(numel > 0){
		//Move data to the send buffer
		for(int i = 0; i < numel; ++i){
			self->sendBuff[i] = self->SendQ.pop(&self->SendQ);
		}

		// Chips select (NCS -> GPIO_PIN_RESET)
		HAL_GPIO_WritePin( self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);

		// Begin send in blocking mode
		HAL_SPI_TransmitReceive(self->spiObj, (uint8_t*)self->sendBuff, (uint8_t*)self->receiveBuff, (uint16_t)numel , 10);

		// Chips de-select (NCS -> GPIO_PIN_SET)
		HAL_GPIO_WritePin( self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
	}
	uint32_t err_code = HAL_SPI_GetError(self->spiObj);
	return err_code;
}


uint32_t transmitQueue_DMA(encoder_t* self) {
	uint16_t numel = self->SendQ.numel;
	if (numel > 0) {
		// Move data to the buffer
		for (uint16_t i = 0; i < numel; i++) {
			self->sendBuff[i] = self->SendQ.pop(&self->SendQ);
		}

		HAL_GPIO_WritePin( self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);
		HAL_SPI_TransmitReceive_DMA(self->spiObj, self->sendBuff, self->receiveBuff, (uint16_t)numel);
	}
	uint32_t err_code = HAL_SPI_GetError(self->spiObj);
	return err_code;
}

uint8_t ICMU_Register_status(encoder_t* self){
	self->SendQ.push(&self->SendQ, ICMU_REG_STATUS_DATA);
	self->SendQ.push(&self->SendQ, 0x00);
	self->SendQ.push(&self->SendQ, 0x00);
	transmitQueue(self);

	uint8_t stat = self->receiveBuff[1];
	return stat;
}



uint8_t ICMU_Read_Register(encoder_t* self, uint8_t Reg){

	int i;

	//uint32_t errorCode=0;

	self->receiveBuff[0]=0;
	self->receiveBuff[1]=0;
	self->receiveBuff[2]=0;
	self->receiveBuff[3]=0;
	self->receiveBuff[4]=0;
	self->encoderState=busy;
	for(i = 0; i<1 ; ++i){
		self->SendQ.push(&self->SendQ, ICMU_READ_REG);
		self->SendQ.push(&self->SendQ, Reg);

		transmitQueue(self);

		self->SendQ.push(&self->SendQ, ICMU_REG_STATUS_DATA);
		self->SendQ.push(&self->SendQ, 0x00);
		self->SendQ.push(&self->SendQ, 0x00);
		transmitQueue(self);


//		uint8_t stat0 = self->receiveBuff[0];
//		uint8_t stat1 = self->receiveBuff[1];
//		uint8_t stat2 = self->receiveBuff[2];
//		uint8_t stat3 = self->receiveBuff[3];
//		uint8_t stat4 = self->receiveBuff[4];

		uint8_t stat = self->receiveBuff[1];
		if(stat == REG_VALID){
			break;
		}
		switch(stat){
		case REG_BUSY:
			break;
		case REG_FAIL:
			break;
		case REG_DISMISS:
//			Error_Handler();
			break;
		case REG_ERROR:
//			Error_Handler();
			break;
//		default:

		}
	}
	self->encoderState=ready;
	return self->receiveBuff[2];
}


/*
 * Returns the data written. This can be used to verify that the correct data was written.
 */
uint8_t ICMU_Write_Register(encoder_t* self, uint8_t Reg, uint8_t val){

	self->encoderState=busy;
	int i;
	for(i = 0; i<1 ; ++i){
		self->SendQ.push(&self->SendQ, ICMU_WRITE_REG);
		self->SendQ.push(&self->SendQ, Reg);
		self->SendQ.push(&self->SendQ, val);

		self->SendQ.push(&self->SendQ, ICMU_REG_STATUS_DATA);
		self->SendQ.push(&self->SendQ, 0x00);
		self->SendQ.push(&self->SendQ, 0x00);
		transmitQueue(self);

//		uint8_t stat0 = self->receiveBuff[0];
//		uint8_t stat1 = self->receiveBuff[1];
//		uint8_t stat2 = self->receiveBuff[2];
//		uint8_t stat3 = self->receiveBuff[3];
//		uint8_t stat4 = self->receiveBuff[4];

		uint8_t stat = self->receiveBuff[4];
		if(stat == REG_VALID){
			break;
		}
		switch(stat){
		case REG_BUSY:
			break;
		case REG_FAIL:
			break;
		case REG_DISMISS:
//			Error_Handler();
			break;
		case REG_ERROR:
//			Error_Handler();
			break;

		}
	}
	self->encoderState=ready;

	return self->receiveBuff[5];
}

/*
 * Combines Status0 and Status1 into one 16-bit flag register in the format [Status0:Status1]
 */
uint16_t ICMU_getStatus(encoder_t* self) {
	uint8_t status0 = ICMU_Read_Register(self,ICMU_REG_STATUS0);
	uint8_t status1 = ICMU_readRegisterValue(self, ICMU_REG_STATUS1);
	return (uint16_t) (status0 << 8) || status1;
}

/*
 *  Gets Status1 from the ICMU chip and checks for encoder position errors
 */
void ICMU_checkEncoder(encoder_t* self) {
	uint16_t status1= ICMU_readRegisterValue(self, ICMU_REG_STATUS1);

	if (status1 == -1) {
		// invalid reading from ICMU or SPI busy
		return;
	} else if(!CHK_FLAG(self->status,encoderNotDetected)) {
		if(CHK_FLAG(status1, NON_CTR)) {
			SET_FLAG(self->status,periodConsistancyError);
		}
	}
}

uint8_t ICMU_Write_Register_Bit(encoder_t* self, uint8_t Reg, uint8_t bit_index, uint8_t bit_value) {
	/*	Arguments:
	 * 		- self
	 * 		- Reg: ICMU register to write to
	 * 		- bit_index: [0-7] index of bit to be set
	 * 		- bit_value: [0,1] value of bit to be set
	 * 	Returns:
	 * 		- data written to the ICMU register
	 */

	// 	get the data from the ICMU at the register specified (Reg)
	uint8_t existing_reg = ICMU_Read_Register(self, Reg);
	uint8_t output_reg = 0;

	//	if writing bit_value == 1, create a mask for bitwise OR operation
	if (bit_value != 0) {
		uint8_t mask = 1 << bit_index;
		output_reg = existing_reg | mask;
	}
	//	else writing bit_value == 0, create a mask for bitwise AND operation
	else {
		uint8_t mask = 0b11111110 << bit_index;
		output_reg = existing_reg & mask;
	}

	//	write to the register and return the data written
	return ICMU_writeRegisterValue(self, Reg, output_reg);
}


void ICMU_Set_Zero(encoder_t* self) {
	uint8_t address = SET_ZERO_ADDRESS;
	uint8_t data = SET_ZERO_DATA;
	ICMU_writeRegisterValue(self, address, data);
}

/*
 * @brief Take in the preset position in degrees, calculates, and sets PRES_POS
 * Note:
 * 		This is mapped to 0x20000 as a full rotation in a 17-bit system.
 */
void ICMU_Set_PresPos(encoder_t* self, float presetPosition, float AB_bits) {
	uint64_t data = (uint64_t)((presetPosition / 360) * (float)pow(AB_bits, 2));

	ICMU_writeRegisterValue(self, 0x2A, (data << 4) & 0xFF);
	ICMU_writeRegisterValue(self, 0x2B, (data << (4 + 8)) & 0xFF);
	ICMU_writeRegisterValue(self, 0x2C, (data << (4 + 16)) & 0xFF);
	ICMU_writeRegisterValue(self, 0x2D, (data << (4 + 24)) & 0xFF);
	ICMU_writeRegisterValue(self, 0x2D, (data << (4 + 32)) & 0xFF);
}

void ICMU_Set_Direction(encoder_t* self, uint8_t direction) {
	uint8_t address = SET_DIRECTION_ADDRESS_1;
	uint8_t data = DIRECTION_REVERSE;
	if (direction == DIRECTION_FORWARD) {
		data = 0;
	}
	ICMU_Write_Register_Bit(self, address, 7, data);

	address = SET_DIRECTION_ADDRESS_2;
	data = SET_DIRECTION_DATA_2;
	ICMU_writeRegisterValue(self, address, data);
}

uint8_t ICMU_Get_Direction(encoder_t* self) {
	int16_t direction_register = ICMU_readRegisterValue(self, SET_DIRECTION_ADDRESS_1);
	if (direction_register < 0) {
		//error
		//TODO: do what?
	}

	uint8_t direction = 0;
	if ((direction_register & CONF_ROT_BIT) == 0) {
		direction = DIRECTION_FORWARD + 1;
	} else if ((direction_register & CONF_ROT_BIT) == CONF_ROT_BIT) {
		direction = DIRECTION_REVERSE + 1;
	}
	return direction;
}
