
#include "Communication/comselect.h"
#include <stdio.h>

void comSelect_init(comSelect_t* self) {
    // Init masterfds
    FD_ZERO(&self->masterfds);
    self->fdMax = 0;
}

void comSelect_add(comSelect_t* self, int newFd) {
    // Add an fd to the set. 
    FD_SET(newFd, &self->masterfds);
    if (newFd > self->fdMax) {
        self->fdMax = newFd;
    }
}

int comSelect_run(comSelect_t* self) {
    int ret;
    self->workingrfds = self->masterfds;
    
    // select will block until either canbus or fifo is ready to be read.
    ret = select(self->fdMax + 1, &self->workingrfds, NULL, NULL, NULL);
    if (ret == -1) {
        perror("run_continuous select() error");
    }
    return ret;
}

int comSelect_check(comSelect_t* self, int fd) {
    return FD_ISSET(fd, &self->workingrfds);
}
