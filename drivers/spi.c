/*
 * spi.c
 *
 *  Created on: 31Jan.,2017
 *      Author: TheBlueprintLabs
 */

/*
 * Initialize the spi object
 */


#include "Communication/spi.h"
uint8_t spiConstruct(spi_t* self, SPI_HandleTypeDef* spiObject, GPIO_TypeDef* chipSelectPort, uint16_t chipSelect, size_t size){

	self->readReceive = spiFetchMessage;
	self->transmitMsg = spiTransmitMessage;
	self->deconstruct = spiDeconstruct;

	self->chipSelect = chipSelect;
	self->chipSelectPort = chipSelectPort;
	self->spiObj = spiObject;

	self->bufferSize = size;
	circularQueueConstruct(&(self->sendQueue), size);

	self->receiveBuff = (volatile uint8_t*)calloc(size, sizeof(uint8_t));
	self->sendBuff =    (uint8_t*)calloc(size, sizeof(uint8_t));

	uint8_t success = 1;
	if(self->receiveBuff == NULL || self->sendQueue.origin == NULL){
		success = 0;
	}
	return success;
}

void spiDeconstruct(spi_t* self){
	free((uint8_t*)self->receiveBuff);
	self->receiveBuff = NULL;

	free(self->sendBuff);
	self->sendBuff = NULL;

	self->sendQueue.deconstruct(&(self->sendQueue));
}

/*
 * This currently sends a message in blocking mode using a GPIO p[in as NCS.
 * This should be improved by swapping to a hardware NCS and using DMA to send a queue of messages.
 * 		There is a queue implemented as part of the object for this purpose.
 * 		The GPIO pin is used as the hardware one available is not 5V tolerant.
 */
uint8_t spiTransmitMessage(spi_t* self, uint8_t* msg, size_t length){
//	size_t numel = self->sendQueue->numel;

//	if(asynchronous){
//		//not implemented currently
//	}else{

	assert_param(self->sendBuff != NULL);

	memcpy(self->sendBuff, msg, length);
	HAL_GPIO_WritePin( self->chipSelectPort, self->chipSelect, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(self->spiObj, (uint8_t*)self->sendBuff, (uint8_t*)self->receiveBuff, (uint16_t)length , 1000);
	HAL_GPIO_WritePin( self->chipSelectPort, self->chipSelect, GPIO_PIN_SET);
//	}
	uint32_t err_code = HAL_SPI_GetError(self->spiObj);
	return err_code;
}

void spiFetchMessage(spi_t* self, uint8_t* destination, size_t length){
	memcpy(destination, (uint8_t*)self->receiveBuff, length);
	memset((uint8_t*)self->receiveBuff, 0, length);
}

