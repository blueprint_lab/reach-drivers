
#include "Communication/BPLProtocol.h"




uint8_t BPLProtocol_encodePacket(packet_t* packet, uint8_t address ,uint8_t code, uint8_t length, uint8_t* data){
	packet->address = address;
	packet->code = code;
	packet->length = length;

	uint8_t crc=0;

	assert_param( length-3 < MAX_PACKET_DATA_SIZE );
	assert_param( data!= NULL );
	memcpy(packet->data, data, length-HEADER_SIZE);

	if(packet->length < 3 || packet->length > MAX_PACKET_DATA_SIZE+3){
			return 0;
		}

		//uint8_t sendStream[packet->length];
		//circularQueue* sendQ = &(self->transmitQueue);



//		uint8_t length = packet->length;
//		uint8_t address = packet->address;
//		uint8_t code = packet->code;

		for(int i = 0; i < packet->length-4; ++i){
			uint8_t byte  = fetchByte(packet->data, packet->length, i);
			writeByte(packet->transmitData, length, i, byte);
		}

		writeByte(packet->transmitData, length, length-4, code);
		writeByte(packet->transmitData, length, length-3, address);
		writeByte(packet->transmitData, length, length-2, length);

		uint8_t stuffedPacket[length + 2]; // extra bytes for cobs byte and delimiter

	//	 crc= TM_CRC_Calculate8(packet->transmitData,length-4,1); // put CRC
		crc=crc8(0xff, packet->transmitData, length-1);
		writeByte(packet->transmitData, length, length-1,(uint8_t)( crc));



		cobs_encode(packet->transmitData, length, stuffedPacket);
		stuffedPacket[packet->length + 1] = 0;
		memcpy(packet->transmitData,stuffedPacket,packet->length + 1);

	return 1;
}

uint8_t BPLProtocol_decodePacket(packet_t* packet,uint8_t* rxBuff, uint8_t rxBuffLen){

	  //Pop the first zero byte in rxBuff using a linear search
	  size_t packetEnd = 0;
	  while(rxBuff[packetEnd] != 0 && packetEnd < rxBuffLen){
		  packetEnd++;
	  }
	  rxBuff[packetEnd] = 0xFF;


	  // fetch header data. None of  these are natrually zero and so are unnaffected by COBS
	  uint8_t length = fetchByte(rxBuff, rxBuffLen, packetEnd - 2);

	  // check to see if the data is valid
	  if( packetEnd >= rxBuffLen || length < 4 || length > MAX_PACKET_DATA_SIZE + 3){
		  packet->length = 0;
		  packet->code = 0;
		  packet->address = 0;
		  return 0;
	  }

	  // copy out the data into an unwrapped buffer so that it can be decoded.
	  uint8_t unwrappedBuffer[length+1];
	  for (int i = 0; i < length+1; ++i ){
		  uint8_t byte = fetchByte(rxBuff, rxBuffLen, packetEnd - length-1 + i);
		  writeByte(unwrappedBuffer, length+1, i, byte);
	  }

	  // decode the cobs
	  uint8_t decodedData[length];
	  cobs_decode(unwrappedBuffer, length+1, decodedData);

	  // decode the data using cobs
	  uint8_t crc= fetchByte(decodedData, length, length-1);
	  uint8_t address = fetchByte(decodedData, length, length-3);
	  uint8_t code = fetchByte(decodedData, length, length-4);

	  uint8_t crcCheck=crc8(0xff, decodedData, length-1);
	  // Read out the particular bytes into the current packet
	  packet->length = length;
	  packet->address = address;
	  packet->code = code;
	  if( length-4 > 0 && crcCheck==crc ){
		  memcpy(packet->data, decodedData, length - 4);
		  return 1;
	  }
	  else
	  {
		  //crcError++;
		  packet->length = 0; // SB 9 May 18: Added to prevent bad packets over UDP
		  packet->code = 0; //
		  packet->address = 0; //
		  return 0;
	  }

}
