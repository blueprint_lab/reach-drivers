/***********************************************************************
* FILENAME :
* 		DRV8311.c
*
* DESCRIPTION :
*       Driver for DRV8311 Brushless Motor Controller with intergated current sensing
*
* AUTHOR :
* 		Kyle McLean
* START DATE :
* 		31/05/2022
*
* Copyright BlueprintLab 2022
***********************************************************************/

#include <stdint.h>
#include <math.h>

#include "Hardware/DRV8311.h"
#include "DataStructures/auxMath.h"

/* Use space vector modulation */
#define SVM

/* Private function definitons */
void DRV8311_Init(DRV8311_t *self);
void DRV8311_Enable(DRV8311_t *self);
void DRV8311_Disable(DRV8311_t *self);

void DRV8311_SetOutput_FOC(DRV8311_t *self, float v_d, float v_q, float motorDirection);

void DRV8311_FindZeroAngle(DRV8311_t *self, float amplitude, float mechanicalPosition);
void DRV8311_CalibrateEncoder(DRV8311_t *self, float amplitude, float mechanicalPosition, float motorDirection);
void DRV8311_CalibrateTest(DRV8311_t *self, float amplitude, float motorDirection);

void DRV8311_updatePWM(DRV8311_t *self, float a, float b, float c);
void DRV8311_inversePark(float* alpha, float* beta, float v_d, float v_q , float theta);
void DRV8311_inversePvClarke(float* a, float* b, float* c, float alpha, float beta);

uint8_t DRV8311_Check(DRV8311_t *self);

/*
 * @brief Construct DRV8311 driver
 */
void DRV8311_Construct(DRV8311_t *self,
		TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel,
		TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel,
		TIM_HandleTypeDef PWMCTimer, uint32_t PWMCChannel,
		uint16_t sleepPin, GPIO_TypeDef* sleepPort,
		uint16_t faultPin, GPIO_TypeDef* faultPort){

	self->Enable 			= DRV8311_Enable;
	self->Check				= DRV8311_Check;
	self->Init 				= DRV8311_Init;
	self->Disable 			= DRV8311_Disable;

	self->SetOutputFOC 		= DRV8311_SetOutput_FOC;

	self->Calibrate 		= DRV8311_FindZeroAngle;
	self->StallDrive 		= DRV8311_CalibrateEncoder;
	self->CalibrateTest 	= DRV8311_CalibrateTest;

	self->sleepPin 			= sleepPin;
	self->sleepPort 		= sleepPort;

	self->faultPin 			= faultPin;
	self->faultPort 		= faultPort;

	self->PWMATimer 		= PWMATimer;
	self->PWMBTimer 		= PWMBTimer;
	self->PWMCTimer 		= PWMCTimer;

	self->PWMAChannel 		= PWMAChannel;
	self->PWMBChannel 		= PWMBChannel;
	self->PWMCChannel 		= PWMCChannel;

	self->mode = DISABLED;
	self->status = GOOD;

	self->index = 0;

	self->positions[0]		= 0;
	self->positions[1]		= 0;

	self->offsetAngle 		= 0;

	self->numPolePairs 		= 7;
}

/*
 * @brief Check for driver faults and update status
 */
uint8_t DRV8311_Check(DRV8311_t *self)
{
	/* Exit if there is no fault */
	if(HAL_GPIO_ReadPin(self->faultPort, self->faultPin)) { return 0; }

	/* Update fault status and disable motor */
	SET_BIT(self->status, FAULT);
	self->mode = DISABLED;

	return FAULT;
}

/*
 * @brief Init DRV8311 driver
 */
void DRV8311_Init(DRV8311_t *self)
{
	HAL_GPIO_WritePin(self->sleepPort, self->sleepPin, GPIO_PIN_RESET);

	HAL_TIM_Base_Start_IT(&self->PWMATimer);
	HAL_TIM_Base_Start_IT(&self->PWMBTimer);
	HAL_TIM_Base_Start_IT(&self->PWMCTimer);

	HAL_TIM_PWM_Start_IT(&self->PWMATimer, self->PWMAChannel);
	HAL_TIM_PWM_Start_IT(&self->PWMBTimer, self->PWMBChannel);
	HAL_TIM_PWM_Start_IT(&self->PWMCTimer, self->PWMCChannel);
}

/*
 * @brief Enable motor
 */
void DRV8311_Enable(DRV8311_t *self)
{
	self->status = GOOD;
	self->mode = ENABLED;

	HAL_GPIO_WritePin(self->sleepPort, self->sleepPin, GPIO_PIN_SET);
}

/*
 * @brief Disable motor
 */
void DRV8311_Disable(DRV8311_t *self)
{
	self->mode = DISABLED;

	HAL_GPIO_WritePin(self->sleepPort, self->sleepPin, GPIO_PIN_RESET);

	DRV8311_updatePWM(self, 0.0f, 0.0f, 0.0f);
}

/*
 * @brief Set FOC output
 */
void DRV8311_SetOutput_FOC(DRV8311_t *self,float v_d, float v_q, float motorDirection)
{
	/* Check motor status */
	if(self->mode == DISABLED) { DRV8311_updatePWM(self, 0.0f, 0.0f, 0.0f); }

	/* Ouput definitons */
	float outAlpha, outBeta, outA, outB, outC;

	/* Copy current electrial angle of motor */
	float theta = self->electricalAngle;

	/* Normalise vector magnitude */
	float mag = sqrtf(v_d*v_d + v_q*v_q);

	if(mag > 1.0f){
		v_d /= mag;
		v_q /= mag;
	}

	/* Inverse Park Transform */
	DRV8311_inversePark(&outAlpha, &outBeta, v_d, v_q, theta);

	/* Correct vetor direction */
	outBeta *= motorDirection;

	/* Inverse power-variant Clarke Transform */
	DRV8311_inversePvClarke(&outA, &outB, &outC, outAlpha, outBeta);

#ifdef SVM
	/* Neutral voltage shifting for full voltage sine waves */
	float v_neutral = 0.5f * (fmaxf(fmaxf(outA, outB), outC) + fminf(fminf(outA, outB), outC));

	outA -= v_neutral;
	outB -= v_neutral;
	outC -= v_neutral;
#endif

	/* Shift output for PWM signal */
	outA += 0.5f;
	outB += 0.5f;
	outC += 0.5f;

	/* Update signals */
	DRV8311_updatePWM(self, outA, outB, outC);
}

///////////////////////////// Intergrated Current Sensing Functions /////////////////////////////


///////////////////////////// Calibration Functions /////////////////////////////

/*
 * @brief Find motor zero electrical angle
 */
void DRV8311_FindZeroAngle(DRV8311_t *self, float amplitude, float mechanicalPosition) {
	uint16_t overflow = 1280;

	self->index %= overflow;

	float position = (float)(self->index) * (2*M_PI / overflow);

	self->electricalAngle = sinf(position);

	if(self->index == (overflow>>2))
	{
		self->positions[0] = lpFilter(self->positions[0],mechanicalPosition,0.1);
	}
	else if(self->index == ((3*overflow)>>2))
	{
		self->positions[1] = lpFilter(self->positions[1],mechanicalPosition,0.1);
	}

	DRV8311_SetOutput_FOC(self, amplitude, 0, 1.0);

	self->offsetAngle = -atan2(sinf(self->positions[0]) + sinf(self->positions[1]), cosf(self->positions[0]) + cosf(self->positions[1]));

	self->index++;
}

/*
 * @brief Set output for stalldrive calibration
 */
void DRV8311_CalibrateEncoder(DRV8311_t *self, float amplitude, float mechanicalPosition, float motorDirection)
{
	uint16_t overflow = 60000;

	if(self->index > overflow) { self->index = 0; }

	float position = (float)(self->index) * (2*M_PI/overflow) * signf(amplitude);

	self->electricalAngle = position*self->numPolePairs;

	DRV8311_SetOutput_FOC(self, 0, fabsf(amplitude), motorDirection);

	self->index++;
}

/*
 * @brief Set output on direct channel. Ensures no current on direct is generating torque.
 */
void DRV8311_CalibrateTest(DRV8311_t *self,float amplitude, float motorDirection)
{
	DRV8311_SetOutput_FOC(self, amplitude, 0, motorDirection);
}

///////////////////////////// Helper Functions /////////////////////////////

/*
 * @brief Update PWM outputs
 */
void DRV8311_updatePWM(DRV8311_t *self, float a, float b, float c)
{

	self->outputA = a;
	self->outputB = b;
	self->outputC = c;

	self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period * a);
	self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period * b);
	self->PWMCConfig.Pulse = (uint32_t)(self->PWMCTimer.Init.Period * c);

	__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse);
	__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse);
	__HAL_TIM_SetCompare(&self->PWMCTimer, self->PWMCChannel, self->PWMCConfig.Pulse);
}

/*
 * @brief Perfrom inverse park transform
 */
void DRV8311_inversePark(float* alpha, float* beta, float v_d, float v_q , float theta)
{
    float c = cosf(theta);
    float s = sinf(theta);

	*alpha = c * v_d - s * v_q;
    *beta  = s * v_d + c * v_q;
}

/*
 * @brief Perfrom inverse power-variant Clarke transform
 */
void DRV8311_inversePvClarke(float* a, float* b, float* c, float alpha, float beta)
{
	*a = ONEONSQRT3  * alpha;
	*b = ONEON2SQRT3 * (-alpha + SQRT3 * beta);
	*c = ONEON2SQRT3 * (-alpha - SQRT3 * beta);
}
