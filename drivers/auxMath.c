/*
 * auxMath.c
 *
 *  Created on: 31Jan.,2017
 *      Author: TheBlueprintLabs
 */

#include <math.h>
#include "DataStructures/auxMath.h"

#ifndef M_TWOPI
#define M_TWOPI 6.28318530718
#endif

float wrapAngle(float x){
    x = fmodf(x + M_PI, M_TWOPI);
    if (x < 0)
        x += M_TWOPI;
    return x - M_PI;
}


float wrapTo2Pi(float angle){

    angle = fmodf(angle, M_TWOPI);
    if(angle < 0){
        angle += M_TWOPI;
    }

    return angle;
}

float wrapAngleDegrees(float x){
    x = fmodf(x + 180.0, 360.0);
    if (x < 0)
        x += 360.0;
    return x - 180.0;
}

/**
 * @brief	Returns the smallest angle (in radians) starting from y, ending at x.
 * 			Angle is wrapped between -PI and +PI
 * @param	float x: end angle
 * @param	float y: start angle
 */
float signedSmallestAngleBetween(float x, float y) {
	float a = x - y;
	a = fmodf(a, M_TWOPI);
	if (a > M_PI)
		return a - M_TWOPI;
	if (a < -M_PI)
		return a + M_TWOPI;
    return a;
}

float smallestAngleBetween(float x, float y){
	float a = x - y;
	a = fmodf(a + M_PI, 2*M_PI);
    if (a < 0)
       a += 2*M_PI;
    return fabs(a - M_PI);
}

float lpFilter(float prevVal, float newMeas, float tau){
	//return (newMeas - prevVal) / tau + prevVal;
	return (newMeas*tau)+prevVal*(1-tau);
}
float hpFilter(float prevVal, float newMeas, float lastMeas,float tau){
	return (prevVal + newMeas - lastMeas);
}

/*
 * Caution when using this as it may very easily overflow the vectors. These are not very safe.
 */
void integrateVec(float* currVec, float* delta, float* newVec, float dt, size_t length){
	for(int i = 0; i < length; ++i){
		newVec[i] = currVec[i] + delta[i]*dt;
	}
}

float signf(float value)
{
	if(value>0)
	{
		return 1.0;
	}
	if(value<0)
	{
		return -1.0;
	}
	if(value ==0)
	{
		return 0.0;
	}
	return 0.0;
}

// cap
//caps value between max and min

uint8_t cap(float* value, float max, float min)
{
	if(*value>max)
	{
		*value=max;
		return 1;
	}
	else if (*value<min)
	{
		*value=min;
	    return 1;
	}
	else
	{
		return 0;
	}
}

float deadbandMapping(float input, float deadband, float deadbandGradient){

	float output;
	if(input >= deadband){
		output = input - (1.0-deadbandGradient)*deadband;
	} else if(input <= -deadband){
		output = input + (1.0-deadbandGradient)*deadband;
	} else {
		output = deadbandGradient*input;
	}
	return output;
}

//uint8_t CHK_FLAG(uint8_t val,uint8_t pos)
//{
//	if ( (val >> pos) & 1)
//	{
//	 return 1;
//	}
//	else
//	{
//	 return 0;
//	}
//}

float fLimitDelta(float prev, float newMeas, float maxDelta) {
	if (fabsf(prev - newMeas) > maxDelta) {
		if (newMeas > prev) {
			return prev + maxDelta;
		} else {
			return prev - maxDelta;
		}
	}
	return newMeas;
}

void computeSpline3(float spline[], float p0,float v0,float p1,float v1,float dt){

	spline[0] = p0;
	spline[1] = v0;
	spline[2] = (3*(p1-p0) - (2*v0+v1)*dt)/(dt*dt);
	spline[3] = (-2*(p1-p0) + (v1+v0)*dt)/(dt*dt*dt);
	return;

}

float evaluateSpline3Position(float spline[],float t){

	return (spline[0] + spline[1]*t + spline[2]*t*t + spline[3]*t*t*t);

}

float evaluateSpline3Velocity(float spline[],float t){

	return (spline[1] + 2*spline[2]*t + 3*spline[3]*t*t);

}

void computeSpline5(float spline[], float p0,float v0,float a0,float p1,float v1,float a1,float dt){

	spline[0] = p0;
	spline[1] = v0;
	spline[2] = 0.5*a0;
	spline[3] = (20*(p1-p0) - (8*v1 + 12*v0)*dt - (3*a0 - a1)*dt*dt)/(2*dt*dt*dt);
	spline[4] = (30*(p0-p1) + (14*v1 + 16*v0)*dt + (3*a0 - 2*a1)*dt*dt)/(2*dt*dt*dt*dt);
	spline[5] = (12*(p1-p0) - (6*v1 + 6*v0)*dt - (a0 - a1)*dt*dt)/(2*dt*dt*dt*dt*dt);
	return;

}

float evaluateSpline5Position(float spline[],float t){

	return (spline[0] + spline[1]*t + spline[2]*t*t + spline[3]*t*t*t  + spline[4]*t*t*t*t + spline[5]*t*t*t*t*t);

}

float evaluateSpline5Velocity(float spline[],float t){

	return (spline[1] + 2*spline[2]*t* + 3*spline[3]*t*t  + 4*spline[4]*t*t*t + 5*spline[5]*t*t*t*t);

}
