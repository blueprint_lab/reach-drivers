/*H**********************************************************************
* FILENAME :        DRV8313.c             DESIGN REF: RS1
*
* DESCRIPTION :
*       Driver for DRV8313 Motor Controller
*
* PUBLIC FUNCTIONS :
*
*
*
* NOTES :
*       These functions are a part of the FM suite;
*       See IMS FM0121 for detailed description.
*
*       Copyright TheBlueprintLabs
*
* AUTHOR :   Paul Phillips        START DATE :   16/8/16
*
* CHANGES :
*
* REF NO  VERSION DATE    WHO     DETAIL
*
*
*H*/

/*
 * 	void (*Initialise)();
	void (*Enable)();
	void (*Disable)();
	int (*GetStatus)();
	void (*SetOutput)(float motorTorque);
	void (*SetDirection)(uint8_t motorDirection);
 */
#include "stm32_device_select.h"
#include "Hardware/DRV8313.h"

void DRV8313_NB_Construct(DRV8313_t* self,
        TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel, TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel,
        uint16_t IN1_pin, GPIO_TypeDef* IN1_port,uint16_t IN2_pin, GPIO_TypeDef* IN2_port, uint16_t resetPin, GPIO_TypeDef* resetPort, uint16_t sleepPin, GPIO_TypeDef* sleepPort,uint16_t faultPin, GPIO_TypeDef* faultPort ){

    self->Enable = DRV8313_NB_Enable;
    self->Check=DRV8313_Check;
    self->Init = DRV8313_NB_Init;
    self->Disable = DRV8313_NB_Disable;
    self->SetOutput = DRV8313_NB_SetOutput;


    self->resetPin = resetPin;
    self->resetPort = resetPort;
    self->sleepPin = sleepPin;
    self->sleepPort = sleepPort;
    self->faultPin = faultPin;
    self->faultPort = faultPort;

    self->IN1_pin=IN1_pin;
    self->IN1_port=IN1_port;

    self->IN2_pin=IN2_pin;
    self->IN2_port=IN2_port;

    self->PWMATimer = PWMATimer;
    self->PWMBTimer = PWMBTimer;

    self->PWMAChannel = PWMAChannel;
    self->PWMBChannel = PWMBChannel;

    self->mode=DISABLED;
    self->torquePrev=0;
}


void DRV8313_NB_Disable(DRV8313_t *self) {
    self->mode=DISABLED;

    HAL_GPIO_WritePin(self->IN1_port, self->IN1_pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(self->IN2_port, self->IN2_pin, GPIO_PIN_RESET);

    self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*0);
    self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*0);

    __HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
    __HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );

}

void DRV8313_NB_Enable(DRV8313_t *self) {
    self->mode=ENABLED;

    HAL_GPIO_WritePin(self->IN1_port, self->IN1_pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(self->IN2_port, self->IN2_pin, GPIO_PIN_SET);

    self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*0);
    self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*0);

    __HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
    __HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );
}

void DRV8313_NB_SetOutput(DRV8313_t *self, float torque) {
    if(torque > 1) torque = 1;
    if(torque  <-1) torque = -1;

    if(self->mode==DISABLED) {
        HAL_GPIO_WritePin(self->IN1_port, self->IN1_pin, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(self->IN2_port, self->IN2_pin, GPIO_PIN_RESET);
    } else if(torque < 0) {
    	HAL_GPIO_WritePin(self->IN2_port, self->IN2_pin, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(self->IN1_port, self->IN1_pin, GPIO_PIN_SET);

        self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*fabs(1));
        self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*fabs(torque));

        __HAL_TIM_SET_COMPARE(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );
        __HAL_TIM_SET_COMPARE(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
    } else if (torque > 0) {
    	HAL_GPIO_WritePin(self->IN1_port, self->IN1_pin, GPIO_PIN_RESET);
    	HAL_GPIO_WritePin(self->IN2_port, self->IN2_pin, GPIO_PIN_SET);

        self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*fabs(1));
        self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*fabs(torque));

        __HAL_TIM_SET_COMPARE(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );
        __HAL_TIM_SET_COMPARE(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
    }

   self->torquePrev = torque;
}

void DRV8313_NB_Init(DRV8313_t *self) {
    HAL_GPIO_WritePin(self->IN1_port, self->IN1_pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(self->IN2_port, self->IN2_pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(self->resetPort, self->resetPin, GPIO_PIN_SET);

    if (self->sleepPort != NULL) {
    	HAL_GPIO_WritePin(self->sleepPort, self->sleepPin, GPIO_PIN_SET);
    }

    self->PWMAConfig.OCMode = TIM_OCMODE_PWM1;
    self->PWMAConfig.Pulse = 300;
    self->PWMAConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
    self->PWMAConfig.OCFastMode = TIM_OCFAST_DISABLE;

    self->PWMBConfig.OCMode = TIM_OCMODE_PWM1;
    self->PWMBConfig.Pulse = 300;
    self->PWMBConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
    self->PWMBConfig.OCFastMode = TIM_OCFAST_DISABLE;

    if (HAL_TIM_PWM_ConfigChannel(&self->PWMATimer, &self->PWMAConfig, self->PWMAChannel) != HAL_OK) SET_BIT(self->status,FAULT);
    if (HAL_TIM_PWM_Start_IT(&self->PWMATimer, self->PWMAChannel) != HAL_OK) SET_BIT(self->status,FAULT);

    if (HAL_TIM_PWM_ConfigChannel(&self->PWMBTimer, &self->PWMBConfig, self->PWMBChannel) != HAL_OK) SET_BIT(self->status,FAULT);
    if (HAL_TIM_PWM_Start_IT(&self->PWMBTimer, self->PWMBChannel) != HAL_OK) SET_BIT(self->status,FAULT);
}

void DRV8313_ConstructSlowDecay(DRV8313_t* self,
		TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel, TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel,
		uint16_t enablePin, GPIO_TypeDef* enablePort, uint16_t resetPin, GPIO_TypeDef* resetPort, uint16_t sleepPin, GPIO_TypeDef* sleepPort,uint16_t faultPin, GPIO_TypeDef* faultPort ){

	self->Enable = DRV8313_Enable;
	self->Check=DRV8313_Check;
	self->Init = DRV8313_Init;
	self->Disable = DRV8313_Disable;
	self->SetOutput = DRV8313_SetOutput_SlowDecay;

	self->enablePin = enablePin;
	self->enablePort = enablePort;
	self->resetPin = resetPin;
	self->resetPort = resetPort;
	self->sleepPin = sleepPin;
	self->sleepPort = sleepPort;
	self->faultPin = faultPin;
	self->faultPort = faultPort;

	self->PWMATimer = PWMATimer;
	self->PWMBTimer = PWMBTimer;

	self->PWMAChannel = PWMAChannel;
	self->PWMBChannel = PWMBChannel;

	self->mode=DISABLED;
}

void DRV8313_Construct(DRV8313_t* self,
		TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel, TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel,
		uint16_t enablePin, GPIO_TypeDef* enablePort, uint16_t resetPin, GPIO_TypeDef* resetPort,
		uint16_t sleepPin, GPIO_TypeDef* sleepPort,uint16_t faultPin, GPIO_TypeDef* faultPort, uint8_t invert){

	self->Enable = DRV8313_Enable;
	self->Check=DRV8313_Check;
	self->Init = DRV8313_Init;
	self->Disable = DRV8313_Disable;
	self->SetOutput = DRV8313_SetOutput;

	self->enablePin = enablePin;
	self->enablePort = enablePort;
	self->resetPin = resetPin;
	self->resetPort = resetPort;
	self->sleepPin = sleepPin;
	self->sleepPort = sleepPort;
	self->faultPin = faultPin;
	self->faultPort = faultPort;

	self->PWMATimer = PWMATimer;
	self->PWMBTimer = PWMBTimer;

	self->PWMAChannel = PWMAChannel;
	self->PWMBChannel = PWMBChannel;

	self->mode=DISABLED;
	self->invert = invert;
}

void DRV8313_Init(DRV8313_t *self) {
	HAL_GPIO_WritePin(self->enablePort, self->enablePin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(self->resetPort, self->resetPin, GPIO_PIN_SET);

	if (self->sleepPort != NULL) {
		HAL_GPIO_WritePin(self->sleepPort, self->sleepPin, GPIO_PIN_SET);
	}

	if (HAL_TIM_Base_Start_IT(&self->PWMATimer) != HAL_OK) SET_BIT(self->status,FAULT);
	if (HAL_TIM_Base_Start_IT(&self->PWMBTimer) != HAL_OK) SET_BIT(self->status,FAULT);

//	if (HAL_TIM_PWM_ConfigChannel(&self->PWMATimer, &self->PWMAConfig, self->PWMAChannel) != HAL_OK) SET_BIT(self->status,FAULT);
	if (HAL_TIM_PWM_Start_IT(&self->PWMATimer, self->PWMAChannel) != HAL_OK) SET_BIT(self->status,FAULT);
//
//	if (HAL_TIM_PWM_ConfigChannel(&self->PWMBTimer, &self->PWMBConfig, self->PWMBChannel) != HAL_OK) SET_BIT(self->status,FAULT);
	if (HAL_TIM_PWM_Start_IT(&self->PWMBTimer, self->PWMBChannel) != HAL_OK) SET_BIT(self->status,FAULT);
}


/**
 * This is not generating Async Fast Decay.
 * 		To implement Async Fast Decay (AKA Drive/Coast):
 * 			EN pins are PWM driven to determine the duty cycle
 * 			IN pins set the direction of rotation and should always be opposing. Even when motor is stopped.
 * 		DRIVE state = EN1, EN2 pins both high. IN1 and IN2 opposing states (HIGH LOW).
 * 		COAST state = EN1, EN2 pins both low. IN1 and IN2 don't care. Inertia from motor slowly dissipates through flyback diodes.
 * 			See:
 * https://www.allaboutcircuits.com/technical-articles/difference-slow-decay-mode-fast-decay-mode-h-bridge-dc-motor-applications/
 */
void DRV8313_SetOutput_AsyncFastDecay(DRV8313_t *self, float torque)
{

	float outA=0;
	float outB=0;
	if(torque>OUTPUT_RANGE)	{
		torque = OUTPUT_RANGE;
	}
	if(torque<-OUTPUT_RANGE){
		torque =-OUTPUT_RANGE;
	}

	if(self->mode==DISABLED) {
		outA = 0;
		outB = 0;
	} else {
		// when torque == 0, outA = 0.5, outB = 0.5
		// torque > 0, outA = +lower, outB = +higher
		// torque < 0, outA = +higher, outB = +lower
		outA = 0.5 * (1 - torque);
		outB = 0.5 * (1 + torque);
	}

	self->PWMAConfig.Pulse = (uint32_t)((self->PWMATimer.Init.Period) * outA);
	self->PWMBConfig.Pulse = (uint32_t)((self->PWMBTimer.Init.Period) * outB);
	__HAL_TIM_SET_COMPARE(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
	__HAL_TIM_SET_COMPARE(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );
}


/**
 * @brief	Run the motor using slow decay mode.
 * 			That is, the motor driver alternates
 * 			between driving and braking during the PWM cycle.
 * 			EN1, EN2 pins both high in all states.
 * 			IN1 and IN2 driven by PWM.
 * 			SLOW DECAY MODE provides FAST deceleration of motor.
 * 			DRIVE state = PWM states are opposite
 * 			BRAKE state = PWM states are the same
 * 			During braking state, current in motor is dissipated as
 * 			heat in the motor and the FETs in the DRV.
 * 			See:
 * https://www.allaboutcircuits.com/technical-articles/difference-slow-decay-mode-fast-decay-mode-h-bridge-dc-motor-applications/
 *
 */
void DRV8313_SetOutput_SlowDecay(DRV8313_t *self, float torque) {
	float outA = 0;
	float outB = 0;

	if(torque>OUTPUT_RANGE)	{
		torque = OUTPUT_RANGE;
	}

	if(torque<-OUTPUT_RANGE){
		torque =-OUTPUT_RANGE;
	}

    if(self->mode == DISABLED) {
        outA = 0;
        outB = 0;
    } else {
        // when torque == 0, outA = 0.5, outB = 0.5
        // torque > 0, outA = +lower, outB = +higher
        // torque < 0, outA = +higher, outB = +lower
        outA = 0.5 * (1 - torque);
        outB = 0.5 * (1 + torque);
    }

	self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*outA);
	self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*outB);

	__HAL_TIM_SET_COMPARE(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse);
	__HAL_TIM_SET_COMPARE(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse);
}


//Uses output value of between 1 and -1
void DRV8313_SetOutput(DRV8313_t *self, float torque) {
	float outA=0;
	float outB=0;

	if (self->invert) {
		torque *= -1;
	}

	if(torque > OUTPUT_RANGE) {
		torque = OUTPUT_RANGE;
	}
	if(torque < -OUTPUT_RANGE) {
		torque = -OUTPUT_RANGE;
	}

	if(self->mode==DISABLED) {
		self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period * 0);
		self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period * 0);
	} else {
		if(torque >= 0) {
			outA = 0.0;
			outB = fabs(torque);
		}
	    if (torque < 0) {
			 outA = 1.0;
			 outB = fabs(1 - fabsf(torque));
		}

	    if (signf(self->torquePrev) != signf(torque)) {
	    	HAL_GPIO_WritePin(self->enablePort, self->enablePin, GPIO_PIN_RESET);
	    }

		self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period * outA);
		self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period * outB);

		__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
		__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );

		if (signf(self->torquePrev) != signf(torque)) {
			HAL_GPIO_WritePin(self->enablePort, self->enablePin, GPIO_PIN_SET);
		}
	}

	self->torquePrev=torque;
}

void DRV8313_Enable(DRV8313_t *self) {
	self->mode=ENABLED;

	HAL_GPIO_WritePin(self->enablePort, self->enablePin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(self->resetPort, self->resetPin, GPIO_PIN_SET);
}

void DRV8313_Disable(DRV8313_t *self) {
	self->mode=DISABLED;

	HAL_GPIO_WritePin(self->enablePort, self->enablePin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(self->resetPort, self->resetPin, GPIO_PIN_RESET);

	self->PWMAConfig.Pulse = (uint32_t)(self->PWMATimer.Init.Period*0);
	self->PWMBConfig.Pulse = (uint32_t)(self->PWMBTimer.Init.Period*0);

	__HAL_TIM_SetCompare(&self->PWMATimer, self->PWMAChannel, self->PWMAConfig.Pulse );
	__HAL_TIM_SetCompare(&self->PWMBTimer, self->PWMBChannel, self->PWMBConfig.Pulse );
}

void DRV8313_Sleep(DRV8313_t *self) {
	if (self->sleepPort == NULL) return;

	HAL_GPIO_WritePin(self->sleepPort, self->sleepPin, GPIO_PIN_RESET);
}

uint8_t DRV8313_Check(DRV8313_t *self) {
    if(!HAL_GPIO_ReadPin(self->faultPort, self->faultPin)) {
    	SET_BIT(self->status,FAULT);
    	return 1;
    } else {
    	CLEAR_BIT(self->status,FAULT);
    	return 0;
    }
}
