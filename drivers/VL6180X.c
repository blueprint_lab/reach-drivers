/*
 * VL6180X.c
 *
 * Time of flight linear distance sensor.
 *
 * See documentation here:
 * 		- Data sheet: https://www.st.com/resource/en/datasheet/vl6180x.pdf
 * 		-  App. note: https://www.st.com/resource/en/application_note/an4545-vl6180x-basic-ranging-application-note-stmicroelectronics.pdf
 *
 *  Created on: May 5, 2021
 *      Author: Shaun Barlow & Kyle McLan
 */

#include "Hardware/VL6180X.h"
#include <string.h>

int8_t vl_I2C_read(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint16_t reg_addr, uint8_t *data, uint16_t len);
int8_t vl_I2C_write(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint16_t reg_addr, uint8_t *reg_data, uint16_t len);

uint8_t dataCSReady[2] = {0, RESULT__INTERRUPT_STATUS_GPIO};
uint8_t dataIntClear[3] = {0, SYSTEM__INTERRUPT_CLEAR, 0x7};
uint8_t dataCheckReading[2] = {0, RESULT__RANGE_VAL};


/* Identify whether VL6190X is on the I2C bus */
uint8_t VL6180X_test(I2C_HandleTypeDef* phi2c) {
	uint8_t buff[10];
	vl_I2C_read(phi2c, VL6180X_DEFAULT_I2C_ADDR, IDENTIFICATION__MODEL_ID, buff, 1);

	return buff[0] == 0xb4;
}

/* Construct the interface. If the enable isn't controlled, set enablePort to NULL*/
void VL6180X_construct(VL6180X_t* self, I2C_HandleTypeDef* phi2c, uint8_t deviceAddress, GPIO_TypeDef* enablePort, uint16_t enablePin) {
	self->phi2c = phi2c;
	self->deviceAddress = deviceAddress;
	self->enablePort = enablePort;
	self->enablePin = enablePin;
}

/* Initialize the interface*/
uint8_t VL6180X_init(VL6180X_t* self, uint8_t offset) {

	if (!(VL6180X_test(self->phi2c))) {return 1;}

	uint8_t data, len = 1;
	uint16_t reg;

	/* private settings from page 24 of app note */
	reg = 0x0207; data = 0x01; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x0208; data = 0x01; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x0096; data = 0x00; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x0097; data = 0xfd; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00e3; data = 0x00; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00e4; data = 0x04; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00e5; data = 0x02; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00e6; data = 0x01; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00e7; data = 0x03; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00f5; data = 0x02; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00d9; data = 0x05; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00db; data = 0xce; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00dc; data = 0x03; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00dd; data = 0xf8; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x009f; data = 0x00; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00a3; data = 0x3c; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00b7; data = 0x00; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00bb; data = 0x3c; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00b2; data = 0x09; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00ca; data = 0x09; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x0198; data = 0x01; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x01b0; data = 0x17; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x01ad; data = 0x00; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x00ff; data = 0x05; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x0100; data = 0x05; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x0199; data = 0x05; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x01a6; data = 0x1b; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x01ac; data = 0x3e; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x01a7; data = 0x1f; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	reg = 0x0030; data = 0x00; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	/* Recommended : Public registers - See data sheet for more detail */

	// Enables polling for 'New Sample ready' when measurement completes
	reg = SYSTEM__MODE_GPIO1; 					data = 0x10; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	// Set the averaging sample period (compromise between lower noise and increased execution time), set to 17ms
	reg = READOUT__AVERAGING_SAMPLE_PERIOD; 	data = 0xFF; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	// sets the # of range measurements after which auto calibration of system is performed
	reg = SYSRANGE__VHV_REPEAT_RATE; 			data = 0x05; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	// Optional: Public registers - See data sheet for more detail. Set default ranging inter-measurement period to 60ms
	reg = SYSRANGE__INTERMEASUREMENT_PERIOD; 	data = 0x05; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	// Configures interrupt on 'New Sample Ready threshold event'
	reg = SYSTEM__INTERRUPT_CONFIG_GPIO; 		data = 0x24; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	// Set max convergence time 50ms
	reg = SYSRANGE__MAX_CONVERGENCE_TIME; 		data = 0x32; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	// Apply manual offset
	reg = SYSRANGE__PART_TO_PART_RANGE_OFFSET; 	data = offset; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	// Initial boot complete
	reg = SYSTEM__FRESH_OUT_OF_RESET; 			data = 0x00; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	self->initialized = 1;
	return 0;
}

/* Initialize the interface by enabling and immediately reassigning address*/
void VL6180X_initMulti(VL6180X_t* self, uint8_t offset, uint8_t newDeviceAddress) {
	if (self->enablePort == NULL) {return;}

	HAL_GPIO_WritePin(self->enablePort, self->enablePin, GPIO_PIN_SET);
	HAL_Delay(1); // Wait for the VL's internal MCU to boot
	VL6180X_init(self, offset);
	VL6180X_changeAddress(self, newDeviceAddress);

	self->initialized = 1;
}

uint8_t VL6180X_changeAddress(VL6180X_t* self, uint8_t newDeviceAddress) {
	uint8_t data, len = 1;
	uint8_t dataReadback = 0;
	uint16_t reg;

	// Send new address and read back to confirm
	reg = I2C_SLAVE__DEVICE_ADDRESS; 	data = newDeviceAddress; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
										data = newDeviceAddress; vl_I2C_read(self->phi2c, newDeviceAddress, reg, &dataReadback, len);

	if (dataReadback == newDeviceAddress) {
		self->deviceAddress = dataReadback;
		return 0;
	} else {
		return 1;
	}
}


#define SAMPLES 500
/* Apply offset calibration procedure */
void VL6180X_offsetcalibration(VL6180X_t* self, int targetDistance) {
	if (!self->initialized) return;
	uint8_t len = 1, data, reg;

	switch (self->calibrationState) {
		case VL_ZERO_OFFSET:
			/* Clear the system offset */
			reg = SYSRANGE__PART_TO_PART_RANGE_OFFSET; 	data = 0x00; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

			self->calibrationState = VL_COLLECT_DATA;
		case VL_COLLECT_DATA:
			/* Get data and calculate offset */
			if (!self->rangeAvailable) return;
			self->rangeAvailable = 0;

			self->calibrationAve += self->range;
			self->sampleCounter++;

			if (self->sampleCounter > SAMPLES) {
				self->sampleCounter = 0;

				self->calibrationAve /= SAMPLES;
				self->calibrationOffset = targetDistance - (int)self->calibrationAve;

				self->calibrationState = VL_APPLY_OFFSET;
			} else {
				break;
			}
		case VL_APPLY_OFFSET:
			/* Apply offset */
			reg = SYSRANGE__PART_TO_PART_RANGE_OFFSET; 	data = (uint8_t)self->calibrationOffset; vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
			self->calibrationState = VL_CALIBRATION_CPLT;
		default:
			break;
	}
}

/* Poll for a single reading from VL6180X - Blocking */
uint8_t VL6180X_readSingleRangePolling(VL6180X_t* self) {
	uint8_t data;
	uint8_t len;
	uint16_t reg;

	// wait for device to be ready for range measurement
	data = 0;
	while (!(data & 0x01)) {
		vl_I2C_read(self->phi2c, self->deviceAddress, RESULT__RANGE_STATUS, &data, 1);
	}

	// Start a range measurement
	reg = SYSRANGE__START; data = 0x01; len = 1;
	vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	// Poll until bit 2 is set
	data = 0;
	while (data != 0x04) {
		vl_I2C_read(self->phi2c, self->deviceAddress, RESULT__INTERRUPT_STATUS_GPIO, &data, 1);
	}

	// read range in mm
	reg = RESULT__RANGE_VAL; data = 0x00; len = 1;
	vl_I2C_read(self->phi2c, self->deviceAddress, reg, &data, 1);
	uint8_t range = data;

	// clear interrupt
	reg = SYSTEM__INTERRUPT_CLEAR; data = 0x07; len = 1;
	vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	return range;
}

/* Initialise continuous measurments - Blocking */
uint8_t VL6180X_initContinuousMeasurement(VL6180X_t* self) {
	uint8_t data;
	uint8_t len;
	uint16_t reg;
	uint8_t status;

	// wait for device to be ready for range measurement
	data = 0;
	while (!(data & 0x01)) {
		status = vl_I2C_read(self->phi2c, self->deviceAddress, RESULT__RANGE_STATUS, &data, 1);
	}

	// Start a continuous range measurement
	reg = SYSRANGE__START; data = 0x03; len = 1;
	status = vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);
	self->nextReadingTime = HAL_GetTick();
	return status;
}

/* Read measurments - Blocking */
uint8_t VL6180X_readContinuousRangePolling(VL6180X_t* self) {
	if (!self->initialized) return 0;

	uint8_t data;
	uint8_t len;
	uint16_t reg;

	// Poll until bit 2 is set
	data = 0;
	while (data != 0x04) {
		vl_I2C_read(self->phi2c, self->deviceAddress, RESULT__INTERRUPT_STATUS_GPIO, &data, 1);
	}

	// read range in mm
	reg = RESULT__RANGE_VAL; data = 0x00; len = 1;
	vl_I2C_read(self->phi2c, self->deviceAddress, reg, &data, 1);
	uint8_t range = data;

	// clear interrupt
	reg = SYSTEM__INTERRUPT_CLEAR; data = 0x07; len = 1;
	vl_I2C_write(self->phi2c, self->deviceAddress, reg, &data, len);

	return range;
}

/**
 * @brief	Read from sensor after VL6180X_SAMPLE_PERIOD_MS (called in systick)
 *
 */
uint8_t VL6180X_readDMA(VL6180X_t* self) {
	if (!self->initialized) return 0;

	switch (self->commState) {
		case VL_CS_READY:
			/* Check that another device does not have the lock . */
			if (self->phi2c->Devaddress != 0) { return 0; }

			/* Check if TOF is ready for next sample */
			if (self->nextReadingTime > HAL_GetTick()) { return 0; }

			I2C_writeDMA(self->phi2c, self->deviceAddress, dataCSReady, 2);
			self->commState = VL_CS_CHECKING_FOR_READING_RX;
			break;
		default:
			break;
	}
	return 0;
}

/**
 * @brief	To be called in both HAL_I2C_MasterTxCpltCallback() and HAL_I2C_MasterRxCpltCallback().
 * @param	transferDirection determines whether this is from the Tx or Rx callbacks.
 * @retval	I2C_CallbackStatus indicates whether callback is for this module instance.
 */
uint8_t VL6180X_DMARxTxCplt(VL6180X_t* self, I2C_HandleTypeDef *hi2c, I2C_txRxDirection_t transferDirection) {
	if (self->phi2c != hi2c) { return I2C_NOT_MY_CALLBACK; } // Not my callback
	if ((self->phi2c->Devaddress >> 1) != self->deviceAddress) { return  I2C_NOT_MY_CALLBACK; } // Not my callback

	switch (self->commState) {
	case VL_CS_READY:
		break;
	case VL_CS_CHECKING_FOR_READING_RX:
		if (transferDirection == I2C_DIR_TX) { // Coming from TX callback
			self->rxData[0] = 0;
			I2C_readDMA(self->phi2c, self->deviceAddress, self->rxData, 1);
			self->commState = VL_CS_CHECK_FOR_READING_TX;
		} else {
			self->errorCount++; self->commState = VL_CS_READY; self->phi2c->Devaddress = 0;
		}
		break;
	case VL_CS_CHECK_FOR_READING_TX:
		if (transferDirection == I2C_DIR_RX) { 	// Coming from RX callback
			if (self->rxData[0] == 0x04) { 		// A reading is ready. Request it.
				I2C_writeDMA(self->phi2c, self->deviceAddress, dataCheckReading, 2);
				self->commState = VL_CS_GET_READING_TX;
			} else {
				I2C_writeDMA(self->phi2c, self->deviceAddress, dataCSReady, 2);
				self->commState = VL_CS_CHECKING_FOR_READING_RX;
			}
		} else {
			self->errorCount++; self->commState = VL_CS_READY; self->phi2c->Devaddress = 0;
		}
		break;
	case VL_CS_GET_READING_TX:
		if (transferDirection == I2C_DIR_TX) { // Coming from TX callback
			I2C_readDMA(self->phi2c, self->deviceAddress, self->rxData, 1);
			self->commState = VL_CS_GET_READING_RX;
		} else {
			self->errorCount++; self->commState = VL_CS_READY; self->phi2c->Devaddress = 0;
		}
		break;
	case VL_CS_GET_READING_RX:
		if (transferDirection == I2C_DIR_RX) { 	// Coming from RX callback
			self->range = lpFilter(self->range, ((float)self->rxData[0]), 0.2);
			self->rangeAvailable = 1;

			// clear interrupts
			I2C_writeDMA(self->phi2c, self->deviceAddress, dataIntClear, 3);
			self->commState = VL_CS_CLEAR_INTERRUPT_TX;
		} else {
			self->errorCount++; self->commState = VL_CS_READY; self->phi2c->Devaddress = 0;
		}
		break;
	case VL_CS_CLEAR_INTERRUPT_TX:
		if (transferDirection == I2C_DIR_TX) { // Coming from TX callback
			self->commState = VL_CS_READY;
			self->phi2c->Devaddress = 0;
			self->nextReadingTime = HAL_GetTick() + VL6180X_SAMPLE_PERIOD_MS;
		} else {
			self->errorCount++; self->commState = VL_CS_READY; self->phi2c->Devaddress = 0;
		}
		break;
	default:
		break;
	}
	return I2C_NOT_MY_CALLBACK;
}

/* Blocking read from sensor */
int8_t vl_I2C_read(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint16_t reg_addr, uint8_t *data, uint16_t len) {
	uint8_t regData[2];

	uint16_t DevAddress = ((uint16_t) dev_id) << 1;
	uint32_t Timeout = 100;

	HAL_StatusTypeDef status;

	regData[0] = reg_addr >> 8;
	regData[1] = reg_addr & 0xFF;

	status = HAL_I2C_Master_Transmit(phi2c, DevAddress, regData, sizeof(reg_addr), Timeout);
	if (status != HAL_OK) return (int8_t) status;

	status = HAL_I2C_Master_Receive(phi2c, DevAddress, data, len, Timeout);

	return (int8_t) status;
}

/* Blocking write to sensor */
int8_t vl_I2C_write(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint16_t reg_addr, uint8_t *reg_data, uint16_t len) {
	uint8_t regData[2];
	uint8_t buffer[len + sizeof(reg_addr)];

	uint16_t DevAddress = (uint16_t) dev_id << 1;
	uint32_t Timeout = 100;

	HAL_StatusTypeDef status;


	regData[0] = reg_addr >> 8;
	regData[1] = reg_addr & 0xFF;

	memcpy(buffer, regData, sizeof(uint16_t));
	memcpy(buffer + sizeof(reg_addr), reg_data, len);
	status = HAL_I2C_Master_Transmit(phi2c, DevAddress,  buffer, len + sizeof(reg_addr), Timeout);

	return (int8_t)status;
}
