#!/bin/sh

SYSTEM=Linux
PROCESSOR=x86_64

system="$(echo $SYSTEM | tr '[A-Z]' '[a-z]')"
processor="$(echo $PROCESSOR | tr '[A-Z]' '[a-z]')"

rm -rf build-${system}_${processor}
mkdir -p build-${system}_${processor}
cd build-${system}_${processor}

cmake .. \
    -DBUILD_SHARED=ON \
    -DCMAKE_SYSTEM_NAME=$SYSTEM \
    -DCMAKE_SYSTEM_PROCESSOR=$PROCESSOR \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++
make
cd ..

mkdir -p -v lib
rm -f -v lib/librs1_drivers_${system}_${processor}.so
cp -v build-${system}_${processor}/librs1_drivers_${SYSTEM}_${PROCESSOR}.so lib/librs1_drivers_${system}_${processor}.so