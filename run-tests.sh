#!/bin/sh

# Locate and remove the test binary if it exists
if [ -f "./build/CMakeCache.txt" ]; then
    project_name=`grep CMAKE_PROJECT_NAME build/CMakeCache.txt | cut -d "=" -f2`
    executable_name="${project_name}-$(echo test)"
    executable_path="./build/test/${executable_name}"
    if [ -f ${executable_path} ]; then
        rm -f -v ${executable_path}
    fi
else 
    mkdir -p -v build
fi

cd build

cmake .. -DBUILD_TESTS=ON 
make
cd ..

# Locate and run the test binary
project_name=`grep CMAKE_PROJECT_NAME build/CMakeCache.txt | cut -d "=" -f2`
./build/test/$project_name-test --gtest_output="xml:test-reports/report.xml"