/**
  ******************************************************************************
  * @file    mock.c
  * @brief   This file mocks any functionality needed for embedded testing that
  *          is not covered by the mock drivers
  *
  *****************************************************************************
  */

/* Mock exported functions */
extern void Error_Handler(void){}