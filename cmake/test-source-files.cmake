set(
    SRC_FILES 
    "drivers/auxMath.c"
    "drivers/circularQueue.c"
    "drivers/comsprocess.c"
    "drivers/control.c"
    "drivers/matrixMath.c"
    "drivers/scheduler.c"
    "drivers/semanticVersion.c"
)

message("SRC_FILES= " "${SRC_FILES}")