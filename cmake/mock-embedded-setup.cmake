add_definitions(-DSTM32F413xx)
add_definitions(-DDEBUG)
add_definitions(-DUSE_HAL_DRIVER)
add_definitions(-D__USE_MISC)
add_definitions(-DG_TEST)

set(CMAKE_CXX_FLAGS "-std=gnu++11 -g3 -fno-strict-aliasing -DUNITY_FLOAT_PRECISION=0.0001f -w")
set(CMAKE_C_FLAGS "-std=gnu11 -g3 -fno-strict-aliasing -DUNITY_FLOAT_PRECISION=0.0001f -w")
set(LINK_FLAGS "-lc -lm -Wl")