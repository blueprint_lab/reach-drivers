file(GLOB MOCK_FILES "mock/*.c" "rs1_drivers/mock/Drivers/STM32F4xx_HAL_Driver/Src/*.c")

# Store any 'include' dirs in the INC_FILES_DIR variable.
set(INC_FILES_DIR 
    inc/Communication/
    mock/Drivers/CMSIS/Include/
    mock/Drivers/STM32F4xx_HAL_Driver/Inc/
    mock/Drivers/CMSIS/Device/ST/STM32F4xx/Include/
    mock/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy/     
)