set(
    SRC_FILES 
    "drivers/canFrameQ.c"
    "drivers/canPacketQ.c"
    "drivers/canRxFrameQ.c"
    "drivers/canTxFrameQ.c"
    "drivers/canbus.c"
    "drivers/circularQueue.c"
    "drivers/comslink.c"
    "drivers/comsprocess.c"
)

message("SRC_FILES= " "${SRC_FILES}")