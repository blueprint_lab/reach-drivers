/*
 * semanticVersion.h
 *
 *  Created on: 07-19-2023
 *      Author: Kyle McLean
 */

#ifndef _SEMANTIC_VERSION_H_
#define _SEMANTIC_VERSION_H_

void version_float2int(float f, uint8_t *major, uint8_t *submaj, uint8_t *minor);
float version_int2float(uint8_t major, uint8_t submaj, uint8_t minor);

#endif /* _SEMANTIC_VERSION_H_ */
