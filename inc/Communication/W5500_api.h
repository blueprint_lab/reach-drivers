/*
 * W5500_api.h
 *
 *  Created on: 29 Oct 2020
 *      Author: shaun
 */

#ifndef RS1DRIVERSINC_COMMUNICATION_W5500_API_H_
#define RS1DRIVERSINC_COMMUNICATION_W5500_API_H_

#ifndef LINUX
#include "../Inc/stm32_device_select.h"
#include "main.h"
#endif

#include "Ethernet/socket.h"
#include "DataStructures/circularQueue.h"

#define W5500_EN
#define NUM_SOCKETS 8
#define W5500_DEFAULT_TX_SIZE 255
#define W5500_DEFAULT_RX_SIZE 255

typedef enum W5500_Error_t {
	OK,
	COMS_ERROR,
	API_ERROR,
} W5500_error_t;

typedef enum W5500_socket_type_t {
	sockType_CLOSED = Sn_MR_CLOSE,
	sockType_TCP = Sn_MR_TCP,
	sockType_UDP = Sn_MR_UDP,
	sockType_IPRAW = Sn_MR_IPRAW,
	sockType_MACRAW = Sn_MR_MACRAW,
} W5500_socket_type_t;

typedef struct W5500_socket_t {
	uint8_t index;
	uint16_t port;
	uint8_t flag;
	uint8_t txSize;
	uint8_t rxSize;
	W5500_socket_type_t type;
	uint8_t status;
	uint8_t remoteIP[4];
	uint16_t remotePort;
	circularQueue txQ;
	circularQueue rxQ;
	uint8_t* rxBuff;
	uint8_t bytesToRead;
	int32_t errorCode;
} W5500_socket_t;

typedef struct W5500_t {
	SPI_HandleTypeDef* phspi;
	GPIO_TypeDef* pSelectPort;
	uint16_t selectPin;
	GPIO_TypeDef* pResetPort;
	uint16_t resetPin;
	uint32_t errorCount;
	W5500_error_t errorState;
	W5500_socket_t sockets[NUM_SOCKETS];
	wiz_NetInfo netInfo;
	wiz_PhyConf phyConf;
} W5500_t;


void W5500_Construct(W5500_t* self, SPI_HandleTypeDef* hspi,
		GPIO_TypeDef* pSelectPort, uint16_t selectPin,
		GPIO_TypeDef* pResetPort, uint16_t resetPin);
void W5500_Init(W5500_t* self);
void W5500_ConstructSocket(W5500_t* self,
								uint8_t sockIndex,
								W5500_socket_type_t sockType);
void W5500_InitSocket(W5500_socket_t* sock);
void W5500_DestroySocket(W5500_socket_t* sock);
uint16_t W5500_Read(W5500_socket_t* sock);
uint32_t W5500_SendUDP(W5500_socket_t* sock);
uint8_t W5500_NetConf(W5500_t* self);

#endif /* RS1DRIVERSINC_COMMUNICATION_W5500_API_H_ */
