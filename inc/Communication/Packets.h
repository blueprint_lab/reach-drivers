/*H**********************************************************************
* FILENAME :        Packets.h             DESIGN REF: RS1
*
* DESCRIPTION :
*       Has the definitions of all packets
*       Has functions for serial communication
*
* PUBLIC FUNCTIONS :
*
*
*
* NOTES :
*       These functions are a part of the FM suite;
*       See IMS FM0121 for detailed description.
*
*       Copyright TheBlueprintLabs
*
* AUTHOR :   Paul Phillips        START DATE :   16/8/16
*
* CHANGES :
*
* REF NO  VERSION DATE    WHO     DETAIL
*
*
*H*/

#ifndef _PACKET_H
#define _PACKET_H

#include "stm32_device_select.h"
// Packet structure
/*

START_BYTE ID_BYTE DATA_BYTES END_BYTES



*/


#define START_BYTE 0x00
#define HEADER_LENGTH 3

#define RX_BUFFER_LENGTH 255
#define TX_BUFFER_LENGTH (COUNTOF(txBuffer) - 1)

enum PACKET_ID {
	enable,
	disable,
	actuate,
};

void Packets_Send( uint8_t* packet, size_t packetLength);
void Packets_Receive(void);
void Packets_Initiliase(void);
void Packets_Parse();
void packetEncode(uint8_t device_id, uint8_t packet_id, uint8_t len, uint8_t * outputData, void* data);
void packetDecode( void* outputData, uint8_t* data);

/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))

#endif
