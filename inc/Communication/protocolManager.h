/*
 * protocolManager.h
 *
 *  Created on: Oct 14, 2020
 *      Author: Shaun Barlow
 */

#ifndef RS1DRIVERSINC_COMMUNICATION_PROTOCOLMANAGER_H_
#define RS1DRIVERSINC_COMMUNICATION_PROTOCOLMANAGER_H_

#include "Communication/protocol3rdParty.h"


void protocolManager_construct(protocolManager_t* self);
uint8_t protocolManager_getQueuedFrame(protocolManager_t* self, canbus_frame_t* frame);

#endif /* RS1DRIVERSINC_COMMUNICATION_PROTOCOLMANAGER_H_ */
