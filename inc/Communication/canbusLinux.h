#ifndef _CANBUS_LINUX_H
#define _CANBUS_LINUX_H

#include <stdint.h>
#include "Communication/canRxFrameQ.h"
#include "Communication/canTxFrameQ.h"
#include "Communication/comslink.h"
#include "Communication/canbus.h"
#include "Communication/canPacketQ.h"

struct _canbus_linux_link {
    coms_link* parent;
    int sock;
    rxFrameQ_t rxFrameQ;
    rxFrameQ_t rxFrameQ_2;
    txFrameQ_t txFrameQ;
    txFrameQ_t txFrameQ_2;

    canPacketQ_t rxQueue;
    uint16_t fifoError;
    uint8_t pendingFrames;
    uint8_t* pDeviceID;
};

void canbusLinux_Construct(canbus_linux_link_t* self, uint16_t frameBuffLen, uint8_t* pDeviceID);
uint8_t canbusLinux_Init(canbus_linux_link_t* self);
uint8_t canbusLinux_ReadFrame(canbus_linux_link_t* self, rxFrame_t* frameOut);
uint8_t canbusLinux_Close(canbus_linux_link_t* self);

uint8_t canbusLinux_Transmit(coms_link* parent, packet_t* packet);
uint8_t canbusLinux_enableRead(coms_link* parent);
canbus_tx_state_t canbusLinux_TransmitFrame(coms_link* parent, txFrame_t* pFrame);

#endif /* _CANBUS_LINUX_H*/
