/*
 * macAddress.h
 *
 *  Created on: Mar 2, 2021
 *      Author: Shaun Barlow
 */

#ifndef INC_COMMUNICATION_MACADDRESS_H_
#define INC_COMMUNICATION_MACADDRESS_H_

#define BPL_MAC_ADDRESS_A 0x70
#define BPL_MAC_ADDRESS_B 0xB3
#define BPL_MAC_ADDRESS_C 0xD5
#define BPL_MAC_ADDRESS_D 0xFE

#endif /* INC_COMMUNICATION_MACADDRESS_H_ */
