/*
 * canFrameQ.h
 *
 *  Created on: 13 Mar 2021
 *      Author: Shaun Barlow
 */

#ifndef RS1DRIVERSINC_COMMUNICATION_CANRXFRAMEQ_H_
#define RS1DRIVERSINC_COMMUNICATION_CANRXFRAMEQ_H_

#if defined(TEST_RX_FRAME_CIRC_Q) || defined(TEST_CANPACKETQ)
#include "../../mock/mockCanbus.h"
#else
#ifndef LINUX
#include "main.h"
#else 
#include <string.h>
#include "DataStructures/canbusStructs.h"
#endif
#endif

#define FRAME_MAX_BYTES 8
typedef struct rxFrameQ_ rxFrameQ_t;


typedef struct rxFrame_t rxFrame_t;
struct rxFrame_t {
	CAN_RxHeaderTypeDef header;
	uint8_t data[FRAME_MAX_BYTES];
};

struct rxFrameQ_ {
	void (*Deconstruct)(rxFrameQ_t* self);
	void (*Reset)(rxFrameQ_t* self);
	uint8_t (*Push)(rxFrameQ_t* self, rxFrame_t* elem);
	uint8_t (*Pop)(rxFrameQ_t* self, rxFrame_t* elem);

	rxFrame_t* origin;
	rxFrame_t* front;
	rxFrame_t* back;

	uint16_t numel;
	uint16_t maxel;

	uint8_t locked;
};

void rxFrameQ_Construct(rxFrameQ_t* self, size_t size);

#endif /* RS1DRIVERSINC_COMMUNICATION_CANFRAMEQ_H_ */
