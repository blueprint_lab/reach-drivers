#ifndef _LINUX_FIFO_H
#define _LINUX_FIFO_H

#include <stdint.h>
#include "DataStructures/circularQueue.h"
#include "Communication/comslink.h"

#define MAX_FIFO_NAME_LEN 40


struct fifo_comms_link_ {
    coms_link* parent;
    int txFileDesc, rxFileDesc;
    char txFifoName[MAX_FIFO_NAME_LEN], rxFifoName[MAX_FIFO_NAME_LEN];
    circularQueue txQ;
	circularQueue rxQ;
};

uint8_t fifo_construct(fifo_comms_link_t* self, 
                        char* txFifoName, char* rxFifoName,
                        uint txQLen, uint rxQLen);
uint8_t fifo_Read(coms_link* parent);
uint8_t fifo_Transmit(coms_link* parent, packet_t* packet);
uint8_t fifo_enableRead(coms_link* parent);

#endif /* _LINUX_FIFO_H */