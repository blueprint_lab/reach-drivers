/*
 * spi.h
 *
 *  Created on: 31Jan.,2017
 *      Author: TheBlueprintLabs
 */

#ifndef HAL_WRAPPERS_SPI_H_
#define HAL_WRAPPERS_SPI_H_

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "stm32_device_select.h"
#include "DataStructures/circularQueue.h"


typedef struct spi_t spi_t;
struct spi_t{

	uint8_t (*transmitMsg)(spi_t * self, uint8_t* msg, size_t length);
	void (*readReceive)(spi_t* self, uint8_t* destination, size_t length);
	void (*deconstruct)(spi_t* self);

	GPIO_TypeDef* chipSelectPort;
	uint16_t chipSelect;
	SPI_HandleTypeDef* spiObj;

	volatile uint8_t* receiveBuff;
	uint8_t* sendBuff;
	circularQueue sendQueue;
	size_t bufferSize;
};


uint8_t spiConstruct(spi_t* self, SPI_HandleTypeDef* spiObject, GPIO_TypeDef* chipSelectPort, uint16_t chipSelect, size_t size);

void spiDeconstruct(spi_t* self);

uint8_t spiTransmitMessage(spi_t* self, uint8_t* msg, size_t length);

void spiFetchMessage(spi_t* self, uint8_t* destination, size_t length);

#endif /* HAL_WRAPPERS_SPI_H_ */
