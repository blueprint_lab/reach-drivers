/*
 * comslink.h
 *
 *  Created on: Aug 16, 2019
 *      Author: Shaun Barlow
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __COMSLINK_H
#define __COMSLINK_H

#include <stdint.h>
#if defined (TEST_COMSPROCESS) || defined (LINUX)
#else
#include "stm32_device_select.h"
#endif

#include "packetid.h"

/* BEGIN Common Communications Header
 * MUST appear before #include uart.h and canbus.h
 * */

/* Forward typedef of comslink */
typedef struct coms_link_ coms_link;

typedef struct uart_comms_link_ cdc_comms_link;
typedef struct uart_comms_link_ uart_comms_link;
typedef struct udp_comms_link_ udp_comms_link;
typedef struct _canbus_linux_link canbus_linux_link_t;
typedef struct _udp_linux_link udp_linux_link_t;
typedef struct fifo_comms_link_ fifo_comms_link_t;


#define PACKET_HAS_OPTIONS	 	0x80
#define PACKET_OPT_FWD_ALLOW 	0b00001
#define PACKET_OPT_FWD_DENY 	0b00000
#define PACKET_OPT_IS_DEMAND	0b00010
#define PACKET_OPT_NOT_DEMAND	0b00000
#define PACKET_OPT_IS_V2		0b00100
#define PACKET_OPT_ALL_ENABLED	0x3

#define PACKET_V2(packet)	            (packet.option & PACKET_OPT_IS_V2)
#define PACKET_FWD_ALLOW(packet)	    (packet.option & PACKET_OPT_FWD_ALLOW)
#define CHK_PACKET_IS_DEMAND(packet)	(packet.option & PACKET_OPT_IS_DEMAND)
#define CHK_PACKET_IS_REPORT(packet)	!(CHK_PACKET_IS_REPORT(packet))
#define SET_PACKET_IS_DEMAND(packet)	(packet->option |= PACKET_OPT_IS_DEMAND)
#define SET_PACKET_IS_REPORT(packet)	(packet->option &= ~(PACKET_OPT_IS_DEMAND))

#define HEADER_SIZE 4
#define UART_PACKET_HEADER_SIZE HEADER_SIZE
#define MAX_PACKET_DATA_SIZE 64

// Don't blindly change this value! Hardware parameters are loaded based on this.
#define MAX_CANBUS_DEVICES 20

/* Include packet structures used by all communication methods */

#if defined (TEST_COMSPROCESS) || defined (LINUX)
#include "../DataStructures/packetStructs.h"
#else
#include "DataStructures/packetStructs.h"
#endif

typedef enum coms_type_t_ {
	COMS_TYPE_NONE,
	COMS_TYPE_UART,
	COMS_TYPE_CANBUS,
	COMS_TYPE_UDP,
	COMS_TYPE_CAN_LINUX,
	COMS_TYPE_FIFO_LINUX,
	COMS_TYPE_UDP_LINUX,
	COMS_TYPE_CDC
} coms_type_t;

typedef enum protocol
{
	BPL_PROTOCOL_V1,
	BPSS_PROTOCOL,
	BPL_PROTOCOL_V2,
} protocol_t;

typedef enum bus_state {
	bus_RX_ONLY,
	bus_TX_QUEUED,
	bus_TX_AT_WILL,
	bus_MASTER_CTRL,
} busState_t;

/* END Common Communications Header */

#ifdef HAL_UART_MODULE_ENABLED
#include "Communication/uart.h" 	// Do not move above Common Communications Header
#endif

#ifdef HAL_CAN_MODULE_ENABLED
#include "DataStructures/canbusStructs.h"
#include "Communication/canbus.h" 	// Do not move above Common Communications Header
#endif

#ifdef HAL_PCD_MODULE_ENABLED
#include "usbd_cdc_if.h" 	        // Do not move above Common Communications Header
#endif

#if defined(HAL_SPI_MODULE_ENABLED) && !defined(G_TEST)
#include "Communication/udp.h"
#endif

#ifdef LINUX
#include "Communication/canbusLinux.h"
#include "Communication/udpLinux.h"
#include "Communication/linuxFifo.h"
#endif

// Forward declare the bus state manager for comslink
typedef struct bus_state_mgr_t bus_state_mgr_t;

struct coms_link_ {
	uint8_t (*enableRead)(coms_link* self);
	uint8_t (*Transmit)(coms_link* self, packet_t* packet);
	uint8_t (*Read)(coms_link* self);
	uint8_t (*transmitDMA)(coms_link* self);
	int8_t (*encodePacket)(packet_t* packet, uint8_t address, uint16_t code, uint8_t length, uint8_t* data, uint8_t priority);
	void (*Destroy)(coms_link* self);

	packet_t transmitPacket;
	packet_t currPacket;

	coms_type_t comsType;

#ifdef HAL_CAN_MODULE_ENABLED
	canbus_comms_link* canbusLink;
#endif
#ifdef HAL_UART_MODULE_ENABLED
	uart_comms_link* uartLink;
#endif
#ifdef HAL_SPI_MODULE_ENABLED
	udp_comms_link* udpLink;
#endif
#ifdef LINUX
	canbus_linux_link_t* canbusLinuxLink;
	fifo_comms_link_t* fifoLinuxLink;
	udp_linux_link_t* udpLinuxLink;
#endif
	uint8_t* pDeviceID;
	protocol_t* pComsProtocol;
	busState_t busState;
	bus_state_mgr_t* pBusStateMgr;
};

/* Bus State manager */
struct bus_state_mgr_t{
	void (*BusStateMgr)(bus_state_mgr_t* self);
	void (*NextValidDevice)(bus_state_mgr_t* self);
	void (*SendBusState)(bus_state_mgr_t* self, uint8_t deviceid, busState_t state);
	busState_t (*HandleBusState)(coms_link* parent, busState_t new_state);

	coms_link* parent;
	uint8_t* pCanbusDevices;
	uint8_t* pDeviceTimeouts; // Multiples of 10us (i.e. 10 = 100us)
	uint8_t* pCurrentDevice;
	uint8_t* pCurrentTimeout;
	uint8_t* pDeviceID;
	uint16_t busStatePID;
	busState_t state;
	uint8_t busy;
	uint8_t scheduleCount;
	uint8_t timeoutCounter;
	uint8_t numdev;
	uint8_t timeoutFlag;
	uint8_t txCplt;
};

/* Function prototypes */
#ifdef HAL_UART_MODULE_ENABLED
uint8_t comslink_construct_uart(coms_link* self, UART_HandleTypeDef* huart,
        uint16_t rxBuffLen, uint16_t txBuffLen, protocol_t* pComsProtocol, uint8_t* pDeviceID);
#endif

#ifdef HAL_CAN_MODULE_ENABLED
uint8_t comslink_construct_canbus(coms_link* self, CAN_HandleTypeDef* hcan, uint8_t* pDeviceID, protocol_t* pComsProtocol);
#endif

#ifdef HAL_PCD_MODULE_ENABLED
uint8_t comslink_construct_cdc(coms_link* self, uint16_t rxBuffLen, uint16_t txBuffLen, protocol_t* pComsProtocol);
#endif

#if defined(HAL_SPI_MODULE_ENABLED) && !defined(G_TEST)
uint8_t comslink_construct_udp(coms_link* self, W5500_socket_t* sock, protocol_t* pComsProtocol);
#endif

#ifdef LINUX
uint8_t comslink_construct_canbus_linux(coms_link* self, uint16_t frameBuffLen, uint8_t* pDeviceID);
uint8_t comslink_construct_udp_linux(coms_link* self, uint8_t rxQLen, float port);
uint8_t comslink_construct_udp_client_linux(coms_link* self, uint8_t rxQLen, float port);
uint8_t comslink_construct_fifo_linux(coms_link* self, char* txFifoName, char* rxFifoName, uint txQLen, uint rxQLen);
#endif


void comslink_destroy(coms_link* self);
uint8_t comslink_null(coms_link* parent);
#endif /* __COMSLINK_H */
