/*
 * canFrameQ.h
 *
 *  Created on: 11 Nov 2020
 *      Author: Shaun Barlow
 */

#ifndef RS1DRIVERSINC_COMMUNICATION_CANFRAMEQ_H_
#define RS1DRIVERSINC_COMMUNICATION_CANFRAMEQ_H_

#ifndef LINUX
#include "main.h"
#endif
#include "DataStructures/canbusStructs.h"

void canbus_InitialiseFrameQueue(canbus_frame_queue* q);
uint8_t canbus_FrameQueueIsEmpty(canbus_frame_queue* q);
void canbus_AddFrameToQueue(canbus_frame_queue* q, canbus_frame_t* frame);
void canbus_PopFrameFromQueue(canbus_frame_queue* q, canbus_frame_t* frame);

#endif /* RS1DRIVERSINC_COMMUNICATION_CANFRAMEQ_H_ */
