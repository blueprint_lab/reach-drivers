/*
 * monitorDtatusBits.h
 *
 *  Created on: 17 Mar 2021
 *      Author: Kyle McLean
 *
 *  Monitor status bits header file.
 *  Monitor status bits must be named as : "status_<name>" where <name> is the name in lowerCamelCase.
 *
 */

#ifndef MONITOR_STATUS_BITS_H_
#define MONITOR_STATUS_BITS_H_

typedef enum monitorStatusBits{
	status_encoderNotDetected					=0,
	status_encoderPositionError					=1,  // RS2 - outer encoder position error
	status_motorDriverFault						=2,
	status_comsCRCerror							=3,

	status_comsSerialError						=4,
	status_hardwareOverTemperature				=5,
	status_hardwareOverHumidity					=6,
	status_flashFailedRead						=7,

	status_motorDriverOverTemp					=8,
	status_motorDriverOverCurrentUnderVoltage	=9,
	status_hardwareOverPressure					=10,
	status_deviceIDConflict						=11,

	status_innerEncoderPositionError 			=12, // RS2 - inner encoder position error
	status_prefixConflict						=13,
	status_ethWizError							=14,
	status_deviceAxisError						=15,

	status_calibrationTimeout					=16,
	status_encoderFault							=17,
	status_readProtectEnabled					=18,
	status_serviceDue							=19,

	status_jaw_zero_required					=20,
	status_encodernIntialisationError			=21,
	status_unknown_22							=22,
	status_unknown_23							=23,

	status_positionReportNotReceived			=24,  // Applicable only to TX2 at the moment. Placed here to avoid conflicts.
	status_canbusError							=25,
	status_canTimeout							=26,
	status_invalidFirmware  					=27,

	status_lowSupplyVoltage						=28,
	status_computerOffline						=29,
	status_unknown_30							=30,
	status_encoderPositionInvalid				=31,
} monitorStatusBits_t;

#endif /* MONITOR_STATUS_BITS_H_ */
