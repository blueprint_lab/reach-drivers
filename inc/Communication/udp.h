/*
 * udp.h
 *
 *  Created on: Nov 9, 2020
 *      Author: Shaun Barlow
 */

#ifndef RS1DRIVERSINC_COMMUNICATION_UDP_H_
#define RS1DRIVERSINC_COMMUNICATION_UDP_H_

#ifndef LINUX
#include "main.h"
#endif
#include "Communication/W5500_api.h"
#include "Communication/comslink.h"
#include "DataStructures/circularQueue.h"
#include <stdint.h>

struct udp_comms_link_ {
	W5500_socket_t* sock;
};

uint8_t udp_construct(udp_comms_link* self, W5500_socket_t* sock);
uint8_t udp_enableRead(coms_link* parent);
uint8_t udp_Read(coms_link* parent);
uint8_t udp_QForTransmit(coms_link* parent, packet_t* packet);
uint8_t udp_Transmit(coms_link* parent);

#endif /* RS1DRIVERSINC_COMMUNICATION_UDP_H_ */
