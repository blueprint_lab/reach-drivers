/*
 * canFrameQ.h
 *
 *  Created on: 13 Mar 2021
 *      Author: Shaun Barlow
 */

#ifndef RS1DRIVERSINC_COMMUNICATION_CANTXFRAMEQ_H_
#define RS1DRIVERSINC_COMMUNICATION_CANTXFRAMEQ_H_

#if defined(TEST_TX_FRAME_CIRC_Q) || defined(TEST_CANPACKETQ)
#include "../../mock/mockCanbus.h"
#else
#ifndef LINUX
#include "main.h"
#else 
#include <string.h>
#include "DataStructures/canbusStructs.h"
#endif
#endif

#define FRAME_MAX_BYTES 8
typedef struct txFrameQ_ txFrameQ_t;


typedef struct txFrame_t txFrame_t;
struct txFrame_t {
	CAN_TxHeaderTypeDef TxHeader;
	uint8_t data[FRAME_MAX_BYTES];
};

struct txFrameQ_ {
	void (*Deconstruct)(txFrameQ_t* self);
	void (*Reset)(txFrameQ_t* self);
	uint8_t (*Push)(txFrameQ_t* self, txFrame_t* elem);
	uint8_t (*Erase)(txFrameQ_t* self);
	uint8_t (*PopPreview)(txFrameQ_t* self, txFrame_t* elem);
	uint8_t (*Pop)(txFrameQ_t* self, txFrame_t* elem);

	txFrame_t* origin;
	txFrame_t* front;
	txFrame_t* back;

	uint16_t numel;
	uint16_t maxel;
	uint8_t locked;
};

void txFrameQ_Construct(txFrameQ_t* self, size_t size);

#endif /* RS1DRIVERSINC_COMMUNICATION_CANFRAMEQ_H_ */
