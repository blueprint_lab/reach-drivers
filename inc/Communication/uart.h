/*
 * uart.h
 *
 *  Created on: 6 Jan 2017
 *      Author: p.phillips
 */

#ifndef COMMUNICATION_MODULES_UART_H_
#define COMMUNICATION_MODULES_UART_H_


#include "../Inc/stm32_device_select.h"
#include "Communication/comslink.h"
#include "DataStructures/circularQueue.h"
#include "main.h"
#include "Communication/tm_stm32_crc.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_LENGTH 128

#define MAX_UART_LINKS 5
coms_link* uart_list[MAX_UART_LINKS];
uint8_t uart_list_length;

typedef enum serial
{
	RS232,
	RS485,
}serial_t;


struct uart_comms_link_ {
	circularQueue transmitQueue;
	circularQueue transmitQueue2;
#ifdef HAL_UART_MODULE_ENABLED
	UART_HandleTypeDef* huart;
#endif
	volatile uint8_t* rxBuff;
	uint16_t readPointer;
	volatile uint16_t writePointer;  // Volatile since it is modified in a ISR of different priority
	uint16_t prevWritePointer;
	uint16_t bytesReceived;
	uint16_t errorCount;
	uint16_t errorIndex;
	uint8_t status;

	uint16_t packetEnd;
	uint8_t* txBuff;
	uint16_t rxBuffLen;
	uint16_t txBuffLen;
	uint8_t test;
	uint8_t dataLength;

	serial_t serialType;

	uint16_t RXE_Pin;
	GPIO_TypeDef* RXE_Port;
	uint16_t TXE_Pin;
	GPIO_TypeDef* TXE_Port;
};


typedef struct DMA_Base_Registers DMA_Base_Registers;
struct DMA_Base_Registers
{
	__IO uint32_t ISR;   /*!< DMA interrupt status register */
	__IO uint32_t Reserved0;
	__IO uint32_t IFCR;  /*!< DMA interrupt flag clear register */
};


uint8_t uart_comms_TransmitData(coms_link* parent, packet_t* packet);
uint8_t uart_comms_AddBytesToTxQ(coms_link* parent, uint8_t* data, uint8_t len);
#ifdef HAL_UART_MODULE_ENABLED
uint8_t uart_comms_construct(uart_comms_link* self, UART_HandleTypeDef* huart,
		uint16_t rxBuffLen, uint16_t txBuffLen, protocol_t protocolType);
#endif
void uart_comms_setRS485(uart_comms_link* self, uint16_t RXE_Pin, GPIO_TypeDef* RXE_Port,
		uint16_t TXE_Pin, GPIO_TypeDef* TXE_Port);
uint8_t uart_comms_enableRead(coms_link* parent);
uint8_t uart_comms_Read_Packet_IT(coms_link* parent);
uint8_t cdc_comms_Read_Packet_IT(coms_link* parent);
void uart_comms_reset(uart_comms_link* self);
size_t uart_comms_numpackets(uart_comms_link* self);
uint8_t uart_comms_TransmitDMA(coms_link* parent);

uint16_t uart_getInWaiting(coms_link* parent);
uint8_t uart_ReadBytes(coms_link* parent, uint8_t* buffer, uint8_t numBytes);

void uart_Irq(uart_comms_link* self);
void uart_DmaRxIrq(uart_comms_link* self);
void uart_IrqHandler (uart_comms_link* self);
void uart_ClearDmaRx(uart_comms_link* self);

// private function
uint8_t fetchByte(volatile uint8_t* buffer, size_t buffLen, size_t index);
void writeByte(uint8_t* buffer, size_t buffLen, size_t index, uint8_t val);

#endif /* COMMUNICATION_MODULES_UART_H_ */
