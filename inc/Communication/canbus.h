/*
 * canbus.h
 *
 *  Created on: Jul 24, 2019
 *      Author: Shaun Barlow
 */

#ifndef CANBUS_H_
#define CANBUS_H_

/* Include the comslink header so that HAL_CAN_MODULE_ENABLED can be checked */
#include "Communication/comslink.h"

#ifdef TEST_CANBUS
/***************** Test only *******************/
#include "../Communication/protocolManager.h"
#include "../Communication/protocol3rdParty.h"
#include "../Communication/canFrameQ.h"
#include "../Communication/canRxFrameQ.h"
#include "../Communication/canPacketQ.h"
#endif

#ifdef HAL_CAN_MODULE_ENABLED
/***************** STM32 Build only ******************/
#include "Communication/canbusStm32.h"
#endif

#ifdef LINUX
/***************** Linux Build only ******************/
#include "Communication/canbusLinux.h"
#endif

#include <stdint.h>
#include <string.h>
#include <stdlib.h>

/* defines */
#define BYTES_PER_FRAME 8

/* Forward declare the bus state manager for canbus */
typedef struct bus_state_mgr_t bus_state_mgr_t;

/* Canbus extended header struct */
typedef struct canbus_headerData canbus_headerData_t;
struct canbus_headerData {
	uint8_t totalFrames;
	uint8_t frameNumber;
	uint8_t deviceID;
	uint8_t option;

	uint16_t packetID;
};

/* Common canbus functions */
uint8_t canbus_Read(coms_link* parent);
uint8_t canbus_Transmit(coms_link* parent, packet_t* packet);
int8_t canbus_PopulatePacket(packet_t* packet, uint8_t deviceID, uint16_t packetID, uint8_t length, uint8_t* pData, uint8_t option);
uint8_t canbus_construct_busStateMgr(coms_link* parent, bus_state_mgr_t* self, busState_t* pBusState, uint8_t* pDeviceID, uint16_t busStatePID,  uint8_t numdev);
uint8_t canbus_checkForBusStateFrame(bus_state_mgr_t* pBusStateMgr, rxFrame_t* newFrame);
#endif /* CANBUS_H_ */
