/*
 * deviceid.h
 *
 *  Created on: 27Nov.,2019
 *      Author: s.barlow
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DEVICEID_H
#define __DEVICEID_H

#include <stdint.h>
#include "stm32_device_select.h"
#include "deviceManager.h"
#include "comslink.h"

#define DEVICE_ID_FLASH_ADDRESS 0x800C000
#define DEVICE_IP_FLASH_ADDRESS 0x0800C004
#define DEVICE_PORT_FLASH_ADDRESS 0x0800C008
#define DEVICE_GW_FLASH_ADDRESS 0x0800C00C
#define DEVICE_SN_FLASH_ADDRESS 0x0800C010

#define DEVICE_ID_CONFLICT_TIMEOUT_DURATION_MS 500


uint8_t Bootloader_SaveDeviceID(uint8_t deviceID);
uint8_t Bootloader_ReadDeviceID();
void Bootloader_SaveEthSettings(uint32_t deviceIP, uint16_t devicePort, uint32_t deviceGW, uint32_t deviceSN);
void Bootloader_SaveDeviceIP(uint32_t deviceIP);
uint32_t Bootloader_ReadDeviceIP();
void Bootloader_SaveDevicePort(uint16_t devicePort);
uint16_t Bootloader_ReadDevicePort();
void Bootloader_SaveDeviceGW(uint32_t deviceGW);
uint32_t Bootloader_ReadDeviceGW();
void Bootloader_SaveDeviceSN(uint32_t deviceSN);
uint32_t Bootloader_ReadDeviceSN();

uint8_t DeviceID_MatchBytesWithInt(uint8_t thousands, uint8_t hundreds,
									uint8_t tens, uint8_t ones, uint16_t full);
uint8_t DeviceID_StartDeviceIDConflictCheck(deviceManager_t* self,
													coms_link* pComsLink);
uint8_t DeviceID_CheckDeviceIDPacketForConflict(deviceManager_t* self,
													packet_t* packet);
#endif /* __DEVICEID_H */
