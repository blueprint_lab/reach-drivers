/*
 * canPacketQ.h
 *
 *  Created on: 17 Mar 2021
 *      Author: Kyle Mclean
 */
#ifndef CANPACKETQ_H_
#define CANPACKETQ_H_

#ifdef TEST_CANPACKETQ
/***************** Test only *******************/
#include "canRxFrameQ.h"
#include "../DataStructures/packetStructs.h"
#include "../DataStructures/canbusStructs.h"

#else
/***************** Build only ******************/
#include "Communication/comslink.h"
#include "DataStructures/canbusStructs.h"

#endif

/***************** Common **********************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/* Forward define structs */
typedef struct canbus_headerData canbus_headerData_t;

// Packet queue functions
void canbus_packetQ_Construct(canPacketQ_t* self, size_t size);
void canbus_packetQ_Deconstruct(canPacketQ_t* self);
void canbus_packetQ_Reset(canPacketQ_t* self);
uint8_t canbus_packetQ_Push(canPacketQ_t* self, packet_node* elem);
uint8_t canbus_packetQ_EraseFrontNode(canPacketQ_t* self);
packet_node* canbus_packetQ_getNextNode(canPacketQ_t* self, packet_node* current_elem);

// Canbus functions
void canbus_PopPacketFromQueue(canPacketQ_t* q, packet_t* packet);
void canbus_parseFrame(canPacketQ_t* self, rxFrame_t* newFrame);
uint8_t canbus_getFirstFullPacketFromQueue(canPacketQ_t* q, packet_t* packetOut);
packet_node* canbus_AddPacketToQueue(canPacketQ_t* q, packet_t* packet);
packet_node* canbus_getPacketNodeWithDeviceIDPacketID(canPacketQ_t* q, canbus_headerData_t* pDecodedHeaderData, uint16_t frameIndexMask);

#endif /* CANPACKETQ_H_ */







