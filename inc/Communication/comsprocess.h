/*
 * comsprocess.h
 *
 *  Created on: 6 Jan 2021
 *      Author: shaun
 */

#include <stdint.h>
#ifdef TEST_COMSPROCESS
#include "comslink.h"
#include "../DataStructures/circularQueue.h"
#else
#include "Communication/comslink.h"
#include "DataStructures/circularQueue.h"
#endif

#define PKT_HEADER_LEN 4
#define PKT_TERMINATING_BYTE 0
#define OPTION_FLAG 0x80

uint8_t coms_readPacketFromQ(packet_t* packet, circularQueue* Q);
int8_t coms_encodePacket(packet_t* packet, uint8_t address, uint16_t code,
		uint8_t length, uint8_t* data, uint8_t priority);
int8_t coms_decodePacket(packet_t* packet, uint8_t* buffer, uint8_t buffLength);
uint8_t cobs_decode(const uint8_t* input, uint8_t length, uint8_t* output);
uint8_t cobs_encode( uint8_t* input, uint8_t length, uint8_t * output);
unsigned crc8(unsigned crc, unsigned char const *data, size_t len);
