/*
 * canbusStm32.h
 *
 *  Created on: 22 April 2021
 *      Author: Kyle McLean
 *
 *	Broken out of canbus.h
 */

#ifndef CANBUS_STM32_H_
#define CANBUS_STM32_H_

#include "Communication/protocolManager.h"
#include "Communication/protocol3rdParty.h"
#include "Communication/canFrameQ.h"
#include "Communication/canRxFrameQ.h"
#include "Communication/canTxFrameQ.h"
#include "Communication/canPacketQ.h"
#include "DataStructures/canbusStructs.h"

#define FRAMES_PER_PACKET 0xF
#define BYTES_PER_PACKET (BYTES_PER_FRAME * FRAMES_PER_PACKET)

/* STM32F4 canbus Comms link */
struct canbus_comms_link_{
	coms_link* parent;
	uint8_t (*Transmit)(coms_link* parent, packet_t* packet);
	uint8_t (*Read)(coms_link* parent);
	uint8_t (*thirdPartyProtocol_Read)(canbus_comms_link* self,
					CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pData);
	CAN_HandleTypeDef* hcan;

	txFrameQ_t txFrameQ;
	txFrameQ_t txFrameQ_2;
	rxFrameQ_t rxFrameQ;
	rxFrameQ_t rxFrameQ_2;
	canPacketQ_t rxQueue;

	uint8_t filterCount;
	uint8_t pendingFrames;

	protocolManager_t protocolManager;
};

/* STM32 canbus function prototypes */
void canbus_AttemptTransmitFrame(canbus_comms_link* self, txFrame_t* pFrame);
void canbus_SetFilterAcceptAll(canbus_comms_link* self);
void canbus_AddFilterForDeviceID(coms_link* parent, uint8_t deviceID);
void canbus_AddFilterForForwardAllow(coms_link* parent);
void canbus_AddFilterForStdFrames(coms_link* parent);

uint8_t canbus_NOP_thirdPartyProtocol_Read(canbus_comms_link* self, CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pData);
uint8_t canbus_thirdPartyProtocol_Read(canbus_comms_link* self, CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pData);
uint8_t canbus_construct(canbus_comms_link* self, CAN_HandleTypeDef* hcan);
uint8_t canbus_thirdPartyProtocol_Transmit(canbus_comms_link* self);
uint8_t canbus_enableRead(coms_link* parent);

canbus_comms_link* canbus_GetLinkFromHcan(CAN_HandleTypeDef* hcan);
canbus_tx_state_t hcan_TransmitFrame(CAN_HandleTypeDef* hcan, txFrame_t* frame);
#endif /* CANBUS_STM32_H_ */
