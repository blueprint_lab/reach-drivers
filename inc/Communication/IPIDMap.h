/*
 * IPIDMap.h
 *
 *  Created on: 24 Feb 2021
 *      Author: Shaun Barlow
 */

#ifndef INC_COMMUNICATION_IPIDMAP_H_
#define INC_COMMUNICATION_IPIDMAP_H_


#include <stdint.h>

#define NUM_MAP_ENTRIES 0xE

typedef struct mapEntry_ mapEntry_t;
struct mapEntry_ {
	uint8_t ip[4];
	uint16_t port;
};


typedef struct IPIDMap_ IPIDMap_t;
struct IPIDMap_ {
	mapEntry_t map[NUM_MAP_ENTRIES];
};


uint8_t IPIDMap_getIpPort(IPIDMap_t* self, uint8_t deviceID, uint8_t* ip, uint16_t* port);
uint8_t IPIDMap_getDeviceID(IPIDMap_t* self, uint8_t* ip, uint16_t port);
uint8_t IPIDMap_addEntry(IPIDMap_t* self, uint8_t deviceID, uint8_t* ip, uint16_t port);
//uint8_t IPIDMap_getIpPort(uint8_t deviceID, uint8_t* ip, uint16_t* port);

#endif /* INC_COMMUNICATION_IPIDMAP_H_ */
