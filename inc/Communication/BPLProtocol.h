/*
 * BPLProtocol.h
 *
 *  Created on: 27Jun.,2018
 *      Author: p.phillips
 */

#ifndef BPLPROTOCOL_H_
#define BPLPROTOCOL_H_

//#include "../Inc/stm32_device_select.h"
#include "DataStructures/circularQueue.h"
#include "Communication/uart.h"




uint8_t BPLProtocol_encodePacket(packet_t* packet, uint8_t address ,uint8_t code, uint8_t length, uint8_t* data);
uint8_t BPLProtocol_decodePacket(packet_t* packet,uint8_t* rxBuff, uint8_t rxBuffLen);
#endif /* BPLPROTOCOL_H_ */
