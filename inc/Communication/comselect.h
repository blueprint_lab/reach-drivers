#ifndef _COMSELECT_H
#define _COMSELECT_H

#include <sys/select.h>
#include <unistd.h>

typedef struct comSelect_ comSelect_t;

struct comSelect_ {
    fd_set masterfds;
    fd_set workingrfds;
    int fdMax;
};

void comSelect_init(comSelect_t* self);
void comSelect_add(comSelect_t* self, int newFd);
int comSelect_run(comSelect_t* self);
int comSelect_check(comSelect_t* self, int fd);
#endif /* _COMSELECT_H */