/*
 * mode.h
 *
 *  Created on: 27 Apr 2021
 *      Author: John S
 *
 *  Operating Modes header file.
 *  Operating Modes must be named as : "mode_<name>" where <name> is your mode name in lowerCamelCase.
 *
 *
 *
 */

#ifndef OPERATING_MODES_H_
#define OPERATING_MODES_H_

typedef enum operatingMode{
	mode_standby=0x00,
	mode_disabled=0x01,
	mode_positionControl=0x02, 		// Bravo: Outer position control
	mode_velocityControl=0x03, 		// Bravo: Outer velocity control
	mode_currentControl=0x04,  		// Bravo: Inner current control
	mode_openLoopControl=0x05, 		// Bravo: Inner open-loop control
	mode_init=0x06 ,
	mode_calibrate=0x07,
	mode_factory=0x08,
	mode_compliant=0x09,
	mode_grip=0x0A,
    mode_stallDrive=0x0B,
    mode_calibrateTest=0x0C,
	mode_autoLimit=0x0D,
    mode_calibrateOuter=0x0E,
    mode_velocityControlInner=0x0F,
    mode_relativePositionControl=0x12,
	mode_indexedPositionControl=0x13,
	mode_positionPreset=0x14,
	mode_zeroVelocity=0x15,
	mode_factoryCurrentControl=0x16,
	mode_kmEndPos = 0x17,
	mode_kmEndVel = 0x18,
	mode_kmEndPosLocal = 0x19,
	mode_kmEndVelLocal = 0x1A,
	mode_velocityFactory=0x1B,
	mode_positionVelocityControl = 0x1C,
	mode_positionHold = 0x1D,
	mode_absPositionVelocityControl = 0x1E,
	mode_calibrateOverSerial=0x1F,
	mode_calibrateCurrentControl=0x20,
	mode_calibrateInnerOverSerial=0x21,
	mode_calibrateOuterOverSerial=0x22,
	mode_kmEndVelCamera=0x23,
	mode_posSequence=0x24,
	mode_directControl=0x25,
	mode_passive=0x26,
	mode_exitPassive=0x27,
	mode_kmEndVelWork=0x28,
	mode_autoLimitTof=0x29,
	mode_shutdown=0x2A,
	mode_autoLimitHall=0x2B,
	mode_kmVelSurfaceTrack=0x2C,
	mode_calibrateTofOffset=0x2D,
	mode_calibrateTofCrosstalk=0x2E,
	mode_hapticControl=0x2F,
	mode_torqueControl=0x30,
	mode_kmEndVelAlwaysUp = 0x31,
	mode_positionPresetDemand=0x32
} operatingMode;

#endif /* OPERATING_MODES_H_ */
