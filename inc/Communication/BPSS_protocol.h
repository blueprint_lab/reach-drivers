/*
 * uartBPSS.h
 *
 *  Created on: 6 Jan 2017
 *      Author: p.phillips
 */

#ifndef COMMUNICATION_MODULES_uartBPSS_H_
#define COMMUNICATION_MODULES_uartBPSS_H_


#include "../Inc/stm32_device_select.h"
#include "DataStructures/circularQueue.h"
#include "Communication/comsprocess.h"
#include "Communication/uart.h"

uint8_t BPSS_protocol_encodePacket(packet_t* pack, uint8_t address ,uint8_t cmd_id, uint8_t length, uint8_t* data);
uint8_t BPSS_protocol_decodePacket(coms_link* parent);

size_t BPSS_protocol_numPackets(coms_link* parent);

#endif /* COMMUNICATION_MODULES_uartBPSS_H_ */
