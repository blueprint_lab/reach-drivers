/*
 * udpLinux.h
 *
 *  Created on: April 2021
 *      Author: Kyle McLean
 */

#ifndef RS1DRIVERSINC_COMMUNICATION_UDP_LINUX_H_
#define RS1DRIVERSINC_COMMUNICATION_UDP_LINUX_H_

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <unistd.h>

#include "Communication/comslink.h"
#include "DataStructures/circularQueue.h"

struct _udp_linux_link {
	coms_link* parent;
    
	int sock;
	int slen;
	uint8_t rxSize;
	float port;
	circularQueue rxQ;
	
	struct sockaddr_in servaddr;
	struct sockaddr_in cliaddr;
};

uint8_t udpLinux_Construct(udp_linux_link_t* self, uint8_t rxSize, uint16_t port);
uint8_t udpLinux_Init(udp_linux_link_t* self);
uint8_t udpLinux_Read(coms_link* parent);
uint8_t udpLinux_ReadBytes(udp_linux_link_t* self);
uint8_t udpLinux_Close(udp_linux_link_t* self);
uint8_t udpLinux_Transmit(coms_link* parent, packet_t* packet);
uint8_t udpLinux_enableRead(coms_link* parent);

uint8_t udpLinuxClient_Construct(udp_linux_link_t* self, uint8_t rxSize, uint16_t port);
uint8_t udpLinuxClient_Init(udp_linux_link_t* self);
uint8_t udpLinuxClient_Read(coms_link* parent);
uint8_t udpLinuxClient_ReadBytes(udp_linux_link_t* self);
uint8_t udpLinuxClient_Close(udp_linux_link_t* self);
uint8_t udpLinuxClient_Transmit(coms_link* parent, packet_t* packet);
uint8_t udpLinuxClient_enableRead(coms_link* parent);

#endif /* RS1DRIVERSINC_COMMUNICATION_UDP_LINUX_H_ */
