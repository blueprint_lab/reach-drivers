/*
 * I2C.h
 *
 *  Created on: Jun 19, 2020
 *      Author: Shaun Barlow
 */

#ifndef COMMUNICATION_I2C_H_
#define COMMUNICATION_I2C_H_

#include "stm32_device_select.h"


typedef enum I2C_CommState_t {
	NOT_READY,
	TX_READY,
	TX_IN_PROGRESS,
	TX_COMPLETE,
} I2C_CommState_t;

enum I2C_CommLockStatus {
	LOCK_OK,
	LOCK_ERROR,
};

typedef enum I2C_txRxDirection_t {
	I2C_DIR_NONE,
	I2C_DIR_TX,
	I2C_DIR_RX,
} I2C_txRxDirection_t;

enum I2C_CallbackStatus {
	I2C_MY_CALLBACK,
	I2C_NOT_MY_CALLBACK,
};

HAL_StatusTypeDef I2C_readDMA(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t* data, uint16_t len);
HAL_StatusTypeDef I2C_writeDMA(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t* data, uint16_t len);

#endif /* COMMUNICATION_I2C_H_ */
