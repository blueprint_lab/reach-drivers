/*
 * KSZ8795CLX.h
 *
 *  Created on: Feb 21, 2021
 *      Author: shaun
 */

#ifndef COMMUNICATION_KSZ8795CLX_H_
#define COMMUNICATION_KSZ8795CLX_H_

#include "../Inc/stm32_device_select.h"

#define KSZ_NUM_STD_PORTS 4

typedef enum KSZ_OP_MODE_t {
	KSZ_OP_MODE_RES0,
	KSZ_OP_MODE_IN_NEG,
	KSZ_OP_MODE_10_BASE_T_HALF_DUP,
	KSZ_OP_MODE_100_BASE_TX_HALF_DUP,
	KSZ_OP_MODE_RES4,
	KSZ_OP_MODE_10_BASE_T_FULL_DUP,
	KSZ_OP_MODE_100_BASE_TX_FULL_DUP,
	KSZ_OP_MODE_RES7,
} KSZ_OP_MODE_t;

typedef struct KSZport_ KSZport_t;
struct KSZport_ {
	KSZ_OP_MODE_t opMode;
};

typedef struct KSZ8795CLX_ KSZ8795CLX_t;
struct KSZ8795CLX_ {
	SPI_HandleTypeDef* phspi;
	KSZport_t port[KSZ_NUM_STD_PORTS];
};


#define KSZ_READ_DATA 0b11
#define KSZ_WRITE_DATA 0b10

#define KSZ_REG_31_P1_CTL11_ST3 0x1F
#define KSZ_REG_47_P2_CTL11_ST3 0x2F
#define KSZ_REG_63_P3_CTL11_ST3 0x3F
#define KSZ_REG_79_P4_CTL11_ST3 0x4F


void KSZ_Construct(KSZ8795CLX_t* self, SPI_HandleTypeDef* phspi);
void KSZ_Init(KSZ8795CLX_t* self);
void KSZ_GetPortOperationModes(KSZ8795CLX_t* self);
#endif /* COMMUNICATION_KSZ8795CLX_H_ */
