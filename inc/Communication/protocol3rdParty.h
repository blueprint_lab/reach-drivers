/*
 * protocol3rdParty.h
 *
 *  Created on: Oct 13, 2020
 *      Author: Shaun Barlow
 *
 *	Third Party Coms Protocol Module
 *
 *	This module provides a framework for
 *	implementing third party coms protocols.
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PROTOCOL3RDPARTY_H
#define __PROTOCOL3RDPARTY_H

#include <stdint.h>
#ifndef LINUX
#include "stm32_device_select.h"
#endif
#include "Communication/canFrameQ.h"

#define NUM_PROTOCOLS 5

typedef struct protocol_ protocol3P_t;
struct protocol_ {
	// Read() returns 1 if frame was parsed, 0 if not.
	// Returning 1 indicates that frame is for this protocol
	// and should be queued for parsing.
	uint8_t (*Read)(protocol3P_t* self,
				CAN_RxHeaderTypeDef* RxHeader, uint8_t* pRxData);

	void (*Send)(protocol3P_t* self,
				CAN_RxHeaderTypeDef* RxHeader, uint8_t* pRxData);
	canbus_frame_queue txFrameQ;
};

typedef struct _protocol_manager protocolManager_t;
struct _protocol_manager {
	uint8_t (*ReadAll)(protocolManager_t* self, CAN_RxHeaderTypeDef* pRxHeader, uint8_t* pData);
	void (*Add)(protocolManager_t* self, protocol3P_t* pProtocol);

	protocol3P_t* protocols[NUM_PROTOCOLS];
	uint8_t protocolCount;
};


uint8_t construct_protocol(protocol3P_t* self);

#endif /* __PROTOCOL3RDPARTY_H */
