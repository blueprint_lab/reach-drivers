/*
 * scheduler.h
 *
 *  Created on: 08-11-2021
 *      Author: Kyle McLean
 *
 *  TODO:
 *  	- Create tasks as global variables
 *  	- Add task pointers to the task list dynamically?
 */

#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

/* Maximum tasks that an individual scheduler can have */
#define MAX_TASKS 10

/*
 * @brief Type definitions
 */
typedef struct scheduler_t scheduler_t;
typedef struct task_t task_t;

/*
 * @brief Scheduled task structure
 */
struct task_t {
	void 		(*Execute) (void);  		 											 		/* Execute task method 									*/

	uint8_t 	IsSingleShot;																 	/* Task single execution flag							*/
	uint8_t 	Registered;																 		/* Task registration flag								*/
	uint32_t 	LastExecuteTick; 			  											 		/* Last tick the the task was executed					*/
	uint32_t 	Period_ms; 			  		  											 		/* Task execution period [ms]							*/
	uint32_t 	*pFrequency; 																	/* Pointer to task execution frequency [1/s]			*/

	float 		measuredFrequency;																/* Tracks the measured frequency - Debug parameter		*/
};

/*
 * @brief Scheduler structure
 */
struct scheduler_t {
	void 		(*Run) (scheduler_t *self, uint32_t ticks_ms);  								/* Run scheduler 						     			*/
	void 		(*RegisterTask) (scheduler_t *self, void (*Execute)(void), uint32_t *pFrequency, uint32_t Period); /* Register new task to scheduler 	*/
	void 		(*RegisterSingleshot) (scheduler_t *self, void (*Execute)(void), uint32_t Period); /* Register new single-shot task to scheduler 		*/
	void 		(*UnregisterTask) (scheduler_t *self, void (*Execute)(void));  					/* Unregister task  			     		 			*/

	task_t  	tasks[MAX_TASKS];        												 		/* List of tasks to execute		   	         			*/
	uint32_t 	ticks;        												 					/* Elapsed ticks				   	         			*/
};

void scheduler_construct(scheduler_t *self);

#endif /* _SCHEDULER_H_ */

