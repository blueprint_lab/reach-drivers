/*
 * Created on: 26-07-2022
 * 		Author: YC
 * 	Number of parameters: 16
 * 	Sectors assigned: 7 and 8 (both 128kB)
 * 	Save frequency: default 1hr. Minimum 10 min, Maximum 2 hrs. Input by user in 10min increments
 * 		(128 kB * 2) / (4B * 16 parameters) = 4000 writes
 *		(4000 writes * 1 hr frequency) / 24 hrs = 166 days
 */

#include "stm32f4xx_hal.h"

#define DATALOG_PARAM_SIZE 			(sizeof(float))
#define DATALOG_NUM_PARAMS			16
#define DATALOG_FLASH_ADDRESS 		0x8060000
#define DATALOG_PAGE_SIZE 			0x40000  	/* Data logging occupies two 128k sectors (7 and 8) */
#define DATALOG_EMPTY_VALUE 		0xFFFF
#define DATALOG_LAST_ADDRESS 		(DATALOG_FLASH_ADDRESS + DATALOG_PAGE_SIZE - (DATALOG_PARAM_SIZE * DATALOG_NUM_PARAMS))
#define DATALOG_LAST_READ_ADDRESS 	(DATALOG_FLASH_ADDRESS + DATALOG_PAGE_SIZE - DATALOG_PARAM_SIZE)
#define DATALOG_MAX_PARAMS 			64000 	/* Max parameters that can be held in the 2 datalog sectors */
#define DATALOG_MAX_READ 			120
#define DATALOG_LOG_OK 				0
#define DATALOG_LOG_EMPTY_ADDRESS 	1
#define DATALOG_LOG_FULL_SECTOR 	2
#define DATALOG_VALID_DATA			3
#define DATALOG_MIN_SAVE_FREQ		1			/* Save freq in 10 min increments */
#define DATALOG_MAX_SAVE_FREQ		12

#define DALALOG_PARAMS_PER_PKT		12
typedef struct DataLog_t DataLog_t;
struct DataLog_t{
	float data[DATALOG_MAX_READ];
	uint16_t numParams;
	uint16_t sentCounter;
	uint8_t saveFreq;
	int saveCounter;
	uint8_t dataLogFlag;
};


void datalog_Save();
void dataLog_VerifySector();
uint8_t dataLog_VerifyAddress(uint32_t address);
void datalog_EraseSector();
void dataLog_GetParams(DataLog_t* dataLog, uint16_t dataIdx, uint16_t numParams, float* arr);
float dataLog_ReadAddress(uint32_t address);
void dataLog_SetSaveFreq(DataLog_t* dataLog, uint8_t freq);
void dataLog_SetSaveCounter(DataLog_t* dataLog);

