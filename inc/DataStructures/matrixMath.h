/*
 * matrixMath.h
 *
 *  Created on: 22Jun.,2018
 *      Author: TheBlueprintLabs
 */

#ifndef MATRIXMATH_H_
#define MATRIXMATH_H_

#define MAX_MATRIX_SIZE 13
#define MATRIXSIZE(A) (sizeof(A) / sizeof(A[0]))

typedef struct Matrix Matrix;
struct Matrix{
	int numRows;
	int numCols;
	float matrix[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
};

void LUDecomposition(Matrix *MATA, Matrix *P);
void LUInverse(Matrix *MATA, Matrix *P, Matrix *IA);
float LUDeterminant(Matrix *MATA, Matrix *P);
void printMatrix(Matrix *MATA);
void matrixMultiply(Matrix *MATA, Matrix *MATB, Matrix *Result);
void matrixAdd(Matrix *MATA, Matrix *MATB, Matrix *Result);
void matrixMinus(Matrix *MATA, Matrix *MATB, Matrix *Result);
void matrixTranspose(Matrix *MATA, Matrix *MATAT);
float matrixTrace(Matrix *MATA);
void identityMatrix(Matrix *MATA);
void matrixScalarMultiply(float scalar, Matrix *MATA, Matrix *Result);
void vectorCross(Matrix *MATA, Matrix *MATB, Matrix *Result);
float vectorDot(Matrix *A, Matrix *B);
void copyMatrix(Matrix *Dest, Matrix *Src, int destRowStart, int destColStart, int srcRowStart, int srcRowEnd, int srcColStart, int srcColEnd);
float vectorNorm(Matrix *MATA);
float axisAngleFromRotationMatrix(Matrix *R, Matrix *v);
void RotX(Matrix *Rx, float angle);
void RotY(Matrix *Ry, float angle);
void RotZ(Matrix *Rz, float angle);

#endif /* MATRIXMATH_H_ */
