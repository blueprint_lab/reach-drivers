/*
 * runtimelog.h
 *
 *  Created on: 29Apr.,2020
 *      Author: Shaun Barlow
 */

#ifndef RUNTIMELOG_H_
#define RUNTIMELOG_H_

#include <stdint.h>
#include "stm32_device_select.h"
#include "HardwareParameters.h"


#define RUNTIMELOG_FLASH_ADDRESS 0x80A0000
#define RUNTIMELOG_PAGE_SIZE 0x20000  /* Page size = 128KB */
#define RUNTIMELOG_LAST_ADDRESS  (RUNTIMELOG_FLASH_ADDRESS + RUNTIMELOG_PAGE_SIZE - sizeof(uint16_t))

#define RUNTIMELOG_EMPTY_VALUE 0xFFFF
#define RUNTIMELOG_MAX_VALUE 0xFFFE

#define RUNTIME_LOG_OK 				0
#define RUNTIME_LOG_VALID_RECORD 	1
#define RUNTIME_LOG_EMPTY_ADDRESS 	2
#define RUNTIME_LOG_ERROR_ADDRESS 	3
#define RUNTIME_LOG_EMPTY_SECTOR 	4
#define RUNTIME_LOG_FULL_SECTOR 	5
#define RUNTIME_LOG_END_OF_SEQUENCE	6


uint8_t runtimelog_VerifySector();
uint8_t runtimelog_VerifyAddress();
uint8_t runtimelog_EraseSector();
void runtimelog_Increment();
uint8_t runtimelog_Save(uint16_t time);
uint16_t runtimelog_Read();
float runtimelog_GetHours();
uint32_t runtimelog_GetReadAddress();
uint32_t runtimelog_GetWriteAddress();
#endif /* RUNTIMELOG_H_ */
