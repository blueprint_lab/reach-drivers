/*
 * auxMath.h
 *
 *  Created on: 31Jan.,2017
 *      Author: TheBlueprintLabs
 */

#ifndef DATA_STRUCTURES_AUXMATH_H_
#define DATA_STRUCTURES_AUXMATH_H_

#include <stdint.h>
#include <string.h>

#define SET_FLAG(p,n) ((p) |= (1 << (n)))
#define CLR_FLAG(p,n) ((p) &= ~((1) << (n)))
#define CHK_FLAG(var, pos) (((var) & (1 << (pos))) >> (pos))

//#define CHK_FLAG(val, bit_no) (((val) >> (bit_no)) & 1)
#define PI 3.14159265359
#define ONE_DEGREE 0.017453292519943295
/*
 * A collection of maths functions built by one Max Revay.
 * These currently aren't optimized at all and could be improved by using a suitable math library.
 * e.g. LAPACK
 */

// Adjust this to a suitable accuracy
typedef float real;

//uint8_t CHK_FLAG(uint8_t val,uint8_t pos);

float wrapAngle(real x);
float wrapTo2Pi(float angle);
float wrapAngleDegrees(real x);
int64_t wrapTo24BitInt(int64_t x);
float smallestAngleBetween(real x, real y);
float signedSmallestAngleBetween(real x, real y);
float lpFilter(real prevVal, real newMeas, real tau);
int64_t lpFilterInt(int64_t prevVal, int64_t newMeas, float tau);
float hpFilter(float prevVal, float newMeas, float lastMeas,float tau);
void integrateVec(float* currVec, float* delta, float* newVec, float dt, size_t length);
float signf(float value);
uint8_t cap(float* value, float max, float min);
float deadbandMapping(float input, float deadband, float deadbandGradient);
float fLimitDelta(float prev, float newMeas, float maxDelta);
void computeSpline3(float spline[], float p0,float v0,float p1,float v1,float dt);
float evaluateSpline3Position(float spline[],float t);
float evaluateSpline3Velocity(float spline[],float t);
void computeSpline5(float spline[], float p0,float v0,float a0,float p1,float v1,float a1,float dt);
float evaluateSpline5Position(float spline[],float t);
float evaluateSpline5Velocity(float spline[],float t);

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _b : _a; })

#endif /* DATA_STRUCTURES_AUXMATH_H_ */
