/*
 * packetStructs.h
 *
 *  Created on: MAR 15, 2021
 *      Author: Kyle McLean
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PACKET_STRUCTS_H
#define __PACKET_STRUCTS_H

#include <stdint.h>

/* Packet structure definitions */
#define DATA_BYTES_PER_PACKET 64

/* Struct type defines */
typedef struct packet_t packet_t; // Single packet
typedef struct packet_node packet_node; // Packet node -> has a packet


/* Packet object - Used for all communication types*/
struct packet_t{
	uint8_t length;
	uint8_t address;
	uint16_t code;
	uint16_t crc;
	uint8_t data[DATA_BYTES_PER_PACKET];
	uint8_t transmitData[DATA_BYTES_PER_PACKET];
	uint8_t protocol;
	uint8_t option;
	uint8_t useOption;
	uint16_t receiveRegister; // used in receive queue to track which frames have been received
	uint8_t totalFrames; // total number of CAN frames that complete this packet
};

/* Packet node struct */
struct packet_node {
	packet_t packet;
//	struct packet_node* next;
	uint8_t timeToErase;
};
#endif /* __CANBUS_STRUCTS_H */
