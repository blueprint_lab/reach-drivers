/*
 * controller2D.h
 *
 *  Created on: 25Jan.,2017
 *      Author: TheBlueprintLabs
 */

#ifndef DATA_STRUCTURES_CONTROLLER2D_H_
#define DATA_STRUCTURES_CONTROLLER2D_H_

#include <math.h>
#include "../Inc/stm32_device_select.h"

#define KP_INIT 0.0
#define KD_INIT 0.0
#define KI_INIT 0.0

typedef enum controllerType{
	CONTROLTYPE_POSITION,
	CONTROLTYPE_VELOCITY,
	CONTROLTYPE_OPENLOOP,
}controlMode_t;

#define IntegralErrorLim 1.0

typedef struct controller_t controller_t;
struct controller_t{

	void  (*setpoint)(controller_t* self, float position, float VelocitySetPoint);
	void  (*setGains)(controller_t* self, float Kp, float Kd, float Ki,float Kvp, float Kvd, float Kvf);
	float (*calculateControl)(controller_t* self);
	float (*calculateVelocityControl)(controller_t* self);
	float (*calculateOpenloopControl)(controller_t* self);
	float (*calculateCurrentControl)(controller_t* self);
	float (*updateStateEst)(controller_t* self, float measurement);

	void  (*setKp)(controller_t* self, float Kp);
	void  (*setKd)(controller_t* self, float Kd);
	void  (*setKi)(controller_t* self, float Ki);

	float (*readKp)(controller_t* self);
	float (*readKi)(controller_t* self);
	float (*readKd)(controller_t* self);

	float (*readPositionEst)(controller_t* self);
	float (*readVelocityEst)(controller_t* self);

	void (*setMaxPosition)(controller_t* self, float val);
	void (*setMinPosition)(controller_t* self, float val);
	void (*setMaxVelocity)(controller_t* self, float val);
	void (*setMinVelocity)(controller_t* self, float val);
	void (*setMaxAcceleration)(controller_t* self, float val);
	void (*setMinAcceleration)(controller_t* self, float val);

	// Gains
	float Kp;
	float Ki;
	float Kd;
	float Kvp;
	float Kvi;
	float Kvf;
	float Kcp;
	float Kci;
	float Kcv;

	//Filter Coefficients
	float tauP;
	float tauD;

	// Position Estimate
	float Position;
	float Velocity;
	float Current;
	float IntegralError;
	float velocityIntegralError;
	float currentIntegralError;
	float PositionOffset;

	// setPoints
	float positionSetPoint;
	float velocitySetPoint;
	float openloopsetpoint;
	float currentSetpoint;
	float output;

	float timestep;

	float acc_max;
	float acc_min;
	float voltage_max;
	float voltage_min;
	float vel_max;
	float vel_min;
	float current_max;
	float maxPosition;
	float minPosition;

	uint16_t controllerStatus;
};

//control loops
void  controller_setGains(controller_t* self, float Kp, float Kd, float Ki,float Kvp, float Kvd, float Kvf);
void  positionController_setpoint(controller_t* self, float PosSetPoint, float velSetPoint);
float controller_Position(controller_t* self);
float controller_Openloop(controller_t* self);
float controller_Velocity(controller_t* self);
float positionController_updateStateEst(controller_t* self, float measurement);

// State measurement
float positionController_readKp(controller_t* self);
float positionController_readKi(controller_t* self);
float positionController_readKd(controller_t* self);
float positionController_readPositionEst(controller_t* self);
float positionController_readVelocityEst(controller_t* self);

// Configuration
void  positionController_setKp(controller_t* self, float Kp);
void  positionController_setKd(controller_t* self, float Kd);
void  positionController_setKi(controller_t* self, float Ki);

void positionController_setMaxPosition(controller_t* self, float val);
void positionController_setMinPosition(controller_t* self, float val);
void positionController_setMaxVelocity(controller_t* self, float val);
void positionController_setMinVelocity(controller_t* self, float val);
void positionController_setMaxAcceleration(controller_t* self, float val);
void positionController_setMinAcceleration(controller_t* self, float val);
void positionController_setPositionOffset(controller_t* self, float val);

uint8_t constructPositionController(controller_t* self,controlMode_t controlType, float timestep, float Kp, float Kd, float Ki, float tauP, float tauD);

float correctPositionSetPoint(float tmpPositionSetPoint, float upperLim, float lowerLim, float tolerance);

#endif /* DATA_STRUCTURES_CONTROLLER2D_H_ */
