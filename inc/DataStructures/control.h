/*
 * controller2D.h
 *
 *  Created on: 25Jan.,2017
 *      Author: TheBlueprintLabs
 */

#ifndef DATA_STRUCTURES_CONTROL_H_
#define DATA_STRUCTURES_CONTROL_H_

#include <math.h>
#include "auxMath.h"
#include "../inc/stm32_device_select.h"

typedef enum controlType{
	CIRCULAR,
	LINEAR,
}controlType_t;

typedef enum controlStatus{
	NORMAL,
	MAX_OUTPUT,
	MIN_OUTPUT,
	MAX_SETPOINT,
	MIN_SETPOINT,
	ZERO_ERROR,
}controlStatus_t;

typedef struct control_t control_t;
struct control_t{
	float (*run)(control_t* self);
	void (*setLimits)(control_t* self, float maxOutput, float minOutput, float maxSetpoint, float minSetpoint, float IntegralError);
	void (*setGains)(control_t* self, float Kp, float Kd, float Ki, float Kf, float Ko);
	void (*reset)(control_t * self);
	void (*setType)(control_t* self,controlType_t controlType);

	// Flags
	uint8_t ignoreSetpointLimits;

	// Gains
	float Kp;
	float Ki;
	float Kd;
	float Kf;
	float Ko;  // Output scale

	// Online parameters
	float autoKp;
	float autoMi;

	//Limits
	float maxIntegralError;
	float minOutput;
	float maxOutput;
	float minSetpoint;
	float maxSetpoint;
	float minSetpointFactory;
	float maxSetpointFactory;
	float previousSetpoint;
	float setpointLimitScale;

	//Filter Coefficients
	float tauP;
	float tauD;

	// state
	float measurement;
	float setpoint;
	float error;
	float integralError;
	float errorTolerence;
	float output;

	float timestep;


	controlType_t controlType;
	uint8_t controlStatus;
};

//control loops
float control_runLinear(control_t* self);
float control_runCircular(control_t* self);
void control_setLimits(control_t* self, float maxOutput, float minOutput, float maxSetpoint, float minSetpoint, float IntegralError);
void control_setGains(control_t* self, float Kp, float Kd, float Ki, float Kf, float Ko);
void control_reset(control_t* self);
uint8_t constructControl(control_t* self, float timestep);
void control_setType(control_t* self,controlType_t controlType);
void capCircularSetpoint(float* positionSetPoint, float upperLimit, float lowerLimit, float tolerance);
float capCircularError(float position,float setpoint, float upperLimit, float lowerLimit);
void control_setFactorySetpointLimits(control_t* self,
float maxSetpointFactory, float minSetpointFactory);

#endif /* DATA_STRUCTURES_CONTROLLER2D_H_ */
