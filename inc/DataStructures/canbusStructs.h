/*
 * canbusStructs.h
 *
 *  Created on: MAR 15, 2021
 *      Author: Kyle McLean
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CANBUS_STRUCTS_H
#define __CANBUS_STRUCTS_H

/* Include lower level structures */
#if defined(TEST_CANPACKETQ) || defined(TEST_CANBUS)
/***************** Test only *******************/
#include "../DataStructures/packetStructs.h"
#include "../../mock/mockCanbus.h"
#else
/***************** Build only ******************/
#include "DataStructures/packetStructs.h"

#endif

#ifdef LINUX
/** @defgroup CAN_identifier_type CAN Identifier Type
  * @{
  */
#define CAN_ID_STD                  (0x00000000U)  /*!< Standard Id */
#define CAN_ID_EXT                  (0x00000004U)  /*!< Extended Id */
/**
  * @}
  */
 /** @defgroup CAN_remote_transmission_request CAN Remote Transmission Request
  * @{
  */
#define CAN_RTR_DATA                (0x00000000U)  /*!< Data frame   */
#define CAN_RTR_REMOTE              (0x00000002U)  /*!< Remote frame */
/**
  * @}
  */

typedef enum
{
  DISABLE = 0U,
  ENABLE = !DISABLE
} FunctionalState;

/** Mock CAN Rx struct
  * @brief  CAN Rx message header structure definition
  */
typedef struct
{
  uint32_t StdId;    /*!< Specifies the standard identifier.
                          This parameter must be a number between Min_Data = 0 and Max_Data = 0x7FF. */

  uint32_t ExtId;    /*!< Specifies the extended identifier.
                          This parameter must be a number between Min_Data = 0 and Max_Data = 0x1FFFFFFF. */

  uint32_t IDE;      /*!< Specifies the type of identifier for the message that will be transmitted.
                          This parameter can be a value of @ref CAN_identifier_type */

  uint32_t RTR;      /*!< Specifies the type of frame for the message that will be transmitted.
                          This parameter can be a value of @ref CAN_remote_transmission_request */

  uint32_t DLC;      /*!< Specifies the length of the frame that will be transmitted.
                          This parameter must be a number between Min_Data = 0 and Max_Data = 8. */

  uint32_t Timestamp; /*!< Specifies the timestamp counter value captured on start of frame reception.
                          @note: Time Triggered Communication Mode must be enabled.
                          This parameter must be a number between Min_Data = 0 and Max_Data = 0xFFFF. */

  uint32_t FilterMatchIndex; /*!< Specifies the index of matching acceptance filter element.
                          This parameter must be a number between Min_Data = 0 and Max_Data = 0xFF. */

} CAN_RxHeaderTypeDef;

/** Mock CAN Tx struct
  * @brief  CAN Tx message header structure definition
  */
typedef struct
{
  uint32_t StdId;    /*!< Specifies the standard identifier.
                          This parameter must be a number between Min_Data = 0 and Max_Data = 0x7FF. */

  uint32_t ExtId;    /*!< Specifies the extended identifier.
                          This parameter must be a number between Min_Data = 0 and Max_Data = 0x1FFFFFFF. */

  uint32_t IDE;      /*!< Specifies the type of identifier for the message that will be transmitted.
                          This parameter can be a value of @ref CAN_identifier_type */

  uint32_t RTR;      /*!< Specifies the type of frame for the message that will be transmitted.
                          This parameter can be a value of @ref CAN_remote_transmission_request */

  uint32_t DLC;      /*!< Specifies the length of the frame that will be transmitted.
                          This parameter must be a number between Min_Data = 0 and Max_Data = 8. */

  FunctionalState TransmitGlobalTime; /*!< Specifies whether the timestamp counter value captured on start
                          of frame transmission, is sent in DATA6 and DATA7 replacing pData[6] and pData[7].
                          @note: Time Triggered Communication Mode must be enabled.
                          @note: DLC must be programmed as 8 bytes, in order these 2 bytes are sent.
                          This parameter can be set to ENABLE or DISABLE. */

} CAN_TxHeaderTypeDef;
#endif

typedef enum canbus_tx_state {
	CANBUS_TX_OK,
	CANBUS_TX_BUSY,
	CANBUS_TX_ERROR,
} canbus_tx_state_t;

/* Link object - contains all canbus specific data */
typedef struct canbus_comms_link_ canbus_comms_link; // Canbus communications object

/* Packets */
typedef struct canbus_packet_queue canbus_packet_queue; // Canbus packet queue -> has a queue of packets

/* Frames */
typedef struct canbus_frame_t canbus_frame_t; // Single canbus frame
typedef struct frame_node frame_node; // Frame node -> has a canbus frame
typedef struct canbus_frame_queue canbus_frame_queue; // Frame queue -> has a queue of canbus frames
typedef struct canPacketQ canPacketQ_t; // Packet Q -> has a queue of canbus packet nodes

/* Canbus packet queue struct */
struct canbus_packet_queue{
	uint16_t count;
	packet_node* front;
	packet_node* rear;
	uint8_t locked;
};

/* Canbus frame */
struct canbus_frame_t {
	CAN_TxHeaderTypeDef TxHeader;
	uint8_t data[8];
};

/* canbus frame node */
struct frame_node {
	canbus_frame_t frame;
	struct frame_node* next;
};

/* canbus frame queue */
struct canbus_frame_queue {
	uint8_t count;
	frame_node* front;
	frame_node* rear;
	uint8_t locked;
};

struct canPacketQ{
	void (*Deconstruct)(canPacketQ_t* self);
	void (*Reset)(canPacketQ_t* self);
	uint8_t (*Push)(canPacketQ_t* self, packet_node* elem);
	uint8_t (*EraseFrontNode)(canPacketQ_t* self);
	packet_node* (*getNextNode)(canPacketQ_t* self, packet_node* current_elem);

	packet_node* origin;
	packet_node* front;
	packet_node* back;

	uint16_t numel;
	uint16_t maxel;
};


#endif /* __CANBUS_STRUCTS_H */
