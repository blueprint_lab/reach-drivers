#ifndef CIRCULAR_QUEUE_HEADER_INC
#define CIRCULAR_QUEUE_HEADER_INC

#ifndef TEST_CIRC_Q
#ifndef TEST_COMSPROCESS
// #include "stm32_device_select.h"
#else
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#endif
#else
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#endif
#include <stdint.h>
#include <string.h>


//#include "Hardware/ICMU.h"


//typedef struct{
//	uint8_t messageHead[4];
//	uint8_t* size;
//}queueEl;

typedef enum {
    Q_UNBLOCKED,
    Q_BLOCKED,
} queueBlock_t;

typedef struct circularQueue circularQueue;
typedef uint8_t queueEl;

struct circularQueue
{
	uint8_t	(*push)(circularQueue* self, queueEl elem);
    uint16_t    (*pushArray)(circularQueue* self, queueEl * data_in, uint16_t length);
    uint16_t (*popArray)(circularQueue * self, uint8_t* buffer, uint16_t numElements);
	queueEl (*pop)(circularQueue* self);
	void (*deconstruct)(circularQueue* self);
	void (*Reset)(circularQueue* self);
	queueEl* (*getNext)(circularQueue* self, queueEl* elem);
	int32_t (*Find)(circularQueue* self, queueEl target);
	uint16_t numel;
	uint16_t maxSize;
	queueBlock_t blockState;

	//.........Private
	queueEl* origin;
	queueEl* front;
	queueEl* back;
};

void circularQueueConstruct(circularQueue * self, size_t size);
void circularQueueDeconstruct(circularQueue * self);
void circularQueueReset(circularQueue * self);
uint8_t circularQueuePush(circularQueue* self, queueEl elem);
queueEl circularQueuePop(circularQueue* self);
uint16_t circularQueuePopArray(circularQueue * self, uint8_t* buffer, uint16_t numElements);
void circularQueuePushArray(circularQueue* self, queueEl* data_in, unsigned int length);
uint16_t circularQueuePushArray2(circularQueue* self, queueEl* buffer, uint16_t numElementsToCopy);
int32_t circularQueueFind(circularQueue* self, queueEl target);



//#define circularBufferSize 16


#define ICMU_ACTIVATE 0xB0
#define ICMU_SDAD_TRANS 0xA6
#define ICMU_SDAD_STATUS 0xF5
#define ICMU_READ_REG 0x97
#define ICMU_WRITE_REG 0xD2
#define ICMU_REG_STATUS_DATA 0xAD



#endif // !CIRCULAR_QUEUE_HEADER_INC

