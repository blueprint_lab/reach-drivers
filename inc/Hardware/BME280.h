/*
 * BME280.h
 *
 *  Created on: 23 Jun 2020
 *      Author: Shaun Barlow
 *
 *
 *
 */

#ifndef BME280_BPL_H_
#define BME280_BPL_H_

#include "stm32_device_select.h"
#include "Hardware/bme280_defs.h"
#include "Hardware/bme280_api.h"

typedef enum commState_t {
	CS_READY,
	CS_TX_IN_PROGRESS,
	CS_RX_IN_PROGRESS,
	CS_DATA_PENDING,
} commState_t;

typedef struct BME280_t BME280_t;
struct BME280_t {
	uint8_t (*Init)(BME280_t* self);
	void (*Destroy)(BME280_t * self);
	uint8_t (*Read)(BME280_t* self);
	uint8_t (*ReadDMA)(BME280_t* self);
	uint8_t (*TxCpltCallback)(BME280_t* self, I2C_HandleTypeDef *hi2c);
	uint8_t (*RxCpltCallback)(BME280_t* self, I2C_HandleTypeDef *hi2c);
	float (*GetPressure)(BME280_t* self);
	float (*GetTemperature)(BME280_t* self);
	float (*GetHumidity)(BME280_t* self);

	struct bme280_dev device;
	I2C_HandleTypeDef* phi2c;
	float pressure, temperature, humidity;
	struct bme280_data comp_data;
	uint8_t DMA_buffer[BME280_P_T_H_DATA_LEN];
	commState_t commState;
	float* pPressureAlias;
	float* pTemperatureAlias;
	float* pHumidityAlias;
	uint32_t nextReadingTime, readingPeriod;
};

void BME280_Construct(BME280_t* self, I2C_HandleTypeDef* phi2c);
uint8_t BME280_Init(BME280_t* self);
uint8_t BME280_Read(BME280_t* self);
uint8_t BME280_ReadDMA(BME280_t* self);
uint8_t BME280_DMAReceiveData(BME280_t* self, I2C_HandleTypeDef *hi2c);
uint8_t BME280_DMAProcessData(BME280_t* self, I2C_HandleTypeDef *hi2c);
void BME280_SetAliases(BME280_t* self, float* pTemperatureAlias, float* pPressureAlias, float* pHumidityAlias);
float BME280_GetPressure(BME280_t* self);
float BME280_GetTemperature(BME280_t* self);
float BME280_GetHumidity(BME280_t* self);
uint8_t BME280_NOP(BME280_t * self);
void BME280_Destroy(BME280_t* self);
#endif /* BME280_BPL_H_ */
