/*
 * DRV8313_Brushless.h
 *
 *  Created on: 22 Nov 2016
 *      Author: Blueprint Lab
 */

#ifndef DRV8313_BRUSHLESS_H_
#define DRV8313_BRUSHLESS_H_


#include <stdint.h>
#include <math.h>

#include "stm32_device_select.h"
#include "Hardware/motorDriver.h"

typedef struct DRV8313_Brushless_t DRV8313_Brushless_t;
struct DRV8313_Brushless_t{

	void (*Enable)(DRV8313_Brushless_t* self);
	void (*Init)(DRV8313_Brushless_t* self);
	void (*Disable)(DRV8313_Brushless_t* self);
	uint8_t (*Check)(DRV8313_Brushless_t* self);

	void (*SetOutputBrushed)(DRV8313_Brushless_t* self, float output);
	void (*SetOutputFOC)(DRV8313_Brushless_t* self, float v_d,float v_q,float motorDirection);
	void (*Calibrate)(DRV8313_Brushless_t* self,float amplitude,float mechanicalPosition);
	void (*StallDrive)(DRV8313_Brushless_t *self,float amplitude,float mechanicalPosition,float motorDirection);
	void (*CalibrateTest)(DRV8313_Brushless_t *self,float amplitude,float motorDirection);

	uint16_t resetPin;
	GPIO_TypeDef* resetPort;
	uint16_t sleepPin;
	GPIO_TypeDef* sleepPort;
	uint16_t faultPin;
	GPIO_TypeDef* faultPort;


	TIM_OC_InitTypeDef PWMAConfig;
	TIM_HandleTypeDef PWMATimer;
	uint32_t PWMAChannel;
	uint16_t enableA_pin;
	GPIO_TypeDef* enableA_port;

	TIM_OC_InitTypeDef PWMBConfig;
	TIM_HandleTypeDef PWMBTimer;
	uint32_t PWMBChannel;
	uint16_t enableB_pin;
	GPIO_TypeDef* enableB_port;

	TIM_OC_InitTypeDef PWMCConfig;
	TIM_HandleTypeDef PWMCTimer;
	uint32_t PWMCChannel;
	uint16_t enableC_pin;
	GPIO_TypeDef* enableC_port;

	uint8_t mode;
	uint8_t status;

	float outputA;
	float outputB;
	float outputC;

	float positions[2];

	float offsetAngle;
	uint16_t index;

	volatile float electricalAngle;
	uint8_t numPolePairs;

};


//Function Definitions:
void DRV8313_Brushless_Construct(DRV8313_Brushless_t *self,
		TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel, uint16_t enableA_pin, GPIO_TypeDef* enableA_port, TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel,uint16_t enableB_pin, GPIO_TypeDef* enableB_port,TIM_HandleTypeDef PWMCTimer, uint32_t PWMCChannel, uint16_t enableC_pin, GPIO_TypeDef* enableC_port,
		uint16_t resetPin, GPIO_TypeDef* resetPort, uint16_t sleepPin, GPIO_TypeDef* sleepPort, uint16_t faultPin, GPIO_TypeDef* faultPort);
void DRV8313_Brushless_Init(DRV8313_Brushless_t *self);
uint8_t DRV8313_Brushless_Check(DRV8313_Brushless_t *self);
void DRV8313_Brushless_Enable(DRV8313_Brushless_t *self);
void DRV8313_Brushless_Disable(DRV8313_Brushless_t *self);
void DRV8313_FindZeroAngle(DRV8313_Brushless_t *self,float amplitude,float mechanicalPosition);
void DRV8313_CalibrateEncoder(DRV8313_Brushless_t *self,float amplitude,float mechanicalPosition,float motorDirection);
void DRV8313_SetOutput_Brushed(DRV8313_Brushless_t *self,float output);
void DRV8313_SetOutput_FOC(DRV8313_Brushless_t *self,float v_d, float v_q, float motorDirection);
void DRV8313_CalibrateTest(DRV8313_Brushless_t *self,float amplitude, float motorDirection);



#endif /* DRV8313_BRUSHLESS_H_ */
