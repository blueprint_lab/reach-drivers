/*
 * SI7210.h
 *
 * SI7210 Sensor driver
 *
 *  Created on: 08-11-2021
 *      Author: Kyle McLean
 */

#ifndef _SI7210_H_
#define _SI7210_H_

#include <stdint.h>

#include "stm32_device_select.h"
#include "Communication/I2C.h"

/* I2C slave addresses */
#define SI7210_ADDRESS_0    (0x30U)
#define SI7210_ADDRESS_1    (0x31U)
#define SI7210_ADDRESS_2    (0x32U)
#define SI7210_ADDRESS_3    (0x33U)

/* Vdd of SI7210. This is used in device's self-test calculations */
#define SI7210_VDD          (3.3f)

/*
 * @brief status result code.
 */
typedef enum {
    SI7210_OK,                  				/* Sucess                               */
    SI7210_E_NULL_PTR,          				/* Null pointer found                   */
    SI7210_E_INVALID_ARG,       				/* Invalid function arguments           */
    SI7210_E_IO,                				/* Device IO error                      */
    SI7210_E_TIMEOUT,           				/* Device timeout                       */
    SI7210_E_DEV_NOT_FOUND,     				/* Device not found                     */
    SI7210_E_SELF_TEST_FAIL,    				/* Device self-test failed              */
    SI7210_E_SETTINGS,          				/* Unable to set device settings        */
    SI7210_W_THRESHOLD_BOUNDS,   				/* Threshold parameter outside bounds   */
} si7210_status_t;


/*
 * @brief state of device.
 */
typedef enum {
    SI7210_IDLE,                  				/* Ready to take measurement            */
	SI7210_MEASURE_FIELD,                  		/* Takeing field measurement            */
	SI7210_CALCULATE_FIELD,                  	/* Calculating field measurement        */
	SI7210_MEASURE_TEMP,                  		/* Takeing temperature measurement      */
	SI7210_CALCULATE_TEMP,                  	/* Calculating temperature measurement  */

} si7210_state_t;

typedef enum si7210_CalibrationState_t {
	SI7210_AUTO_LOWER_LIMIT,
	SI7210_CAPTURE_LOWER_LIM,
	SI7210_AUTO_UPPER_LIMIT,
	SI7210_CAPTURE_UPPER_LIM,
	SI7210_CALIBRATION_CPLT
} si7210_CalibrationState_t;

/*
 * @brief Measurement scale of reading.
 */
typedef enum {
    SI7210_20mT,    							/* 20mT measurement range 				*/
    SI7210_200mT    							/* 200mT measurement range 				*/
} si7210_range_t;

/*
 * @brief Magnet temperature compensation.
 */
typedef enum {
    SI7210_COMPENSATION_NONE,     				/* No magnet temperature compensation          		*/
    SI7210_COMPENSATION_TEMP_NEO, 				/* Magnet temperature compensation (Neodymium) 		*/
    SI7210_COMPENSATION_TEMP_CER, 				/* Magnet temperature compensation (Ceramic)   		*/
} si7210_compensation_t;

/*
 * @brief State of Si7210's output pin when field is above threshold setting.
 */
typedef enum {
    SI7210_OUTPUT_PIN_LOW,  					/* Output pin is LOW when field is above threshold.  */
    SI7210_OUTPUT_PIN_HIGH  					/* Output pin is HIGH when field is above threshold. */
} si7210_output_pin_t;

/*
 * @brief Mode of Si7210. Defines how the output should be interperated.
 */
typedef enum {
    SI7210_DISABLED,  							/* SI7210 is not used.  							 */
	SI7210_STARTUP,  							/* SI7210 is used only on startup.					 */
	SI7210_CONTINUOUS,  						/* SI7210 is used continuously.						 */
	SI7210_SECTOR,  							/* SI7210 is used to determine operating sector.	 */
	SI7210_SWITCHING,  							/* SI7210 is used in conjunction with encoder.		 */
} si7210_mode_t;


/*
 * @brief Type definitions
 */
typedef struct si7210_t si7210_t;
typedef si7210_status_t (*si7210_com_fptr_t)(I2C_HandleTypeDef* phi2c, uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len);
typedef si7210_status_t (*si7210_TxRxCplt_fptr_t)(si7210_t *self, I2C_HandleTypeDef *hi2c, I2C_txRxDirection_t transferDirection);
typedef si7210_status_t (*si7210_interface_fptr_t)(si7210_t *self);

/*
 * @brief calibration data structure.
 */
struct si7210_calib_data {
    int8_t temperature_offset; 					/* Temperature sensor offset adjustment */
    int8_t temperature_gain;   					/* Temperature sensor gain adjustment */
};

/*
 * @brief settings data structure.
 */
struct si7210_settings {
    si7210_range_t         range;        		/* Measurement range of magentic reading             */
    si7210_compensation_t  compensation; 		/* Temperature compensation of magentic reading      */
    si7210_output_pin_t    output_pin;   		/* State of output pin when field is above threshold */
};

/*
 * @brief Si7210 sensor structure
 */

struct si7210_t {
	si7210_status_t (*Init) (si7210_t *self);

	uint8_t dev_id;                      		/* Device ID                               */
	uint8_t enabled;							/* Enabled flag							   */
	uint8_t initialized;						/* Init flag							   */
	uint8_t calibrationState;					/* Current state of the calibration seq	   */

	si7210_status_t status;              		/* Device status                           */
	si7210_state_t state;              			/* Device measurment state                 */

	float mode;              					/* Usage mode							   */

	float max_flux;              				/* Flux reading at min distance            */
	float min_flux;              				/* Flux reading at max distance            */

	float max_distance;              			/* Minimum expected distance 	           */
	float min_distance;              			/* Maximum expected distance               */

	float offset;              					/* Offset between sensor and magnet face   */

	float range;								/* Range measurment						   */
	float prevRange;							/* Previous range measurment			   */
	float velocity;								/* Velocity estimate					   */
	float deltaSqrtFlux; 						/* Rate of change in range measurement	   */
	float rangeAlpha;							/* Range measurment low pass filter coeff  */
	float temperature;							/* Temperature measurment				   */
	float flux;									/* Flux measurment						   */

	float limit;								/* Position limit while using hall sensor  */

	double conversionFreq;						/* Frequency of range conversion		   */

    I2C_HandleTypeDef* phi2c;			 		/* I2C structure                           */

    si7210_interface_fptr_t calculateRange;		/* Calcualte the range from flux reading   */
    si7210_interface_fptr_t calculateTemp;		/* Calcualte the tempoerature			   */

    si7210_com_fptr_t read;              		/* I2C read function pointer               */
    si7210_com_fptr_t write;             		/* I2C write function pointer              */

    si7210_com_fptr_t readDMA;           		/* Non-blocking I2C read function pointer  */
    si7210_com_fptr_t writeDMA;          		/* Non-blocking I2C write function pointer */

    si7210_TxRxCplt_fptr_t RxTxCpltCallback; 	/* DMA Rx/Tx complete callback 			   */

    struct si7210_settings settings;     		/* Si7210 device settings 				   */
    struct si7210_calib_data calib_data; 		/* Calibration data                        */

};

si7210_status_t si7210_construct(si7210_t *self, I2C_HandleTypeDef *phi2c);
si7210_status_t si7210_init(si7210_t *self);
si7210_status_t si7210_deinit(si7210_t *self);
si7210_status_t si7210_set_sensor_settings(si7210_t *self);
si7210_status_t __attribute__((optimize("O0")))si7210_calculate_range(si7210_t *self);
si7210_status_t si7210_calculate_temperature(si7210_t *self);
si7210_status_t si7210_start_periodic_measurement(si7210_t *self);
si7210_status_t si7210_read_reg(si7210_t *self, uint8_t reg, uint8_t *val);
si7210_status_t si7210_write_reg(si7210_t *self, uint8_t reg, uint8_t mask, uint8_t val);
si7210_status_t __attribute__((optimize("O0")))si7210_check(si7210_t *self);
si7210_status_t si7210_self_test(si7210_t *self);
si7210_status_t si7210_sleep(si7210_t *self);
si7210_status_t si7210_wakeup(si7210_t *self);

void si7210_irq_handler(si7210_t *self);

#endif /* SI7210_H_ */

