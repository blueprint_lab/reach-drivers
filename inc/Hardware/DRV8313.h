/*H**********************************************************************
* FILENAME :        DRV8313.h             DESIGN REF: RS1
*
* DESCRIPTION :
*       Driver for DRV8313 Motor Controller
*
* PUBLIC FUNCTIONS :
*
*
*
* NOTES :
*       These functions are a part of the FM suite;
*       See IMS FM0121 for detailed description.
*
*       Copyright TheBlueprintLabs
*
* AUTHOR :   Paul Phillips        START DATE :   16/8/16
*
* CHANGES :
*
* REF NO  VERSION DATE    WHO     DETAIL
*
*
*H*/

/*
 * 	void (*Initialise)();
	void (*Enable)();
	void (*Disable)();
	int (*GetStatus)();
	void (*SetOutput)(float motorTorque);
	void (*SetDirection)(uint8_t motorDirection);
 */
#ifndef _DRV8313_H
#define _DRV8313_H

#include <stdint.h>
#include <math.h>

#include "stm32_device_select.h"
#include "DataStructures/auxMath.h"
#include "Hardware/motorDriver.h"

typedef struct DRV8313_t DRV8313_t;
struct DRV8313_t{
	void (*Enable)(DRV8313_t* self);
	void (*Init)(DRV8313_t* self);
	void (*Disable)(DRV8313_t* self);
	uint8_t (*Check)(DRV8313_t* self);
	void (*SetOutput)(DRV8313_t* self, float Torque);

	uint16_t enablePin;
	GPIO_TypeDef* enablePort;
    uint16_t IN1_pin;
    GPIO_TypeDef* IN1_port;
    uint16_t IN2_pin;
    GPIO_TypeDef* IN2_port;

	uint16_t resetPin;
	GPIO_TypeDef* resetPort;
	uint16_t sleepPin;
	GPIO_TypeDef* sleepPort;
	uint16_t faultPin;
	GPIO_TypeDef* faultPort;


	TIM_OC_InitTypeDef PWMAConfig;
	TIM_HandleTypeDef PWMATimer;
	uint32_t PWMAChannel;

	TIM_OC_InitTypeDef PWMBConfig;
	TIM_HandleTypeDef PWMBTimer;
	uint32_t PWMBChannel;

	TIM_OC_InitTypeDef PWMCConfig;
	TIM_HandleTypeDef PWMCTimer;
	uint32_t PWMCChannel;

	uint8_t mode;
	uint8_t status;
	float torquePrev;
	uint8_t invert;

	uint8_t numPolePairs;

};



void DRV8313_Construct(DRV8313_t* self,
		TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel, TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel,
		uint16_t enablePin, GPIO_TypeDef* enablePort, uint16_t resetPin, GPIO_TypeDef* resetPort, uint16_t sleepPin,
		GPIO_TypeDef* sleepPort, uint16_t faultPin, GPIO_TypeDef* faultPort, uint8_t invert);
void DRV8313_ConstructSlowDecay(DRV8313_t* self,
		TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel, TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel,
		uint16_t enablePin, GPIO_TypeDef* enablePort, uint16_t resetPin, GPIO_TypeDef* resetPort, uint16_t sleepPin, GPIO_TypeDef* sleepPort, uint16_t faultPin, GPIO_TypeDef* faultPort );
void DRV8313_NB_Construct(DRV8313_t* self,
        TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel, TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel,
        uint16_t IN1_pin, GPIO_TypeDef* IN1_port,uint16_t IN2_pin, GPIO_TypeDef* IN2_port, uint16_t resetPin, GPIO_TypeDef* resetPort, uint16_t sleepPin, GPIO_TypeDef* sleepPort,uint16_t faultPin, GPIO_TypeDef* faultPort );


//Function Definitions:
void DRV8313_Enable(DRV8313_t *DRV8313Context);
void DRV8313_Init(DRV8313_t *DRV8313Context);
void DRV8313_Disable(DRV8313_t *DRV8313Context);
uint8_t DRV8313_Check(DRV8313_t *DRV8313Context);
void DRV8313_Init();
void DRV8313_SetOutput(DRV8313_t *DRV8313Context, float torque);

void DRV8313_SetOutput_AsyncFastDecay(DRV8313_t *self, float torque);
void DRV8313_SetOutput_SlowDecay(DRV8313_t *self, float torque);

void DRV8313_NB_SetOutput(DRV8313_t *self, float torque);
void DRV8313_NB_Disable(DRV8313_t *self);
void DRV8313_NB_Enable(DRV8313_t *self);
void DRV8313_NB_Init(DRV8313_t *self);


#endif //_DRV8313_H
