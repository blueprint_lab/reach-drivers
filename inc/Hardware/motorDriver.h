/*
 * motorDriver.h
 *
 *  Created on: 28-01-2022
 *      Author: Kyle McLean
 */

#ifndef MOTOR_DRIVER_H_
#define MOTOR_DRIVER_H_

#define SQRT3 (sqrtf(3))
#define ONEONSQRT3 (1.0/sqrtf(3))
#define ONEON2SQRT3 (0.5/sqrtf(3))

#define OUTPUT_RANGE  1.0

typedef enum motorDriver_Mode{
	DISABLED,
	ENABLED,
}motorDriver_Mode_t;

typedef enum motorDriver_Status{
	GOOD,
	FAULT,
	OTW,
}motorDriver_Status_t;

#endif /* MOTOR_DRIVER_H_ */
