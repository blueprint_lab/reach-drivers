/*
 * AMC25A8M.h
 *
 *  Created on: Aug 25, 2016
 *      Author: p.phillips
 */

#ifndef AMC25A8M_H_
#define AMC25A8M_H_

//#include "../../Utilities/Hardware_Modules/HardwareModules.h"
#include "stm32_device_select.h"

typedef struct AMC25A8M_t AMC25A8M_t;
struct  AMC25A8M_t{

	// public interface
	void (*setOutput)(AMC25A8M_t* self, float out1);
	void (*Enable)(AMC25A8M_t* self);
	void (*Disable)(AMC25A8M_t* self);


//	AMC25A8M_t* self; // This could maybe be used for some interesting stuff
	TIM_OC_InitTypeDef PWM1Config;
	TIM_HandleTypeDef PWM1Timer;
	uint32_t PWM1Channel;

	TIM_OC_InitTypeDef PWMRefConfig;
	TIM_HandleTypeDef PWMRefTimer;
	uint32_t PWMRefChannel;

	uint16_t enablePin;
	GPIO_TypeDef* enablePort;


};


// private methods
//void AMC25A28M_Construct(AMC25A8M_t* self, GPIO_TypeDef* enablePort, uint16_t enablePin, TIM_OC_InitTypeDef pwm1Config,
//		TIM_HandleTypeDef pwm1Timer, TIM_OC_InitTypeDef pwm2Config, TIM_HandleTypeDef pwm2Timer);

void AMC25A28M_Construct(AMC25A8M_t* self, GPIO_TypeDef* enablePort, uint16_t enablePin,
		TIM_HandleTypeDef pwm1Timer, uint32_t pwm1_channel, TIM_HandleTypeDef pwm2Timer, uint32_t pwm2_channel);

void AMC25A8M_setOutput(AMC25A8M_t* self, float out1);
void AMC25A8M_Enable(AMC25A8M_t * AMC25A8Mcontext);
void AMC25A8M_Disable(AMC25A8M_t * AMC25A8Mcontext);

#endif /* AMC25A8M_H_ */
