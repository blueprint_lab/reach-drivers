/*
 * ICMU.h
 *
 *  Created on: 21 Nov 2016
 *      Author: Administrator
 */

#ifndef HARDWARE_MODULES_ICMU_H_
#define HARDWARE_MODULES_ICMU_H_

#include <stdint.h>
#include "stm32_device_select.h"
#include "DataStructures/circularQueue.h"
#include "DataStructures/auxMath.h"
#include "Hardware/encoder.h"

typedef enum encoderCommsState {
	ready,
	busy
}encoderCommsState_t;

void ICMU_Construct(encoder_t* self, SPI_HandleTypeDef* serialObj, uint16_t chipSelect, GPIO_TypeDef* chipSelectPort);
 //Private
void ICMU_Init(encoder_t * self);
void ICMU_Enable(encoder_t* self);
void ICMU_Disable(encoder_t * self);
float ICMU_Read(encoder_t* self);
void ICMU_Callibrate(encoder_t* self);
void ICMU_Destroy(encoder_t* self);

// Move Q to transmit buffer
uint32_t transmitQueue(encoder_t* self);
uint32_t transmitQueueDMA(encoder_t* self);

int16_t ICMU_readRegisterValue(encoder_t * self, uint8_t addr);
uint8_t ICMU_writeRegisterValue(encoder_t * self, uint8_t addr, uint8_t value);
void ICMU_ReadWriteTarget(encoder_t * self, uint8_t icmu_opCode, uint8_t addr, uint8_t value);
void ICMU_ReadRaw(encoder_t * self);

uint8_t ICMU_Read_Register(encoder_t* self, uint8_t Reg);
uint8_t ICMU_Write_Register(encoder_t* self, uint8_t Reg, uint8_t val);
uint8_t ICMU_Register_status(encoder_t* self);
uint8_t ICMU_Write_Register_Bit(encoder_t* self, uint8_t Reg, uint8_t bit_index, uint8_t bit_value);

uint16_t ICMU_getStatus(encoder_t* self);
void ICMU_checkEncoder(encoder_t* self);
void ICMU_Set_Direction(encoder_t* self, uint8_t direction);
void ICMU_Set_Zero(encoder_t* self);
uint8_t ICMU_Get_Direction(encoder_t* self);
void ICMU_NOP(encoder_t * self);


// Defines and enums
#define ICMU_PACKET_SIZE 10
#define ICMU_RX_BUFF_SIZE 16
#define ICMU_TX_BUFF_SIZE 16

enum {
	TRANSFER_WAIT,
	TRANSFER_COMPLETE,
	TRANSFER_ERROR
};

typedef enum status0{
	CMD_EXE,
	FRQ_CNV,
	FRQ_ABZ,
	NON_CTR,
	MT_CTR,
	MT_ERR,
	EPR_ERR,
	CRC_ERR,
}status0_t;

typedef enum status1{
	AM_MIN,
	AM_MAX,
	AN_MIN,
	AN_MAX,
	STUP,
}status1_t;


typedef enum ICMU_regState{
	ICMU_SEND_OPCODE,
	ICMU_REG_REQUEST,
	ICMU_COMPLETE
}ICMU_regState_t;

// ICMU ACTIVATE States
#define ICMU_ACTIVATE_RA_PA 0x03
#define ICMU_ACTIVATE_RA_PD 0x02
#define ICMU_ACTIVATE_RD_PA 0x01
#define ICMU_ACTIVATE_RD_PD 0x00

// IC MU OPCODES
#define ICMU_ACTIVATE 0xB0
#define ICMU_SDAD 0xA6
#define ICMU_SDAD_STATUS 0xF5
#define ICMU_READ_REG 0x97
#define ICMU_WRITE_REG 0xD2
#define ICMU_REG 0xAD

// Register read/write status
#define REG_VALID 0x01
#define REG_BUSY 0x02
#define REG_FAIL 0x04
#define REG_DISMISS 0x08
#define REG_ERROR 0x80

// Registers
#define ICMU_REG_MPC 	0x0F
#define ICMU_REG_LIN 	0x0E //bit 4
#define ICMU_REG_CMD_MU 0x75
#define ICMU_REG_STATUS0 0x076
#define ICMU_REG_STATUS1 0x077
#define ICMU_REG_ACC_STAT 0x0D

// CMD_MU
#define CMD_MU_WRITE_ALL 0x01
#define CMD_MU_WRITE_OFF 0x02
#define CMD_MU_ABS_RESET 0x03
#define CMD_MU_NON_VER 0x04
#define CMD_MU_MT_RESET 0x05
#define CMD_MU_MT_VER 0x06
#define CMD_MU_SOFT_RESET 0x07
#define CMD_MU_SOFT_PRES 0x08
#define CMD_MU_SOFT_E2P_PRES 0x09
#define CMD_MU_I2C_COM 0x0A
#define CMD_MU_EVENT_COUNT 0x0B
#define CMD_MU_SWITCH 0x0C

// Macro function constants
#define SET_ZERO_ADDRESS 0x75
#define SET_ZERO_DATA 0x09
#define SET_DIRECTION_ADDRESS_1 0x15
#define DIRECTION_FORWARD 0
#define DIRECTION_REVERSE 1
#define SET_DIRECTION_ADDRESS_2 0x75
#define SET_DIRECTION_DATA_2 0x01

#define CONF_ROT_BIT 0x80

#define ICMU_ACC_STAT_ENABLE 1
#define ICMU_ACC_STAT_DISABLE 0


#endif /* HARDWARE_MODULES_ICMU_H_ */
