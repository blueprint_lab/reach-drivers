/*
 * AnalogueJoystick.h
 *
 *  Created on: Sep 1, 2016
 *      Author: p.phillips
 */

#ifndef ANALOGUEJOYSTICK_H_
#define ANALOGUEJOYSTICK_H_


#include "stm32_device_select.h"
//#define ADC_CONVERTED_DATA_BUFFER_SIZE   ((uint32_t)  32)   /* Size of array aADCxConvertedData[] */

typedef struct AnalogueJoystick_t AnalogueJoystick_t;

struct AnalogueJoystick_t{
	float (*Read)(AnalogueJoystick_t* self);
	void (*Enable)(AnalogueJoystick_t* self);
	void (*Deconstruct)(AnalogueJoystick_t* self);
	void (*callibrate)(AnalogueJoystick_t* self, float center, float deadband, float uppLim, float lowerLim);


	//private
	uint16_t*  ADC_Buff; //points to the location of the ADC buffer specified by rank
	uint8_t numSamples;

	ADC_HandleTypeDef AdcHandle;
	uint8_t rank;

	float(*mapping)(AnalogueJoystick_t* self, float input);

	uint8_t customLimits;
	float center;
	float lowerLim ;
	float upperLim ;
	float deadBand ;

};


void AnalogueJoystick_Deconstruct(AnalogueJoystick_t* self);
void AnalogueJoystick_Construct(AnalogueJoystick_t* self, ADC_HandleTypeDef AdcHandle,
		uint8_t size, float (*mapping)(AnalogueJoystick_t* self, float input));

float AnalogueJoystick_Read(AnalogueJoystick_t* self);
void AnalogueJoystick_Initialise(AnalogueJoystick_t* self);


void AnalogueJoystick_callibrate(AnalogueJoystick_t* self, float center, float deadband, float uppLim, float lowerLim);

#endif /* ANALOGUEJOYSTICK_H_ */

