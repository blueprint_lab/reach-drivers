/*
 * ATIFT.h
 *
 *  Created on: Oct 14, 2020
 *      Author: Shaun Barlow
 *
 *
 *	Driver for ATI Force Torque sensor NETCANOEM
 */

#ifndef RS1DRIVERSINC_HARDWARE_ATIFT_H_
#define RS1DRIVERSINC_HARDWARE_ATIFT_H_

#include "Communication/protocol3rdParty.h"
#include "DataStructures/matrixMath.h"

#define TX_DATA_LENGTH 30
#define ATI_SERIAL_NUM_LENGTH 8
#define ATI_NUM_ADC_VOLTAGES 6

#define ATIFT_ENABLED 1
#define ATIFT_DISABLED 0

// response opcodes sent by the ATIFT
//https://www.ati-ia.com/app_content/documents/9610-05-1030.pdf
typedef enum {
	STRAIN_GAUGE_DATA_SG0_SG2_SG4 	= 0x0,
	STRAIN_GAUGE_DATA_SG1_SG3_SG5 	= 0x1,
	MTRX_COEFF_SG0_SG1 				= 0x2,
	MTRX_COEFF_SG2_SG3 				= 0x3,
	MTRX_COEFF_SG4_SG5 				= 0x4,
	FT_SERIAL_NUMBER 				= 0x5,
	CALIB_INDEX						= 0x6,
	CP_FORCE_TORQUE					= 0x7,
	FT_UNITS						= 0x8,
	ADC_VOLTAGE						= 0x9,
	FT_RESET						= 0xC,
	BASE_ID							= 0xD,
	BAUD_RATE						= 0xE,
	FT_FIRMWARE_VERSION				= 0xF,
} ATIFT_packetID_t;

typedef enum {
	ATI_FLAG_UNSET,
	ATI_FLAG_SET,
} ATIFT_flag_t;

// transmit opcodes to be sent to the ATIFT
typedef enum {
	GET_STRAIN_GAUGE_DATA 	= 0x0,
	GET_CALIB_MTRX 			= 0x2,
	GET_FT_SERIAL_NUMBER 	= 0x5,
	SET_CALIB_INDEX			= 0x6,
	GET_CP_FORCE_TORQUE		= 0x7,
	GET_FT_UNITS			= 0x8,
	GET_ADC_VOLTAGE			= 0x9,
	DO_FT_RESET				= 0xC,
	SET_BASE_ID				= 0xD,
	SET_BAUD_RATE			= 0xE,
	GET_FT_FIRMWARE_VERSION	= 0xF,
} ATIFT_txPacketID_t;

// requests to the 708 related to the ATIFT
typedef enum {
	atiTx_STRAIN_GAUGE_DATA = (0x10 + GET_STRAIN_GAUGE_DATA),
	atiTx_STATUS,
	atiTx_CALIB_MTRX,
	atiTx_SERIAL_NUMBER = (0x10 + GET_FT_SERIAL_NUMBER),
	atiTx_CALIB_INDEX,
	atiTx_COUNTS,
	atiTx_UNITS,
	atiTx_ADC_VOLTAGE,
	atiTx_RESET = (0x10 + DO_FT_RESET),
	atiTx_BASE_ID,
	atiTx_BAUD_RATE,
	atiTx_FIRMWARE_VERSION,
	atiCmd_SET_ENABLE = 0x20,
	atiCmd_SET_STATE,
	atiCmd_GET_ENABLE = 0x40,
	atiCmd_GET_STATE,
	atiCmd_START_PROGRAM,
	atiCmd_SET_BAUD_AND_ID,
	atiCmd_GET_BAUD_RESPONSE,
	atiCmd_GET_ID_RESPONSE,

} txDataID_t;

typedef enum calibIndex {
	FX, FY, FZ, TX, TY, TZ,
} calibIndex_t;

enum calibMatrixRow {
	G0, G1, G2, G3, G4, G5,
};
calibIndex_t calibMatrixCol;

typedef enum atiftBaudRate {
	MBPS_2,
	MBPS_1,
	KBPS_500,
	KBPS_250,
	KBPS_125,
} atiftBaudRate_t;

// ATIFT_run states
typedef enum {
	// Keep DISABLED and START at the beginning of the enum
	ATI_DISABLED=0,
	ATI_START=1,
	ATI_SET_CALIB,
	ATI_VERIFY_CALIB,
	ATI_GET_MTRX_FX,
	ATI_GET_MTRX_FY,
	ATI_GET_MTRX_FZ,
	ATI_GET_MTRX_TX,
	ATI_GET_MTRX_TY,
	ATI_GET_MTRX_TZ,
	ATI_GET_CPFT,
	ATI_INIT_COMPLETE,
	ATI_GET_SG_DATA,
	ATI_WAITING_FOR_SG_DATA,
	ATI_PAUSE_BETWEEN_SG_REQUESTS,
	ATI_SG_DATA_COMPLETE,
	ATI_TIMEOUT,
	ATI_ERROR,
	ATI_TEST,
	ATI_SET_CAN_BAUD,
	ATI_SET_CAN_ID,
	ATI_DO_RESET,
	ATI_PROG_FT,
	ATI_LAST_STATE // KEEP THIS AT THE END OF THE ENUM
} runState_t;

typedef enum {
	unitF_lbf=1,
	unitF_N,
	unitF_Klbf,
	unitF_kN,
	uintF_kgf,
	unitF_gf,
} forceUnit_t;

typedef enum {
	unitT_lbfin=1,
	unitT_lbfft,
	unitT_Nm,
	unitT_Nmm,
	uintT_kgfcm,
	unitT_kNm,
} torqueUnit_t;

typedef struct _ATIFT ATIFT_t;
struct _ATIFT {
	uint8_t enabled;
	protocol3P_t protocol;
	uint32_t CANIDMask;
	Matrix SGData;
	int16_t SG0_2_4[3];
	int16_t SG1_3_5[3];
	uint16_t status;
	calibIndex_t calibRow;
	float calibMatrix[6][6];
	Matrix runtimeMatrix;
	uint8_t calibIndex;
	uint8_t adcVoltageIndex;
	int16_t adcVoltage[ATI_NUM_ADC_VOLTAGES];
	runState_t state;
	uint8_t timeoutCounter;
	uint32_t countsPerForce;
	uint32_t countsPerTorque;
	uint8_t firmwareVersion[4];
	Matrix forceTorqueValues;
	uint16_t numTimeouts;
	uint16_t runtimeCountdown;
	uint16_t runtimePeriodMs;
	float fx, fy, fz, tx, ty, tz;
	float tare[6]; // Order: fx, fy, fz, tx, ty, tz
	forceUnit_t forceUnit;
	torqueUnit_t torqueUnit;
	atiftBaudRate_t baudRate;
	uint8_t txDataLen;
	uint8_t txData[TX_DATA_LENGTH];
	uint8_t serialNumberASCII[ATI_SERIAL_NUM_LENGTH];
	ATIFT_flag_t idReturn, baudReturn;
	void (*setCANBaud250kbps)();
	void (*zeroBusStateSched)();
};

void ATIFT_construct(ATIFT_t* self, protocolManager_t* pProtocolManager,
			uint8_t baseIDAddressvoid, void (*fnChangeCANBaud250kbps), void (*fnZeroBusStateSched));
void ATIFT_init(ATIFT_t* self, uint8_t calibIndex);
void ATIFT_run(ATIFT_t* self);
void ATIFT_setTare(ATIFT_t* self, float* tareValues);
void ATIFT_messageHandler(ATIFT_t* self, uint8_t packetID, uint8_t data);

#endif /* RS1DRIVERSINC_HARDWARE_ATIFT_H_ */
