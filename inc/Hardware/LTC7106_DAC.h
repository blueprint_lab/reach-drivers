/*
 * LTC7106_DAC.h
 *
 *  Created on: 18 Mar 2021
 *      Author: Shaun Barlow
 */

#ifndef LTC7106_DAC_H_
#define LTC7106_DAC_H_

#include "../Inc/stm32_device_select.h"

#define LTC_ADDRESS 			0xB4
#define LTC_ADDRESS_R 			0xB5
#define LTC_OPERATION 			0x01
#define LTC_PMBUS_REVISION 		0x98
#define LTC_STATUS_BYTE 		0x78
#define LTC_MFR_IOUT_COMMAND 	0xE8
#define LTC_MFR_IOUT_MAX 		0xE6
#define LTC_SINK_MAX_CURRENT 	0x40
#define LTC_DAC_CODE_8_NOM 		0b0001000
#define LTC_DAC_CODE_16_NOM 	0b0010000
#define LTC_DAC_CODE_32_NOM 	0b0100000
#define LTC_DAC_CODE_63_NOM 	0b0111111
#define LTC_DAC_CODE_NEG_64_NOM 0b1000000
#define LTC_DAC_CODE_NEG_63_NOM 0b1000001
#define LTC_DAC_CODE_NEG_62_NOM 0b1000010
#define LTC_DAC_CODE_NEG_61_NOM 0b1000011
#define LTC_DAC_CODE_NEG_60_NOM 0b1000100
#define LTC_DAC_CODE_NEG_59_NOM 0b1000101
#define LTC_DAC_CODE_NEG_58_NOM 0b1000110
#define LTC_DAC_CODE_NEG_57_NOM 0b1000111
#define LTC_DAC_CODE_NEG_32_NOM 0b1100000
#define LTC_DAC_CODE_NEG_16_NOM 0b1110000
#define LTC_DAC_CODE_NEG_8_NOM  0b1111000

#define V_OUT_12V LTC_DAC_CODE_63_NOM
#define V_OUT_24V LTC_DAC_CODE_NEG_64_NOM

typedef struct LTC7106_t LTC7106_t;
struct LTC7106_t {
	uint8_t (*Init)(LTC7106_t* self);
	uint8_t (*setVoltage)(LTC7106_t* self, float voltage);
	uint8_t (*CheckAlert)(LTC7106_t* self);

	I2C_HandleTypeDef* phi2c;
	uint8_t output;
	float voltage;
};


void LTC7106_Construct(LTC7106_t* self, I2C_HandleTypeDef* phi2c);
uint8_t LTC7106_Init(LTC7106_t* self);
uint8_t LTC7106_SetVoltage(LTC7106_t* self, float voltage);
uint8_t LTC7106_CheckAlert(LTC7106_t* self);

#endif /* LTC7106_DAC_H_ */
