/*
 * DRV8311.h
 *
 *  Created on: 31 May 2022
 *      Author: Kyle McLean
 */

#ifndef DRV8311_H_
#define DRV8311_H_

/* Include HAL drivers */
#include "stm32_device_select.h"
#include "Hardware/motorDriver.h"

typedef struct DRV8311_t DRV8311_t;
struct DRV8311_t {
	void (*Enable)				(DRV8311_t* self);
	void (*Init)				(DRV8311_t* self);
	void (*Disable)				(DRV8311_t* self);

	uint8_t (*Check)			(DRV8311_t* self);

	void (*SetOutputBrushed)	(DRV8311_t* self, float output);
	void (*SetOutputFOC)		(DRV8311_t* self, float v_d, float v_q, float motorDirection);
	void (*Calibrate)			(DRV8311_t* self, float amplitude, float mechanicalPosition);
	void (*StallDrive)			(DRV8311_t *self, float amplitude, float mechanicalPosition, float motorDirection);
	void (*CalibrateTest)		(DRV8311_t *self, float amplitude, float motorDirection);

	uint16_t sleepPin;
	GPIO_TypeDef* sleepPort;

	uint16_t faultPin;
	GPIO_TypeDef* faultPort;

	TIM_OC_InitTypeDef 	PWMAConfig;
	TIM_HandleTypeDef 	PWMATimer;
	uint32_t 			PWMAChannel;
	uint16_t 			enableA_pin;
	GPIO_TypeDef* 		enableA_port;

	TIM_OC_InitTypeDef 	PWMBConfig;
	TIM_HandleTypeDef 	PWMBTimer;
	uint32_t 			PWMBChannel;
	uint16_t 			enableB_pin;
	GPIO_TypeDef* 		enableB_port;

	TIM_OC_InitTypeDef 	PWMCConfig;
	TIM_HandleTypeDef 	PWMCTimer;
	uint32_t 			PWMCChannel;
	uint16_t 			enableC_pin;
	GPIO_TypeDef* 		enableC_port;

	float				outputA;
	float 				outputB;
	float 				outputC;
	float 				positions[2];
	float 				offsetAngle;

	volatile float 		electricalAngle;

	uint16_t 			index;

	uint8_t 			numPolePairs;
	uint8_t 			mode;
	uint8_t 			status;

};

/* Contructor */
void DRV8311_Construct(DRV8311_t *self,
		TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel,
		TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel,
		TIM_HandleTypeDef PWMCTimer, uint32_t PWMCChannel,
		uint16_t sleepPin, GPIO_TypeDef* sleepPort,
		uint16_t faultPin, GPIO_TypeDef* faultPort);

#endif /* DRV8311_H_ */
