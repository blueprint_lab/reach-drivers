/*
 * VL6180X.h
 *
 *  Created on: May 5, 2021
 *      Author: Shaun Barlow
 */

#ifndef VL6180X_H_
#define VL6180X_H_

#include <stdint.h>
#include "stm32_device_select.h"
#include "DataStructures/auxMath.h"
#include "Communication/I2C.h"

#define VL6180X_DEFAULT_I2C_ADDR 0x29 ///< The fixed I2C addres

#define VL6180X_ALS_GAIN_1 0x06    ///< 1x gain
#define VL6180X_ALS_GAIN_1_25 0x05 ///< 1.25x gain
#define VL6180X_ALS_GAIN_1_67 0x04 ///< 1.67x gain
#define VL6180X_ALS_GAIN_2_5 0x03  ///< 2.5x gain
#define VL6180X_ALS_GAIN_5 0x02    ///< 5x gain
#define VL6180X_ALS_GAIN_10 0x01   ///< 10x gain
#define VL6180X_ALS_GAIN_20 0x00   ///< 20x gain
#define VL6180X_ALS_GAIN_40 0x07   ///< 40x gain

#define VL6180X_ERROR_NONE 0        ///< Success!
#define VL6180X_ERROR_SYSERR_1 1    ///< System error
#define VL6180X_ERROR_SYSERR_5 5    ///< Sysem error
#define VL6180X_ERROR_ECEFAIL 6     ///< Early convergence estimate fail
#define VL6180X_ERROR_NOCONVERGE 7  ///< No target detected
#define VL6180X_ERROR_RANGEIGNORE 8 ///< Ignore threshold check failed
#define VL6180X_ERROR_SNR 11        ///< Ambient conditions too high
#define VL6180X_ERROR_RAWUFLOW 12   ///< Raw range algo underflow
#define VL6180X_ERROR_RAWOFLOW 13   ///< Raw range algo overflow
#define VL6180X_ERROR_RANGEUFLOW 14 ///< Raw range algo underflow
#define VL6180X_ERROR_RANGEOFLOW 15 ///< Raw range algo overflow

typedef enum vlCommState_t {
	VL_CS_READY,
	VL_CS_CHECKING_FOR_READING_RX,
	VL_CS_CHECK_FOR_READING_TX,
	VL_CS_GET_READING_TX,
	VL_CS_GET_READING_RX,
	VL_CS_CLEAR_INTERRUPT_TX,
} vlCommState_t;

typedef enum vlCalibrationState_t {
	VL_AUTO_LIMIT,
	VL_ZERO_OFFSET,
	VL_COLLECT_DATA,
	VL_APPLY_OFFSET,
	VL_CALIBRATION_CPLT
} vlCalibrationState_t;

typedef struct VL6180X_ VL6180X_t;
struct VL6180X_ {
	uint8_t rxData[5];
	uint8_t errorCount;
	uint8_t status;
	uint8_t deviceAddress;
	uint8_t initialized;
	uint8_t sampleCounter;
	uint8_t rangeAvailable;

	GPIO_TypeDef* enablePort;
	uint16_t enablePin;

	int calibrationOffset;

	vlCalibrationState_t calibrationState;

	vlCommState_t commState;

	uint32_t nextReadingTime;

	float range;
	float calibrationAve;

	I2C_HandleTypeDef* phi2c;
};

uint8_t VL6180X_test(I2C_HandleTypeDef* phi2c);
uint8_t VL6180X_init(VL6180X_t* self, uint8_t offset);
void VL6180X_initMulti(VL6180X_t* self, uint8_t offset, uint8_t newDeviceAddress);
void VL6180X_offsetcalibration(VL6180X_t* self, int targetDistance);
void VL6180X_construct(VL6180X_t* self, I2C_HandleTypeDef* phi2c, uint8_t deviceAddress, GPIO_TypeDef* enablePort, uint16_t enablePin);
uint8_t VL6180X_changeAddress(VL6180X_t* self, uint8_t newDeviceAddress);
uint8_t VL6180X_readSingleRangePolling(VL6180X_t* self);
uint8_t VL6180X_initContinuousMeasurement(VL6180X_t* self);
uint8_t VL6180X_readContinuousRangePolling(VL6180X_t* self);
uint8_t VL6180X_readDMA(VL6180X_t* self);
uint8_t VL6180X_DMARxTxCplt(VL6180X_t* self, I2C_HandleTypeDef *hi2c, I2C_txRxDirection_t transferDirection);


#define IDENTIFICATION__MODEL_ID 			0x000
#define IDENTIFICATION__MODEL_REV_MAJOR 	0x001
#define IDENTIFICATION__MODEL_REV_MINOR 	0x002
#define IDENTIFICATION__MODULE_REV_MAJOR 	0x003
#define IDENTIFICATION__MODULE_REV_MINOR 	0x004
#define IDENTIFICATION__DATE_HI 			0x006
#define IDENTIFICATION__DATE_LO 			0x007
#define IDENTIFICATION__TIME 				0x008 /* 0x008:0x009 */
#define SYSTEM__MODE_GPIO0 					0x010
#define SYSTEM__MODE_GPIO1 					0x011
#define SYSTEM__HISTORY_CTRL 				0x012
#define SYSTEM__INTERRUPT_CONFIG_GPIO 		0x014
#define SYSTEM__INTERRUPT_CLEAR 			0x015
#define SYSTEM__FRESH_OUT_OF_RESET 			0x016
#define SYSTEM__GROUPED_PARAMETER_HOLD 		0x017
#define SYSRANGE__START 					0x018
#define SYSRANGE__THRESH_HIGH 				0x019
#define SYSRANGE__THRESH_LOW 				0x01A
#define SYSRANGE__INTERMEASUREMENT_PERIOD 	0x01B
#define SYSRANGE__MAX_CONVERGENCE_TIME 		0x01C
#define SYSRANGE__CROSSTALK_COMPENSATION_RATE 0x01E
#define SYSRANGE__CROSSTALK_VALID_HEIGHT 	0x021
#define SYSRANGE__EARLY_CONVERGENCE_ESTIMATE 0x022
#define SYSRANGE__PART_TO_PART_RANGE_OFFSET 0x024
#define SYSRANGE__RANGE_IGNORE_VALID_HEIGHT 0x025
#define SYSRANGE__RANGE_IGNORE_THRESHOLD 	0x026
#define SYSRANGE__MAX_AMBIENT_LEVEL_MULT 	0x02C
#define SYSRANGE__RANGE_CHECK_ENABLES 		0x02D
#define SYSRANGE__VHV_RECALIBRATE 			0x02E
#define SYSRANGE__VHV_REPEAT_RATE 			0x031
#define SYSALS__START 						0x038
#define SYSALS__THRESH_HIGH 				0x03A
#define SYSALS__THRESH_LOW 					0x03C
#define SYSALS__INTERMEASUREMENT_PERIOD 	0x03E
#define SYSALS__ANALOGUE_GAIN 				0x03F
#define SYSALS__INTEGRATION_PERIOD 			0x040
#define RESULT__RANGE_STATUS 				0x04D
#define RESULT__ALS_STATUS 					0x04E
#define RESULT__INTERRUPT_STATUS_GPIO 		0x04F
#define RESULT__ALS_VAL 					0x050
#define RESULT__HISTORY_BUFFER_x 			0x052 /* 0x052:0x060 (0x2) */
#define RESULT__HISTORY_BUFFER_0 			0x052
#define RESULT__HISTORY_BUFFER_1 			0x054
#define RESULT__HISTORY_BUFFER_2 			0x056
#define RESULT__HISTORY_BUFFER_3 			0x058
#define RESULT__HISTORY_BUFFER_4 			0x05A
#define RESULT__HISTORY_BUFFER_5 			0x05C
#define RESULT__HISTORY_BUFFER_6 			0x05E
#define RESULT__HISTORY_BUFFER_7 			0x060
#define RESULT__RANGE_VAL 					0x062
#define RESULT__RANGE_RAW 					0x064
#define RESULT__RANGE_RETURN_RATE 			0x066
#define RESULT__RANGE_REFERENCE_RATE 		0x068
#define RESULT__RANGE_RETURN_SIGNAL_COUNT 	0x06C
#define RESULT__RANGE_REFERENCE_SIGNAL_COUNT 0x070
#define RESULT__RANGE_RETURN_AMB_COUNT 		0x074
#define RESULT__RANGE_REFERENCE_AMB_COUNT 	0x078
#define RESULT__RANGE_RETURN_CONV_TIME 		0x07C
#define RESULT__RANGE_REFERENCE_CONV_TIME 	0x080
#define READOUT__AVERAGING_SAMPLE_PERIOD 	0x10A
#define FIRMWARE__BOOTUP 					0x119
#define FIRMWARE__RESULT_SCALER 			0x120
#define I2C_SLAVE__DEVICE_ADDRESS 			0x212
#define INTERLEAVED_MODE__ENABLE 			0x2A3


#define VL6180X_SAMPLE_PERIOD_MS 60

#endif /* VL6180X_H_ */
