/*
 * AMC7823.h
 *
 *  Created on: Aug 25, 2016
 *      Author: p.phillips
 */

#ifndef AMC7823_H_
#define AMC7823_H_

//#include "../../Utilities/Hardware_Modules/HardwareModules.h"
#include "stm32_device_select.h"
#include <stdint.h>
#include "stm32_device_select.h"
#include "DataStructures/circularQueue.h"
#include "DataStructures/auxMath.h"
#include <math.h>
#include <stdlib.h>

#define RW 15
#define PG0 12
#define PG1 13
#define SADR 6
#define EADR 0
#define DAVF 13
#define GREF 6
#define SREF 7

#define READ_OPERATION 1
#define WRITE_OPERATION 0
// POWER DOWN REGISTER
#define PADC 15
#define PDAC0 7
#define PREFB 5

// ADC CONTROL REGISTER
#define ADC_CTRL_CMODE 15
#define ADC_CTRL_SADR 8
#define ADC_CTRL_EADR 4

#define ADC_DIRECT_MODE 0
#define ADC_AUTO_MODE 1

// DAC CONFIGURATION REGISTER
#define SLDA0 8
#define GDAC0 0

// DACn DATA REGISTER


// ADDRESS:
#define GPIO_ADDRESS 0x0A
#define ADC_ADDRESS 0x00
#define ADC_END_ADDRESS 0x09
#define POWER_DOWN 0x0D
#define ADC_CONTROL 0x0B
#define AMC_STATUS 0x0A
#define DAC_CONFIG 0x09
#define DAC_DATA_0 0x00

// PAGE ASSIGNMENT
#define PAGE_0 0
#define PAGE_1 1


#define AMC7823_PACKET_SIZE 10
#define AMC7823_RX_BUFF_SIZE 16
#define AMC7823_TX_BUFF_SIZE 16
#define AMC7823_NUM_ADC_CHANNELS 9

typedef struct AMC7823_t AMC7823_t;
struct  AMC7823_t{

	void (*ReadGPIO)(AMC7823_t * self);
	void (*ReadADC)(AMC7823_t * self);
	void (*WriteDAC)(AMC7823_t* self, uint8_t pin, uint16_t value);
	void (*Destroy)(AMC7823_t * self);

	SPI_HandleTypeDef* spiObj;
	uint16_t chipSelect;
	GPIO_TypeDef* chipSelectPort;
	volatile uint16_t* receiveBuff;
	uint16_t* sendBuff;
	uint8_t GPIOState[6];
	uint16_t ADCReadings[AMC7823_NUM_ADC_CHANNELS];
	uint8_t enabledADCStartChannel;
	uint8_t enabledADCEndChannel;
};


// private methods
//void AMC25A28M_Construct(AMC7823_t* self, GPIO_TypeDef* enablePort, uint16_t enablePin, TIM_OC_InitTypeDef pwm1Config,
//		TIM_HandleTypeDef pwm1Timer, TIM_OC_InitTypeDef pwm2Config, TIM_HandleTypeDef pwm2Timer);

void AMC7823_Construct(AMC7823_t* self, SPI_HandleTypeDef* serialObj, uint16_t chipSelect, GPIO_TypeDef* chipSelectPort);
void AMC7823_ReadGPIO(AMC7823_t* self);
void AMC7823_ReadADC(AMC7823_t* self);
void AMC7823_WriteRegister(AMC7823_t* self, uint8_t page,
						uint8_t startRegister, uint8_t endRegister, uint16_t* dataBuffer);
void AMC7823_ReadRegisters(AMC7823_t* self, uint8_t page,
						uint8_t startRegister, uint8_t endRegister, uint16_t* dataBuffer);
void AMC7823_WriteDAC(AMC7823_t* self, uint8_t channel, uint16_t value);
void AMC7823_EnableDAC(AMC7823_t* self, uint8_t channel);
void AMC7823_ReadContinuousADC(AMC7823_t* self);
void AMC7823_EnableContinuousADC(AMC7823_t* self, uint8_t startChannel, uint8_t endChannel);
void AMC7823_Destroy(AMC7823_t* self);
void AMC7823_NOP(AMC7823_t* self);
void AMC7823_NOP_DAC(AMC7823_t* self, uint8_t pin, uint16_t value);
#endif /* AMC7823_H_ */
