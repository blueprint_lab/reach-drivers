/*
 * TMotor.h
 *
 *  Created on: 4 Oct. 2022
 *      Author: JamieJacobson
 *
 */

#ifndef TMotor_H_
#define TMotor_H_

#include "Communication/protocol3rdParty.h"
#include "Communication/comslink.h"
#include "DataStructures/auxMath.h"

/* Device spec parameters*/
#define MOTOR_COMMAND_BUFFER_LENGTH ((uint8_t)8u)
#define MAX_AK_DEVICES ((uint8_t)8u)

/**
 * @brief Function return statuses (0 = OK)
 */
typedef enum TMotor_ErrorCode_ {
	TMotor_ERROR_OK = 0,
	TMotor_ERROR_MOTOR_START,
	TMotor_ERROR_CAN_TRANSMIT,
	TMotor_ERROR_CAN_RECIEVE,
	TMotor_ERROR_NULL_HANDLE,
	TMotor_ERROR_NOT_INIT
}TMotor_ErrorCode_t;

/**
 * @brief Motor status enum
 * @note: from AK Series Actuator Manual V1.0.9 page 39
 */
typedef enum TMotor_MotorStatus_ {
	TMotor_STATUS_OK = 0,
	TMotor_STATUS_OVER_CURRENT,
	TMotor_STATUS_OVER_VOLTAGE,
	TMotor_STATUS_UNDER_VOLTAGE,
	TMotor_STATUS_ENCODER,
	TMotor_STATUS_HARDWARE_FAILIURE,
} TMotor_MotorStatus_t;

/**
 * @brief Control demand structure
 */
typedef struct TMotor_Demand_ {
	float positionDemand;
	float velocityDemand;
	float torqueDemand;
	float kpDemand;
	float kdDemand;
} TMotor_Demand_t;

/**
 * @brief State structure to store data read from motor
 */
typedef struct TMotor_State_ {
	float position;
	float velocity;
	float torque;

	TMotor_MotorStatus_t motorStatus;
} TMotor_State_t;

/**
 * @brief State structure to store motor properties
 */
#define NUM_PARAMS 5
typedef struct TMotor_Parameters_ {
	float Kt;			/* Torque const. [Nm/A] 		*/
	float Kv;			/* Voltage const. [rad/s/V]		*/
	float friction;		/* Coulomb friction [Nm]		*/
	float damping;		/* Viscous friction [Nm/rad/s]	*/
	float gearRatio;	/* External gearing (i.e belt)	*/
	float direction; 	/* Motor direction 				*/
	float pos_offset; 	/* Motor position offset		*/
} TMotor_Parameters_t;

/**
 * @brief State structure to store motor scaling parameters
 */
typedef struct TMotor_VariableScale_ {
	float T_max; 		/* Max torque demand 		*/
	float T_min; 		/* Min torque demand 		*/

	float I_max; 		/* Max current demand 		*/
	float I_min; 		/* Min current demand 		*/

	float P_max; 		/* Max position  			*/
	float P_min; 		/* Min position 			*/

	float V_max; 		/* Max velocity 			*/
	float V_min; 		/* Min velocity 			*/

	float Kd_max; 		/* Max Kd (velocity gain) 	*/
	float Kd_min; 		/* Min Kd (velocity gain) 	*/

	float Kp_max; 		/* Max Kp (position gain) 	*/
	float Kp_min; 		/* Min Kp (position gain) 	*/
} TMotor_VariableScale_t;

/**
 * @brief State structure to store motor scaling parameters
 */
typedef struct TMotor_Limits_ {
	float T_max; 		/* Max torque demand [N/m]	*/
	float T_min; 		/* Min torque demand [N/m]	*/

	float I_max; 		/* Max current demand [A]	*/
	float I_min; 		/* Min current demand [A]	*/
} TMotor_Limits_t;

/* Device structure forward declarations */
typedef struct TMotor_t TMotor_t;
typedef struct TMotor_DeviceHandler_t TMotor_DeviceHandler_t;

/**
 * @brief t-motor device object
 */
struct TMotor_t {
	TMotor_ErrorCode_t (*SetZeroPosition)		(TMotor_t* self);
	TMotor_ErrorCode_t (*EnterMotorMode)		(TMotor_t* self);
	TMotor_ErrorCode_t (*ExitMotorMode)			(TMotor_t* self);

	TMotor_ErrorCode_t (*SendMITControlDemand)	(TMotor_t* self, float position, float speed, float kp, float kd, float torque);
	TMotor_ErrorCode_t (*SendTorqueDemand)		(TMotor_t* self, float torque);

	uint8_t id; 								/* CAN bus ID					*/
	TMotor_Demand_t demand; 					/* Device demand structure 		*/
	TMotor_State_t state; 						/* Device state structure 		*/
	TMotor_VariableScale_t scale; 				/* Device int to float scale	*/
	TMotor_Limits_t limit; 						/* Device internal limits 		*/
	TMotor_Parameters_t param; 					/* Device parameters	 		*/

	uint8_t is_initialised;						/* Initialized flag 			*/
};

/**
 * @brief t-motor device handler
 */
struct TMotor_DeviceHandler_t {
	void (*AddDevice) (TMotor_t* self);

	protocol3P_t TMotorProtocol; 				/* Thrid-party protocol manager */
	canbus_comms_link* comms_link;				/* Communication link handle 	*/
	CAN_RxHeaderTypeDef canRxHeader;			/* CAN bus header structure		*/

	TMotor_t* devices[MAX_AK_DEVICES];			/* Device list 					*/
	uint8_t device_count;						/* Device count					*/

	uint8_t is_initialised;						/* Initialized flag 			*/
};

/* Device and device handler constructors */
void TMotor_Device_Construct(TMotor_t* self, uint8_t id, float direction, float gearRatio, float offset);
void TMotor_DeviceHandler_Construct(TMotor_DeviceHandler_t* self, protocolManager_t* pProtocolManager, canbus_comms_link* comms_link);

#endif /* TMotor_H_ */
