#include "Communication/uart.h"
//#include "circularQueue.h"


typedef struct razorIMU_t razorIMU_t;

#define RX_MSG_LEN 6

struct razorIMU_t{
	float (*Read)(razorIMU_t* self);
	void (*Transmit)(razorIMU_t* self, uint8_t* transmitQueue);
	void (*reset)(razorIMU_t* self);
	uint8_t (*enable)(razorIMU_t* self);
	UART_HandleTypeDef* huart;
	volatile uint8_t* rxBuff;
	uint8_t* txBuff;
	uint16_t rxBuffLen;
	uint16_t txBuffLen;

};


void razorIMU_Construct(razorIMU_t* self, UART_HandleTypeDef* huart, size_t size);
float razorIMU_readAngle(razorIMU_t* self);
void razorIMU_transmit(razorIMU_t* self, uint8_t* transmitQueue);
void razorIMU_reset(razorIMU_t* self);

uint8_t razorImu_enable(razorIMU_t* self);
