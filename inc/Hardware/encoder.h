/*
 * encoder.h
 *
 *  Created on: 21-05-2021
 *      Author: Kyle McLean
 */

#ifndef HARDWARE_MODULES_ENCODER_H_
#define HARDWARE_MODULES_ENCODER_H_

/* General encoder interface */
typedef struct encoder_t encoder_t;
struct encoder_t{
	/* Interface function pointers */
	void 	(*Init)				(encoder_t* self);
	void	(*Enable)			(encoder_t* self);
	void 	(*Disable)			(encoder_t* self);
	void 	(*Destroy)			(encoder_t* self);
	void 	(*Callibrate)		(encoder_t* self);

	float 	(*Read)				(encoder_t* self);
	void 	(*ReadDMA)			(encoder_t* self);
	void 	(*CheckStatus)		(encoder_t* self);
	void	(*WriteRegister)	(encoder_t* self, uint8_t addr, uint8_t val);
	void	(*ReadRegister)		(encoder_t* self, uint8_t addr);
	void 	(*DMACallback)		(encoder_t* self);

	void 	(*SetDirection)		(encoder_t* self, uint8_t direction);
	void 	(*SetZero)			(encoder_t* self);
	uint8_t	(*checkAbsolute)    (encoder_t* self, float velocity, float velocityLimit, float dt);
	uint8_t (*GetDirection)		(encoder_t* self);

	/* Status variables */
	uint16_t transmitCount;
	uint16_t readCount;
	uint8_t status;
	volatile uint8_t encoderState;

	/* Data structures */
	SPI_HandleTypeDef* spiObj;
	GPIO_TypeDef* chipSelectPort;
	circularQueue SendQ;

	/* SPI variables */
	uint16_t chipSelect;
	uint8_t* receiveBuff;
	uint8_t* sendBuff;
	uint8_t lastReadAddr;
	uint8_t lastReadByte;

	/* Calculation variables */
	float encoderAngle;

	/* Calibration parameters */
	uint8_t calibrationState;
	uint8_t sysOveride;
	uint8_t invertFlex;
	uint8_t invert;
	uint8_t ac_count;
	uint16_t flexId;
	uint16_t flexLength;


	/* Construct flags */
	uint8_t initialized;
	uint8_t constructed;
	uint8_t encoderType;
	uint8_t absolute;

	/* ICMU specific variables and functions  */
	uint8_t (*WriteRegisterBit)(encoder_t * self, uint8_t Reg, uint8_t bit_index, uint8_t bit_value);
	void (*ReadRaw)(encoder_t * self);

	uint32_t lastStreamTime;
	uint16_t masterRaw;
	uint16_t noniusRaw;
	uint16_t rawStreamDt;
	int16_t rawStreamCount;
	uint16_t spiError;
	int16_t rawStreamCount_static;
	uint8_t statusFlags;
	uint8_t notValidError;
	float currentSetpoint;
};

/******** Enumerated types ********/
/* ICPZ encoder state state flags (used for error reporting) */
typedef enum encoderState {
	encoderValid			= 0x00,
	encoderError			= 0x01,
	encoderNotDetected		= 0x02,
	periodConsistancyError  = 0x04,
	calibrationTimeout		= 0x05,
	encoderFault			= 0x06,
} encoderState_t;

typedef enum encoderType {
	ICPZ					= 0x00,
	ICMU					= 0x01,
} encoderType_t;


#endif /* HARDWARE_MODULES_ENCODER_H_ */
