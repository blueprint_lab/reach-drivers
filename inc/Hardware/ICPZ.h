/*
 * ICPZ.h
 *
 *  Created on: 20-05-2021
 *      Author: Kyle McLean
 */

#ifndef HARDWARE_MODULES_ICPZ_H_
#define HARDWARE_MODULES_ICPZ_H_

#include <stdint.h>
#include "stm32_device_select.h"
#include "DataStructures/circularQueue.h"
#include "DataStructures/auxMath.h"
#include "Hardware/encoder.h"

#define ICPZ_RX_BUFF_SIZE 16
#define ICPZ_TX_BUFF_SIZE 16

/******** ICPZ interface ********/
void ICPZ_Construct(encoder_t* self, SPI_HandleTypeDef* serialObj, uint16_t chipSelect, GPIO_TypeDef* chipSelectPort);

/******** Enumerated types ********/
/* ICPZ SPI state */
typedef enum ICPZ_spiState {
	ICPZ_spiReady			= 0x00,  // SPI interface is not in use
	ICPZ_spiBusy			= 0x01,  // SPI interface is currently in use
} ICPZ_spiState_t;

typedef enum ICPZ_regState {
	ICPZ_SEND_OPCODE		= 0x00,  // Sending op code
	ICPZ_REG_REQUEST		= 0x01,  // Sending request
} ICPZ_regState_t;

/* ICPZ command set */
typedef enum ICPZ_cmd {
	ICPZ_reboot 			= 0x10,  // Equivalent to power-on
	ICPZ_readAll  			= 0x40,  // Read configuration for all banks from EEPROM
	ICPZ_writeAll  			= 0x41,  // Write current configuration of all banks to EEPROM
	ICPZ_MtStPresetStore	= 0x88,  // Calculate and apply MT/ST_OFF so that the current position will be equal to MT/ST_PRE
	ICPZ_autoAdjAna  		= 0xB0,  // Automatic analog adjustment
	ICPZ_autoAdjDig  		= 0xB1,  // Automatic digital adjustment (initial)
	ICPZ_autoReAdjDig  		= 0xB2,  // Automatic digital re-adjustment (in-field)
	ICPZ_autoAdjEcc  		= 0xB3,  // Automatic eccentricity adjustment
	ICPZ_sClear				= 0x20,  // Clear the complete diagnosis register
} ICPZ_cmd_t;

/* ICPZ SPI operation codes */
typedef enum ICPZ_opCode {
	ICPZ_readRegisters 		= 0x97,  // Request Data From I2C Slave
	ICPZ_writeRegisters 	= 0xD2,  // Transmit Data To I2C Slave
	ICPZ_readPosition  		= 0xA6,	 // Read the absolute position data from iC-PZ
	ICPZ_writeCommand  		= 0xD9,  // Write data to register. Successful completion can be detected by polling the CMD register
	ICPZ_readStatus  		= 0x9C,  // Read the registers at address 0x6C to 0x73 containing error ERR and warning WARN information
	ICPZ_activate  			= 0xB0,  // Activate Slave in Chain
	ICPZ_transInfo 			= 0xAD,  // Returns SPI status byte which indicates the status of the last opcode
} ICPZ_opCode_t;

/* ICPZ register activation bits */
typedef enum ICPZ_activateBits {
	ICPZ_RD_PD 				= 0x00,  // Position and register data channels closed
	ICPZ_RD_PA 				= 0x01,  // Position data channel open
	ICPZ_RA_PD  			= 0x02,  // Register data channel open
	ICPZ_RA_PA 				= 0x03,  // Position and register data channels open
} ICPZ_activateBits_t;

/* ICPZ SPI status bits */
typedef enum ICPZ_spiStatus {
	ICPZ_dataValid 			= 0x00,  // Data received is valid
	ICPZ_slaveBusy			= 0x01,  // Slave is currently busy
	ICPZ_dataRequestFailed	= 0x02,  // Data request failed
	ICPZ_illegalAddress 	= 0x04,  // Can not read/write at specified address
	ICPZ_invalidOpCode 		= 0x40,  // Op-code is invalid
} ICPZ_spiStatus_t;

/* ICPZ registers
 *
 * Note: Registers 0x40..0x7F are not affected and can always be accessed disregarding the active bank. Bank must be selected for
 * all other addresses. The bank range is 0x00..0x0E.
 */
typedef enum ICPZ_register {
	/* Non-banked registers */
	ICPZ_CMD_STAT			= 0x76,  // Command status register
	ICPZ_CMD 				= 0x77,	 // Command register
	ICPZ_BSEL 				= 0x40,	 // bank selection register

	/* Bank 0 registers */
	ICPZ_IO_SLEW_RATE       = 0x000,
	ICPZ_SYS_OVR_INV		= 0x007, // SYS_OVR is related to the encoder disc used
	ICPZ_ST_PDL 			= 0x008, // Length of the ST output data
	ICPZ_MT_PDL 			= 0x009, // Length of the MT output data
	ICPZ_RAN_FLD_TOL_VAL	= 0x00F, // In case of an error, the counter value is either kept (RAN_FLD = 0) or reloaded with the sampled absolute position

	/* Bank 1 registers */

	/* Bank 2 registers */
	ICPZ_SC_GAIN_OFF_SEL    = 0x200,
	ICPZ_SC_PHASE_SEL  		= 0x201,
	ICPZ_AI_S_P_SEL			= 0x203,

	/* Bank 3 registers */

	/* Bank 4 registers */
	ICPZ_ABZ_CFG 			= 0x460,
	/* Bank 5 registers */

	/* Bank 6 registers */

	/* Bank 7 registers */
	ICPZ_SPI_ST_DL			= 0x708,
	ICPZ_SPI_MT_DL			= 0x709,

	/* Bank 8 registers */
	ICPZ_FCL_LSB 			= 0x800, // Flex length LSB
	ICPZ_FCL_MSB 			= 0x801, // Flex length MSB
	ICPZ_FCS_LSB 			= 0x802, // Flex ID LSB
	ICPZ_FCS_MSB 			= 0x803, // Flex ID MSB
	/* Bank 9 registers */

	/* Bank A registers */

	/* Bank B registers */

	/* Bank C registers */

	/* Bank D registers */

	/* Bank E registers */

} ICPZ_register;

/* ICPZ calibration states */
typedef enum ICPZ_calibrationState {
	ICPZ_calibrateInit 		= 0x00,  // Record time the calibration started
	ICPZ_calibrateAnalog 	= 0x01,	 // Send the command to start analog calibration
	ICPZ_calibrateAnaWait	= 0x02,	 // Wait for analog calibration to complete
	ICPZ_calibrateDigital 	= 0x03,	 // Send the command to start digital calibration
	ICPZ_calibrateDigWait	= 0x04,  // Wait for digital calibration to complete
	ICPZ_calibrateEcc		= 0x05,	 // Send the command to start eccentricity calibration
	ICPZ_calibrateEccWait	= 0x06,  // Wait for eccentricity calibration to complete
	ICPZ_calibrateFinish	= 0x07,  // Finish calibration sequence
	ICPZ_calibrateExit		= 0x08,  // Waiting to exit calibration mode
} ICPZ_calibrationState;

/* ICPZ analog errors */
typedef enum ICPZ_analogError {
	ICPZ_saturation 		= 0x00,  // Illumination of at least one of the digital photo-diodes is too high,
	ICPZ_lowCurrent 		= 0x01,	 // LED current smaller than 50% of the nominal current
	ICPZ_anaSteadyState 	= 0x02,	 // Temperature sensor has not yet found the actual chip temperature
	ICPZ_lowVoltage			= 0x03,	 // Voltage at VDDIO below threshold
	ICPZ_interpolator		= 0x04,  // Sinusoidal (analog) interpolator position is not within ±22.5°e of the digital interpolator position
} ICPZ_analogError;

/* ICPZ digital errors */
typedef enum ICPZ_digitalError {
	ICPZ_pfAlpha 			= 0x00,  // Acceleration register alpha exceeds an internally defined limit
	ICPZ_pfOmega 			= 0x01,	 // Velocity register omega exceeds an internally defined limit
	ICPZ_digSteadyState 	= 0x02,	 // Digital photo-amplifiers did not yet startup successfully
	ICPZ_syncFailed			= 0x03,	 // PRC mismatching tolerance set in RAN_TOL exceeded. Most likely the PRC track was not sampled correctly
	ICPZ_anaBoundry			= 0x04,  // Parameter COS_OFFS, SIN_OFFS, SC_GAINS or SC_PHASES reached minimum or maximum value
} ICPZ_digitalError;

#endif /* HARDWARE_MODULES_ICPZ_H_ */
