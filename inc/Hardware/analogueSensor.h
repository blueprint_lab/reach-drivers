/*
 * analogueSensor.h
 *
 *  Created on: 20Feb.,2017
 *      Author: The Blueprint Labs
 */

#ifndef HARDWARE_MODULES_ANALOGUESENSOR_H_
#define HARDWARE_MODULES_ANALOGUESENSOR_H_

#include "stm32_device_select.h"

typedef struct AnalogueSensor_t AnalogueSensor_t;
struct AnalogueSensor_t{
	float (*Read)(AnalogueSensor_t* self);
	float (*Get)(AnalogueSensor_t* self);
	void (*Enable)(AnalogueSensor_t* self);
	void (*Deconstruct)(AnalogueSensor_t* self);
	void (*callibrate)(AnalogueSensor_t* self, float center, float deadband, float uppLim, float lowerLim);


	//private
	uint16_t*  ADC_Buff; //points to the location of the ADC buffer specified by rank
	uint32_t  ADC_Accumalator_Buf;
	float ADCvalue;
	uint8_t numSamples;
	uint32_t sampleCounter;

	ADC_HandleTypeDef AdcHandle;
	uint8_t rank;
	uint8_t numChannels;
	float(*mapping)(AnalogueSensor_t* self, float input);

	uint8_t customLimits;
	float center;
	float lowerLim ;
	float upperLim ;
	float deadBand ;
	float  a,b,c;

	float prev_value;
	float curr_value;

};


void AnalogueSensor_Deconstruct(AnalogueSensor_t* self);
void AnalogueSensor_Construct(AnalogueSensor_t* self, ADC_HandleTypeDef AdcHandle,
		uint8_t size, float (*mapping)(AnalogueSensor_t* self, float input));

float AnalogueSensor_Read(AnalogueSensor_t* self);
float AnalogueSensor_Get(AnalogueSensor_t* self);
void AnalogueSensor_Initialise(AnalogueSensor_t* self);
void AnalogueSensor_Calibrate(AnalogueSensor_t* self, float center, float deadband, float uppLim, float lowerLim);

float sign_mapping(AnalogueSensor_t* self, float input);
float polynomial_mapping(AnalogueSensor_t* self, float input);
float linear_joystick_mapping(AnalogueSensor_t* self, float input);
#endif /* HARDWARE_MODULES_ANALOGUESENSOR_H_ */
