/*
 * DRV8332.h
 *
 *  Created on: 28Feb.,2019
 *      Author: marai
 */

#ifndef DRV8332_H_
#define DRV8332_H_


#include <stdint.h>
#include <math.h>

#include "stm32_device_select.h"
#include "Hardware/motorDriver.h"

typedef struct DRV8332_t DRV8332_t;
struct DRV8332_t{
	void (*Enable)(DRV8332_t* self);
	void (*Init)(DRV8332_t* self);
	void (*Disable)(DRV8332_t* self);
	uint8_t (*Check)(DRV8332_t* self);

	void (*SetOutputBrushed)(DRV8332_t* self, float output);
	void (*SetOutputFOC)(DRV8332_t* self,float v_d,float v_q,float motorDirection);
	void (*SetOutputSin)(DRV8332_t* self, float amplitude);
	void (*Calibrate)(DRV8332_t *self,float amplitude,float mechanicalPosition);
	void (*StallDrive)(DRV8332_t *self,float amplitude,float mechanicalPosition,float motorDirection, uint16_t period_counter);
	void (*CalibrateTest)(DRV8332_t *self,float amplitude,float motorDirection);
	uint32_t (*SetPWMFrequency)(DRV8332_t *self, uint32_t frequency);

//	uint16_t enablePin;
//	GPIO_TypeDef* enablePort;
//    uint16_t IN1_pin;
//    GPIO_TypeDef* IN1_port;
//    uint16_t IN2_pin;
//    GPIO_TypeDef* IN2_port;

	uint16_t otwPin;
	GPIO_TypeDef* otwPort;
	uint16_t faultPin;
	GPIO_TypeDef* faultPort;


	TIM_OC_InitTypeDef PWMAConfig;
	TIM_HandleTypeDef PWMATimer;
	uint32_t PWMAChannel;
	uint16_t ResetA_pin;
	GPIO_TypeDef* ResetA_port;

	TIM_OC_InitTypeDef PWMBConfig;
	TIM_HandleTypeDef PWMBTimer;
	uint32_t PWMBChannel;
	uint16_t ResetB_pin;
	GPIO_TypeDef* ResetB_port;

	TIM_OC_InitTypeDef PWMCConfig;
	TIM_HandleTypeDef PWMCTimer;
	uint32_t PWMCChannel;
	uint16_t ResetC_pin;
	GPIO_TypeDef* ResetC_port;

	uint8_t mode;
	uint8_t status;
	float torquePrev;

	float outputA;
	float outputB;
	float outputC;

	uint16_t index;

	float positions[2];
	float offsetAngle;
	float electricalAngle;
	float rotorDirection;

	float coggingProfile[720];

	uint8_t numPolePairs;

};



void DRV8332_Construct(DRV8332_t* self,
		TIM_HandleTypeDef PWMATimer, uint32_t PWMAChannel, uint16_t ResetA_pin, GPIO_TypeDef* ResetA_port, TIM_HandleTypeDef PWMBTimer, uint32_t PWMBChannel, uint16_t ResetB_pin, GPIO_TypeDef* ResetB_port, TIM_HandleTypeDef PWMCTimer, uint32_t PWMCChannel, uint16_t ResetC_pin, GPIO_TypeDef* ResetC_port,
		uint16_t otwPin, GPIO_TypeDef* otwPort,uint16_t faultPin, GPIO_TypeDef* faultPort );

//Function Definitions:
void DRV8332_Enable(DRV8332_t *DRV8332Context);
void DRV8332_Init(DRV8332_t *DRV8332Context);
void DRV8332_Disable(DRV8332_t *DRV8332Context);
uint8_t DRV8332_Check(DRV8332_t *DRV8332Context);
void DRV8332_Init();
void DRV8332_SetOutput_Brushed(DRV8332_t *self,float output);
void DRV8332_SetOutput_FOC(DRV8332_t *self,float v_d, float v_q,float motorDirection);
void DRV8332_SetOutput_DutyCycleZero(DRV8332_t *self);
void DRV8332_SetOutput_DirectSinDrive(DRV8332_t *self, float amplitude);
void DRV8332_FindZeroAngle(DRV8332_t *self,float amplitude,float mechanicalPosition);
void DRV8332_FindCoggingProfile(DRV8332_t *self,float amplitude,float mechanicalPosition);
void DRV8332_CalibrateEncoder(DRV8332_t *self,float amplitude,float mechanicalPosition,float motorDirection, uint16_t period_counter);
void DRV8332_CalibrateTest(DRV8332_t *self,float amplitude,float motorDirection);
#endif /* DRV8332_H_ */
