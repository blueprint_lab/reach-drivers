/*
 * memoryJump.h
 *
 * Memory jump helper driver
 *
 *  Created on: 08-11-2021
 *      Author: Kyle McLean
 */

#ifndef _MEMORY_JUMP_SI7210_H_
#define _MEMORY_JUMP_SI7210_H_

void __attribute__((optimize("O0")))JumpToSTMBootloader(void);
void __attribute__((optimize("O0")))JumpToBootloader(void);
void __attribute__((optimize("O0")))SystemReset(void);

#endif /* _MEMORY_JUMP_SI7210_H_ */

