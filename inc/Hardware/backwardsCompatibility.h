/*
 * backwardsCompatibility.h
 *
 *  Created on: 10 Sep 2020
 *      Author: Shaun
 */

#ifndef BACKWARDS_COMPATIBILITY_H_
#define BACKWARDS_COMPATIBILITY_H_

#include <stdint.h>
#include "../Inc/stm32_device_select.h"

uint8_t compat_isElectricalVersion(uint8_t* pDeviceElectricalVersion, uint8_t maj, uint8_t submaj, uint8_t min);
void compat_GPIO_Init(GPIO_TypeDef * gpio_port, uint16_t gpio_pin, uint8_t pin_state);

#ifdef HAL_ADC_MODULE_ENABLED
void compat_ADC2_Init(ADC_HandleTypeDef * hadc2);
void compat_ADC2_MspInit(ADC_HandleTypeDef* hadc);
void compat_ADC2_MspDeInit(ADC_HandleTypeDef* hadc);
#endif

#endif /* BACKWARDS_COMPATIBILITY_H_ */
