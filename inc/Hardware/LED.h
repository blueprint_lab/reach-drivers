/*
 * LED.h
 *
 *  Created on: 23 Jan 2018
 *      Author: p.phillips
 */

#ifndef INC_HARDWARE_LED_H_
#define INC_HARDWARE_LED_H_

#include "../Inc/stm32_device_select.h"

typedef enum LED_Mode{
	led_mode_standby=0,
	led_mode_disabled=1,
	led_mode_calibrate=7,
	led_mode_factory=8,

}LED_Mode_t;

typedef struct LED_t LED_t;
struct LED_t{

	float frequency;
	float duty;
	float dutyDemand;
	TIM_HandleTypeDef PWMTimer;
	uint32_t PWMChannel;
	TIM_OC_InitTypeDef PWMConfig;
	LED_Mode_t Mode;
	uint32_t clockSpeed;

	uint8_t ID;

	float temperature;
	float maxTemperature;
	float maxBrightness;

	void (*setFrequency)(LED_t* self, float frequency);
	void (*setDuty)(LED_t* self, float duty);
	float (*LPFTemperature)(LED_t*self,float newReading, float alpha);

};

void LED_construct(LED_t *self,TIM_HandleTypeDef PWMTimer, uint32_t PWMChannel, uint32_t clockSpeed);
void LED_setFrequency(LED_t *self,float  frequency);
void LED_setDuty(LED_t *self,float  duty);
float LED_LPFTemperature(LED_t*self,float newReading, float alpha);
#endif /* INC_HARDWARE_LED_H_ */
