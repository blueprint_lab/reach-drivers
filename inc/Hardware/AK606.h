/*
 * AK606.h
 *
 *  Created on: 4 Oct. 2022
 *      Author: JamieJacobson
 *
 */

#ifndef AK606_H_
#define AK606_H_

#include "Communication/protocol3rdParty.h"
#include "Communication/comslink.h"


/* Device spec parameters*/
#define AK606_MOTOR_COUNT ((uint8_t)3);
#define MOTOR_COMMAND_BUFFER_LENGTH ((uint8_t)8u)
#define AK606_KT ((float)0.068 * 6.0)

/* AK60-6 parameter limits */
#define AK606_TOR_MAX ((float)15.00f) //N.m
#define AK606_TOR_MIN ((float)-15.00f) // N.m
#define AK606_I_MAX ((float)AK606_TOR_MAX / AK606_KT)
#define AK606_I_MIN ((float)AK606_TOR_MIN / AK606_KT)


#define AK606_P_MAX ((float)12.5f)  //rad
#define AK606_P_MIN ((float)-12.5f) //rad
#define AK606_V_MAX ((float)45.0f)  //rad/s
#define AK606_V_MIN ((float)-45.0f) //rad/s

#define AK606_KP_MAX ((uint16_t)500u)
#define AK606_KP_MIN ((uint16_t)0u)
#define AK606_KD_MAX ((uint8_t)5u)
#define AK606_KD_MIN ((uint8_t)0u)


/**
 * @brief Function return statuses (0 = OK)
 */
typedef enum AK606_ErrorCode_ {
	AK606_ERROR_OK = 0,
	AK606_ERROR_MOTOR_START,
	AK606_ERROR_CAN_TRANSMIT,
	AK606_ERROR_CAN_RECIEVE
}AK606_ErrorCode_t;

/**
 * @brief Motor status enum
 * @note: from AK Series Actuator Manual V1.0.9 page 39
 */
typedef enum AK606_MotorStatus_ {
	AK606_STATUS_OK = 0,
	AK606_STATUS_OVER_CURRENT,
	AK606_STATUS_OVER_VOLTAGE,
	AK606_STATUS_UNDER_VOLTAGE,
	AK606_STATUS_ENCODER,
	AK606_STATUS_HARDWARE_FAILIURE,
} AK606_MotorStatus_t;

/**
 * @brief Control demand structs
 */
typedef struct AK606_Demand_ {
	uint16_t positionDemand;
	uint16_t velocityDemand;
	uint16_t torqueDemand;
	uint16_t kpDemand;
	uint16_t kdDemand;
} AK606_Demand_t;

/**
 * @brief state struct to store data read from motor
 */
typedef struct AK606_State_ {
	float position;
	float velocity;
	float torque;
	AK606_MotorStatus_t motorStatus;
} Ak606_State_t;

/**
 * @brief ak60-6 motor handler object
 */
typedef struct AK606_t AK606_t;
struct AK606_t {
	uint8_t canId;
	uint8_t motorId;
	AK606_Demand_t motorDemand;
	Ak606_State_t motorState;
	protocol3P_t ak606Protocol;
	canbus_comms_link* comms_link;
	CAN_RxHeaderTypeDef canRxHeader;
	uint8_t motorCommandBuffer[MOTOR_COMMAND_BUFFER_LENGTH];
};

/**
 * @brief create an ak60-6 instance
 * @param[in, out] self Pointer to AK60-6 handler object
 * @param[in] pProtocolManager 3rd comm protocol manager for  can
 * @param[in] comms_link	comm_link handler for CAN communication
 * @param[in] canId device CAN ID
 * @return None
 */
void AK606_Construct(AK606_t* self, protocolManager_t* pProtocolManager, canbus_comms_link* comms_link, uint8_t canId);

/**
 * @brief Enter MIT motor control mode
 * @param[in, out] self Pointer to AK60-6 handler object
 * @return AK606_ERROR_OK if successful
 */
AK606_ErrorCode_t AK606_EnterMotorMode(AK606_t* self);

/**
 * @brief Exit motor mode (only if already in MIT control motor mode)
 * @param[in, out] self Pointer to AK60-6 handler object
 * @return AK606_ERROR_OK if successful
 */
AK606_ErrorCode_t ExitMotorMode(AK606_t* self);

/**
 * @brief Set current encoder position as zero
 * @param[in, out] self Pointer to AK60-6 handler object
 * @return AK606_ERROR_OK if successful
 */
AK606_ErrorCode_t AK606_SetZeroPosition(AK606_t* self);

/**
 * @brief calculate and send Torque command to motor
 * @param[in, out] self Pointer to AK60-6 handler object
 * @param[in] torque Desired torque in N.m
 * @return AK606_ERROR_OK if successful
 */
AK606_ErrorCode_t AK606_sendTorqueDemand(AK606_t* self, float torque);

/**
 * @brief sends an MIT demand to control the position, velocity,
 * 		  Kp, kd and torque state of the motor. Calculates demand values
 * 		  and packs data into a recognizable packet.
 * @note Set parameter to zero if not using
 * @param[in, out] self Pointer to AK60-6 handler object
 * @param[in] position motor position in rad
 * @param[in] speed rad/s
 * @param kp Proportional Gain
 * @param kd Differential Gain
 * @param torque desired torque in N.m
 * @return AK606_ERROR_OK if successful
 */
AK606_ErrorCode_t AK606_sendMITControlDemand(AK606_t* self, float position, float speed, float kp, float kd, float torque);

/* TODO: Init Function - enter motor mode, zero + set
 * 		 Run Function - motor control state machine
 */


#endif /* AK606_H_ */
