/*
 * buttons.h
 *
 *  Created on: 17May,2019
 *      Author: p.phillips
 */

#ifndef BUTTONS_H_
#define BUTTONS_H_

#include <stdint.h>
#include "stm32_device_select.h"
#include <math.h>
#include "DataStructures/circularQueue.h"

#define EVENT_Q_SIZE 16
#define MAX_BUTTONS 3

typedef enum indicatorMode_t{
	OFF,
	ON,
	FLASH_FAST,
	FLASH_SLOW,
	LATCH,
	MOMENTARY,
	MOMENTARY_FLASH_ON,
	MOMENTARY_FLASH_OFF,

}indicatorMode_t;

typedef enum buttonEvent_t {
	RELEASE,
	PRESS,
	DOUBLE_PRESS,
	SINGLE_PRESS,
	LONG_PRESS,
	DOUBLE_RELEASE,
	SINGLE_RELEASE,
	LONG_RELEASE,
} buttonEvent_t;

typedef enum inverted_t {
	NOT_INVERTED,
	INVERTED,
} inverted_t;

typedef struct button_t button_t;

struct button_t{

	uint8_t (*Run)(button_t* self);
	void (*Press)(button_t* self);
	void (*releaseCallback)(button_t* self);
	void (*longReleaseCallback)(button_t* self);
	void (*pressCallback)(button_t* self);
	void (*longPressCallback)(button_t* self);
	void (*doublePressCallback)(button_t* self);
	void (*singlePressCallback)(button_t* self);
	void (*doubleReleaseCallback)(button_t* self);
	void (*singleReleaseCallback)(button_t* self);
	void (*setIndicatorMode)(button_t* self,indicatorMode_t mode);
	uint8_t (*getState)(button_t* self);
	void (*receiveEvent)(button_t* self, buttonEvent_t);

	uint8_t id;

	uint32_t pressTimer;
	uint32_t longPressTime;
	uint32_t releaseTimer;
	uint32_t doublePressTime;
	uint32_t indicatorTimer;
	uint8_t wasDoublePressed;
	uint8_t wasLongPressed;

    uint32_t* pIndicatorTimer;

	indicatorMode_t indicatorMode;
	uint8_t indicatorModeTransmitRequired;

	uint16_t inputPin;
	GPIO_TypeDef* inputPort;
	uint8_t state;
	uint8_t isIndicator;
	uint8_t invertIndicator;
	uint16_t outputPin;
	GPIO_TypeDef* outputPort;
	uint8_t invertInput;
	uint8_t* statePointer;

	circularQueue eventQ;
};

void button_construct( button_t *self, uint16_t inputPin, GPIO_TypeDef* inputPort);
void button_constructGPIO(button_t *self, uint16_t inputPin, GPIO_TypeDef* inputPort);
void button_constructPointer(button_t *self, uint8_t* statePointer, uint8_t invertInput);
void button_constructReceiver(button_t *self);
void button_destroy(button_t *self);
void button_constructIndicator(button_t *self, uint16_t outputPin, GPIO_TypeDef* outputPort);
void button_invertIndicator(button_t *self, inverted_t inverted);
void button_setReleaseCallback( button_t *self,void (*Callback)( button_t *self));
void button_setPressCallback( button_t *self,void (*Callback)( button_t *self));
void button_setLongPressCallback( button_t *self,void (*Callback)( button_t *self));
void button_setLongReleaseCallback( button_t *self,void (*Callback)( button_t *self));
void button_setDoublePressCallback( button_t *self,void (*Callback)( button_t *self));
void button_setSinglePressCallback( button_t *self,void (*Callback)( button_t *self));
void button_setDoubleReleaseCallback( button_t *self,void (*Callback)( button_t *self));
void button_setSingleReleaseCallback( button_t *self,void (*Callback)( button_t *self));
uint8_t button_run(button_t *self);
void button_receiveEvent(button_t* self, buttonEvent_t event);

button_t* Buttons[MAX_BUTTONS];

#endif /* BUTTONS_H_ */
