/*
 * LPS25HB.h
 *
 *  Created on: Jun 19, 2020
 *      Author: Shaun Barlow
 */

#ifndef LPS25HB_H_
#define LPS25HB_H_

#include "Communication/I2C.h"
#include "stm32_device_select.h"


typedef enum LPS25HB_Registers_t {
	// Reference pressure registers
    REF_P_XL = 0x08,         // RW
    REF_P_L = 0x09,          // RW
    REF_P_H = 0x0A,          // RW
    // Who am I register
    WHO_AM_I = 0x0F,         // R
    // Resolution register
    RES_CONF = 0x10,         // RW
    // Control registers
    CTRL_REG1 = 0x20,        // RW
    CTRL_REG2 = 0x21,        // RW
    CTRL_REG3 = 0x22,        // RW
    CTRL_REG4 = 0x23,        // RW
    // Interrupt registers
    INTERRUPT_CFG = 0x24,    // RW
    INT_SOURCE = 0x25,       // R
    // Status register
    STATUS_REG = 0x27,       // R
    // Pressure outputs
    PRESS_OUT_XL = 0x28,     // R
    PRESS_OUT_L = 0x29,      // R
    PRESS_OUT_H = 0x2A,      // R
    // Temperature outputs
    TEMP_OUT_L = 0x2B,       // R
    TEMP_OUT_H = 0x2C,       // R
    // FIFO configure registers
    FIFO_CTRL = 0x2E,        // RW
    FIFO_STATUS = 0x2F,      // R
    // Pressure threshold registers
    THS_P_L = 0x30,          // RW
    THS_P_H = 0x31,          // RW
    // Pressure offset  registers
    RPDS_L = 0x39,           // RW
    RPDS_H = 0x3A,           // RW
} LPS25HB_Registers_t;


typedef enum LPS25HB_Address_t {
	ZERO = 0x5C,
	ONE = 0x5D,
} LPS25HB_Address_t;


typedef struct LPS25HB_t {
	I2C_HandleTypeDef* phi2c;
	LPS25HB_Address_t address;
	uint8_t rxBuffer[20];
	I2C_CommState_t commState;

	float pressure;
	float temperature;

} LPS25HB_t;


void LPS25HB_construct(LPS25HB_t* self, I2C_HandleTypeDef* phi2c);
void LPS25HB_init(LPS25HB_t* self);
void LPS25HB_read(LPS25HB_t* self);
void LPS25HB_readDMA(LPS25HB_t* self);
void LPS25HB_onRxCplt(LPS25HB_t* self, uint8_t address);
float LPS25HB_getPressure(LPS25HB_t* self);
float LPS25HB_getTemperature(LPS25HB_t* self);


#endif /* LPS25HB_H_ */
