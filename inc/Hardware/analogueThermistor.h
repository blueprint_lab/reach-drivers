/*
 * analogueThermistor.h
 *
 *  Created on: 2023-05-30
 *      Author: YC
 */

#include "Hardware/analogueSensor.h"
#include "DataStructures/auxMath.h"

float AnalogueThermistor_Curve(AnalogueSensor_t* self, float input);
