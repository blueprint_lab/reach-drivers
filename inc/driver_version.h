/*
 * reach_driver_verison.h
 *
 *  Created on: 27 Sept 2023
 *      Author: Kyle McLean
 */

#ifndef REACH_DRIVER_VERSION_H_
#define REACH_DRIVER_VERSION_H_


#define REACH_DRIVER_VERSION_MAJOR 1 
#define REACH_DRIVER_VERSION_SUBMAJ 0
#define REACH_DRIVER_VERSION_MINOR 1 

#endif /* REACH_DRIVER_VERSION_H_ */
